<?php get_header(); ?>

<div class="error404__container text-center">
	
	<img class="error404__image" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/404.jpg" />

	<h3 class="text-orange">Oops!<br>This is embarrassing.</h3>

	<p class="text-darkgray">Sorry, we can’t find the page you’re looking for. Please check the URL, return <a href="/">home</a>, or reach out to our <a href="/contact-us">customer support team</a>.</p>

</div>

<?php get_footer(); ?>