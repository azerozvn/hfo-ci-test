module.exports = {
    endOfLine: "lf",
    semi: false,
    singleQuote: false,
    tabWidth: 2,
      printWidth: 80,
    trailingComma: "es5",
    overrides: [
      // Separate rules for CSS:
    //   {
    //     files: ["*.scss"],
    //     options: {
    //       singleQuote: true
    //     }
    //   }
    ]
  };
  