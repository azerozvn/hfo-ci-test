<?php

/*
Template Name: Contact
*/

// Re-enqueue Contact Form 7 scripts and styles
wpcf7_enqueue_scripts();
wpcf7_enqueue_styles();

get_header();

?>

<?php /*START LOOP */ if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

<?php /*END LOOP */ endwhile; endif; ?>


<?php get_footer(); ?>
