<?php

get_header();

?>
<style>.learning-center-article__header-topic{border:none !important; padding-left:0 !important;}</style>
<?php if ( has_post_thumbnail( $post->ID ) ): ?>
    <?php
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
    $image = $image[0];
    $image_alt = get_post_meta( get_post_thumbnail_id( $post->ID), '_wp_attachment_image_alt', true);
    ?>
<?php endif; ?>

<?php /*START LOOP */ if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php
    $current_post_id = $post->ID;
    $tags = wp_get_post_terms( $post->ID, 'post_tag' );
    $tag_ids = array();
    foreach ($tags as $tag) {
        array_push($tag_ids, $tag->term_id);
    }
    $categories = wp_get_post_terms( $post->ID, 'category' );
    $category = array_values(array_slice($categories, 0, 1))[0];
    $topics = array_values(array_slice(wp_get_post_terms( $post->ID, 'topics' ), -1));
    if ( $topics ) {
        $last_topic = $topics[0];
    }

    $hide_breadcrumbs_topics = false;
    foreach ( $categories as $category ) :
        if ( $category->name == 'Meal Plan' ) {
            $hide_breadcrumbs_topics = true;
        }
    endforeach;

    $post_color = esc_attr(get_field('color', 'category_'. $category->term_id));
    $article_content_width_class = 'with-sidebar';
    $article_content_font_size_class = '';
    $is_recipes_or_meal_plans = false;
    $is_recipe = false;
    $is_meal_plan = false;
    $category_disclaimer = '';
    $search_tab = '/#articles';
    $schema_container = '';
    foreach ( $categories as $category ) :

        if ( $category->name == 'Meal Plan' || $category->name == 'Recipe' || $category->name == 'Recipes & Meal Plans' ) {
            $article_content_width_class = 'full-width';
            $article_content_font_size_class = 'content-big-h';
            $is_recipes_or_meal_plans = true;
            $main_category = 'Recipes &amp; Meal Plans';
            $main_category_link = '/learning-center/recipes-meal-plans';
        } elseif ( $category->parent == 0 ) {
            $main_category = $category->name;
            $main_category_link = get_term_link( $category );
        }

        if ( $category->name == 'Meal Plan' ) {
            $category_disclaimer = $category->description;
            $is_meal_plan = true;
            $search_tab = '/#recipes';
        }

        if ( $category->name == 'Recipe' ) {
            $is_recipe = true;
            $schema_container = ' itemscope itemtype="http://schema.org/Recipe"';
            $search_tab = '#recipes';
        }

    endforeach;
    ?>

    <?php include(locate_template('snippet-rich-snippet.php')); ?>

    <section class="clearfix learning-center-article learning-center-article--<?php echo $post_color; ?> learning-center-article--<?php echo esc_attr($article_content_width_class) ?> learning-center-article--<?php echo esc_attr($article_content_font_size_class) ?>"<?php echo $schema_container; ?>>

        <div class="learning-center-article__container clearfix">
            <div class="learning-center-article__header clearfix">
                <nav>
                    <!-- Top Breadcrumbs -->
                
                    <?php 
                    // List all categories this post belongs to, wrapped in a search link. ("category" taxonomy)
                    custom_breadcrumbs();
                    ?>

                    <?php 
                    // Primary topic ("topics" taxonomy)
                    $top = get_primary_category("topics");
                    $search_tab = $is_recipes_or_meal_plans ? "recipes" : "articles"
                    ?>
                    <?php if ( !$hide_breadcrumbs_topics && $topics ): ?>
                        |<a class="learning-center-article__header-topic" href="/search/?q=<?php echo strtolower( $top['title'] ) . '&tab=' . $search_tab ?>">
                            <?php if ( $is_recipe ) echo '<span itemprop="recipeCategory">'; ?>
                            <?php echo esc_html($top['title']); if ( $is_recipe ) echo '</span>'; ?></a>
                    <?php endif ?>
                </nav>

                <?php if ( has_post_thumbnail( $post->ID ) && $is_recipes_or_meal_plans ): ?>
                    <img itemprop="image" class="mobile-only learning-center-article__header-mobile-image" src="<?php echo esc_url($image) ?>" alt="<?php echo esc_html($image_alt) ?>">
                <?php endif; ?>

                <h1 itemprop="name"><?php the_title(); ?></h1>

                <?php if ( get_field('recipes_meal_plans_subtitle') ): ?>
                    <h2><?php the_field('recipes_meal_plans_subtitle'); ?></h2>
                <?php endif ?>

                <?php if ( !$is_recipe): ?>
                    <div class="learning-center-article__author">
                        <?php
                        $author_id = get_the_author_meta( 'ID' );
                        $avatar = get_field( 'coach_photo', 'user_'. $author_id );
                        $coach_certifications = get_field( 'coach_certifications', 'user_'. $author_id );
                        $coach_shortbio = get_field( 'coach_shortbio', 'user_'. $author_id );
                        ?>

                        <img src="<?php echo esc_url($avatar); ?>" alt="<?php esc_attr(the_author()); ?>">
                        <span itemprop="author" class="learning-center-article__author-credentials">
							<?php the_author(); ?><?php if ( $coach_certifications ) { echo ', ' . esc_html($coach_certifications); } ?>
						</span>
                        <div class="learning-center-article__author-info">
                            <a class="learning-center-article__author-info-trigger" href="#"></a>
                            <div id="learning-center-article__author-info-modal" class="learning-center-article__author-info-modal modal">
                                <div class="modal-content">
                                    <div class="modal-close"><button></button></div>
                                    <p><strong><?php echo esc_html($coach_certifications); ?></strong></p>
                                    <?php echo $coach_shortbio; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ( $is_recipes_or_meal_plans ) : ?>
                    <?php if ( $is_recipe):
                        $share_heading = "Share this recipe";
                    else :
                        $share_heading = "Share this meal plan";
                    endif ?>
                    <div class="mobile-hide">
                        <?php include(locate_template('snippet-share.php')); ?>

                        <?php if ( $is_meal_plan ): ?>
                            <a class="learning-center-article__jump-meal-plan" href="#filled-by-js-on-load-to-trigger-smooth-scroll">Jump to meal plan</a>
                        <?php endif ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="learning-center-article__content">
                <?php if ( get_field('learning_center_summary') ): ?>
                    <?php if ( $is_recipe ) echo '<span itemprop="description">'; ?>
                    <?php if ( isset($allowed_tags) ) echo wp_kses( get_field('learning_center_summary'), $allowed_tags ); ?>
                    <?php if ( $is_recipe ) echo '</span>'; ?>
                <?php endif; ?>

                <?php if ( get_field('learning_center_nutritional_guide') ): ?>

                    <?php $guide_type = strpos(get_field('learning_center_nutritional_guide'), 'Prenatal') ? 'prenatal' : 'infant'; ?>

                    <div class="lc-nutritional-guide section-blue text-white clearfix">

                        <img class="lc-nutritional-guide__icon mobile-hide float-left" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-nutritional-guide.svg" alt="Nutritional Guide">
                        <p class="lc-nutritional-guide__copy mobile-hide float-left">
                            <?php echo $guide_type; ?> nutrition isn't easy. We can help.
                        </p>

                        <a class="button mobile-hide float-right lc-nutritional-guide__button" href="<?php echo esc_url( get_field('learning_center_nutritional_guide') ); ?>" target="_blank">Download our guide</a>

                        <a class="button mobile-only lc-nutritional-guide__button" href="<?php echo esc_url( get_field('learning_center_nutritional_guide') ); ?>" target="_blank">Download our <?php echo $guide_type; ?> nutrition guide</a>
                    </div>

                <?php endif; ?>

                <?php if ( has_post_thumbnail( $post->ID ) ): ?>
                    <img itemprop="image" class="learning-center-article__header-image<?php if ( $is_recipes_or_meal_plans ) { echo ' mobile-hide'; } ?>" src="<?php echo esc_url($image) ?>" alt="<?php echo esc_html($image_alt) ?>">
                <?php endif; ?>

                <?php if ( $is_recipe ): ?>
                    <ul class="learning-center-article__content-recipe-details">
                        <?php if ( get_field('cook_time')) : ?>
                            <li>
                                <div class="learning-center-article__content-recipe-image">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/recipe-icon-hourglass.png" alt="Hourglass">
                                </div>
                                <span itemprop="cookTime" class="learning-center-article__content-recipe-text"><?php the_field('cook_time'); ?></span>
                            </li>
                        <?php endif; ?>

                        <?php if ( get_field('serving_size')) : ?>
                            <li class="learning-center-article__content-recipe-detail--full-width">
                                <div class="learning-center-article__content-recipe-image">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/recipe-icon-cook-time.png" alt="Serving size">
                                    <span class="learning-center-article__content-recipe-text-inside-image"><?php the_field('serving_size_amount'); ?></span>
                                </div>
                                <span itemprop="recipeYield" class="learning-center-article__content-recipe-text"><?php the_field('serving_size'); ?></span>
                            </li>
                        <?php endif; ?>

                        <?php include(locate_template('snippet-email-share.php')); ?>

                        <li class="learning-center-article__content-recipe-detail--vertical-border mobile-hide"></li>

                        <?php $contains_object = get_field_object('contains'); // for the snippet-recipe-contains.php ?>
                        <?php include(locate_template('snippet-recipe-contains.php')); // needs $contains_object ?>
                    </ul>

                    <ul class="learning-center-article__content-recipe-details mobile-only">
                        <?php include(locate_template('snippet-recipe-contains.php')); // needs $contains_object ?>
                    </ul>

                    <?php if( have_rows('what_youll_need') ): ?>
                        <h1 class="learning-center-article__content-ingredient-heading active" data-expand="#learning-center-article__content-ingredient-details" data-expand-breakpoint="600">What you’ll need</h1>
                        <ul id="learning-center-article__content-ingredient-details" class="learning-center-article__content-ingredient-details">
                            <?php while ( have_rows('what_youll_need') ) : the_row(); ?>
                                <li itemprop="recipeIngredient"><?php esc_html(the_sub_field('ingredient_details')); ?></li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif;  ?>
                <?php endif ?>

                <?php if ( $is_meal_plan ): ?>
                    <ul class="learning-center-article__content-recipe-details learning-center-article__content-recipe-details--meal-plan mobile-only">
                        <li>
                            <div class="learning-center-article__content-recipe-image">
                                <a href="#filled-by-js-on-load-to-trigger-smooth-scroll" class="learning-center-article__jump-meal-plan-mobile">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-jump.svg" alt="Jump">
                                </a>
                            </div>
                            <a class="learning-center-article__jump-meal-plan-mobile" href="#filled-by-js-on-load-to-trigger-smooth-scroll"><span class="learning-center-article__content-recipe-text">Jump to meal plan</span></a>
                        </li>
                        <?php include(locate_template('snippet-email-share.php')); ?>
                    </ul>
                <?php endif ?>


                <?php

                if ( $is_recipe ):
                    echo '<span itemprop="recipeInstructions">';
                endif;

                the_content();

                if ( $is_recipe ):
                    echo '</span>';
                endif;

                ?>

                <?php
                if( have_rows('meal_plan_stages') ):
                    while ( have_rows('meal_plan_stages') ) : the_row();
                        ?>
                        <h1 class="meal-plan-stage-title" id="<?php esc_attr(the_sub_field('meal_plan_stage_slug')); ?>">
                            <?php the_sub_field('meal_plan_stage_title'); ?>
                        </h1>
                        <?php the_sub_field('meal_plan_stage_body'); ?>
                    <?php
                    endwhile;
                endif;
                ?>

                <?php if ( $category_disclaimer ) : ?>
                    <p class="learning-center-article__content-disclaimer"><?php echo esc_html($category_disclaimer) ?></p>
                <?php endif; ?>

                <?php if ( get_field('learning_center_sources') ) : ?>
                    <div class="learning-center-article__content-sources">
                        <div class="learning-center-article__content-sources-header" data-expand="#learning-center-article__content-sources-content">Sources</div>
                        <div id="learning-center-article__content-sources-content" class="learning-center-article__content-sources-content" style="display: none;">
                            <?php the_field('learning_center_sources') ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <?php if ( !$is_recipes_or_meal_plans ) : ?>
                <div class="desktop-only">
                    <?php if(!wp_is_mobile()): ?>
                        <?php include(locate_template('snippet-newsletter.php')); ?>
                    <?php endif; ?>

                    <?php  include(locate_template('snippet-related-recipes.php')); ?>

                    <hr class="learning-center-article__sidebar-hr desktop-only">

                    <?php $share_heading = "Share with other parents"; ?>
                    <?php include(locate_template('snippet-share.php')); ?>

                </div>
            <?php endif; ?>

            <?php if ( $is_recipes_or_meal_plans ) : ?>
                <?php if ( $is_recipe):
                    $share_heading = "Share this recipe";
                else :
                    $share_heading = "Share this meal plan";
                endif ?>
                <div class="mobile-only learning-center-article__share-wrapper-recipe-meal-plan"><?php include(locate_template('snippet-share.php')); ?></div>
            <?php endif; ?>

            <?php if ( !$is_recipes_or_meal_plans ) : ?>
                <div class="learning-center-article__tags clearfix">
                    <!-- Bottom Breadcrumbs -->
                    <?php
                    if ( $topics ):
                        // Topic (only the last one.  "topics" taxonomy)
                        echo '<a class="button" href="/search?q='. strtolower($last_topic->name) . '&tab=' . $search_tab . '">' . esc_html($last_topic->name) .'</a>';
                    endif;
                    
                    // All tags ("post_tags" taxonomy)
                    foreach ($tags as $tag) {
                        echo '<a class="button" href="/search?q='. strtolower($tag->name) . '&tab=' . $search_tab . '">' . esc_html($tag->name) ."</a>";
                    }
                    ?>
                </div>
            <?php endif; ?>

            <?php if ( !$is_recipes_or_meal_plans ) : ?>
                <div class="tablet-only mobile-only text-center">
                    <?php if(wp_is_mobile()): ?>
                        <?php include(locate_template('snippet-newsletter.php')); ?>
                    <?php endif; ?>

                    <?php $share_heading = "Share with other parents"; ?>
                    <?php include(locate_template('snippet-share.php')); ?>

                </div>
            <?php endif; ?>
        </div> <!-- /learning-center-article__container -->

        <?php
        // You may also like
        $you_may_like_desktop_slide_count = 3;
        $you_may_like_tablet_slide_count = 2;
        $you_may_like_mobile_slide_count = 1;
        $args = array(
            'posts_per_page' => 12,
            'post_status' => 'publish',
            'post_type' => 'post',
            'orderby' => 'rand',
            'post__not_in' => array($current_post_id),
            'tax_query' => array( array(
                'taxonomy' => 'post_tag',
                'field' => 'id',
                'terms' => $tag_ids
            ) )
        );

        if ( $is_recipe ) :
            $you_may_like_title = 'More yummy recipes.';
            $args['category_name'] = 'Recipe';
        elseif ( $is_meal_plan ) :
            $you_may_like_title = 'More yummy meal plans.';
            $args['category_name'] = 'Meal Plan';
        else :
            $you_may_like_title = 'You may also like';
            $you_may_like_desktop_slide_count = 4;
            $you_may_like_tablet_slide_count = 3;
            $you_may_like_mobile_slide_count = 1;
            $topic_ids = array();
            foreach ($topics as $topic) {
                array_push($topic_ids, $topic->term_id);
            }
            $category_ids = array();
            foreach ($categories as $category) {
                array_push($category_ids, $category->parent);
            }
            $args['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'topics',
                    'field' => 'id',
                    'terms' => $topic_ids,
                ),
                array(
                    'taxonomy' => 'category',
                    'field' => 'id',
                    'terms' => $category_ids,
                )
            );
        endif;
        $you_may_like_posts = new WP_Query( $args );
        ?>

        <?php if ( $you_may_like_posts->get_posts() ) : ?>
            <div class="learning-center-carousel--article learning-center-carousel <?php echo $post_color; ?>" data-slides-desktop="<?php echo $you_may_like_desktop_slide_count; ?>" data-slides-tablet="<?php echo $you_may_like_tablet_slide_count; ?>" data-slides-mobile="<?php echo $you_may_like_mobile_slide_count; ?>">
                <h1><?php echo esc_html( $you_may_like_title ); ?></h1>

                <div class="learning-center-carousel__container clearfix">

                    <?php while ( $you_may_like_posts->have_posts() ) :
                        $you_may_like_posts->the_post();
                        $post_title = get_the_title(); ?>
                        <div class="learning-center-card <?php echo $post_color; ?>">
                            <a class="learning-center-card__link" href="<?php echo esc_url( get_permalink() ); ?>">
                                <?php if ( has_post_thumbnail( get_the_ID() ) ): ?>
                                    <?php
                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
                                    $image = $image[0];
                                    $image_alt = get_post_meta( get_post_thumbnail_id( get_the_ID() ), '_wp_attachment_image_alt', true);
                                    ?>
                                    <img class="learning-center-card__image" src="<?php echo esc_url( $image ) ?>" alt="<?php echo esc_html($image_alt) ?>">
                                <?php endif; ?>
                            </a>
                            <div class="learning-center-card__details">
                                <p class="learning-center-card__breadcrumb"><strong>
                                        <?php if ( $is_recipes_or_meal_plans ) : ?>
                                            <?php
                                            $categories = wp_get_post_terms( $post->ID, 'category' );
                                            $category = array_values(array_slice($categories, -1))[0];

                                            echo esc_html( $category->name ). " | "
                                            ?>
                                        <?php endif; ?>
                                        <?php
                                        $topics = array_values( array_slice( wp_get_post_terms( get_the_ID(), 'topics' ), -1) );
                                        if ( $topics ) {
                                            $last_topic = $topics[0];
                                            echo esc_html( $last_topic->name );
                                        }

                                        ?>
                                    </strong></p>

                                <h4 class="learning-center-card__title">
                                    <a href="<?php echo esc_url( get_permalink() ); ?>">
                                        <?php echo esc_html( $post_title ); ?>
                                    </a>
                                </h4>


                                <?php if ( !$is_recipes_or_meal_plans ) : ?>
                                    <p class="learning-center-card__excerpt">
                                        <?php echo get_excerpt( str_replace( 'What to Know', '', get_field('learning_center_summary') ), 40 ); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </section> <!-- .learning-center-article -->

    <?php if ( $is_recipes_or_meal_plans ) : ?>
        <?php include(locate_template('snippet-book.php')); ?>
    <?php endif; ?>

<?php /*END LOOP */ endwhile; endif; ?>

<?php get_footer(); ?>
