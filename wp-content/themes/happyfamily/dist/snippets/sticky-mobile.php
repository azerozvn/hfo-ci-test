<?php
/**
 * Created by PhpStorm.
 * User: corra-274
 * Date: 07/10/19
 * Time: 8:35 PM
 */
$stickyheader = get_field('hamburger_menu');
if($stickyheader == 'true'):

if( have_rows('sticky_section') ):
while( have_rows('sticky_section') ): the_row();
?>
    <div class="mobsticky">
        <span class="left"><h1> <?php echo get_sub_field('banner_title_mobile') ?></h1></span>
        <span class="right mbl"><a class="banner_link banner-link-x tog-ham">
        <img class="alignnone size-medium wp-image-3570" role="img" src="<?php the_sub_field('sticky_menu_iconmobile'); ?>" alt="" /></a>
       <a href="javascript:void(0)" class="cross">&times;</a></span>
    </div>
<?php
endwhile;
endif;
endif;
?>
