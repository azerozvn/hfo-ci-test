<?php
include('search-form.php');
$with_sub_header = false;
if ( have_rows('navigation_items') || is_page( array( 'our-mission') ) ) {
    $with_sub_header = true;
}
?>


    <header id="header" class="site-header <?php if ($with_sub_header) {
        echo 'site-header--with-sub-header';
    }
    if (get_field('short_subheader')) {
        echo ' --short-sub-header';
    } ?>">


        <div class="desktop-hide nav-toggle">
            <div class="nav-toggle__icon"></div>
        </div>
        <div class="header-container">
            <div class="desktop-hide mobile-search">
                <?php search_form("mobile") ?>
            </div>
            <div class="site-branding">
                <a href="<?php echo esc_url(home_url()); ?>" alt="<?php bloginfo('name'); ?>">
                    <?php
                    $custom_logo_id = get_theme_mod('custom_logo'); 
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    ?>
                    <img class="site-branding__logo" src="<?php echo $logo[0]; ?>"/>
                </a>
            </div>
        </div>
        <?php include('sticky-mobile.php'); ?>

        <div class="menus-container">
            <div class="mobile-search-space desktop-hide"></div>
            <div class="explore-topmenu mobile-hide"><?php wp_nav_menu(array('theme_location' => 'menu-hf-top')); ?></div>
            <?php include('sticky-menu.php'); ?>
            <?php wp_nav_menu(array('theme_location' => 'menu-hf-left')); ?>

            <div class="float-right menu-right">
                <ul class="administrative-menu main-menu clearfix">

                    <li class="main-menu__item administrative-menu__item search desktop-search">
                        <?php search_form("desktop") ?>
                    </li>
                    <li class="main-menu__item administrative-menu__item store-locator desktop-only"><a
                                href="<?php echo esc_url(home_url()); ?>/store-locator">Find in Store</a></li>
                    <li class="main-menu__item administrative-menu__item globe-icon desktop-only"><a
                            href="<?php echo get_theme_mod('shop_url'); ?>/quality-and-safety-of-our-products">Quality & Safety News</a></li>
                    </li>
                </ul>
                <ul class="navigational-menu main-menu">
                    <?php wp_nav_menu(array('theme_location' => 'menu-hf-right')); ?>
                    <li class="main-menu__item navigational-menu__item globe-icon desktop-hide"><a href="/quality-and-safety-of-our-products/">Quality & Safety News</a></li>
                    <li class="main-menu__item navigational-menu__item store-locator desktop-hide"><a href="/store-locator">Find in Store</a></li>
                </ul>
            </div>

            <div class="explore-topmenu desktop-hide"><?php wp_nav_menu(array('theme_location' => 'menu-hf-top')); ?></div>
        </div>

        <?php if (is_page(array('our-mission'))) : ?>
            <div class="site-sub-header">
                <ul class="site-sub-header-menu clearfix">
                    <li class="site-sub-header-menu__item">
                        <a href="<?php echo esc_url(home_url()); ?>/our-mission"
                           class="<?php if (is_page('our-mission')) {
                               echo 'active';
                           } ?>">
                            <span class="site-sub-header-menu__title">Our Mission</span>
                        </a>
                    </li>
                    <li class="site-sub-header-menu__item">
                        <a href="<?php echo esc_url(home_url()); ?>/our-story" class="<?php if (is_page('our-story')) {
                            echo 'active';
                        } ?>">
                            <span class="site-sub-header-menu__title">Our Story</span>
                        </a>
                    </li>
                    <li class="site-sub-header-menu__item">
                        <a href="<?php echo esc_url(home_url()); ?>/our-team" class="<?php if (is_page('our-team')) {
                            echo 'active';
                        } ?>">
                            <span class="site-sub-header-menu__title">Our Team</span>
                        </a>
                    </li>
                    <li class="site-sub-header-menu__item">
                        <a href="<?php echo esc_url(home_url()); ?>/our-experts"
                           class="<?php if (is_page('our-experts')) {
                               echo 'active';
                           } ?>">
                            <span class="site-sub-header-menu__title">Our Experts</span>
                        </a>
                    </li>
                </ul>
            </div>
        <?php endif; ?>

        <?php if (have_rows('navigation_items')): ?>
            <div class="site-sub-header site-sub-header--with-images">
                <ul class="site-sub-header-menu clearfix">
                    <?php while (have_rows('navigation_items')) : the_row(); ?>
                        <li class="site-sub-header-menu__item site-sub-header-menu__item--hover-<?php the_sub_field('navigation_item_link_hover_color') ?>">
                            <a href="<?php esc_attr(the_sub_field('navigation_item_link')); ?>">
                                <?php $image = get_sub_field('navigation_item_icon'); ?>
                                <div class="site-sub-header-menu__item-image-wrapper">
                                    <img src="<?php echo $image['url']; ?>"
                                         alt="<?php esc_attr(the_sub_field('navigation_item_name')); ?>">
                                </div>
                                <span class="site-sub-header-menu__title">
								<?php esc_html(the_sub_field('navigation_item_name')); ?>
							</span>
                            </a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>

    </header>
<?php include('hamburger-content.php'); ?>

