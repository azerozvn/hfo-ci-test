<?php
/**
 * Created by PhpStorm.
 * User: corra-274
 * Date: 07/10/19
 * Time: 8:27 PM
 */
$stickyheader = get_field('hamburger_menu');
if($stickyheader == 'true'):
?>

<div class="overlay-content" id="hamburger">
    <?php if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="master-hero brand-master" style="background-color:#fff;">
            <div class="tophead">
                <?php
                if( have_rows('sticky_section') ):
                    while( have_rows('sticky_section') ): the_row();

                        ?>
                        <div class="lefttop">
                            <div class="desktop"><h1> <?php echo get_sub_field('banner_title_mobile') ?></h1></div>
                            <div class="mobile"><h1> <?php echo get_sub_field('banner_title_mobile') ?></h1></div>
                        </div>
                    <?php endwhile; endif; ?>
                <div class="righttop">
                    <div class="hamicon">
                        <?php
                        if( have_rows('sticky_section') ):
                            while( have_rows('sticky_section') ): the_row();

                                ?>
                                <a class="banner_link banner-link-0" href="<?php echo get_sub_field('link_url_one'); ?>">
                                    <p> <?php echo get_sub_field('link_title_one'); ?></p>
                                    <picture>
                                        <source media="(max-width: 767px)" srcset="<?php the_sub_field('our_story_iconmobile'); ?>">
                                        <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_one'); ?>" alt="" />
                                    </picture>
                                </a>
                                <a class="banner_link banner-link-1" href="<?php echo get_sub_field('link_url_two'); ?>">
                                    <p> <?php echo get_sub_field('link_title_two'); ?></p>
                                    <picture>
                                        <source media="(max-width: 767px)" srcset="<?php the_sub_field('download_summary_iconmobile'); ?>">
                                        <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_two'); ?>" alt="" />
                                    </picture>
                                </a>
                                <a class="banner_link banner-link-x tog-ham desk toscrl">
                                    <img class="alignnone size-medium wp-image-3570" role="img" src="<?php the_sub_field('sticky_menu_icondesktop'); ?>" alt="" />
                                </a>
                                <a href="javascript:void(0)" class="cross">&times;</a>
                            <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
</div>
<?php // hfo-marketing-script-start ?>
<script src="https://cdn.jsdelivr.net/npm/mediaCheck@0.4.6/js/mediaCheck.js"></script>
<?php // hfo-marketing-script-end ?>
<script type="text/javascript">
    $ = jQuery;
    function scrollStickyHeader() {
        var topHeight = 0;
        mediaCheck({
            media: '(min-width: 768px)',
            entry: $.proxy(function () {
                topHeight = 300;
            }),
            exit: $.proxy(function () {
                topHeight = 200;
            })
        });
        $(window).on('scroll.happy', function (event) {
            var sc = $(window).scrollTop(),
                headerSectionElem = $('header#header').innerHeight(),
                topbarSectionElem = $('.top-banner-block').innerHeight(),
                actualStickyHeight = headerSectionElem + topbarSectionElem + topHeight;
            if($('.sticky-header').length <= 0) {
                if (sc >= actualStickyHeight) {
                    $("body").addClass("darkHeader");
                } else {
                    $("body").removeClass("darkHeader");
                }
            }
        });
    }
    scrollStickyHeader();
</script>
<?php endif; ?>