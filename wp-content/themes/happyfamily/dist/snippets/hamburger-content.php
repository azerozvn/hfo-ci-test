<?php
/**
 * Created by PhpStorm.
 * User: corra-274
 * Date: 07/10/19
 * Time: 8:36 PM
 */
$stickyheader = get_field('hamburger_menu');
if($stickyheader == 'true'):
?>

<div id="myNav" class="overlay_1">
    <div class="overlay-content" id="hamburger">
        <?php if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="master-hero brand-master" id="headfix" style="background-color:#fff;">
                <div class="tophead">
                    <?php
                    if( have_rows('sticky_section') ):
                        while( have_rows('sticky_section') ): the_row();

                            ?>
                            <div class="lefttop">
                                <div class="desktop"><h1> <?php echo get_sub_field('banner_title_mobile') ?></h1></div>
                                <div class="mobile"><h1> <?php echo get_sub_field('banner_title_mobile') ?></h1></div>
                            </div>
                            <a href="javascript:void(0)" class="cross">&times;</a>
                        <?php endwhile; endif; ?>
                </div>
            </div>

            <div class="content-master-wrapper">
                <?php
                $fontSize = get_field('font_sizedesktop_post_hamburger');
                $lineHeight = get_field('line_heightdesktop_post_hamburger');
                global $post;
                $args = array( 'post_type' => 'mater landing posts', 'posts_per_page' => 30, 'orderby'   => 'meta_value',
                    'order' => 'ASC',
                    'meta_key' => 'post_title_sort');
                $lastposts = get_posts( $args );
                ?>
                <?php
                foreach ( $lastposts as $key => $post ) :
                    setup_postdata( $post );
                    $title = get_field('post_titlehamburger_desktop');
                    $externalLink = get_field('post_external_link');
                    $mobileTitle = get_field('post_titlehamburger');
                    $shortDescription = get_field('short_description');
                    $briefDescription = get_field('brief_description');
                    $iconHoverColorDesktop = get_field('icon_and_title_hover_colour_hamburgerdesktop');
                    $iconHoverColorMobile = get_field('icon_and_title_hover_colourmobile');
                    if($title){
                        ?>

                        <div class="content-master-list desktop">
                            <a class="list-items" href="<?php if($externalLink): echo $externalLink;  else: the_permalink(); endif;  ?>">

                                <div class="content-block full">
                                    <div class="titleblock" data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                         data-class-desktop="post_color_desk_ham<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorMobile; ?>"
                                         data-class-mobile="post_color_mob_ham<?php echo $key; ?>" data-size="<?php echo $fontSize; ?>" data-line-height="<?php echo $lineHeight; ?>"
                                         data-class="hamburger-post-title-font-style<?php echo $key; ?>">
                                        <span class="post_title_desktop post_title dynamic-font"><?php echo $title; ?></span>
                                        <span class="post_title_mobile post_title"><?php echo $mobileTitle; ?></span>
                                    </div>
                                    <div class="descblock">
                                        <div class="cover">
                                            <span class="descript"><?php echo $shortDescription; ?></span>
                                        </div>
                                    </div>
                                    <div class="iconblock">
                                        <span class="more-icon-desktop" data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                              data-class-desktop="post_color_desk_icon_ham<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorMobile; ?>"
                                              data-class-mobile="post_color_mob_icon_ham<?php echo $key; ?>"> <img class="alignnone size-medium wp-image-3570" role="img" src="https://www.happyfamilyorganics.com/wp-content/uploads/2018/05/trans-arrow.svg" alt=""/>
                                        </span>
                                    </div>
                                </div>

                            </a>
                        </div>



                    <?php } endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <div class="footfix">

                <?php
                if( have_rows('sticky_section') ):
                    while( have_rows('sticky_section') ): the_row();

                        ?>
                        <a class="banner_link banner-link-0" href="<?php echo get_sub_field('link_url_one'); ?>">
                            <p class="desktop-only"> <?php echo get_sub_field('link_title_one'); ?></p>
                            <p class="mobile-only"> <?php echo get_sub_field('our_story_titlemobile'); ?></p>

                            <picture>
                                <source media="(max-width: 767px)" srcset="<?php the_sub_field('our_story_iconmobile'); ?>">
                                <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_one'); ?>" alt="" />
                            </picture>
                        </a>
                        <a class="banner_link banner-link-1" href="<?php echo get_sub_field('link_url_two'); ?>">
                            <p class="desktop-only"> <?php echo get_sub_field('link_title_two'); ?></p>
                            <p class="mobile-only"> <?php echo get_sub_field('download_summary_titlemobile'); ?></p>
                            <picture>
                                <source media="(max-width: 767px)" srcset="<?php the_sub_field('download_summary_iconmobile'); ?>">
                                <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_two'); ?>" alt="" />
                            </picture>
                        </a>

                    <?php endwhile; endif; ?>
            </div>
        <?php /*END LOOP */ endwhile; endif; ?>
    </div>
</div>
<?php endif; ?>