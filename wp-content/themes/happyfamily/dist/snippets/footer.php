
<footer id="footer" class="site-footer clearfix">

<div class="row site-footer__container">

    <div class="news-letter-wrapper">
        <h1>Get more good stuff from Happy!</h1>
        <p>Sign up to receive coupons, recipes, and more&mdash;right in your inbox.</p>

        <!-- Begin Mailchimp Signup Form -->
        <div id="mc_embed_signup_footer" class="footer-newsletter">
            <form action="/newsletter/subscriber/new/" method="post" id="mc-embedded-subscribe-form-footer" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <div class="clearfix">
                        <input type="email" value="" placeholder="email address*" name="email" class="required email" id="subscription_email" aria-required="true">
                        <div class="footer-newsletter__submit-container button mobile-hide hide">
                            <input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe-footer-one" class="button">
                        </div>
                    </div>
                    <div class="footer-newsletter__birthday">
                        <label for="mce-MMERGE3-month"><p><strong>Your little one's birthday</strong></p></label>
                        <div class="footer-newsletter__birthday-fields clearfix visible">
                            <span class="subfield monthfield"><input class="datepart required" type="text" pattern="[0-9]*" value="" placeholder="mm*" size="2" maxlength="2" name="MMERGE1[month]" id="mce-MMERGE1-month" aria-required="true"></span>
                            <span class="subfield dayfield"><input class="datepart required" type="text" pattern="[0-9]*" value="" placeholder="dd*" size="2" maxlength="2" name="MMERGE1[day]" id="mce-MMERGE1-day" aria-required="true"></span>
                            <span class="subfield yearfield"><input class="datepart required" type="text" pattern="[0-9]*" value="" placeholder="yyyy*" size="4" maxlength="4" name="MMERGE1[year]" id="mce-MMERGE1-year" aria-required="true"></span>
                            <input type="text" id="birthday_1" name="birthday_1" class="hidden birthday-input">
                        </div>
                        <div class="footer-newsletter__birthday-fields clearfix">
                            <span class="subfield monthfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="mm*" size="2" maxlength="2" name="MMERGE2[month]" id="mce-MMERGE2-month"></span>
                            <span class="subfield dayfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="dd*" size="2" maxlength="2" name="MMERGE2[day]" id="mce-MMERGE2-day"></span>
                            <span class="subfield yearfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="yyyy*" size="4" maxlength="4" name="MMERGE2[year]" id="mce-MMERGE2-year"></span>
                            <input type="text" id="birthday_2" name="birthday_2" class="hidden birthday-input">
                        </div>
                        <div class="footer-newsletter__birthday-fields clearfix">
                            <span class="subfield monthfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="mm*" size="2" maxlength="2" name="MMERGE3[month]" id="mce-MMERGE3-month"></span>
                            <span class="subfield dayfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="dd*" size="2" maxlength="2" name="MMERGE3[day]" id="mce-MMERGE3-day"></span>
                            <span class="subfield yearfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="yyyy*" size="4" maxlength="4" name="MMERGE3[year]" id="mce-MMERGE3-year"></span>
                            <input type="text" id="birthday_3" name="birthday_3" class="hidden birthday-input">
                        </div>
                        <div class="footer-newsletter__birthday-fields clearfix">
                            <span class="subfield monthfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="mm*" size="2" maxlength="2" name="MMERGE4[month]" id="mce-MMERGE4-month"></span>
                            <span class="subfield dayfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="dd*" size="2" maxlength="2" name="MMERGE4[day]" id="mce-MMERGE4-day"></span>
                            <span class="subfield yearfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="yyyy*" size="4" maxlength="4" name="MMERGE4[year]" id="mce-MMERGE4-year"></span>
                            <input type="text" id="birthday_4" name="birthday_4" class="hidden birthday-input">
                        </div>
                        <div class="footer-newsletter__birthday-fields clearfix">
                            <span class="subfield monthfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="mm*" size="2" maxlength="2" name="MMERGE5[month]" id="mce-MMERGE5-month"></span>
                            <span class="subfield dayfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="dd*" size="2" maxlength="2" name="MMERGE5[day]" id="mce-MMERGE5-day"></span>
                            <span class="subfield yearfield"><input class="datepart " type="text" pattern="[0-9]*" value="" placeholder="yyyy*" size="4" maxlength="4" name="MMERGE5[year]" id="mce-MMERGE5-year"></span>
                            <input type="text" id="birthday_5" name="birthday_5" class="hidden birthday-input">
                        </div>
                        <img class="footer-newsletter__add-birthday" src="/wp-content/themes/happyfamily/dist/images/icon-circled-plus.svg" />
                    </div>
                    <div class="footer-newsletter__submit-container button">
                        <input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe-footer" class="button">
                    </div>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8c03ea84fb510d1d8a20f1792_afec97eb77" tabindex="-1" value=""></div>
                </div>
            </form>
            <div id="responses">
                <div class="response" id="error-response" style="display:none">Error message</div>
                <div class="response" id="success-response" style="display:none">Success message</div>
            </div>
        </div>
        <!--End mc_embed_signup-->

    </div>
    <div class="footer-menu">
        <?php
        $menu1 = wp_get_nav_menu_name("menu-hf-footer1" );
        $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
        // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

        $menuID = $menuLocations['menu-hf-footer1']; // Get the *primary* menu ID

        $primaryNav = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.
        echo '<span class="footer_menu_heading">'.$menu1.'</h3></span>';
        echo '<ul class="footer_menu_content footer_panel">';
        foreach ( $primaryNav as $navItem ) {
            echo '<li><a href="'.$navItem->url.'" title="'.$navItem->title.'" class="'.$navItem->title.'">'.$navItem->title.'</a></li>';
        }
        echo '</ul>';
        ?>
    </div>

    <div class="footer-menu">
        <?php
        $menu2 = wp_get_nav_menu_name("menu-hf-footer2" );
        $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
        // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

        $menuID = $menuLocations['menu-hf-footer2']; // Get the *primary* menu ID

        $primaryNav = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.
        echo '<span class="footer_menu_heading">'.$menu2.'</h3></span>';
        echo '<ul class="footer_menu_content footer_panel">';
        foreach ( $primaryNav as $navItem ) {
            echo '<li><a href="'.$navItem->url.'" title="'.$navItem->title.'" class="'.$navItem->title.'">'.$navItem->title.'</a></li>';
        }
        echo '</ul>';
        ?>
    </div>

    <div class="social-connect-wrapper">
        <?php
        $menu3 = wp_get_nav_menu_name("menu-hf-footer3" );
        $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
        // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

        $menuID = $menuLocations['menu-hf-footer3']; // Get the *primary* menu ID

        $primaryNav = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.
        echo '<span class="footer_menu_heading">'.$menu3.'</h3></span>';
        echo '<ul class="social_icons">';
        foreach ( $primaryNav as $navItem ) {
            echo '<li><a href="'.$navItem->url.'" title="'.$navItem->title.'" class="'.$navItem->title.'">'.$navItem->title.'</a></li>';
        }
        echo '</ul>';
        ?>
    </div>
    <!--        <div class="col-2"></div>-->
    <!--        <div class="col-2"></div>-->
    <!--		<ul class="col-3 footer-menu">-->
    <!--			<li class="footer-menu__item"><a class="footer-menu__link" href="--><?php //echo esc_url(home_url()); ?><!--/press">Press</a></li>-->
    <!--			<li class="footer-menu__item"><a class="footer-menu__link" href="--><?php //echo esc_url(home_url()); ?><!--/contact-us">Contact us</a></li>-->
    <!--			<li class="footer-menu__item"><a class="footer-menu__link" href="--><?php //echo esc_url(home_url()); ?><!--/faqs">FAQs</a></li>-->
    <!--			<li class="footer-menu__item"><a class="footer-menu__link" href="--><?php //echo esc_url(home_url()); ?><!--/careers">Careers</a></li>-->
    <!--			<li class="footer-menu__item"><a class="footer-menu__link" href="--><?php //echo esc_url(home_url()); ?><!--/tmall">International</a></li>-->
    <!--		</ul>-->

    <!--		<div class="col-4">-->
    <!---->
    <!--			<ul class="footer-social">-->
    <!--				<li class="footer-social__item"><a class="footer-social__link twitter" target="_blank" href="https://twitter.com/HappyFamily">Twitter</a></li>-->
    <!--				<li class="footer-social__item"><a class="footer-social__link instagram" target="_blank" href="https://instagram.com/happyfamilyorganics">Instagram</a></li>-->
    <!--				<li class="footer-social__item"><a class="footer-social__link facebook" target="_blank" href="https://www.facebook.com/HappyFamilyOrganics">Facebook</a></li>-->
    <!--				<li class="footer-social__item"><a class="footer-social__link youtube" target="_blank" href="https://www.youtube.com/happyfamilyorganics">YouTube</a></li>-->
    <!--				<li class="footer-social__item"><a class="footer-social__link pinterest" target="_blank" href="https://www.pinterest.com/happyfamilyorganics">Pinterest</a></li>-->
    <!--			</ul>-->
    <!---->
    <!--			<div class="footer-legal">-->
    <!--				<a class="footer-legal__badge" href="--><?php //echo esc_url(home_url()); ?><!--/our-mission#bcorp">-->
    <!--					<img src="/wp-content/themes/happyfamily/dist/images/bcorp-badge.svg" />-->
    <!--				</a>-->
    <!--				<p>&copy; 2018 Nurture, Inc. All rights reserved.</p>-->
    <!--				<p><a href="/privacy-policy">Privacy Policy</a></p>-->
    <!--				<a href="/terms-conditions">Terms of Use</a>-->
    <!--			</div>-->
    <!---->
    <!--		</div>-->



</div>
<div class="footer-copyright-section">
    <?php
    $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
    // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

    $menuID = $menuLocations['menu-hf-footer4']; // Get the *primary* menu ID
    echo '<ul class="footer_copyright_content">';
    $primaryNav = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.
    foreach ( $primaryNav as $navItem ) {
        if($navItem->url) {
            echo '<li><a href="' . $navItem->url . '" title="' . $navItem->title . '">' . $navItem->title . '</a></li>';
        }else{
            echo '<li><span class="copyrightText">' . $navItem->title . '</span></li>';
        }
    }
    echo '</ul>';
    ?>
    <a class="footer-legal__badge" href="<?php echo esc_url(home_url()); ?>/our-mission#bcorp">
        <img src="/wp-content/themes/happyfamily/dist/images/bcorp-badge.svg" />
    </a>
</div>

</footer>
<script src="https://cdn.jsdelivr.net/npm/mediaCheck@0.4.6/js/mediaCheck.js"></script>
<script>
//Footer Accordion for Mobile
function footerAccordion() {
    (function($) {
        $(".footer_menu_heading").off("click.accordion").on("click.accordion", function () {
            $(this).parent().toggleClass("active");
            $(this).next('.footer_panel').slideToggle();
        });
    })(jQuery);
}

(function($) {
    mediaCheck({
        media: '(min-width: 601px)',
        entry: $.proxy(function () {
            $('.footer-menu').removeClass('active');
            $(".footer_menu_heading").off("click.accordion");
        }),
        exit: $.proxy(function () {
            footerAccordion();
        })
    });
})(jQuery);

//End here

// Header menu name updation for mob and desktop
function mobRename(){
    (function($) {
        $('.deskMobTitle').each(function(){
            var mobVal = $(this).data('mob');
            $(this).parents('li').next().find('h4').text(mobVal);
        });
    })(jQuery);
}

function desktopDefault(){
    (function($) {
        $('.deskMobTitle').each(function(){
            var mobVal = $(this).data('desc');
            $(this).parents('li').next().find('h4').text(mobVal);
        });
    })(jQuery);
}

function mainMenuClickClass(){
    (function($) {
        $(document).off('click.mainmenu').on('click.mainmenu', '.mobile-view .wpmm_mega_menu > a', function(e){
            if($(this).parent().find('.wpmm-strees-row-and-content-container').length > 0) {
                e.preventDefault();
                e.stopPropagation();
                $(this).parent().toggleClass('active-list');
                $(this).parents('ul.wp-megamenu').toggleClass('active-wrap');
                $(this).parents('.menus-container').toggleClass('submenu-active');
                $('#menu-main-menu-left').toggleClass('submenu-active');
                var submenu = $(this).parent().find('.level-1');
                submenu.toggleClass('sub-expanded');
                submenu.removeClass('menu-visible menu-invisible');
                //replace with CSS
                submenu.addClass((submenu.hasClass('sub-expanded')) ? 'menu-visible' : 'menu-invisible');
            }
        });
    })(jQuery);
}

function subMenuClickClass() {
    (function($) {
        $(document).off('click.submenu').on('click.submenu', '.mobile-view .sub-submenu', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('sub-submenu-active');
            var submenu = $(this).next();
            submenu.toggleClass('sub-sub-expanded');
            submenu.removeClass('menu-visible menu-invisible');
            // replace with CSS
            submenu.addClass((submenu.hasClass('sub-sub-expanded')) ? 'menu-visible' : 'menu-invisible');
        });
    })(jQuery);
}

(function($) {
    mediaCheck({
        media: '(min-width: 1024px)',
        entry: $.proxy(function () {
            $('body').addClass('desktop-view').removeClass('mobile-view');
            desktopDefault();
            $('.mobile-view .wpmm_mega_menu').off('click.mainmenu');
            $('.mobile-view .sub-submenu').off('click.submenu');
            $('ul, li, div, h4').removeClass('active-list active-wrap sub-submenu-active sub-sub-expanded submenu-active sub-expanded menu-visible menu-invisible');
            // Parent menu to add active class
            $(".desktop-view .wpmm-submenu-right > h4.wpmm-item-title:contains('shop')").css("display", "block");
            // End here
        }),
        exit: $.proxy(function () {
            $('body').addClass('mobile-view').removeClass('desktop-view');
            mobRename();
            mainMenuClickClass();
            subMenuClickClass();
            // Parent menu to add active class
            $(".mobile-view .wpmm-submenu-right > h4.wpmm-item-title:contains('shop')").css("display", "none");
            // End here
        })
    });

})(jQuery);
//End here

// (function($) {

// })(jQuery);

</script>