<?php
/*
Template Name: Masterbrand
*/
get_header();
?>

<?php /*START LOOP */ if (get_posts()) : while (have_posts()) : the_post(); ?>

	<?php
     if (have_rows('banner_module')):
     while (have_rows('banner_module')): the_row();
     $imageBgMobile = get_sub_field('banner_imagemobile');
     $imageBgDesktop = get_sub_field('banner_image_desktop');
     $videoBgMobile = get_sub_field('banner_video_mobile');
     $videoBgDesktop = get_sub_field('banner_video_desktop');
    ?>
	<!--------MOBILE------->
	 <div class="master-banner moblay" id="mainM">
        <div class="master-hero brand-master">
            <div class="banner-links up">
                <div class="top left">
                    <h1><?php echo get_sub_field('banner_title_mobile') ?></h1>
                </div>
				<?php $stickyheader = get_field('hamburger_menu');
                 if ($stickyheader == 'true') {?>
                 <a class="tog-ham">
                     <div class="top right mobile" style="cursor:pointer;">
                         <img class="alignnone size-medium wp-image-3570" role="img" src="<?php the_sub_field('banner_menu_iconmobile'); ?>" alt="" />
                     </div>
                 </a>
                <a href="javascript:void(0)" class="cross">&times;</a>
				 <?php } ?>
            </div>
            <?php if ($videoBgMobile): ?>
                <video class="master-hero__video" autoplay loop muted>
                    <source type="video/mp4" src="<?php echo $videoBgMobile; ?>" />
                </video>
            <?php elseif ($imageBgMobile): ?>
                <img src="<?php echo $imageBgMobile; ?>" class="alignnone banner-background-mobile" />
            <?php endif; ?>
           <?php
     if (have_rows('video_module')):
     while (have_rows('video_module')): the_row();
     
    ?>
            <div class="banner-links">
                <div class="bottom left">
                    <a id="banner-link-video" class="banner-popup-link" data-fancybox-type="iframe" href="<?php echo get_sub_field('video_url_mobile'); ?>">
                        <span class="triangle">                       
                        <img class="alignnone size--image-3571" role="img" src="<?php echo get_sub_field('video_icon_mobile')['url']; ?>" alt="<?php echo get_sub_field('video_icon_mobile')['alt']; ?>"/>
                        </span>
                    </a>
                    <h2 class="sty"><?php echo get_sub_field('video_title_mobile'); ?></h2>
                    <p class="sty1"><?php echo get_sub_field('video_description_mobile'); ?></p>
                    <span class="mob-video-title"><?php //echo get_sub_field('video_title_mobile');?></span>
                </div>
                <div class="bottom right">
                    <?php //$cnt = 1;
                    //while (have_rows('banner_links')) : the_row();?>
                        <a class="banner_link banner-link-0" href="<?php echo get_sub_field('link_url_one'); ?>">
                            <p> <?php echo get_sub_field('link_title_one'); ?></p>
                            <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_one'); ?>" alt="" />
                        </a>
						<a class="banner_link banner-link-1" href="<?php echo get_sub_field('link_url_two'); ?>">
                            <p> <?php echo get_sub_field('link_title_two'); ?></p>
                            <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_two'); ?>" alt="" />
                        </a>
                        <?php //$cnt++; endwhile;?>
                </div>
            </div>
		<?php
         endwhile; endif;
        ?>	
        </div>
    </div>
	
	<!-------DESKTOP--------->
	

    <div class="master-banner desklay" id="main">
        <div class="master-hero brand-master">
            <div class="banner-links up">
                <div class="top left">
                    <h1><?php echo get_sub_field('banner_title_desktop') ?></h1>
                </div>
				<?php $stickyheader = get_field('hamburger_menu');
                 if ($stickyheader == 'true') {?> 
                <a class="tog-ham">
                    <div class="top right desktop" style="cursor:pointer;">
                        <img class="alignnone size-medium wp-image-3570" role="img" src="<?php the_sub_field('banner_menu_icondesktop'); ?>" alt="" />
                    </div>
                </a>
                <a href="javascript:void(0)" class="cross">&times;</a>
				 <?php } ?>
            </div>
            <?php if ($videoBgDesktop): ?>
                <video class="master-hero__video" autoplay loop muted>
                    <source type="video/mp4" src="<?php echo $videoBgDesktop; ?>" />
                </video>
            <?php elseif ($imageBgDesktop): ?>
                <img src="<?php echo $imageBgDesktop; ?>" class="alignnone banner-background-desktop">
            <?php endif; ?>
			<?php
     if (have_rows('video_module')):
     while (have_rows('video_module')): the_row();
     
    ?>
            <div class="banner-links">
                <div class="bottom left">
                    <a id="banner-link-video" class="banner-popup-link" data-fancybox-type="iframe" href="<?php echo get_sub_field('video_url_desktop'); ?>">
                        <span class="triangle">                       
                        <img class="alignnone size--image-3571" role="img" src="<?php echo get_sub_field('video_icon_desktop')['url']; ?>" alt="<?php echo get_sub_field('video_icon_desktop')['alt']; ?>"/>
                        </span>
                    </a>
                    <h2><?php echo get_sub_field('video_title_desktop'); ?></h2>
                    <p><?php echo get_sub_field('video_description_desktop'); ?></p>
                    <span class="mob-video-title"><?php //the_field('mob_video_title');?></span>
                </div>
                <div class="bottom right">
                    <?php //$cnt = 1;
                    //while (have_rows('banner_links')) : the_row();?>
                        <a class="banner_link banner-link-0" href="<?php echo get_sub_field('link_url_one'); ?>">
                            <p> <?php echo get_sub_field('link_title_one'); ?> </p>
                            <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_one'); ?>" alt="" />
                        </a>
						<a class="banner_link banner-link-1" href="<?php echo get_sub_field('link_url_two'); ?>">
                            <p> <?php echo get_sub_field('link_title_two'); ?> </p>
                            <img class="alignnone " role="img" src="<?php echo get_sub_field('icon_url_two'); ?>" alt="" />
                        </a>
                        <?php //$cnt++; endwhile;?>
                </div>
            </div>
		<?php
         endwhile; endif;
        ?>	
        </div>
    </div>
	
	
	<?php endwhile; endif; ?>
	
	
        <div class="content-master-wrapper" id="main_1">
            <?php

            $fontSize = get_field('font_sizedesktop_post');
            $lineHeight = get_field('line_heightdesktop_post');
            $fontSizeMob = get_field('post_title_font_sizemobile');
            $lineHeightMob = get_field('post_title_line_heightmobile');

            global $post;
            $args = array( 'post_type' => 'mater landing posts', 'posts_per_page' => 30, 'orderby'   => 'meta_value',
                'order' => 'ASC',
                'meta_key' => 'post_title_sort');
            $lastposts = get_posts($args);

            foreach ($lastposts as $key => $post) :
                setup_postdata($post);
                $title = get_field('post_titledesktop');
                $externalLink = get_field('post_external_link');
                $mobileTitle = get_field('post_titlemobile');
                $shortDescription = get_field('short_description');
                $briefDescription = get_field('brief_description');
                $iconHoverColorDesktop = get_field('icon_and_title_hover_colourdesktop');
                $img = get_field('image');
                if ($title) {
                    ?>
                
                    <div class="content-master-list">

                        <a class="list-items" href="<?php if ($externalLink): echo $externalLink; else: the_permalink();
                    endif; ?>">
                            <div class="content-block" data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                 data-class-desktop="post_color_desk_main<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorDesktop; ?>"
                                 data-class-mobile="post_color_mob_main<?php echo $key; ?>" data-size="<?php echo $fontSize; ?>" data-line-height="<?php echo $lineHeight; ?>"
                                data-class="post-title-font-style<?php echo $key; ?>" data-mob-size="<?php echo $fontSizeMob; ?>" data-mob-line-height="<?php echo $lineHeightMob; ?>"
                                 data-mob-class="post-mob-title-font-style<?php echo $key; ?>">
                                <span class="post_title_desktop post_title dynamic-font"><?php echo $title; ?></span>
                                <span class="post_title_mobile post_title dynamic-font-mob"><?php echo $mobileTitle; ?></span>
                                <div class="cover">
                                  <span class="descript desktop"><?php echo $shortDescription; ?></span>
                                  <span class="descript mobile"><?php echo $briefDescription; ?></span>
                                  <span class="more-icon-desktop"  data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                     data-class-desktop="post_color_desk_icon<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorDesktop; ?>"
                                     data-class-mobile="post_color_mob_icon<?php echo $key; ?>"> <img class="alignnone size-medium wp-image-3570" role="img" src="https://www.happyfamilyorganics.com/wp-content/uploads/2018/05/trans-arrow.svg" alt=""/>
                                  </span>
                               </div>
                            </div>
                            <?php if ($img) { ?>
                                <div class="image-block">
                                    <img src="<?php echo $img; ?>" alt="" class="alignnone size-full wp-image-3588" />
                                </div>
                            <?php } else { ?>
                                <div class="brief-desc-block">
                                    <p><?php echo $briefDescription; ?></p>
                                </div>
                            <?php } ?>
                        </a>
                    </div>
									
                <?php
                } endforeach;
            wp_reset_postdata(); ?>
        </div>
		
							

    
<?php /*END LOOP */ endwhile; endif; ?>

<?php get_footer(); ?>

<?php // hfo-marketing-script-start?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.css" type="text/css" media="screen" />
<script src="https://cdn.jsdelivr.net/npm/mediaCheck@0.4.6/js/mediaCheck.js"></script>
<?php // hfo-marketing-script-end?>
<script>
    $ = jQuery;
    $(function(){
        $("#banner-link-video").fancybox();
    });
    function changeColorIcon(elem, mode){
        $(elem).each(function () {
            // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
            var $img = jQuery(this);
            // Get all the attributes.
            var attributes = $img.prop("attributes");
            // Dynamic Color
            var dynamicColor = $img.parent().data(mode);
            // Get the image's URL.
            var imgURL = $img.attr("src");
            // Fire an AJAX GET request to the URL.
            $.get(imgURL, function (data) {
                // The data you get includes the document type definition, which we don't need.
                // We are only interested in the <svg> tag inside that.
                var $svg = $(data).find('svg');
                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                // Loop through original image's attributes and apply on SVG
                $.each(attributes, function() {
                    $svg.attr(this.name, this.value);
                });
                $svg.css('fill', dynamicColor);
                // Replace image with new SVG
                $img.replaceWith($svg);
            });
        });
    }
    /*icon hover*/
    mediaCheck({
        media: '(min-width: 768px)',
        entry: $.proxy(function () {
            changeColorIcon(".more-icon-desktop img", "color-desktop");
        }),
        exit: $.proxy(function () {
            changeColorIcon(".more-icon-desktop img", "color-mobile");
        })
    });
</script>