<?php

add_action( 'wp_ajax_nopriv_load_year', 'load_year' );
add_action( 'wp_ajax_load_year', 'load_year' );
function load_year () {
	global $post;

	$year = $_POST[ 'year' ];
	$args = array(
		'posts_per_page'  => -1,
		'post_type'       => 'press-article',
		'post_status'     => 'publish',
		'suppress_filters' => false,
		'date_query' => array(
			array(
				'year' => $year
			)        
		)
	);

	$posts = get_posts( $args );

	foreach ( $posts as $post ) {
		setup_postdata( $post );

		$id = get_the_id();
		$title = esc_html(get_the_title());

		$output .= '<div class="press-article ' . $custom_class . '">';

		if ( has_post_thumbnail() ) {
			$featured_image = esc_url(get_the_post_thumbnail_url());
		} else {
			$featured_image = '';
		}
		$link = esc_url(get_field('press_link'));
		$source = esc_html(get_field('press_source'));
		$date = esc_html(get_the_date('m.d.y'));
		
		$output .= <<<CPT

				<a class="press-article__logo-container" target="_blank" href="{$link}">
					<img class="press-article__logo" src="{$featured_image}" alt="{$source}" />
				</a>
				<div class="press-article__details-container">
					<p class="press-article__date">{$date}</p>
					<h3 class="press-article__title"><a target="_blank" href="{$link}">{$title}</a></h3>
				</div>
CPT;

		$output .= '</div>'; // close press-article
	} 

	$output .= '<div class="press-pagination">' . paginate_links() . '</div';

	wp_reset_postdata();

	echo $output;
	die(1);
}
