<?php

add_role(
	'team_member',
	__( 'Team Member' ),
	array(
		'read'         => true,  // true allows this capability
		'edit_posts'   => true,
	)
);

add_role(
	'coach',
	__( 'Coach' ),
	array(
		'read'         => true,  // true allows this capability
		'edit_posts'   => true,
	)
);

function remove_personal_options(){
    echo '<script type="text/javascript">jQuery(document).ready(function($) {
  
$(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
  
$(\'h2:contains("About Yourself"), h2:contains("About the user")\').remove(); // remove the "About Yourself" and "About the user" titles
  
$(\'form#your-profile tr.user-description-wrap\').remove(); // remove the "Biographical Info" field
  
$(\'form#your-profile tr.user-profile-picture\').remove(); // remove the "Profile Picture" field
 
});</script>';
  
}
  
add_action('admin_head','remove_personal_options');
