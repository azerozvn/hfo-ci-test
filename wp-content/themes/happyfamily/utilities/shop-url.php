<?php

add_action( 'customize_register', 'shop_url_customizer' );
/*
 * Register Our Customizer Stuff Here
 */
function shop_url_customizer( $wp_customize ) {

	$wp_customize->add_section( 'custom_shop_url' , array(
		'title'    => __('Shop URL'),
		'priority' => 10
	) );

	$wp_customize->add_setting( 'shop_url' );

	$wp_customize->add_control( new WP_Customize_Control(
	    $wp_customize,
		'custom_footer_text',
		    array(
		        'label'    => __( 'Shop URL' ),
		        'section'  => 'custom_shop_url',
		        'settings' => 'shop_url',
		        'type'     => 'text'
		    )
	    )
	);
}
?>