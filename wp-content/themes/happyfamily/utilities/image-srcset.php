<?php
/**
* Generate srcset attribute from KeyCDN
*
*
* @return bool
*/
if (!function_exists('get_srcset')) {
  function get_srcset($url)
  {
    $widths = [1920, 1440, 1024, 768, 425, 210];
    $prefix = 'https://hfoprod-18c6d.kxcdn.com';
    
    // If staging, set staging prefix
    if( strpos( parse_url( esc_url($url) )['host'], 'hfodev2' ) != false ) {
      $prefix = 'hfostaging-18c6d.kxcdn.com';
    }

    // Remove the domain part of $url
    $path = parse_url( esc_url($url) )['path'];

    // Array of KeyCDN URLs, resized to $width using KeyCDN Image Processing
    $arr = array_map(function($width) use ($prefix, $path) {
      return $prefix . $path . '?width=' . $width . ' ' . $width . 'w';
    }, $widths);

    // return $arr;
    return implode(', ', $arr);
  }
}