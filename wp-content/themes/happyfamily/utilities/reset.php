<?php

// Remove admin bar
add_filter('show_admin_bar', '__return_false');

// Remove specific Visual Components
function mad_remove_other_elements() {
    vc_remove_element('vc_tta_accordion'); // Remove Accordion
}

// Remove version of WP from meta tags and RSS feed
function madwell_remove_version() {
  return '';
}
add_filter('the_generator', 'madwell_remove_version');

// Hide feed links from source code
// remove_action('wp_head', 'feed_links', 2); 
// add_action('wp_head', 'my_feed_links');

// Remove emoji support
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

// Remove items from CMS menu
function remove_menu_pages() {
    remove_menu_page('edit-comments.php');
    remove_menu_page('link-manager.php');
}
add_action( 'admin_menu', 'remove_menu_pages' );

// Reorder CMS menu
function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
     
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php?post_type=page', // Pages
        'edit.php', // Posts
        'edit.php?post_type=press-article', // Press Articles
        'edit.php?post_type=faq-question', // FAQ Questions
        'edit.php?post_type=career-listing', // Career Listings
        'upload.php', // Media
        'separator2', // Second separator
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'edit.php?post_type=acf-field-group', // ACF
        'separator-last', // Last separator        
    );
}
add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');

// Change Posts to Learning Center

function change_post_object() {
    $get_post_type = get_post_type_object('post');
    $labels = $get_post_type->labels;
    $labels->name = 'Learning Center';
    $labels->singular_name = 'Learning Center Post';
    $labels->add_new = 'Add Post';
    $labels->add_new_item = 'Add Post';
    $labels->edit_item = 'Edit Post';
    $labels->new_item = 'Learning Center Post';
    $labels->view_item = 'View Post';
    $labels->search_items = 'Search Learning Center';
    $labels->not_found = 'No Learning Center Post found';
    $labels->not_found_in_trash = 'No Learning Center Posts found in Trash';
    $labels->all_items = 'All Learning Center Posts';
    $labels->menu_name = 'Learning Center';
    $labels->name_admin_bar = 'Learning Center';
    $get_post_type->menu_icon = 'dashicons-lightbulb';
}
add_action( 'init', 'change_post_object' );

// Add coaches to the authors dropdown only for the regular posts (Learning center posts)
function add_coaches_to_dropdown_authors( $query_args, $r ) {
    global $post;

    if ( $post->post_type == ('post') ) {
        $query_args['role'] = array('coach');
        unset( $query_args['who'] );
    }
    return $query_args;
}
add_filter( 'wp_dropdown_users_args', 'add_coaches_to_dropdown_authors', 10, 2 );


// Extend WordPress search to include custom fields

// Join posts and postmeta tables
function acf_search_join( $join ) {
    global $wpdb;
    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
add_filter('posts_join', 'acf_search_join' );

// Modify the search query with posts_where
function acf_search_where( $where ) {
    global $pagenow, $wpdb;
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
    return $where;
}
add_filter( 'posts_where', 'acf_search_where' );

// Ignore duplicates
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );
