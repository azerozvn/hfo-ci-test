<?php

// Thumbnail sizes
add_filter('algolia_post_images_sizes', function($sizes) {
    $sizes[] = 'medium_large';

    return $sizes;
});

// Card excerpts
add_filter( 'algolia_post_shared_attributes', 'my_post_attributes', 10, 2 );
add_filter( 'algolia_searchable_post_shared_attributes', 'my_post_attributes', 10, 2 );

function my_post_attributes( array $attributes, WP_Post $post ) {

    $attributes['card_excerpt'] = get_excerpt( str_replace( 'What to Know', '', get_field('learning_center_summary', $post->ID) ), 40 );

    return $attributes;
}
