<?php

// Define global styles following BEM standards


//
// Change Visual Composer default class assignment
//

add_filter( 'vc_shortcodes_css_class', 'custom_css_classes', 10, 2 );
function custom_css_classes( $class_string, $tag ) {

	$class_string = str_replace( array( 'vc_row', 'wpb_row', 'vc_row-fluid', 'wpb_wrapper', 'wpb_text_column', 'wpb_content_element', 'wpb_column', 'vc_general'), '', $class_string );

	if ( $tag == 'vc_column'|| $tag == 'vc_column_container' ) {
		$class_string = str_replace( 'vc_column_container', '', $class_string ); 
	}

	if ( $tag == 'vc_row' || $tag == 'vc_row_inner' ) {
    	$class_string = preg_replace( array( '/-fluid/', '/-has-fill/', '/-inner/' ), 'main', $class_string ); 
	}
	
	if ( $tag == 'vc_column' || $tag == 'vc_column_inner' ) {
		$class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', '', $class_string ); 
	}

	if ( $tag == 'vc_btn' ) {
		$class_string = preg_replace( '/vc_btn(\d{1,2})/', 'button', $class_string );
	}

	if ( $tag == 'vc_btn' ) {
		$class_string = preg_replace( array( '/-size-md/', '/-shape-rounded/', '/style-modern/', '/-color-grey/'), '', $class_string );
	}


  // Return our classes
  return $class_string;
}