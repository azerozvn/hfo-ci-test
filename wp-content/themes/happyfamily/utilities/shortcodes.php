<?php

function newsletter_recipes(){
	ob_start();
	echo '<div class="learning-center-article__inline">';
	get_template_part( 'snippet-newsletter' );
	get_template_part( 'snippet-related-recipes' );
	echo '</div>';
	return ob_get_clean();
}
add_shortcode( 'newsletter-and-recipes', 'newsletter_recipes' );

function newsletter_signup(){
	ob_start();
	echo '<div class="learning-center-article__inline">';
	get_template_part( 'snippet-newsletter' );
	echo '</div>';
	return ob_get_clean();
}
add_shortcode( 'join-our-newsletter', 'newsletter_signup' );