<?php

function topics_init() {
		$labels = array(
			'name'                       => _x( 'Topic', 'taxonomy general name', 'textdomain' ),
			'singular_name'              => _x( 'Topic', 'taxonomy singular name', 'textdomain' ),
			'search_items'               => __( 'Search Topics', 'textdomain' ),
			'popular_items'              => __( 'Popular Topics', 'textdomain' ),
			'all_items'                  => __( 'All Topics', 'textdomain' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Topic', 'textdomain' ),
			'update_item'                => __( 'Update Topic', 'textdomain' ),
			'add_new_item'               => __( 'Add New Topic', 'textdomain' ),
			'new_item_name'              => __( 'New Topic Name', 'textdomain' ),
			'separate_items_with_commas' => __( 'Separate topics with commas', 'textdomain' ),
			'add_or_remove_items'        => __( 'Add or remove topics', 'textdomain' ),
			'choose_from_most_used'      => __( 'Choose from the most used topics', 'textdomain' ),
			'not_found'                  => __( 'No topics found.', 'textdomain' ),
			'menu_name'                  => __( 'Topics', 'textdomain' ),
		);

		$args = array(
			'hierarchical'          => true,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
            'show_in_rest'          => true,
			'rewrite'               => array( 'slug' => 'topic' ),
		);

		register_taxonomy( 'topics', 'post', $args );
}

function faq_category_init() {
		$labels = array(
			'name'                       => _x( 'FAQ Category', 'taxonomy general name', 'textdomain' ),
			'singular_name'              => _x( 'FAQ Category', 'taxonomy singular name', 'textdomain' ),
			'search_items'               => __( 'Search FAQ Categories', 'textdomain' ),
			'popular_items'              => __( 'Popular FAQ Categories', 'textdomain' ),
			'all_items'                  => __( 'All FAQ Categories', 'textdomain' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit FAQ Category', 'textdomain' ),
			'update_item'                => __( 'Update FAQ Category', 'textdomain' ),
			'add_new_item'               => __( 'Add New FAQ Category', 'textdomain' ),
			'new_item_name'              => __( 'New FAQ Category Name', 'textdomain' ),
			'separate_items_with_commas' => __( 'Separate FAQ categories with commas', 'textdomain' ),
			'add_or_remove_items'        => __( 'Add or remove FAQ category', 'textdomain' ),
			'choose_from_most_used'      => __( 'Choose from the most used FAQ categories', 'textdomain' ),
			'not_found'                  => __( 'No FAQ categories found.', 'textdomain' ),
			'menu_name'                  => __( 'FAQ Categories', 'textdomain' ),
		);

		$args = array(
			'hierarchical'          => true,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'show_in_menu'			=> true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'category' ),
		);

		register_taxonomy( 'faq_category', 'faq-question', $args );
}


function career_location_init() {
		$labels = array(
			'name'                       => _x( 'Career Location', 'taxonomy general name', 'textdomain' ),
			'singular_name'              => _x( 'Career Location', 'taxonomy singular name', 'textdomain' ),
			'search_items'               => __( 'Search Career Locations', 'textdomain' ),
			'popular_items'              => __( 'Popular Career Locations', 'textdomain' ),
			'all_items'                  => __( 'All Career Locations', 'textdomain' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Career Location', 'textdomain' ),
			'update_item'                => __( 'Update Career Location', 'textdomain' ),
			'add_new_item'               => __( 'Add New Career Location', 'textdomain' ),
			'new_item_name'              => __( 'New Career Location Name', 'textdomain' ),
			'separate_items_with_commas' => __( 'Separate Career Locations with commas', 'textdomain' ),
			'add_or_remove_items'        => __( 'Add or remove Career Location', 'textdomain' ),
			'choose_from_most_used'      => __( 'Choose from the most used Career Locations', 'textdomain' ),
			'not_found'                  => __( 'No Career Locations found.', 'textdomain' ),
			'menu_name'                  => __( 'Career Locations', 'textdomain' ),
		);

		$args = array(
			'hierarchical'          => true,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'show_in_menu'			=> true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'category' ),
		);

		register_taxonomy( 'career_location', 'career-listing', $args );
}

add_action( 'init', 'topics_init' );
add_action( 'init', 'faq_category_init', 0 );
add_action( 'init', 'career_location_init', 0 );

