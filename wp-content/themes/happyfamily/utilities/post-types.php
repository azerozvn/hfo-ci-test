<?php

function initPress() {

	$labels = array(
		'name'                => _x( 'Press', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Press Article', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Press', 'text_domain' ),
		'name_admin_bar'      => __( 'Press', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Press Article:', 'text_domain' ),
		'all_items'           => __( 'All Press Articles', 'text_domain' ),
		'add_new_item'        => __( 'Add New Press Article', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Press Article', 'text_domain' ),
		'edit_item'           => __( 'Edit Press Article', 'text_domain' ),
		'update_item'         => __( 'Update Press Article', 'text_domain' ),
		'view_item'           => __( 'View Press Article', 'text_domain' ),
		'search_items'        => __( 'Search Press Articles', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'Press', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'custom-fields', 'title', 'thumbnail', 'author', 'editor', 'revisions', 'page-attributes' ),
		'taxonomies'          => array( ),
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => false,
		'menu_icon'           => 'dashicons-list-view',
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => '',
	);
	register_post_type( 'press-article', $args );

}


function initFAQ() {

	$labels = array(
		'name'                => _x( 'FAQ', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'FAQ Question', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'FAQ', 'text_domain' ),
		'name_admin_bar'      => __( 'FAQ', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent FAQ Questions:', 'text_domain' ),
		'all_items'           => __( 'All FAQ Questions', 'text_domain' ),
		'add_new_item'        => __( 'Add New FAQ Question', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New FAQ Question', 'text_domain' ),
		'edit_item'           => __( 'Edit FAQ Question', 'text_domain' ),
		'update_item'         => __( 'Update FAQ Question', 'text_domain' ),
		'view_item'           => __( 'View FAQ Question', 'text_domain' ),
		'search_items'        => __( 'Search FAQ Questions', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'FAQ', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'custom-fields', 'title', 'thumbnail', 'author', 'editor', 'revisions', 'page-attributes' ),
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => false,
		'menu_icon'           => 'dashicons-format-status',
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => '',
		'taxonomies'          => array('faq_category' )
	);
	register_post_type('faq-question', $args );

}


function initCareers() {

	$labels = array(
		'name'                => _x( 'Careers', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Career Listing', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Careers', 'text_domain' ),
		'name_admin_bar'      => __( 'Careers', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Careers:', 'text_domain' ),
		'all_items'           => __( 'All Career Listings', 'text_domain' ),
		'add_new_item'        => __( 'Add New Career Listing', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Career Listing', 'text_domain' ),
		'edit_item'           => __( 'Edit Career Listing', 'text_domain' ),
		'update_item'         => __( 'Update Career Listing', 'text_domain' ),
		'view_item'           => __( 'View Career Listing', 'text_domain' ),
		'search_items'        => __( 'Search Career Listings', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'Careers', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'custom-fields', 'title', 'thumbnail', 'author', 'editor', 'revisions', 'page-attributes' ),
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => false,
		'menu_icon'           => 'dashicons-businessman',
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => '',
		'taxonomies'          => array('career_location' )
	);
	register_post_type('career-listing', $args );

}

add_action( 'init', 'initPress', 0 );
add_action( 'init', 'initFAQ', 0 );
add_action( 'init', 'initCareers', 0 );
