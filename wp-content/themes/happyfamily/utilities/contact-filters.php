<?php function dynamic_select_list($tag, $unused) {

    if ( $tag['name'] != 'department-email' )  
        return $tag;  
  
   $departments = get_field('contact_departments');

    if (!$departments)  
        return $tag;  

    foreach ( $departments as $department ) {  
        if($department['show_in_form_dropdown']) {
            $tag['raw_values'][] = $department['department_email'];  
            $tag['values'][] = $department['department_email'];  
            $tag['labels'][] = $department['department_inquiry_label'];
        } 
       
    }  
  
    return $tag; 

} // end function dynamic_select_list


add_filter('wpcf7_form_tag', 
             'dynamic_select_list', 10, 2);
?>