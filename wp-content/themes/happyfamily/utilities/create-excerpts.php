<?php

// Create excerpt from post content

function get_excerpt($description, $length){
    $excerpt_length = $length;
    $excerpt = strip_shortcodes($description);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $length);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'...';
    return $excerpt;
}