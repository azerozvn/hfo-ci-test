<?php

/**
 * Class WP_REST_GlobalNavigation_Controller
 */
class WP_REST_GlobalNavigation_Controller extends WP_REST_Controller
{
    /**
     * Global_Navigation_Controller constructor.
     */
    public function __construct() {
        $this->namespace = 'gnav/v1';
    }

    /**
     * Route registration
     */
    public function register_routes()
    {
        register_rest_route(
            $this->namespace,
            '/get_navigation_items',
            array(
                array(
                    'methods'             => WP_REST_Server::READABLE,
                    'callback'            => array( $this, 'get_navigation_items' ),
                    'permission_callback' => array( $this, 'get_items_permissions_check' ),
                    'args'                => $this->get_collection_params(),
                ),
                'schema' => array( $this, 'get_public_item_schema' ),
            )
        );
    }

    /**
     * Checks whether a given request comes from the same domain.
     *
     * @since 4.7.0
     *
     * @param WP_REST_Request $request Full details about the request.
     * @return WP_Error|bool True if the request has read access, WP_Error object otherwise.
     */
    public function get_items_permissions_check( WP_REST_Request $request ) {
        $referer = $request->get_header( 'referer' );
        if ( !$referer ) {
            // Additional header in the case of BE requests, where referer is not available
            $referer = $request->get_header( 'authorization' );
        }
        if ( !$referer ) {
            return false;
        }

        $host = wp_parse_url( $referer, PHP_URL_HOST );
        if ( !$host ) {
            return false;
        }
        
        $site_url = wp_parse_url( get_site_url(), PHP_URL_HOST );

        if ( $site_url !== $host ) {
            return new WP_Error(
                'invalid-referer',
                'Only internal requests are allowed',
                compact( 'referer' )
            );
        }

        return true;
    }

    /**
     * Generate home page content and extract header&footer HTML from there
     * 
     * @return array|WP_Error
     */
    public function get_navigation_items()
    {
        try{
            ob_start();
            require_once( ABSPATH . WPINC . '/template-loader.php' );
            $pageContent = ob_get_clean();

            // Consider all the content between the end of the opening body tag and the closing header tag
            // as a header content (this will include banners); the footer should be fine as is
            preg_match('/<body[^>]+>(.+<\/header>).+(<footer.+<\/footer>)/s', $pageContent, $matches);
            $header = isset($matches[1]) ? $this->removeScripts($matches[1]) : '';
            $footer = isset($matches[2]) ? $this->removeScripts($matches[2]) : '';

            // Adding the JS config object coming from the MegaMenu module
            $footer .= $this->getMegamenuJsConfig();

            return [
                'header' => $header,
                'footer' => $footer
            ];
        } catch (Throwable $exception) {
            return new WP_Error(
                'rest-failure',
                'Unable to load navigation data',
                compact( $exception->getMessage() )
            );
        }
    }

    /**
     * @param string $content
     * @return string
     */
    private function removeScripts($content)
    {
        return preg_replace('@(?:<script)(.*)</script>@msU', '', (string) $content);
    }

    /**
     * @return string
     */
    private function getMegamenuJsConfig()
    {
        ob_start();
        wp_scripts()->print_extra_script('wpmm_js', true);

        return ob_get_clean();
    }
}

$controller = new WP_REST_GlobalNavigation_Controller();
$controller->register_routes();