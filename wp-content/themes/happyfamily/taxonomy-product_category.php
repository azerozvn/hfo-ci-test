<?php
  ////////////////////////////////////////////////////////////////////////////////////
  // PROPS

  // Define Checkbox Groups.  These are custom taxonomies registered in functions.php.
  $taxonomies = [
    array('label' => 'Age Range', 'slug' => 'age_range'),
    array('label' => 'Development Stage', 'slug' => 'lifestage'),
    array('label' => 'Product Category', 'slug' => 'product_group'),
    array('label' => 'Allergies & Dietary Needs', 'slug' => 'allergies'),
    array('label' => 'Product Line', 'slug' => 'product_line')
  ];

?>

<?php
  ////////////////////////////////////////////////////////////////////////////////////
  // METHODS
  
  // Get product fields & fallbacks
  function get_product_fields($id) {
    // Category
    $category = get_the_terms($id, 'product_category');
    if($category && $category[0]) {
      $category = $category[0] -> name;
    } else {
      $category = '';
    }

    // Category Color
    if($category == 'Baby') { $category_color = 'green'; }
    if($category == 'Tot') { $category_color = 'purple'; }
    if($category == 'Kid') { $category_color = 'blue'; }
    if($category == 'Mama') { $category_color = 'red'; }

    // Product Line
    $product_line = get_the_terms($id, 'product_line');
    if($product_line && $product_line[0]) {
      $product_line = $product_line[0] -> name;
    } else {
      $product_line = '';
    }
    
    return array(
      'category' => $category,
      'category_color' => $category_color,
      'product_line' => $product_line,
      'url' => get_permalink($id),
      'taxonomies' => array(
        'age_range' => format_taxonomy($id, 'age_range'),
        'lifestage' => format_taxonomy($id, 'lifestage'),
        'product_group' => format_taxonomy($id, 'product_group'),
        'allergies' => format_taxonomy($id, 'allergies'),
        'product_line' => format_taxonomy($id, 'product_line')
      )
    );
  }

  function format_taxonomy($id, $name) {
    $array = get_the_terms($id, $name) ?: [];
    return implode( ',', array_map( 'map_taxonomy', $array ) );
  }

  function map_taxonomy($term) {
    return $term -> slug;
  }

  function get_purchase_url($upc_code) {
    return 'https://www.happyfamilyorganics.com/store-locator/?PROD=' . $upc_code . '&MM=panel2';
  }

  function get_price_per_quantity($price, $qty) {
    if (!$price || !$qty) { return ''; }
    return '$' . number_format( ( floatval($price) / floatVal($qty) ), 2 );
  }

  function echo_packaging() {
    $packaging_size = get_field('packaging_size');
    if( !$packaging_size ) {
      return;
    }
    echo '<span class="packaging">' . $packaging_size; // "3.17 oz"
    echo '</span>';
  }

  function echo_price_per_quantity() {
    $packaging_qty = get_field('packaging_qty');
    $price = get_field('price');
    $packaging_type = get_field('packaging_type') ?: 'Item';

    if( !$packaging_qty || !$price ) {
      return;
    }
    echo '<span class="price-per-quantity">' . get_price_per_quantity($price, $packaging_qty) . ' / ' . $packaging_type . '</span>';
  }
  
?>

<?php
  ////////////////////////////////////////////////////////////////////////////////////
  // TEMPLATE
?>

<?php get_header(); ?>

<div class="plp">
  <!-- Product List -->
  <div id="plp-list">
    <?php
      
      $args = array(
        'post_type' => 'products',
        'posts_per_page' => -1,
      );

      $queried_obj = get_queried_object();
      if( $queried_obj instanceof WP_Term && $queried_obj->taxonomy == 'product_category' ) {

        $category_slug = $queried_obj->slug;

        $args['tax_query'] = array(
          array(
              'taxonomy' => 'product_category',
              'field'    => 'slug',
              'terms'    => $category_slug,
          ),
        );
      }

      $query = new WP_Query($args);

      // print_r( get_queried_object() );
      // print_r( 'have_posts: ' . $query->have_posts() ); die();

      if ($query->have_posts() ) : 
        while ( $query->have_posts() ) : $query->the_post(); ?>
          <?php $product = get_product_fields( get_the_ID() ); ?>
          <div 
            class="product-wrap"
            data-age_range="<?php echo $product['taxonomies']['age_range'] ?>"
            data-lifestage="<?php echo $product['taxonomies']['lifestage'] ?>"
            data-product_group="<?php echo $product['taxonomies']['product_group'] ?>"
            data-allergies="<?php echo $product['taxonomies']['allergies'] ?>"
            data-product_line="<?php echo $product['taxonomies']['product_line'] ?>"
          >
            <div class="product">
              <a class="item-image" href="<?php echo $product['url'] ?>" style="background-image:url('<?php echo get_field('thumbnail_image')['url'] ?>')" aria-label="<?php echo get_the_title() ?>"></a>
              
              <div class="item-terms">
                <?php if($product['category']) : ?> <span class="term term-category color-<?php echo $product['category_color'] ?>"><?php echo $product['category'] ?></span> <?php endif; ?>
                  <?php if($product['product_line']) : ?> <span class="term term-product-line"><?php echo $product['product_line'] ?></span> <?php endif; ?>
              </div>
              
              <a class="item-title" href="<?php echo $product['url'] ?>" tabindex="-1">
                <?php echo get_the_title() ?>
              </a>

              <div class="item-packaging">
                <?php echo_packaging(); ?>
                <?php echo_price_per_quantity(); ?>
              </div>

              <div class="price-and-buy">
                <a href="<?php echo get_permalink() . '#buy-now' ?>" class="btn small outline arrow product-buy">Buy Now</a>
              </div>

            </div>
          </div>
        <?php endwhile;
        wp_reset_postdata();
      endif;
    ?>
    <div class="filter-no-match hide">
      <div class="inner">
        We can't find products matching the selection. Please try again!
        <button id="plp-filter-clear-all-2" class="btn small arrow" >Clear Filters</button>
      </div>
    </div>
  </div>

  <!-- Filter -->
  <div id="plp-filter" class="plp-filter-wrap closed">
    <button class="title-mobile">Filters (<span class="count">0</span>) <span class="arrow"></span></button>
    <div class="filters-selected-wrap">
      <h4 class="filter-title">Filters Selected <button id="plp-filter-clear-all" class="text-btn text-orange" aria-label="Clear all filters">Clear All</button></h4>
      <div class="filters-selected">
        <!-- Rendered by JS -->
        <!-- <div class="filter-selected">0-6 Months <button class="text-btn" data-slug="...">✖️</button></div> -->
      </div>
    </div>
    
    <h4 class="filter-title">Filter Results</h4>
    <?php foreach ( $taxonomies as $key => $taxonomy ) : ?> 
      <?php  
        $terms = get_terms([
          'taxonomy' => $taxonomy['slug'],
          'orderby' => 'slug',
          'order' => 'ASC',
          'hide_empty' => false,
        ]);        
      ?>
      <div class="filter-group" data-slug="<?php echo $taxonomy['slug'] ?>">
        <div class="group-title desktop"><?php echo $taxonomy['label'] ?></div>
        <button class="group-title mobile" data-slug="<?php echo $taxonomy['slug'] ?>"><span class="icon"></span><?php echo $taxonomy['label'] ?></button>
        <div class="terms" data-slug="<?php echo $taxonomy['slug'] ?>">
          <?php foreach ( $terms as $key => $term ) : ?> 
            <div class="term">
              <input type="checkbox" id="id-<?php echo $term->slug ?>" name="<?php echo $taxonomy['slug'] ?>" value="<?php echo $term->slug ?>" aria-label="<?php echo $term->name ?>">
              <label for="id-<?php echo $term->slug ?>"><?php echo $term->name ?></label>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endforeach; ?>
    
    <span class="buttons-mobile">
      <button class="btn grey outline plp-filter-clear">Clear All</button>
      <button class="btn plp-filter-close">Close</button>
    </span>
  </div>


</div>

<?php get_footer(); ?>