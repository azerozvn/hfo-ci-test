<?php
/*

Redirect all Learning Center "date archive" pages back to the main "/learning-center".
This is because these URLs are getting a few hits, and Wordpress does not throw a 404, it shows the header + blank page
because there's no template designed for this variant of the archive page.

Examples:
/learning-center/2019/
/learning-center/2018/page/13/
/learning-center/2016/?post_type=press-article

*/

// Redirect the user to "/learning-center"
header("Location: " . get_bloginfo('url') . "/learning-center", TRUE, 301);