<?php

	$current_post_id = $post->ID;
	$tags = wp_get_post_terms( $post->ID, 'post_tag' );
	$tag_ids = array();
	foreach ($tags as $tag) {
		if( $tag->slug != 'baby' && $tag->slug != 'tot' && $tag->slug != 'kid' && $tag->slug != 'mama' )
			array_push($tag_ids, $tag->term_id);
	}

	$posts_by_tags = array();
	$posts_by_tag_ids = new WP_Query( array(
		'posts_per_page' => 3,
		'category_name' => 'Recipe',
		'post_status' => 'publish',
		'orderby' => 'rand',
		'tax_query' => array( array(
			'taxonomy' => 'post_tag',
			'field' => 'id',
			'terms' => $tag_ids,
	) ) ) );
	while ( $posts_by_tag_ids->have_posts() ) {
		$posts_by_tag_ids->the_post();
		
		$recipe_title = get_the_title();
		
		array_push( $posts_by_tags,  array('title' => $recipe_title, 'url' => get_permalink()) );
	}
	wp_reset_postdata();
?>

<?php if ( count($posts_by_tags) >= 2 ) : ?>
	<hr class="learning-center-article__sidebar-hr">

	<div class="learning-center-article__related-articles">
		<h1>Related recipes</h1>

		<?php foreach ($posts_by_tags as $post_by_tag) : ?>
			<a href="<?php echo esc_url($post_by_tag['url']); ?>"><?php echo esc_html($post_by_tag['title']); ?></a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>