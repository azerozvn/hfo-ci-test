<li class="mobile-only">
	<div class="learning-center-article__content-recipe-image">
		<a target="_blank" href="mailto:?&subject=<?php echo esc_url(the_title()); ?>&body=<?php echo esc_url(get_permalink()); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-email.svg" alt="Email"></a>
	</div>
	<a target="_blank" href="mailto:?&subject=<?php echo esc_url(the_title()); ?>&body=<?php echo esc_url(get_permalink()); ?>"><span class="learning-center-article__content-recipe-text">Email</span></a>
</li>