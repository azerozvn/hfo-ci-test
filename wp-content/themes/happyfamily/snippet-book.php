<?php $category_page_id = get_page_by_path( 'learning-center/recipes-meal-plans' ); ?>
<section class="learning-center-book border-purple section-purple rough-border">
	<h1 class="mobile-only"><?php echo esc_html( get_field( 'title', $category_page_id ) ); ?></h1>
	<div class="learning-center-book__image">
		<?php $image = get_field( 'book_image', $category_page_id ) ?>
		<img src="<?php echo $image['url']; ?>" alt="<?php echo esc_attr( get_field( 'title', $category_page_id ) ); ?>">
	</div>
	<div class="learning-center-book__description">
		<h1 class="mobile-hide"><?php echo esc_html( get_field( 'title', $category_page_id ) ); ?></h1>
		<p><?php echo esc_html( get_field( 'description', $category_page_id ) ); ?></p>
		<a class="button" target="_blank" href="<?php echo esc_url( get_field( 'book_link', $category_page_id ) ); ?>">Get it here</a>
	</div>
</section>
