// Deps
const csv = require('csvtojson')
const fsPromises = require('fs').promises; 
const fs = require('fs');
const request = require('request');


const colors = [
    'b6172b',
    'efb71d',
    'ea7425',
    '273d8f',
    'f48423',
    'dd244c',
    '88bd40',
    'd52f27',
    'ed9122',
    '57b547',
    'ea4276',
    '943b82',
    '4a7f3b',
    'ec6124',
    '3da348',
    'edc034',
    '197d3e',
    'ed9922',
    '63a744',
    'ea6625',
    '2f2e79',
    'e4b072',
    'fd1f1e',
    '7aa51f',
    '42343f',
    '94c83d',
    '891456',
    'efb71d',
    'ff2c56',
    'f26924',
    '559d4a',
];


const imagePrefix = 'https://hfodev2.wpengine.com/wp-content/themes/happyfamily/colorize.php?path=dist/images/happyfarms/';

const images = [
    'arrow-black.png',
    'shine-left-b.png',
    'shine-right-b.png',
    'icon-play-black.png',
    'stain-circle.png',
    'media-arrow.png',
    'separator-line-vertical.png',
];

const download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);
        
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

colors.forEach((color, index) => {
    images.forEach((image, index2) => {
        const url = imagePrefix + image + '&color=' + color;
        const destination = image.split('.png')[0] + '-' + color + '.png';
        
        // console.log('image, color: ', image, color);
        
        setTimeout(() => {
            console.log('fetching url, destination: ', url, destination);
            download(url, 'downloads/' + destination, function(){
                console.log('done');
            });
        }, index * 1000);
    });
});
