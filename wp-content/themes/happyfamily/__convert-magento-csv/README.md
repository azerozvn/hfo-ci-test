

# Quick Start

`nvm use`
`yarn`

Generate the XML file
`node convert-products.js`

Check the XML to confirm it's valid
`xmllint --noout --valid _output/products.xml`

Then go to Wordpress CMS > Tools > Import > Wordpress, and import "products.xml"

## To delete the posts from Wordpress (so you can import again)

You must delete the posts before importing again.  Wordpress Importer will NOT overwrite any existing posts or meta fields.

Open the WP DB in your SQL editor of choice.  

Delete Product posts:
```
DELETE
FROM `wp_posts`
WHERE `post_type` = 'products' AND `ID` > '7000'
LIMIT 200;
```

Delete ACF field data: (Product posts, but not product images)
```
DELETE
FROM `wp_postmeta`
WHERE `post_id` > '7000' AND `meta_key` NOT REGEXP '.*attach.*'
LIMIT 500000;
```

Delete ACF field data: (Product posts AND product images)
```
DELETE
FROM `wp_postmeta`
WHERE `post_id` > '7000'
LIMIT 500000;
```



## Quick Summary

The old HFO website was part Magento, part Wordpress.  Magento controlled all the /shop pages.  For this project we moved all the products into Wordpress.  We exported the products from Magento to a CSV, and added that here ("HFO Product Export Dec 1 - one.csv").

Next we decided to use Wordpress's import and export feature (Tools > Import > Wordpress).  This uses XML files to import/export posts (all post types: pages, custom post types, and media attachment posts (images)).  

I build the product custom post type with ACF fields.  Then I exported it, to see an example of the XML setup I'd need to use in order to import the Magento products as Wordpress XML format (_output from wp/happyfamilyorganics.WordPress.2020-12-09 products.xml).  I also exported the media gallery to see what format I need for images.  

Next I wrote a node script (convert-products-3.js) to parse the Magento CSV, and write the XML to a file (in _output folder) that I can import to WP.

TLDR, it was trial and error but it eventually worked.  See below for the steps I took to get it working.

## More detailed summary

The Wordpress importer (Tools > Import > Wordpress) took some effort.  First, when I ran it, it said "Failed to import..." for all the images.  I had to turn on debug mode to see more info: the file is "wordpress-importer.php", the line is define( 'IMPORT_DEBUG', true );.  

Then I saw errors: there were some mismatched XML closing tags.  To track them down, I ran an XML validator in the terminal: xmllint --noout --valid _output/products.xml.  

With that fixed, I saw another error: Wordpress Importer was deliberately not importing my image posts because I didn't check the box "Download and import file attachments".

Error when I DID check "Download and import file attachments":
704: Failed to import Media “20201104-disco-icon-9”: Remote server did not respond.
Here's why I think it didn't work.  I had already downloaded all the Magento product images from the current site (using a node script).  Then I had copied them into wp-uploads/....  Then in the import XML I created, in the image posts, in the wp:attachment-url fields, I had put the web address to the image  (ie. https://hfo02.localdev/wp-content/uploads/2018/12/kid-yogurt-pouch-bananamangospinach.png).  In the PHP wordpress importer (wordpress-importer.php), the code grabs the wp:attachment_url, and tries to download it and save it to disk.  The code doesn't seem to want to just add them to the database and not touch the file on disk.

So, I edited my script and made it so in the image posts, the wp:attachment_url pointed back to the image hosted on current production website.  

When I ran the import again, (with "download and import file attachments" checked), it worked: PHP fetched all the product images from the current production server, and wrote the files into the wp-content/uploads/2020/12.  It also generated sized-down versions of each image.  I got several errors still, "Media "..." already exists", for duplicate posts, but that looked okay.

But then I noticed, although all the text fields had content, none of the image fields had content.  This was because the ACF field data still existed in the WP database, in the "wp_postmeta" table, and the Wordpress Importer will not overwrite any posts (or meta fields) that already contain content.  Although I had been deleting the product posts from "wp_posts" table (in Adminer), I had neglected to delete the corresponding ACF field data in the "wp_postmeta" table.   One of my old imports had put empty strings in the image fields, and they were stuck there.  So, I removed the offending rows from "wp_postmeta" table.  Then I re-ran the import.  Boom, the image fields were populated.