<?php
/*
Template Name: Modular Landing Page
Template Post Type: mater landing posts, page
*/
global $post;
get_header();
if ( ! post_password_required( $post ) ) {
?>
<?php // hfo-marketing-script-start ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.css" type="text/css" media="screen" />
<?php // hfo-marketing-script-end ?>

<?php /*START LOOP */
$overcolor = "green";
if (get_posts()) : while (have_posts()) : the_post(); ?>
    <!--    <div class="controls-wrapper">-->
    <!--        <div class="controls">-->
    <!--            <div class="title">Happy is</div>-->
    <!--            <div class="actions-wrapper">-->
    <!--                <div class="actions">-->
    <!--                    <div class="our-story-link">-->
    <!--                        <div class="text">OUR STORY</div>-->
    <!--                        <div class="icon"></div>-->
    <!--                    </div>-->
    <!--                    <div class="download">-->
    <!--                        <div class="text">DOWNLOAD SUMMARY</div>-->
    <!--                        <div class="icon"></div>-->
    <!--                    </div>-->
    <!--                    <div class="menu">-->
    <!--                        <div class="icon"></div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
   <div class="modular-wrapper" id="main_1">
    <?php if( have_rows('modular_landing_section_one') ): ?>
        <?php while( have_rows('modular_landing_section_one') ): the_row();
            $chk_enable_mod_one = get_sub_field('show_module_one');
            if($chk_enable_mod_one === 'Yes'):
                ?>
                <div class="modular-top-section main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_one'); ?>">
                    <div class="modular-landing-wrapper">
                        <div class="modular-info" data-class="module_one_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_one'); ?>" data-line-height="<?php the_sub_field('line_height_module_one'); ?>">
                            <a href="<?php the_sub_field('back_link_url_module_one') ?>" class="modular-back"><?php the_sub_field('back_link_text_module_one') ?></a>
                            <?php the_sub_field('title_module_one') ?>
                            <span class="dynamic-font-modular">
                            <?php the_sub_field('description_module_one'); ?>
                            </span>
                        </div>

                        <?php 
                        $image_desktop = get_sub_field('image_desk_module_one');
                        $image_mobile = get_sub_field('image_mob_module_one');
                        $video_desktop = get_sub_field('video_desk_module_one');
                        $video_mobile = get_sub_field('video_mob_module_one');                        
                        ?>

                        <div class="modular-img-wrap tablet-up">
                            <?php if ($video_desktop): ?>
                                <video autoplay loop muted>
                                    <source type="video/mp4" src="<?php echo $video_desktop; ?>" />
                                </video>
                            <?php elseif ($image_desktop): ?>
                                <img src="<?php echo $image_desktop; ?>">
                            <?php endif; ?>                            
                        </div>

                        <div class="modular-img-wrap tablet-down">
                            <?php if ($video_mobile): ?>
                                <video autoplay loop muted>
                                    <source type="video/mp4" src="<?php echo $video_mobile; ?>" />
                                </video>
                            <?php elseif ($image_mobile): ?>
                                <img src="<?php echo $image_mobile; ?>">
                            <?php endif; ?>                            
                        </div>

                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_two') ): ?>
        <?php while( have_rows('modular_landing_section_two') ): the_row();
            $chk_enable_mod_two = get_sub_field('show_module_two');
            if($chk_enable_mod_two === 'Yes'):
                ?>
                <div class="modular-button-wrapper main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_two'); ?>">
                    <div class="modular-buttons" style="background-color: <?php echo the_sub_field('background_color_module_two')?> !important;">
                       <?php if (get_sub_field('download_url_new_tab_module_two')=="Yes") { ?>
                           <a href="<?php echo the_sub_field('download_url_module_two') ?>" target="_blank">
                               <button style="background-color: <?php echo the_sub_field('button_color_module_two') ?> !important;color: <?php echo the_sub_field('font_color_module_two') ?>"><?php echo the_sub_field('button_title_module_two') ?></button>
                           </a>
                           <?php
                       }
                       else { ?>
                            <a href="<?php echo the_sub_field('download_url_module_two') ?>"  target="_self">
                               <button style="background-color: <?php echo the_sub_field('button_color_module_two') ?> !important;color: <?php echo the_sub_field('font_color_module_two') ?>"><?php echo the_sub_field('button_title_module_two') ?></button>
                           </a>
                           <?php
                       } ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_three') ): ?>
        <?php while( have_rows('modular_landing_section_three') ): the_row();
            $chk_enable_mod_three = get_sub_field('show_module_three');
            if($chk_enable_mod_three === 'Yes'):
                ?>
                <div class="modular-subbanner-section main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_three'); ?>">
                    <div class="subbanner-info" data-class="module_three_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_three'); ?>" data-line-height="<?php the_sub_field('line_height_module_three'); ?>">
                        <?php if(get_sub_field('subhead_module_three')!="") : ?>
                        <?php the_sub_field('subhead_module_three') ?>
                        <?php endif; ?>

                        <?php if(get_sub_field('copy_text_module_three')!="") : ?>
                        <span class="dynamic-font-modular"><?php the_sub_field('copy_text_module_three') ?> </span>
                        <?php endif; ?>
                    </div>

                    <div class="subbanner-wrapper">
                        <?php 
                            $image_desktop = get_sub_field('image_desk_module_three');
                            $image_mobile = get_sub_field('image_mob_module_three');
                            $video_desktop = get_sub_field('video_desk_module_three');
                            $video_mobile = get_sub_field('video_mob_module_three');                        
                        ?>
                        <?php if ($video_desktop): ?>
                            <video autoplay loop muted class="tablet-up">
                                <source type="video/mp4" src="<?php echo $video_desktop; ?>" />
                            </video>
                        <?php elseif ($image_desktop): ?>
                            <img src="<?php echo $image_desktop; ?>" class="tablet-up">
                        <?php endif; ?>                            

                        <?php if ($video_mobile): ?>
                            <video autoplay loop muted class="tablet-down">
                                <source type="video/mp4" src="<?php echo $video_mobile; ?>" />
                            </video>
                        <?php elseif ($image_mobile): ?>
                            <img src="<?php echo $image_mobile; ?>" class="tablet-down">
                        <?php endif; ?>                            
                        
                        <span class="caption-title"><?php the_sub_field('captions_title_module_three') ?></span>
                        <span class="caption-description"><?php the_sub_field('captions_description_module_three') ?></span>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

     <?php if( have_rows('modular_landing_section_four') ): ?>
        <?php while( have_rows('modular_landing_section_four') ): the_row();
            $chk_enable_mod_four = get_sub_field('show_module_four');
            if($chk_enable_mod_four === 'Yes'):
            ?>
            <div class="image-slider view-1 main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_four'); ?>"
                data-slick-options='
                    {
                        "slidesToShow": 2,
                        "dotsSelector": ".dots",
                        "dots": true,
                        "infinite": false,
                        "focusOnSelect": true,
                        "responsive": [{
                            "breakpoint": 1024,
                            "settings": {
                                "slidesToShow": 2,
                                "arrows": false
                            }
                        }, {
                            "breakpoint": 640,
                            "settings": {
                                "slidesToShow": 1,
                                "arrows": false
                            }
                        }]
                    }
                '>
                <div class="after-icons">
                    <div class="info" data-class="module_four_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_four'); ?>" data-line-height="<?php the_sub_field('line_height_module_four'); ?>">
                        <?php if(get_sub_field('lead_in_module_four')!="") : ?>
                        <div class="caption-title"><?php the_sub_field('lead_in_module_four') ?></div>
                        <?php endif; ?>
                        <?php if(get_sub_field('copy_text_module_four')!="") : ?>
                        <span class="caption-description dynamic-font-modular"><?php the_sub_field('copy_text_module_four') ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                <ul class="items">
                    <?php
                    if( have_rows('slider_module_four') ): while ( have_rows('slider_module_four') ) : the_row();
                        if( have_rows('gallery_section_module_four') ): while ( have_rows('gallery_section_module_four') ) : the_row();
                            ?>
                            <?php $nineslidermages = get_sub_field('gallery_section_module_four');?>
                                <li class="item">
                                    <div class="image-wrapper">
                                        <picture>
                                            <source media="(max-width: 767px)" srcset="<?php the_sub_field('image_mobile_slide_four'); ?>">
                                            <img src="<?php the_sub_field('image_desktop_slide_four') ?>" alt="neww">
                                        </picture>
                                    </div>
                                    <div class="details">
                                        <span class="caption-description"><?php the_sub_field('caption_title_silde_four') ?></span>
                                    </div>
                                </li>
                        <?php
                        endwhile; endif;
                    endwhile; endif;
                    ?>
                </ul>
                <div class="dots"></div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_five') ): ?>
        <?php while( have_rows('modular_landing_section_five') ): the_row();
            $chk_enable_mod_five = get_sub_field('show_module_five');
            if($chk_enable_mod_five === 'Yes'):
                ?>
                <div class="modular-subbanner-sec-section main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_five'); ?>">
                    <div class="subbanner-wrapper">

                        <?php 
                            $image_desktop = get_sub_field('image_desk_module_five');
                            $image_mobile = get_sub_field('image_mob_module_five');
                            $video_desktop = get_sub_field('video_desk_module_five');
                            $video_mobile = get_sub_field('video_mob_module_five');                        
                        ?>
                        <?php if ($video_desktop): ?>
                            <video autoplay loop muted class="tablet-up">
                                <source type="video/mp4" src="<?php echo $video_desktop; ?>" />
                            </video>
                        <?php elseif ($image_desktop): ?>
                            <img src="<?php echo $image_desktop; ?>" class="tablet-up">
                        <?php endif; ?>                            

                        <?php if ($video_mobile): ?>
                            <video autoplay loop muted class="tablet-down">
                                <source type="video/mp4" src="<?php echo $video_mobile; ?>" />
                            </video>
                        <?php elseif ($image_mobile): ?>
                            <img src="<?php echo $image_mobile; ?>" class="tablet-down">
                        <?php endif; ?>                            

                    </div>
                    <div class="subbanner-info" data-class="module_five_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_five'); ?>" data-line-height="<?php the_sub_field('line_height_module_five'); ?>">
                        <?php if(get_sub_field('subhead_module_five')!="") : ?>
                        <h3><?php the_sub_field('subhead_module_five') ?></h3>
                        <?php endif; ?>
                        <?php if(get_sub_field('lead_in_module_five')!="") : ?>
                        <p><strong><?php the_sub_field('lead_in_module_five') ?></strong></p>
                        <?php endif; ?>
                        <?php if(get_sub_field('copy_text_module_five')!="") : ?>
                        <span class="dynamic-font-modular"><?php the_sub_field('copy_text_module_five') ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_six') ): ?>
        <?php while( have_rows('modular_landing_section_six') ): the_row();
            $chk_enable_mod_six = get_sub_field('show_module_six');
            if($chk_enable_mod_six === 'Yes'):
            ?>
                <div class="video-module-wrapper main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_six'); ?>">
                    <div class="after-icons">
                        <div class="info" data-class="module_six_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_six'); ?>" data-line-height="<?php the_sub_field('line_height_module_six'); ?>">
                            <?php if(get_sub_field('lead_in_module_six')!="") : ?>
                            <div class="caption-title"><?php the_sub_field('lead_in_module_six') ?></div>
                            <?php endif; ?>
                            <?php if(get_sub_field('copy_text_module_six')!="") : ?>
                            <span class="caption-description dynamic-font-modular"><?php the_sub_field('copy_text_module_six') ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="video-module">
                        <div class="video-controls-wrapper">
                            <div class="video-controls">
                                <div class="title"><?php the_sub_field('video_title_module_six') ?></div>
                                <div class="description"><?php the_sub_field('video_description_module_six') ?></div>
                                <a href="#" class="video-icon" data-sources='
                                {
                                    "mobile": "<?php the_sub_field('video_url_mob_module_six') ?>",
                                    "desktop": "<?php the_sub_field('video_url__desk_module_six') ?>"
                                }
                                '></a>
                            </div>
                        </div>
                        <div class="image-wrapper">
                            <!--<img src="<?php /*the_sub_field('video_image_desk_module_six'); */?>"  alt="Image">-->
                            <picture>
                                <source media="(max-width: 767px)" srcset="<?php the_sub_field('video_image_mob_module_six'); ?>">
                                <img src="<?php the_sub_field('video_image_desk_module_six') ?>" alt="Image">
                            </picture>
                        </div>
                        <div class="torn-line mob"></div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_seven') ): ?>
        <?php while( have_rows('modular_landing_section_seven') ): the_row();
            $chk_enable_mod_seven = get_sub_field('show_module_seven');
            if($chk_enable_mod_seven === 'Yes'):
                ?>
                <div class="modular-subbanner-section main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_seven'); ?>">
                    <div class="subbanner-info" data-class="module_seven_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_seven'); ?>" data-line-height="<?php the_sub_field('line_height_module_seven'); ?>">
                        <?php if(get_sub_field('subhead_module_seven')!="") : ?>
                        <h3><?php the_sub_field('subhead_module_seven') ?></h3>
                        <?php endif; ?>
                        <?php if(get_sub_field('copy_text_module_seven')!="") : ?>
                        <span class="dynamic-font-modular"><?php the_sub_field('copy_text_module_seven') ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="subbanner-wrapper">

                        <?php 
                            $image_desktop = get_sub_field('image_desk_module_seven');
                            $image_mobile = get_sub_field('image_mob_module_seven');
                            $video_desktop = get_sub_field('video_desk_module_seven');
                            $video_mobile = get_sub_field('video_mob_module_seven');                        
                        ?>
                        <?php if ($video_desktop): ?>
                            <video autoplay loop muted class="tablet-up">
                                <source type="video/mp4" src="<?php echo $video_desktop; ?>" />
                            </video>
                        <?php elseif ($image_desktop): ?>
                            <img src="<?php echo $image_desktop; ?>" class="tablet-up">
                        <?php endif; ?>                            

                        <?php if ($video_mobile): ?>
                            <video autoplay loop muted class="tablet-down">
                                <source type="video/mp4" src="<?php echo $video_mobile; ?>" />
                            </video>
                        <?php elseif ($image_mobile): ?>
                            <img src="<?php echo $image_mobile; ?>" class="tablet-down">
                        <?php endif; ?>                            


                        <?php if(get_sub_field('captions_title_module_seven')!="") : ?>
                        <span class="caption-title"><?php the_sub_field('captions_title_module_seven') ?></span>
                        <?php endif; ?>
                        <?php if(get_sub_field('captions_description_module_seven')!="") : ?>
                        <span class="caption-description"><?php the_sub_field('captions_description_module_seven') ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_eight') ): ?>
        <?php while( have_rows('modular_landing_section_eight') ): the_row();
            $chk_enable_mod_eight = get_sub_field('show_module_eight');
            if($chk_enable_mod_eight === 'Yes'):
                ?>
                <div class="icons-wrapper main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_eight'); ?>">
                    <?php  $iconimages = get_sub_field('icons_module_eight');?>
                    <?php foreach( $iconimages as $iconimage ): ?>
                        <div class="icon">
                            <div class="image-wrapper">
                                <img src="<?php echo $iconimage['url']; ?>" alt="">
                            </div>
                            <div class="icons-content">
                                <?php if($iconimage['title']!="") : ?>
                                <span class="caption-title"><?php echo $iconimage['title']; ?></span>
                                <?php endif; ?>
                                <?php if($iconimage['caption']!="") : ?>
                                <span class="caption-description"><?php echo $iconimage['caption']; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_nine') ): ?>
        <?php while( have_rows('modular_landing_section_nine') ): the_row();
            $chk_enable_mod_nine = get_sub_field('show_module_nine');
            if($chk_enable_mod_nine === 'Yes'):
                ?>
                <div class="image-slider default main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_nine'); ?>"
                     data-slick-options='
                    {
                        "slidesToShow": 3,
                        "dotsSelector": ".dots",
                        "rows": 0,
                        "responsive": [{
                            "breakpoint": 1024,
                            "settings": {
                                "slidesToShow": 2
                            }
                        }, {
                            "breakpoint": 480,
                            "settings": {
                                "slidesToShow": 1,
                                "dots": true,
                                "arrows": false
                            }
                        }]
                    }
                '>
                    <div class="after-icons">
                        <div class="info" data-class="module_nine_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_nine'); ?>" data-line-height="<?php the_sub_field('line_height_module_nine'); ?>">
                            <?php if(get_sub_field('lead_in_module_nine')!="") : ?>
                            <div class="caption-title"><?php the_sub_field('lead_in_module_nine') ?></div>
                            <?php endif; ?>
                            <?php if(get_sub_field('copy_text_module_nine')!="") : ?>
                            <span class="caption-description dynamic-font-modular"><?php the_sub_field('copy_text_module_nine') ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <ul class="items">
                        <?php
                        if( have_rows('slider_module_nine') ): while ( have_rows('slider_module_nine') ) : the_row();
                            if( have_rows('image_section_nine') ): while ( have_rows('image_section_nine') ) : the_row();
                            ?>
                            <li class="item">
                                <div class="image-wrapper">
                                    <picture>
                                    <source media="(max-width: 767px)" srcset="<?php the_sub_field('image_mobile_slide_nine'); ?>">
                                    <img src="<?php the_sub_field('image_desktop_slide_nine') ?>" alt="">
                                    </picture>
                                </div>
                                <div class="details">
                                    <span class="caption-title"><?php the_sub_field('caption_title_silde_nine') ?></span>
                                </div>
                            </li>
                            <?php
                            endwhile; endif;
                        endwhile; endif;
                        ?>
                    </ul>
                    <div class="dots"></div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_ten') ): ?>
        <?php while( have_rows('modular_landing_section_ten') ): the_row();
            $chk_enable_mod_ten = get_sub_field('show_module_ten');
            if($chk_enable_mod_ten === 'Yes'):
                ?>
                <div class="action-interest main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_ten'); ?>">
                    <div class="modular-landing-wrapper">
                        <div class="modular-info" data-class="module_ten_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_ten'); ?>" data-line-height="<?php the_sub_field('line_height_module_ten'); ?>">
                            <a href="<?php the_sub_field('back_link_url_module_ten') ?>" class="modular-back"><?php the_sub_field('back_link_text_module_ten') ?></a>
                            <?php if(get_sub_field('title_module_ten')!="") : ?>
                            <?php the_sub_field('title_module_ten') ?>
                            <?php endif; ?>
                            <?php if(get_sub_field('description_module_ten')!="") : ?>
                            <span class="dynamic-font-modular"><?php the_sub_field('description_module_ten') ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('modular_landing_section_eleven') ): ?>
        <?php while( have_rows('modular_landing_section_eleven') ): the_row();
            $chk_enable_mod_eleven = get_sub_field('show_module_eleven');
            if($chk_enable_mod_eleven === 'Yes'):
                ?>
                <div class="module-11 main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_eleven'); ?>">
                    <div class="modular-subbanner-section">
                        <div class="subbanner-info" data-class="module_eleven_dynamic_<?php echo get_row_index(); ?>"  data-size="<?php the_sub_field('font_size_module_eleven'); ?>" data-line-height="<?php the_sub_field('line_height_module_eleven'); ?>">
                            <?php the_sub_field('subhead_module_eleven') ?>
                            <span class="dynamic-font-modular"><?php the_sub_field('copy_text_module_eleven') ?></span>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

   <?php if( have_rows('modular_landing_section_twelve') ): ?>
       <?php while( have_rows('modular_landing_section_twelve') ): the_row();
           $chk_enable_mod_twelve = get_sub_field('show_module_twelve');
           if($chk_enable_mod_twelve === 'Yes'):
               ?>
               <div class="module-12 main_module_modular" data-sequence="<?php the_sub_field('sequence_mod_twelve'); ?>">
                   <div class="module-twelve-container">
                        <?php 
                            $image_desktop = get_sub_field('image_desk_module_12');
                            $image_mobile = get_sub_field('image_mob_module_12');
                            $video_desktop = get_sub_field('video_desk_module_12');
                            $video_mobile = get_sub_field('video_mob_module_12');                        
                        ?>
                        <?php if ($video_desktop): ?>
                            <video autoplay loop muted class="tablet-up">
                                <source type="video/mp4" src="<?php echo $video_desktop; ?>" />
                            </video>
                        <?php elseif ($image_desktop): ?>
                            <img src="<?php echo $image_desktop; ?>" class="tablet-up">
                        <?php endif; ?>                            

                        <?php if ($video_mobile): ?>
                            <video autoplay loop muted class="tablet-down">
                                <source type="video/mp4" src="<?php echo $video_mobile; ?>" />
                            </video>
                        <?php elseif ($image_mobile): ?>
                            <img src="<?php echo $image_mobile; ?>" class="tablet-down">
                        <?php endif; ?>                            

                   </div>
               </div>
           <?php endif; ?>
       <?php endwhile; ?>
   <?php endif; ?>
  </div>
    <?php
    global $post;
    $args = array( 'post_type' => 'materlandingposts', 'posts_per_page' => 10, 'orderby' => 'date');
    $current_post = get_posts( $args );
//print_r($current_post);exit;
    $posts = array();
    $postsData = [];
    $customData = [];
     if ( $current_post ) {
         foreach ($current_post as $post) {
             setup_postdata($post);
             $posts[] += $post->ID;
             $postsData[$post->ID] = $post;

             $title = get_field('post_titledesktop');
             $externalLink = get_field('post_external_link');
             $mobileTitle = get_field('post_titlemobile');
             $shortDescription = get_field('short_description');
             $briefDescription = get_field('brief_description');
             $iconHoverColorDesktop = get_field('icon_and_title_hover_colourdesktop');

             $img = get_field('image');
             $customData[$post->ID]['post_titledesktop'] = $title;
             $customData[$post->ID]['post_external_link'] = $externalLink;
             $customData[$post->ID]['post_titlemobile'] = $mobileTitle;
             $customData[$post->ID]['short_description'] = $shortDescription;
             $customData[$post->ID]['brief_description'] = $briefDescription;
             $customData[$post->ID]['icon_and_title_hover_colourdesktop'] = $iconHoverColorDesktop;
             $customData[$post->ID]['image'] = $img;
         }
         wp_reset_postdata();
     }
//print_r($customData);exit;
    $get_the_ID = get_the_ID();
    $iterate_limit = 2;
    $modular_post = [];
    $current = array_search( $get_the_ID, $posts );
    if ($iterate_limit) {
        $modular_post = [];
        for ($k=1;$k<=$iterate_limit;$k++) {
            if(isset($posts[$current + $k])) {
                $nextID = $posts[$current + $k];
                $modular_post[] = $postsData[$nextID];
            }
        }
    }
    if (isset($modular_post)) {
        $modular_post_cnt = count($modular_post);
        if ($modular_post_cnt < $iterate_limit) {

            if ($modular_post_cnt==1) {
                $cnt = 1;
                foreach ($postsData as $postkey => $postval) {
                    $modular_post[] = $postval;
                    $cnt++;
                    break;
                }
            } else if($modular_post_cnt==0) {
                $cnt = 1;
                foreach ($postsData as $postkey => $postval) {
                    $modular_post[] = $postval;
                    if ($cnt==$iterate_limit) {
                        break;
                    }
                    $cnt++;
                }
            }
        }
    }
    //print_r($modular_post);
    ?>
    <?php
    $enabledBottomContent = get_field('modular_bottom_section_enabled');
    if($enabledBottomContent['value'] === 'yes'):
        ?>
    <div class="modular-bottom-sections-wrapper">
        <div class="modular-bottom-sections">
            <div class="back-to-all-topics">
                <div class="text">BACK TO ALL TOPICS</div>
                <a href="/our-mission"> <div class="icon"></div></a>
            </div>

            <?php
            $fontSize = get_field('font_sizedesktop_post');
            $lineHeight = get_field('line_heightdesktop_post');
            $fontSizeMob = get_field('post_title_font_sizemobile');
            $lineHeightMob = get_field('post_title_line_heightmobile');
            if (!empty($modular_post)) {
                foreach ($modular_post as $modkey => $modval){
                    //print_r($modular_post);exit;
                    $getPostID = $modval->ID;

                    $key = $modkey;
                    $title = $customData[$getPostID]['post_titledesktop'];
                    $externalLink = $customData[$getPostID]['post_external_link'];
                    $mobileTitle = $customData[$getPostID]['post_titlemobile'];
                    $shortDescription = $customData[$getPostID]['short_description'];
                    $briefDescription = $customData[$getPostID]['brief_description'];
                    $iconHoverColorDesktop = $customData[$getPostID]['icon_and_title_hover_colourdesktop'];
                    $img = $customData[$getPostID]['image'];
                    if($title):
                    ?>
                    <div class="bottom-section top">
                        <a href="<?php if($externalLink): echo $externalLink;  else: the_permalink($getPostID); endif;  ?>">
                            <div class="content-master-list">
                                <div class="content-block" data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                     data-class-desktop="post_color_desk_main<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorDesktop; ?>"
                                     data-class-mobile="post_color_mob_main<?php echo $key; ?>" data-size="<?php echo $fontSize; ?>" data-line-height="<?php echo $lineHeight; ?>"
                                     data-class="post-title-font-style<?php echo $key; ?>" data-mob-size="<?php echo $fontSizeMob; ?>" data-mob-line-height="<?php echo $lineHeightMob; ?>"
                                     data-mob-class="post-mob-title-font-style<?php echo $key; ?>">
                                    <span class="post_title_desktop post_title dynamic-font"><?php echo $title; ?></span>
                                    <span class="post_title_mobile post_title dynamic-font-mob"><?php echo $mobileTitle; ?></span>
                                    <div class="cover">
                                        <span class="descript desktop"><?php echo $shortDescription; ?></span>
                                        <span class="descript mobile"><?php echo $briefDescription; ?></span>
                                        <span class="more-icon-desktop"  data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                              data-class-desktop="post_color_desk<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorDesktop; ?>"
                                              data-class-mobile="post_color_mob<?php echo $key; ?>"> <img class="alignnone size-medium wp-image-3570" role="img" src="https://www.happyfamilyorganics.com/wp-content/uploads/2018/05/trans-arrow.svg" alt=""/></span>
                                    </div>
                                    <span class="more-icon-mobile"  data-color-desktop="<?php echo $iconHoverColorDesktop; ?>"
                                          data-class-desktop="post_color_desk<?php echo $key; ?>" data-color-mobile="<?php echo $iconHoverColorMobile; ?>"
                                          data-class-mobile="post_color_mob<?php echo $key; ?>"> <img class="alignnone size-medium wp-image-3570" role="img" src="https://www.happyfamilyorganics.com/wp-content/uploads/2018/05/trans-arrow.svg" alt=""/></span>
                                </div>
                                <?php if($img){ ?>
                                    <div class="image-block">
                                        <img src="<?php echo $img; ?>" alt="" class="alignnone size-full wp-image-3588" />
                                    </div>
                                <?php } else{ ?>
                                    <div class="brief-desc-block">
                                        <p><?php echo $briefDescription; ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </a>
                    </div>
                    <?php endif;
                }
            }
            ?>
        </div>
    </div>
    <?php
    endif;
    ?>
<?php /*END LOOP */ endwhile; endif; ?>
    <?php // hfo-marketing-script-start ?>
    <script src="https://cdn.jsdelivr.net/npm/mediaCheck@0.4.6/js/mediaCheck.js"></script>
    <?php // hfo-marketing-script-end ?>
    <script type="text/javascript">
        $ = jQuery;
        // Sort by functionality for all modules
        function sortModule(element, wrapper, dataAttr){
            $(element).sort(sort_li).appendTo(wrapper);
            function sort_li(a, b) {
               return ($(b).data(dataAttr)) < ($(a).data(dataAttr)) ? 1 : -1;
            }
        }
        sortModule(".main_module_modular", ".modular-wrapper", "sequence");
        // End here

        // Dynamic font size implementation
        var cssStyles = "";
        function addDynamicClassModular(title, mode, lHeight, clsElement) {
            $(title).each(function() {
                var size = $(this).parent().data(mode),
                    lineHeight = $(this).parent().data(lHeight),
                    clsName = $(this).parent().data(clsElement);
                $(this).addClass(clsName);
                cssStyles += "." + clsName + "{ font-size:" + size + "; line-height:" +lineHeight+"; color:#6e6c69}";
            });
            $("<style type='text/css'>" + cssStyles + "</style>").appendTo("head");
            cssStyles = "";
        }
        addDynamicClassModular('.dynamic-font-modular', 'size', 'line-height', 'class');

        function changeColorIcon(elem, mode){
            $(elem).each(function () {
                // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
                var $img = jQuery(this);
                // Get all the attributes.
                var attributes = $img.prop("attributes");
                // Dynamic Color
                var dynamicColor = $img.parent().data(mode);
                // Get the image's URL.
                var imgURL = $img.attr("src");
                // Fire an AJAX GET request to the URL.
                $.get(imgURL, function (data) {
                    // The data you get includes the document type definition, which we don't need.
                    // We are only interested in the <svg> tag inside that.
                    var $svg = $(data).find('svg');
                    // Remove any invalid XML tags as per http://validator.w3.org
                    $svg = $svg.removeAttr('xmlns:a');
                    // Loop through original image's attributes and apply on SVG
                    $.each(attributes, function() {
                        $svg.attr(this.name, this.value);
                    });
                    $svg.css('fill', dynamicColor);
                    // Replace image with new SVG
                    $img.replaceWith($svg);
                });
            });
        }
        /*icon hover*/
        mediaCheck({
            media: '(min-width: 768px)',
            entry: $.proxy(function () {
                changeColorIcon(".more-icon-desktop img", "color-desktop");
            }),
            exit: $.proxy(function () {
                changeColorIcon(".more-icon-desktop img", "color-mobile");
            })
        });
        // End here

        // Dynamic class for above btn wrapper
        if($('.modular-button-wrapper').length > 0){
            $('.modular-button-wrapper').each(function(){
                $(this).prev().first().addClass('mod-btn-prev');
            })
        }
        // End here
    </script>
<?php
}else{
    // we will show password form here
    echo get_the_password_form();
}
?>
<?php get_footer(); ?>


