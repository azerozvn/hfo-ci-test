// Webpack uses this to work with directories
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { StatsWriterPlugin } = require("webpack-stats-plugin");
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

// This is the main configuration object.
// Here you write different options and tell Webpack what to do
module.exports = {
	
	entry: {
		'shop-js': [
			'./src/js/index.js', 
		],
		'shop-css': [
			'./src/style/index.styl',
		],
		'search-js': [
			'./src/js/search/index.js', 
		],
		'search-css': [
			'./src/style/search/index.styl',
		],
		'madwell-js': [
			'./src-madwell/js-one-piece/header.js', 
			'./src-madwell/js-one-piece/app.js', 
		],
		'madwell-css': [
			'./src-madwell/lib/slick/slick.less',
			'./src-madwell/less/theme.less',
		]
	},
	
	// Path and filename of your result bundle.
	// Webpack will bundle all JavaScript into this file
	output: {
		path: path.resolve(__dirname, 'dist-webpack'),
		publicPath: '/wp-content/themes/happyfamily/dist-webpack/',
		filename: '[name].[contenthash].js'
	},
	
	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: '[name].[contenthash].css'
		}),
		new WebpackManifestPlugin(),
		// Generate file with random hash.  We'll use this in functions.php to cache-bust our CSS and JS.
		new StatsWriterPlugin({
			filename: "build-version.txt",
			transform(data, opts) {
				return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
			}
		}),
	],

	module: {
		rules: [

			// Javascript
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },

			// Stylus
			{
				test: /\.styl$/,
				use: [
					// Loaders run last to first
					{loader: MiniCssExtractPlugin.loader},
					{loader: "css-loader"},
					{loader: "stylus-loader"}
				]
			},

			// Less (Madwell CSS)
			{
				test: /\.less$/,
				use: [
					// Loaders run last to first
					{loader: MiniCssExtractPlugin.loader},
					{loader: "css-loader"},
					{
						loader: "less-loader",
					}
				]
			},

			// Fonts
			{
				// Now we apply rule for fonts
				test: /\.(eot|eot?#iefix|ttf|woff|woff2)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: '[name].[contenthash].[ext]',
							outputPath: 'fonts'
						}
					}
				]
			},

			// Images
			{
				// Now we apply rule for images
				test: /\.(svg|gif|png|jpg|jpeg)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: '[name].[contenthash].[ext]',
							outputPath: 'images'
						}
					}
				]
			},
		]
	},
	externals: {
		// require("jquery") is external and available
		//  on the global var jQuery
		"jquery": "jQuery"
	},	
	optimization: {
		minimize: true,
		minimizer: [
		  // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
		  // `...`,
		  new CssMinimizerPlugin(),
		],
	},

};