<?php
/*
Template Name: Learning Center Category
*/

get_header();

?>

<?php /*START LOOP */ if ( get_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

<?php /*END LOOP */ endwhile; endif; ?>


<?php if ( is_page( 'recipes-meal-plans' ) ) : ?>
  <?php include(locate_template('snippet-book.php')); ?>
<?php endif; ?>

<?php get_footer(); ?>
