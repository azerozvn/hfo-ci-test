<?php
/*
Template Name: Search
*/

get_header();

?>

<div id="search-results" class="room-for-header">

  <div class="search-head">
    <div class="stats-widget"></div>
  </div>

  <div class="result-wrap">
    <?php // Menu Widget: Products, Articles, Recipes & Meal Plans ?>
    <div class="menu-select">
      <button class="tab" data-label="products" data-refinement="product" aria-label="Show only product search results">Products (<span class="count">0</span>)</button>
      <button class="tab" data-label="articles" data-refinement="article" aria-label="Show only article search results">Articles (<span class="count">0</span>)</button>
      <button class="tab" data-label="recipes" data-refinement="recipe" aria-label="Show only recipe and meal plan search results">Recipes & Meal Plans (<span class="count">0</span>)</button>
    </div>
    <div class="results no-results product">
      <p>Looks like we don’t have any products matching your search. Please try and search a more general term and check your spelling or
<a href="/shop">see all products</a>.</p>
    </div>
    <div class="results no-results article">
      <p>We couldn’t find any article topics matching your search! Please try and search a more general term and check your spelling or
<a href="/learning-center/">see all articles</a>.</p>
    </div>
    <div class="results no-results recipe">
      <p>Looks like you are missing a few ingredients! Please refine your search and check your spelling to help us find what you are looking for or
<a href="/learning-center/recipes-meal-plans/">see all recipes & meal plans</a>.</p>
    </div>
    <div class="results no-results all">
      <p>We couldn't find anything matching your search! Please try and search a more general term and check your spelling. Or you may: 
        <a href="/shop">see all products</a>,
        <a href="/learning-center/">see all articles</a>, or
        <a href="/learning-center/recipes-meal-plans/">see all recipes & meal plans</a>.
      </p>
    </div>
    <?php // Infinite Hits Widget: Search Results ?>
    <div class="results infinite-hits-widget"></div>
  </div>
</div>

<?php get_footer(); ?>
