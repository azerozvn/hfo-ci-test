<?php

/*
Element Description: Madwell VC Partner Grid
*/
// Element Class
class madwell_productCarousel extends WPBakeryShortCode {

    const PRODUCT_CAROUSEL = 'productcarousel';
    const PRODUCT_CAROUSEL_GROUP = 'product_carousel_settings_group';
    const PRODUCT_ATTRIBUTE_PREFIX = 'product_carousel_';
    const PRODUCT_LINE = 'product_line';
    const PRODUCT_GROUP = 'product_group';

    /**
     * A list of previously hardcoded product attribute names
     *
     * @var array
     */
    private $defaultProductAttributes = array(
        'product_line' => array(
            'Infant Formula',
            'Clearly Crafted Pouches',
            'Simple Combos Pouches',
            'Gentle Teethers',
            'Hearty Meals Pouches',
            'Superfood Puffs',
            'Organic Yogis Snack',
            'Coconut Creamies Snack',
            'Rice Cakes Snack',
            'Superfood Munchies',
            'Organic Toddler Milk',
            'Love My Veggies Pouches',
            'Super Smart Snack',
            'Super Foods Pouches',
            'Super Foods Snack',
            'Super Morning Pouches',
            'Super Smart Pouches',
            'Superfood Puffs Snack',
            'Superfood Munchies Snack',
            'Fiber & Protein Pouches',
            'Happy Squeeze Pouches',
            'Fruit & Oat Bars',
            'Breastfeeding Support Bars',
            'Probiotic Cereal',
            'Clearly Crafted Jars',
            'Clearly Crafted Cereal',
            'Fiber & Protein Bars',
            'Love My Veggies Snack',
            'Whole Milk Yogurt',
            'Subscribe',
            'Fiber & Protein Bowls',
            'Simple Combos + Hearty Meals',
            'Love My Veggies Bowls',
            'Super Morning Bowls',
            'Toddler Bowls',
            'Toddler Pasta Bowl'
        ),
        'product_group' => array(
            'Formula',
            'Pouches',
            'Jars',
            'Cereal',
            'Bowls',
            'Bars',
            'Yogurt',
            'Snacks',
            'Breastfeeding',
            'Happy Bundles',
            'Bananars',
            'Mealtime'
        ),
    );
    
    private $productAttributeLabels = array(
        'product_line' => 'Product Line',
        'product_group' => 'Product Category'
    );

    /**
     * @var array
     */
    protected $options = array();

    // Element Init
    public function __construct() {
        add_action( 'init', array( $this, 'madwell_productcarousel_mapping' ) );
        add_shortcode( 'madwell_productcarousel', array( $this, 'madwell_productcarousel_html' ) );

        if(is_admin()) {
            add_action( 'admin_menu', array($this,'admin_menu') );
            add_action( 'admin_init', array($this,'pcarousel_register_settings') );
        }
    }

    /**
     * Add options page
     */
    public function admin_menu()
    {
        add_options_page(
            'Product Carousel Settings',
            'Product Carousel',
            'manage_options',
            self::PRODUCT_CAROUSEL,
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeOptionId($attribute)
    {
        return self::PRODUCT_ATTRIBUTE_PREFIX . $attribute;
    }

    /**
     * @param $attribute
     * @return mixed|void
     */
    public function getConfigAttributeOptions($attribute)
    {
        $attributeId = $this->getAttributeOptionId($attribute);
        
        return get_option($attributeId);
    }
    
    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        foreach ($this->defaultProductAttributes as $attribute=>$values) {
            $attributeOptionId = $this->getAttributeOptionId($attribute);
            $this->options[$attributeOptionId] = get_option( $attributeOptionId );
        }

        ?>
        <div class="wrap">
            <h1>Product Carousel Settings</h1>
            <form method="post" action="options.php">
                <?php
                settings_fields( self::PRODUCT_CAROUSEL_GROUP );
                do_settings_sections( self::PRODUCT_CAROUSEL );
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }
    /**
     * Register and add settings
     */
    public function pcarousel_register_settings()
    {
        add_settings_section(
            self::PRODUCT_CAROUSEL,
            'Product Carousel Settings',
            array( $this, 'print_section_info' ),
            self::PRODUCT_CAROUSEL
        );

        foreach ($this->defaultProductAttributes as $attribute=>$values) {
            $attributeOptionId = $this->getAttributeOptionId($attribute);
            register_setting(
                self::PRODUCT_CAROUSEL_GROUP,
                $attributeOptionId,
                array( $this, 'sanitize_product_attribute' )
            );
            add_settings_field(
                $attributeOptionId,
                $this->productAttributeLabels[$attribute],
                array( $this, 'product_attribute_options_callback' ),
                self::PRODUCT_CAROUSEL,
                self::PRODUCT_CAROUSEL,
                $attribute
            );
        }
    }
    /**
     * Sanitize each setting field
     */
    public function sanitize_product_attribute( $input )
    {
        $new_input = array();
        foreach ($input as $attribute) {
            if ($attribute) {
                $new_input[] = sanitize_text_field( $attribute );
            }
        }
        return $new_input;
    }
    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Get product line names
     */
    public function get_product_attribute_names($attribute = '')
    {
        $configAttributeNames = $this->getConfigAttributeOptions($attribute);
        if ($configAttributeNames) {
            return $configAttributeNames;
        }
        if (array_key_exists($attribute, $this->defaultProductAttributes)) {
            return $this->defaultProductAttributes[$attribute];
        }

        return array();
    }
    /**
     * Get the settings option array and print the values
     */
    public function product_attribute_options_callback($attribute = '')
    {
        $values = $this->get_product_attribute_names($attribute);

        $attributeId = $this->getAttributeOptionId($attribute);
        
        ?>
        <div id="<?php echo $attributeId ?>-list">
            <ol>
                <?php foreach ($values as $value) : ?>
                    <li>
                        <input type="text" name="<?php echo $attributeId ?>[]" value="<?php echo $value ?>" />
                        <a href="#" class="button button-secondary remove-line">Remove</a>
                    </li>
                <?php endforeach; ?>
                <li id="<?php echo $attributeId ?>-empty" style="display: none">
                    <input type="text" name="<?php echo $attributeId ?>[]" />
                    <a href="#" class="button button-secondary remove-line">Remove</a>
                </li>
            </ol>
            <a href="#" class="button button-secondary add-line">Add New Line</a>
            <script>
                jQuery(".remove-line").click(function(e) {
                    e.preventDefault();
                    jQuery(this).parent().remove();
                });
                jQuery(".add-line").click(function(e) {
                    e.preventDefault();
                    var newLine = jQuery("#<?php echo $attributeId ?>-empty").clone().show();
                    jQuery(newLine).find(".remove-line").click(function() {
                        jQuery(this).parent().remove();
                    });
                    jQuery("#<?php echo $attributeId ?>-list ol").append(newLine);
                });
            </script>
        </div>
        <?php
    }

    /**
     * Format preconfigured product line names to be used in the carousel widget
     */
    public function get_product_attribute_options($attribute)
    {
        $attributeNames = $this->get_product_attribute_names($attribute);

        $options = array();
        foreach ($attributeNames as $attributeName) {
            $options[__( $attributeName . '<br>', 'js_composer' )] = $attributeName;
        }

        return $options;
    }

    // Element Mapping
    public function madwell_productcarousel_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('Product Carousel', 'madwell-vc-plugin'),
                'base' => 'madwell_productcarousel',
                'description' => __('Product carousel categorized by line or life stage.', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type' => 'checkbox',
                        'heading' => __( $this->productAttributeLabels[self::PRODUCT_LINE], 'js_composer' ),
                        'param_name' => self::PRODUCT_LINE,
                        'value' => $this->get_product_attribute_options(self::PRODUCT_LINE),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( $this->productAttributeLabels[self::PRODUCT_GROUP], 'js_composer' ),
                        'param_name' => self::PRODUCT_GROUP,
                        'value' => $this->get_product_attribute_options(self::PRODUCT_GROUP),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Bundles Product', 'js_composer' ),
                        'param_name' => 'bundles',
                        'value' => array(
                            __( 'Include Bundles product in carousel', 'js_composer' ) => true
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Category',  "madwell-elements" ),
                        'param_name' => 'category',
                        'value' => array(
                            __( 'All',  "js_composer"  ) => '',
                            __( 'Baby',  "js_composer"  ) => 'Baby',
                            __( 'Tot',  "js_composer"  ) => 'Tot',
                            __( 'Kid',  "js_composer"  ) => 'Kid',
                            __( 'Mama',  "js_composer"  ) => 'Mama',
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Lifestage',  "madwell-elements" ),
                        'param_name' => 'lifestage',
                        'value' => array(
                            __( 'All',  "js_composer"  ) => '',
                            __( 'Advancing tastes',  "js_composer"  ) => 'Advancing+tastes',
                            __( 'Advancing textures',  "js_composer"  ) => 'Advancing+textures',
                            __( 'Fiber & protein',  "js_composer"  ) => 'Fiber+%26+protein',
                            __( 'Infant feeding',  "js_composer"  ) => 'Infant+feeding',
                            __( 'Love my veggies',  "js_composer"  ) => 'Love+my+veggies',
                            __( 'Self-feeding',  "js_composer"  ) => 'Self-feeding',
                            __( 'Starting solids',  "js_composer"  ) => 'Starting+solids',
                            __( 'Superfoods',  "js_composer"  ) => 'Superfoods',
                            __( 'Super smart',  "js_composer"  ) => 'Super+smart',
                            __( 'Whole milk yogurt',  "js_composer"  ) => 'Whole+milk+yogurt',
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textarea',
                        'holder' => 'div',
                        'heading'     => __( 'Filter Description', 'madwell-elements' ),
                        'param_name'  => 'filter_description',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell'
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Shop Now URL', 'madwell-elements' ),
                        'param_name'  => 'filter_url',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell'
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Age Range', 'madwell-elements' ),
                        'param_name'  => 'age_range',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell'
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell'
                    )
                )
            )
        );
    }


    // Element HTML
    public function madwell_productcarousel_html( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'bundles'   => '',
            'category'      => '',
            'lifestage'     => '',
            'age_range'     => '',
            'filter_description'     => '',
            'filter_url'     => '',
            'custom_class'  => ''
        ) );
        $attributeData = array();
        foreach ($this->defaultProductAttributes as $attribute=>$value) {
            if (array_key_exists($attribute, $atts)) {
                // Ensuring double escaping for already escaped and non-escaped values
                $attributeOptions = html_entity_decode($atts[$attribute]);
                $attributeOptions = htmlentities(htmlentities($attributeOptions));
                $attributeData[$attribute] = explode(',', $attributeOptions);
            }
        }

        // Start output
        $output = '';

        // Render product carousel section
        $output .= '<section data-category="' . esc_html( $data['category'] ) . 
            '" data-lifestage="' . esc_html( $data['lifestage'] ) . 
            '" data-attributes=\'' . json_encode($attributeData) . 
            '\' data-bundles="' . htmlentities( esc_html( $data['bundles'] ) ) . 
            '" data-agerange="' . htmlentities( esc_html( $data['age_range'] ) ) . 
            '" class="stages-products-carousel-container component-product-carousel ' . 
            esc_html ( $data['custom_class'] ) . ' carousel-products-small"  style="display: none;">';

        if ( $data['filter_description'] != '' ) {
            $output .= '<p class="text-center stages-filter__description text-darkgray mobile-hide">' . 
                wp_kses( $data['filter_description'], $allowedtags ) . '</p>';
        }

        if ( $data['filter_url'] != '' ) {
            $output .= '<a class="stages-filter__item--link mobile-hide button" href="' .  
                esc_url( $data['filter_url'] ) . '">Shop now</a>';
        }

        // Close product carousel section
        $output .= '</section>';

        return $output;

    }

} // End Element Class

// Element Class Init
new madwell_productCarousel();
