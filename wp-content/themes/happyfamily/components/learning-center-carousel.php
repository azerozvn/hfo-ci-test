<?php

/*
Element Description: Madwell VC get all posts
*/

// Element Class
class madwell_learningcenter extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'madwell_learningcenter_mapping' ) );
        add_shortcode( 'madwell_learningcenter', array( $this, 'madwell_learningcenter_html' ) );
    }

    // Element Mapping
    public function madwell_learningcenter_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // get all post titles for Featured Post selector
        $titlesArgs = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => '-1',
            'orderby' => 'title',
            'order' => 'ASC'
        );
        $titlesLoop = new WP_Query( $titlesArgs );
        $titles = array();
        $titles['No featured post'] = '0';
        while ( $titlesLoop->have_posts() ) :
            $titlesLoop->the_post();
            $id = get_the_ID();
            $titles[esc_html(get_the_title())] = $id;
        endwhile;
        wp_reset_query();

        $categoryOptions = array();
        $categories = get_categories(array('post_type' => 'learning-center'));
        foreach($categories as $category) {
            $categoryOptions[$category->name] = $category->term_id;
        }

        $topicOptions = array();
        $topics = get_terms([ 'taxonomy' => 'topics', 'hide_empty' => false ]);
        foreach($topics as $topic) {
            $topicOptions[$topic->name] = $topic->term_id;
        }

        $tagOptions = array();
        $tagOptions['All'] = '0';
        $tags = get_terms([ 'taxonomy' => 'post_tag', 'hide_empty' => false ]);
        foreach($tags as $tag) {
            $tagOptions[$tag->name] = $tag->term_id;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('Learning Center Posts', 'madwell-vc-plugin'),
                'base' => 'madwell_learningcenter',
                'description' => __('Grid, Carousel or Two Columns', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __( 'How should the posts be displayed?', 'madwell-elements' ),
                        'param_name'  => 'display_type',
                        'value' => array(
                            __( 'Select',  'madwell-elements'  ) => 'select',
                            __( 'Carousel',  'madwell-elements'  ) => 'learning-center-carousel',
                            __( 'Grid',  'madwell-elements'  ) => 'learning-center-grid',
                            __( 'Two Column',  'madwell-elements'  ) => 'learning-center-columns'
                        ),
                        'description' => 'Typically, Recipes & Meal Plans are displayed in a grid, other categories are displayed in a carousel and posts specific to a topic are displayed in two columns.',
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Cards in Grid or Carousel', 'madwell-elements' ),
                        'param_name'  => 'slide_count',
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                        'description' => 'Entering -1 will display all posts.'
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Cards to Show (desktop)', 'madwell-elements' ),
                        'param_name'  => 's2s_desktop',
                        'group' => 'Madwell',
                        'admin_label' => true,
                        'holder' => 'div',
                        'description' => 'Carousel only.'
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Cards to Show (tablet)', 'madwell-elements' ),
                        'param_name'  => 's2s_tablet',
                        'group' => 'Madwell',
                        'admin_label' => true,
                        'holder' => 'div',
                        'description' => 'Carousel only.'
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Cards to Show (mobile)', 'madwell-elements' ),
                        'param_name'  => 's2s_mobile',
                        'group' => 'Madwell',
                        'admin_label' => true,
                        'holder' => 'div',
                        'description' => 'Carousel only.'
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Color',  'madwell-elements' ),
                        'param_name' => 'lccarousel_color',
                        'value' => array(
                            __( 'Select',  'madwell-elements'  ) => 'select',
                            __( 'blue',  'madwell-elements'  ) => 'blue',
                            __( 'purple',  'madwell-elements'  ) => 'purple',
                            __( 'green',  'madwell-elements'  ) => 'green',
                            __( 'yellow',  'madwell-elements'  ) => 'yellow',
                            __( 'orange',  'madwell-elements'  ) => 'orange',
                        ),
                        'description' => 'Color palette of cards and arrows (if carousel).',
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => __( "Feature a post?", 'madwell-elements' ),
                        "param_name" => "lcfeatured",
                        "value" => $titles,
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Enter Post IDs to show in Carousel', 'madwell-elements' ),
                        'param_name'  => 'lcpostsids',
                        'group' => 'Madwell',
                        'admin_label' => true,
                        'holder' => 'div',
                        'description' => 'Carousel only.'
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Filter by Category',  'madwell-elements' ),
                        'param_name' => 'lccategories',
                        'value' => $categoryOptions,
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Filter by Topic',  'madwell-elements' ),
                        'param_name' => 'lctopics',
                        'value' => $topicOptions,
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Filter by Tag',  'madwell-elements' ),
                        'param_name' => 'lctags',
                        'value' => $tagOptions,
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => true,
                        'holder' => 'div',
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }


    // Element HTML
    public function madwell_learningcenter_html( $atts, $content = null ) {
        $output = '';

        $data = wp_parse_args( $atts, array(
            'display_type'             => '',
            'slide_count'              => '',
            's2s_desktop'              => '',
            's2s_tablet'               => '',
            's2s_mobile'               => '',
            'lcpostsids'               => '',
            'lccarousel_color'         => '',
            'lcfeatured'               => '',
            'lccategories'             => '',
            'lctopics'                 => '',
            'lctags'                   => '',
            'custom_class'             => '',
        ) );

        $displayType = esc_html( $data['display_type']);
        $themeColor = esc_html( $data['lccarousel_color'] );
        $custom_class = esc_html( $data['custom_class']);
        $featured_first = esc_html($data['lcfeatured']);
        $categories = esc_html($data['lccategories']);
        $tags = esc_html( $data['lctags']);
        $topics = esc_html( $data['lctopics']);
        $count = esc_html( $data['slide_count']);

        if ( $tags != 0 ) {
            $tags = explode(',', $tags);
        }

        $slidesshown_desktop = esc_html( $data['s2s_desktop']);
        $slidesshown_tablet = esc_html( $data['s2s_tablet']);
        $slidesshown_mobile = esc_html( $data['s2s_mobile']);
        $datapostsid = esc_html( $data['lcpostsids']);
        $slideshow_posts_ids = explode(",",esc_html( $data['lcpostsids']));

        for($i=0;$i<count($slideshow_posts_ids);$i++)
        {
            $args1 = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => '-1',
                'orderby' => 'rand',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'learning_center_id',
                        'value' => $slideshow_posts_ids[$i],
                        'compare' => '=',
                    )
                )
            );

            $loop1 = new WP_Query( $args1 );

            if ( $loop1->have_posts() ) : while ( $loop1->have_posts() ) : $loop1->the_post();
                $slideshow_posts[$i] = get_the_id();
            endwhile;
            endif;

        }
        if ( $slidesshown_desktop != 0 && $slidesshown_tablet != 0 && $slidesshown_mobile != 0 ) {
            $slidesshown_values = '" data-slides-desktop="' . $slidesshown_desktop . '" data-slides-tablet="' . $slidesshown_tablet . '" data-slides-mobile="' . $slidesshown_mobile . '"';
        } else {
            $slidesshown_values = '';
        }

        $output .= '<div class="' . $displayType . ' ' . $themeColor . ' ' . $custom_class . $slidesshown_values . '">';

        // If a featured post is indicated, show it first in its own container
        if ( $featured_first != '' ) {
            $featured_title = esc_html(get_the_title($featured_first));

            $featured_alt = esc_html( get_post_meta( get_post_thumbnail_id($featured_first), '_wp_attachment_image_alt', true) );
            $featured_url = get_permalink($featured_first);
            // 134 characters fits in all resolutions
            $featured_description = get_excerpt(get_post_field('post_content', $featured_first), 220);
            $featured_image = esc_url(get_the_post_thumbnail_url($featured_first, 'medium_large'));
            $featured_breadcrumb = '';
            $featured_topics = wp_get_post_terms($featured_first, 'topics', array('fields' => 'all'));
            if ( $featured_topics != '' ) {
                foreach($featured_topics as $topic) {
                    $featured_breadcrumb = esc_html($topic->name);
                }
            }

            $output .= <<<CPT

                <div class="learning-center-card learning-center-card--featured {$themeColor}">
                    <a class="learning-center-card__link" href="{$featured_url}" alt="{$featured_title}">
                        <img class="learning-center-card__image" src="{$featured_image}" alt="{$featured_alt}" />
                    </a>
                    <div class="learning-center-card__details">
                        <p class="learning-center-card__breadcrumb"><strong>{$featured_breadcrumb}</strong></p>
                        <h3 class="learning-center-card__title"><a href="{$featured_url}" alt="{$featured_title}">{$featured_title}</a></h3>
                        <p class="learning-center-card__excerpt">{$featured_description}</p>
                    </div>
                </div>
                <div class="clearfix"></div>
CPT;

        } // close .learning-center-card--featured

        $output .= '<div class="' . $displayType . '__container">';

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => '-1',
            'orderby' => 'rand',
            'order' => 'ASC'
        );


        if ( $datapostsid != ''){
            $args['post__in'] = $slideshow_posts;
        }
        else
        {
            if ($featured_first != '') {
                $args['post__not_in'] = array($featured_first);
            }
            if ($count != '') {
                $args['posts_per_page'] = $count;
            }
            if ($categories != '') {
                $args['cat'] = $categories;
            }
            if ($tags != 0) {
                $args['tag__in'] = $tags;
            }
            if ($topics != '') {
                $args['tax_query'] = array(
                    'taxonomy' => 'topics',
                    'field' => 'term_id',
                    'terms' => $topics,
                );
            }
        }
        $loop = new WP_Query( $args );

        if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
            $title = esc_html(get_the_title());
            $id = get_the_id();
            $url = get_permalink($id);
            $alt = esc_html( get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true) );
            $descriptionCopy = get_excerpt( str_replace( 'What to Know', '', get_field('learning_center_summary') ), 40 );
            $image = esc_url( get_the_post_thumbnail_url(get_the_ID(), 'medium') );
            $topics = wp_get_post_terms($id, 'topics', array('fields' => 'all'));

            if ( in_category('recipes-meal-plans') && $displayType == 'learning-center-grid' ) {
                $image = esc_url( get_the_post_thumbnail_url(get_the_ID(), 'medium_large') );
            }

            $breadcrumb = '';

            if ( in_category('recipes-meal-plans') ) {
                $breadcrumb = 'Recipes &amp; Meal Plans';
                $description = '';

                if ( $topics != '' ) {
                    foreach($topics as $topic) {
                        $breadcrumb .= ' | ' . esc_html($topic->name);
                    }
                }
            } else if ( $topics != '' ) {
                foreach($topics as $topic) {
                    $breadcrumb = esc_html($topic->name);
                }

                $description = '<p class="learning-center-card__excerpt">' . $descriptionCopy . '</p>';
            }

            $output .= <<<CPT

                <div class="learning-center-card {$themeColor}">
                    <a class="learning-center-card__link" href="{$url}" alt="{$title}">
                        <img class="learning-center-card__image" src="{$image}" alt="{$alt}" />
                    </a>
                    <div class="learning-center-card__details">
                        <p class="learning-center-card__breadcrumb"><strong>{$breadcrumb}</strong></p>
                        <h4 class="learning-center-card__title"><a href="{$url}" alt="{$title}">{$title}</a></h4>
                        {$description}
                    </div>
                </div>
CPT;

        endwhile; endif;
        wp_reset_query();

        $output .= '</div>'; // close learning-center-[displayType]__container
        $output .= '</div>'; // close learning-center-[displayType]

        return $output;
    }

} // End Element Class

// Element Class Init
new madwell_learningcenter();