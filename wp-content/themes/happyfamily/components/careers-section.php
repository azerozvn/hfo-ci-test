<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_careersSection extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_careerssection_mapping' ) );
		add_shortcode( 'madwell_careerssection', array( $this, 'madwell_careerssection_html' ) );
	}

	// Element Mapping
	public function madwell_careerssection_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('Careers Section', 'madwell-vc-plugin'),
				'base' => 'madwell_careerssection',
				'description' => __('List of open positions', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => false,
						'weight' => 0,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_careerssection_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'custom_class' => ''
		) );

		$custom_class = esc_html( $data['custom_class'] );

		$post_type = 'career-listing';
		$taxonomy = 'career_location';

		$all_careers = array();

		$careers_query = new WP_Query(array(
		  'post_type' => $post_type
		));

		while($careers_query->have_posts() ) : $careers_query->the_post();
			$careerObj = new \stdClass();

			$careerObj->name = get_the_title();
			$careerObj->slug = get_post_field( 'post_name', get_post() );
			$careerObj->description = get_the_content();
			$careerObj->responsibilities = get_field('responsibilities');
			$careerObj->qualifications = get_field('qualifications');
			$careerObj->apply = get_field('apply');


			$careerObj->applyUrl = get_field('apply_url');
			$careerObj->locations = array();
			
			$terms = wp_get_post_terms(get_the_id(), $taxonomy);

			foreach($terms as $term ) {
				array_push($careerObj->locations, $term->name);
			}

			array_push($all_careers, $careerObj);

		endwhile;


	 	wp_reset_query();


		$output .= <<<CPT
			<div class="expandable-categories">
				<!-- Careers list -->
				<section class="career-listings__menu expandable-categories__categories mobile-hide">
					<ul>
CPT;
			foreach($all_careers as $category ) {
				$output .= 	'<li>';
				ob_start(); 
				include(locate_template('snippet-button-expand-category.php'));
				$output .= 	ob_get_contents();
				ob_end_clean();	
				$output .=  '</li>';
			}
				
				$output .= <<<CPT
				 	</ul>
				</section>

				<!-- Careers Content -->
				<section class="career-listings__content expandable-categories__content container-shadow">
					<ul>
CPT;
					foreach($all_careers as $category ) {
						$output .= '<li>';
						ob_start(); 
						include(locate_template('snippet-button-expand-category.php'));
						$output .= 	ob_get_contents();
						ob_end_clean();	

						$output .= <<<CPT

						<article id="{$category->slug}" class="expandable-categories__group">
							<div class="career-listings-item__header-wrapper  mobile-hide">
								<div class="career-listings-item__header">
									<p class="career-listings-item__title">{$category->name}</p>
CPT;
							foreach($category->locations as $location ) {
								$output .= '<p class="career-listings-item__location">' . $location . '</p>';
							}

							$output .= '</div></div>';

							if($category->description) { 
								$output .= '<div class="career-listings-item-description">';
								$output .= '	<p class="career-listings-item-description__description">';
								$output .= 		$category->description;
								$output .= '</p></div>';
							}
							
							if($category->responsibilities) {
								$output .= '<div class="career-listings-item-description">';
								$output .= '	<p class="career-listings-item-description__subhead">Key Responsibilities:</p> ';
								$output .= '	<div class="career-listings-item-description__description">';
								$output .= 			$category->responsibilities;
								$output .= '	</div>';
								$output .= '</div>';
							}
							
							if($category->qualifications) {
								$output .= '<div class="career-listings-item-description">';
								$output .= '	<p class="career-listings-item-description__subhead">Qualifications:</p> ';
								$output .= '	<div class="career-listings-item-description__description">';
								$output .= 			$category->qualifications;
								$output .= '	</div>';
								$output .= '</div>';
							}
							
							if($category->apply) {
								$output .= '<div class="career-listings-item-description">';
								$output .= '	<div class="career-listings-item-description__description">';
								$output .= '		<p><span class="career-listings-item-description__subhead">To apply:</span></p> ';
								$output .= 			$category->apply;
								$output .= '	</div>';
								$output .= '</div>';
							}

						$output .= '</article></li>'; // close job listing
					}
				$output .= '</ul>'; // close list of jobs
	
	$output .= '</section></div>'; // close .main and section

	return $output;
}

} // End Element Class

// Element Class Init
new madwell_careersSection();
