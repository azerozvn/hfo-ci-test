<?php

/*
Element Description: Mini Hero With Background Image
Displays hero background with content box
*/
 
// Element Class 
class miniHeroImage extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'mini_hero_mapping' ) );
        add_shortcode( 'mini_hero', array( $this, 'mini_hero_html' ) );
    }
     
    // Element Mapping
    public function mini_hero_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }                       

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Mini Hero Bg Image', 'madwell-vc-plugin'),
                'base' => 'mini_hero',
                'description' => __('Mini Hero Bg Image', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Header Image', 'madwell-elements' ),
                        'param_name'  => 'image',
                        'description' => 'The background image for the hero area',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Tablet Header Image', 'madwell-elements' ),
                        'param_name'  => 'tablet-image',
                        'description' => 'The background image for the hero area on tablet devices',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Mobile Header Image', 'madwell-elements' ),
                        'param_name'  => 'mobile-image',
                        'description' => 'The background image for the hero area on mobile devices',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'h1',
                        'heading'     => __( 'Title', 'madwell-elements' ),
                        'param_name'  => 'title',
                        'description' => 'The heading on the hero image',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textarea_html',
                        'holder' => 'div',
                        'heading'     => __( 'Content', 'madwell-elements' ),
                        'param_name'  => 'content',
                        'description' => 'The subheading of the hero image',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Icon One', 'madwell-elements' ),
                        'param_name'  => 'icon-one',
                        'description' => 'Certification icons',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Icon Two', 'madwell-elements' ),
                        'param_name'  => 'icon-two',
                        'description' => 'Certification icons',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function mini_hero_html( $atts, $content = null ) {
         
        $data = wp_parse_args( $atts, array(
            'image'         => '',
            'tablet-image'  => '',
            'mobile-image'  => '',
            'title'         => '',
            'icon-one'         => '',
            'icon-two'         => '',
            'custom_class'  => '',
        ) );

        // Grab the images
        $image = wp_get_attachment_image_src( $data['image'], 'full' );
        $tabletImage = wp_get_attachment_image_src( $data['tablet-image'], 'full' );
        $mobileImage = wp_get_attachment_image_src( $data['mobile-image'], 'full' );
        $iconOne = wp_get_attachment_image_src( $data['icon-one'], 'full' );
        $iconTwo = wp_get_attachment_image_src( $data['icon-two'], 'full' );

        // Start output
        $output = '';

        // Start section
        $output .= '<section class="mini-hero-bg clearfix ' . esc_html( $data['custom_class'] ) . '" style="background-image: url(' . esc_url( $image[0] ) . ');">';

        $output .= '<img class="mini-hero-bg-tablet ' . esc_html( $data['custom_class'] ) . '" src="' . esc_url( $tabletImage[0] ) . '" />';

        $output .= '<img class="mini-hero-bg-mobile ' . esc_html( $data['custom_class'] ) . '" src="' . esc_url( $mobileImage[0] ) . '" />';

        // Start content container
        $output .= '<div class="mini-hero-bg__contentbox-container ' . esc_html( $data['custom_class'] ) . '">';

        // Start content div
        $output .= '<div class="mini-hero-bg__contentbox ' . esc_html( $data['custom_class'] ) . '">';

        // Output the title if one exists
        $output .= $data['title'] ? '<h1 class="mini-hero-bg__headline ' . esc_html( $data['custom_class'] ) . '">' . esc_html( $data['title'] ) . '</h1>' : '';

        // Output the content if it exists
        $output .= $content ? apply_filters( 'the_content', $content ) : '';

        // Close content div
        $output .= '</div>';

        // Close content container
        $output .= '</div>';

        // Output the certification icons
        $output .= '<img class="mini-hero-bg__icons" src="' . esc_url( $iconOne[0] ) . '">';

        // Close section
        $output .= '</section>';
        return $output;
    }
     
} // End Element Class
 
// Element Class Init
new miniHeroImage();