<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_hovertabs extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_hovertabs_mapping' ) );
		add_shortcode( 'madwell_hovertabs', array( $this, 'madwell_hovertabs_html' ) );
	}

	// Element Mapping
	public function madwell_hovertabs_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('Hover Tabs', 'madwell-vc-plugin'),
				'base' => 'madwell_hovertabs',
				'description' => __('Tab icons with hover interaction', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 1', 'madwell-elements' ),
						'param_name'  => 'tab_title1',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 1", "madwell-elements" ),
						"param_name" => "tab_icon1",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 1", "madwell-elements" ),
						"param_name" => "tab_id1",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 1", "madwell-elements" ),
						"param_name" => "tab_description1",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 2', 'madwell-elements' ),
						'param_name'  => 'tab_title2',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 2", "madwell-elements" ),
						"param_name" => "tab_icon2",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 2", "madwell-elements" ),
						"param_name" => "tab_id2",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 2", "madwell-elements" ),
						"param_name" => "tab_description2",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 3', 'madwell-elements' ),
						'param_name'  => 'tab_title3',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 3", "madwell-elements" ),
						"param_name" => "tab_icon3",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 3", "madwell-elements" ),
						"param_name" => "tab_id3",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 3", "madwell-elements" ),
						"param_name" => "tab_description3",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 4', 'madwell-elements' ),
						'param_name'  => 'tab_title4',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 4", "madwell-elements" ),
						"param_name" => "tab_icon4",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 4", "madwell-elements" ),
						"param_name" => "tab_id4",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 4", "madwell-elements" ),
						"param_name" => "tab_description4",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 5', 'madwell-elements' ),
						'param_name'  => 'tab_title5',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 5", "madwell-elements" ),
						"param_name" => "tab_icon5",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 5", "madwell-elements" ),
						"param_name" => "tab_id5",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 5", "madwell-elements" ),
						"param_name" => "tab_description5",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 6', 'madwell-elements' ),
						'param_name'  => 'tab_title6',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 6", "madwell-elements" ),
						"param_name" => "tab_icon6",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 6", "madwell-elements" ),
						"param_name" => "tab_id6",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 6", "madwell-elements" ),
						"param_name" => "tab_description6",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 7', 'madwell-elements' ),
						'param_name'  => 'tab_title7',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 7", "madwell-elements" ),
						"param_name" => "tab_icon7",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 7", "madwell-elements" ),
						"param_name" => "tab_id7",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 7", "madwell-elements" ),
						"param_name" => "tab_description7",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'div',
						'heading'     => __( 'Tab Title 8', 'madwell-elements' ),
						'param_name'  => 'tab_title8',
						'admin_label' => true,
						'group' => 'Madwell'
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Tab Icon 8", "madwell-elements" ),
						"param_name" => "tab_icon8",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab ID 8", "madwell-elements" ),
						"param_name" => "tab_id8",
						'group' => 'Madwell'
					),
					array(
						"type" => "textfield",
						"heading" => __( "Tab Description 8", "madwell-elements" ),
						"param_name" => "tab_description8",
						'group' => 'Madwell'
					),
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => true,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_hovertabs_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'tab_title1' => '',
			'tab_icon1' => '',
			'tab_id1' => '',
			'tab_description1' => '',
			'tab_title2' => '',
			'tab_icon2' => '',
			'tab_id2' => '',
			'tab_description2' => '',
			'tab_title3' => '',
			'tab_icon3' => '',
			'tab_id3' => '',
			'tab_description3' => '',
			'tab_title4' => '',
			'tab_icon4' => '',
			'tab_id4' => '',
			'tab_description4' => '',
			'tab_title5' => '',
			'tab_icon5' => '',
			'tab_id5' => '',
			'tab_description5' => '',
			'tab_title6' => '',
			'tab_icon6' => '',
			'tab_id6' => '',
			'tab_description6' => '',
			'tab_title7' => '',
			'tab_icon7' => '',
			'tab_id7' => '',
			'tab_description7' => '',
			'tab_title8' => '',
			'tab_icon8' => '',
			'tab_id8' => '',
			'tab_description8' => '',
			'custom_class' => ''
		) );
			
		$custom_class = esc_html( $data['custom_class'] );

		$output .= '<div class="hover-tabs__container">';

			for ($x = 1; $x <= 8; $x++) {

				$id_field = 'tab_id' . $x;
				$description_field = 'tab_description' . $x;
				$title_field = 'tab_title' . $x;
				$icon_field = 'tab_icon' . $x;

				$tab_id = esc_html( $data[$id_field] );
				$tab_description = esc_html( $data[$description_field] );
				$tab_title = esc_html( $data[$title_field] );
				$tab_icon = wp_get_attachment_url( esc_html( $data[$icon_field] ) );

				if ( $tab_id != '' ) {

					$output .= '<div class="hover-tab" data-content="' . $tab_id . '">';

					if ($tab_icon != '') {
					$output .= <<<CPT
						<div class="hover-tab__icon-container">
							<img class="hover-tab__icon" src="{$tab_icon}" alt="{$tab_title}" />
						</div>
CPT;
					}

					$output .= '<p class="hover-tab__title" >' . $tab_title . '</p>';

					if ($tab_description != '') {
						$output .= '<p class="hover-tab__description" >' . $tab_description . '</p>';
					}

					$output .= '</div>';


				} // close .tab

			} // end loop
		
		$output .= '</div>'; // hover-tabs-container

	return $output;
}

} // End Element Class

// Element Class Init
new madwell_hovertabs();