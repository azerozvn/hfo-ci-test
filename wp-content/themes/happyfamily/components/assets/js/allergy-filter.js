jQuery(document).ready(function($){

	$('.allergy-filter__item').on('click', function() {
		$(this).parent().removeClass('color');
		$(this).toggleClass('active');

		if ( $('.allergy-filter__item').hasClass('active') ) {
			$('.allergy-filter__list').addClass('filtered');
		} else {
			$('.allergy-filter__list').removeClass('filtered');
		}
	});

	$(".kosher-toggle__button").on('click', function() {
		$(this).children().toggleClass('active');
		$(this).toggleClass('active')
	});

	var productsContainer = $(".stages-allergy-carousel-container.desktop.carousel-products-small"),
		categoryItem = $(".stages-allergy-carousel-container").data('category'),
		stagingUrl = window.location.protocol+'//'+'staging.happyfamilyorganics.com',
		productionUrl = window.location.protocol+'//'+'www.happyfamilyorganics.com',
		location = window.location.protocol+'//'+window.location.host,
		proxy = 'http://'+window.location.hostname+'/proxy.php',
		xProxy = {},
		targetUrl = '',
		category = 'categories'+encodeURIComponent('[]')+'='+categoryItem,
		proxyTarget = shopDomain+'/json-category-products/index/products?'+category;

		if( !( location == stagingUrl || location == productionUrl) ) { 
			xProxy = { 'X-Proxy-URL': proxyTarget } 
		}

		if( !( location == stagingUrl || location == productionUrl) ) {
			targetUrl = proxy
		} else {
			targetUrl = proxyTarget
		}

	$.ajax({
		type: "POST",
		url: targetUrl,
		headers: xProxy,
		success: function(data) {

			var output = "<ul class=\"main stages-allergy-carousel product-line-allergy-carousel no-padding\">";
			for (var i in data) {
				if ( data[i].name != 'Bundles' ) {
					// Allergy names come from magento as an array an may be upper-cased
					var allergens = data[i].allergies.join(',').toLowerCase();
					output += "<li class=\"stages-product allergy-product\" data-allergens=\"" + allergens + "\"><a href=\"" + data[i].url + "\"><img src=\"" + data[i].image + "\"/><p>" + data[i].name + "</p></li>";
				}			
			}
			output += "</ul>";

			productsContainer.append(output);

			$('.stages-allergy-carousel').slick({
				slidesToShow: 4,
				slidesToScroll: 4,
				rows: 0,
				responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
					}
				}, {
					breakpoint: 601,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				}]
			});
		}
	});



	$('.allergy-filter__item, .kosher-toggle__button').each( function(index) {
		$(this).on("click", function() {
			var filter;
			var key = '';

			$('.allergy-filter__item, .kosher-toggle__button').each(function(index) {
				if ($(this).hasClass('active')) {
					filter = $(this).data('allergy');

					if( filter == 'kosher' ) {
						key += '[data-allergens*="'+filter+'"]';
					} else if( filter == 'tree' ) {
						key += '[data-allergens*="'+filter+' nut free"]';
					} else {
						key += '[data-allergens*="'+filter+' free"]';
					}

				}
			});

			$('.showme').removeClass('showme');

			if( key.length > 0 ) {

				$('.stages-allergy-carousel').slick('slickUnfilter');
				$(key).addClass('showme');
				$('.stages-allergy-carousel').slick('slickFilter', '.showme').slick('refresh');
				$('.stages-allergy-carousel').slick('slickGoTo', 0);

			} else {

				$('.stages-allergy-carousel').slick('slickUnfilter');
				$('.stages-allergy-carousel').slick('refresh');
				$('.stages-allergy-carousel').slick('slickGoTo', 0);
				$('.allergy-filter__list').addClass('default');
			}
		});
	});
});