jQuery(document).ready(function($){

	if ( $('.component-product-carousel').length ) {
		var x = 0

		$('.component-product-carousel').each(function(){
			x++;

			var productsContainer = $(this),
				categoryItem = $(this).data('category'),
				lifestageFilter = $(this).data('lifestage'),
				includeBundles = $(this).data('bundles'),
				ageRange = $(this).data('agerange'),
				productAttributeFilter = $(this).data('attributes'),
				stagingUrl = window.location.protocol+'//'+'staging.happyfamilyorganics.com',
				productionUrl = window.location.protocol+'//'+'www.happyfamilyorganics.com',
				location = window.location.protocol+'//'+window.location.host,
				proxy = window.location.protocol+'//'+window.location.host+'/proxy.php',
				xProxy = {},
				targetUrl = '',
				lifestage = 'lifestage'+encodeURIComponent('[]')+'='+lifestageFilter,
				category = 'categories'+encodeURIComponent('[]')+'='+categoryItem,
				classFilter = lifestageFilter.replace(/\s/g, '-').toLowerCase();
				lifestageTitle = lifestageFilter.replace(/[+\d]/g, ' ').replace(/[%\d]/g, '&').toLowerCase();
				ageRangeCopy = ageRange.toLowerCase();
				
				if ($(this).data('lifestage')) {
					var proxyTarget = shopDomain+'/json-category-products/index/products?'+category+'&'+lifestage;
						if( x == 1 ) {
							var filterButton  = "<div class=\"stages-filter__item active\" data-lifestageFilter=\""+lifestageFilter+"\">";
						} else {
							var filterButton  = "<div class=\"stages-filter__item\" data-lifestageFilter=\""+lifestageFilter+"\">";
						}
						filterButton += "<h4 style=\"text-align: center;\">"+lifestageTitle+"</h4>";
						filterButton += "<p style=\"text-align: center;\">"+ageRangeCopy+"</p>";
						filterButton += "</div>";
					$('.lifestage-filter').append(filterButton);
				} else {
					var proxyTarget = shopDomain+'/json-category-products/index/products?'+category;
				}

				if( !( location == stagingUrl || location == productionUrl) ) { 
					xProxy = { 'X-Proxy-URL': proxyTarget } 
				}

				if( !( location == stagingUrl || location == productionUrl) ) {
					targetUrl = proxy
				} else {
					targetUrl = proxyTarget
				}

				if( x == 1 ) { productsContainer.css('display','block'); }

			$.ajax({
				type: "POST",
				url: targetUrl,
				headers: xProxy,
				success: function(data) {
					console.log(data);
					var output = "<ul class=\"main stages-products-carousel " + classFilter + 
                        " product-line-products-carousel no-padding\">";
					for (var i in data) {
						var productListItem = "<li class=\"stages-product allergy-product\"><a href=\"" + 
                            data[i].url + "\"><img src=\"" + data[i].image + "\"/><p>" + data[i].name + "</p></li>";
						var addProduct = true;
						
						if ( data[i].name == 'Bundles' && includeBundles != true ) {
							addProduct = false;
							continue;
						}
						
						if ( productAttributeFilter !== [] ) {
                            for (var attribute in productAttributeFilter) {
                                if (typeof data[i][attribute] === 'undefined') {
                                    continue;
                                }
                                var filter = productAttributeFilter[attribute];
                                if (data[i][attribute].length === $(data[i][attribute]).not(filter).length) {
                                    addProduct = false;
                                    break;
                                }
                            }
                        }
                        if (addProduct) {
                            output += productListItem;
                        }
					}

					output += "</ul>";

					if ( productsContainer.find('.stages-filter__item--link').length ) {
						productsContainer.find('.stages-filter__item--link').before(output);
					} else {
						productsContainer.append(output);
					}

					$('.stages-filter__item--mobile .stages-products-carousel-container').each(function(){
						if ( $(this).hasClass(classFilter) ){
							$(this).append(output);
						}
					});

				},
				complete: function() {

					productsContainer.children('.stages-products-carousel').slick({
						slidesToShow: 4,
						slidesToScroll: 4,
						rows: 0,
						responsive: [{
							breakpoint: 1024,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
							}
						}]
					});

					$('.stages-filter__item--mobile').find('.stages-products-carousel').not('.slick-initialized').slick({
						rows: 0,
						slidesToShow: 2,
						slidesToScroll: 2
					});

				}
			});

		});

	}

	$('.stages-filter__container').on('click','.stages-filter__item', function() {
		var filter = $(this).data('lifestagefilter');
		$('.component-product-carousel').fadeOut();
		$(".component-product-carousel[data-lifestage='" + filter + "']").fadeIn( function(){
			if ( $(this).find('.slick-slider').length ) {
				$(this).find('.slick-slider').slick('setPosition');
			}
		});
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('.stages-filter__item--toggle').click(function() {
		if ( $(this).closest('.stages-filter__item--content').find('.slick-slider').length ) {
			$(this).find('.slick-slider').slick('setPosition');
		}
	});

});