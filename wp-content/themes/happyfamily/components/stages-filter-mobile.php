<?php

/*
Element Description: Mini Hero With Background Image
Displays hero background with content box
*/
 
// Element Class 
class stagesFilterMobile extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'stages_filter_mobile_mapping' ) );
        add_shortcode( 'stages_filter_mobile', array( $this, 'stages_filter_mobile_html' ) );
    }
     
    // Element Mapping
    public function stages_filter_mobile_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }                       

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Stages Filter Mobile', 'madwell-vc-plugin'),
                'base' => 'stages_filter_mobile',
                'description' => __('Stages Filter Mobile', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type'        => 'textfield',
                        'holder' => 'h4',
                        'heading'     => __( 'Title', 'madwell-elements' ),
                        'param_name'  => 'title',
                        'description' => 'The title of the filter',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'p',
                        'heading'     => __( 'Age', 'madwell-elements' ),
                        'param_name'  => 'age',
                        'description' => 'The age range of the filter',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textarea_html',
                        'holder' => 'div',
                        'heading'     => __( 'Content', 'madwell-elements' ),
                        'param_name'  => 'content',
                        'description' => 'The copy associated with the filter range',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'p',
                        'heading'     => __( 'Slug', 'madwell-elements' ),
                        'param_name'  => 'slug',
                        'description' => 'The slug of the extended product line',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function stages_filter_mobile_html( $atts, $content = null ) {
         
        $data = wp_parse_args( $atts, array(
            'title'         => '',
            'age'         => '',
            'content'         => '',
            'slug'         => '',
            'custom_class'  => '',
        ) );

        // Start output
        $output = '';

        // Start filter item container
        $output .= '<div class="stages-filter__item--mobile clearfix ' . esc_html( $data['custom_class'] ) . '">';

            // Start filter toggle
            $output .= '<div class="stages-filter__item--toggle ' . esc_html( $data['custom_class'] ) . '">';

                // Output toggle content
                $output .= '<h4>' . esc_html( $data['title'] ) . '</h4><p>' . esc_html( $data['age'] ) . '</p>';

            // Close filter toggle
            $output .= '</div>';

            // Start filter content container
            $output .= '<div class="stages-filter__item--content ' . esc_html( $data['custom_class'] ) . '">';

                // Output filter content
                $output .= '<p class="stages-filter__item--copy ' . esc_html( $data['custom_class'] ) . '">';
                    $output .= $content;
                $output .= '</p>';

                // Heres a divider
                $output .= '<hr class="stages-filter__item--divider" />';

                // Dynamic carousel
                $output .= '<div class="stages-products-carousel-container ' . esc_html ( $data['custom_class'] ) . ' carousel-products-small mobile-only"></div>';

                // Output product line link
                $output .= '<a class="stages-filter__item--link button ' . esc_html( $data['custom_class'] ) . '" href="' . esc_url( $data['slug'] ) . '">See more</a>';

            // Close filter content container
            $output .= '</div>';

        // Close filter item container
        $output .= '</div>';

        return $output;
    }
     
} // End Element Class
 
// Element Class Init
new stagesFilterMobile();