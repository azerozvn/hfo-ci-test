<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_faqSection extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_faqsection_mapping' ) );
		add_shortcode( 'madwell_faqsection', array( $this, 'madwell_faqsection_html' ) );
	}

	// Element Mapping
	public function madwell_faqsection_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('FAQ Section', 'madwell-vc-plugin'),
				'base' => 'madwell_faqsection',
				'description' => __('List of FAQ questions and category navigation', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => false,
						'weight' => 0,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_faqsection_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'custom_class' => ''
		) );

		$custom_class = esc_html( $data['custom_class'] );

		$taxonomy = 'faq_category';
		$post_type = 'faq-question';

		$parent_categories = array();
		$subcategories = array();

		$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ));
				 
		$terms = get_terms( array(
			'taxonomy' => $taxonomy,
			'hide_empty' => false,
			'orderby'   => 'meta_value',
			'meta_key'  => 'order',
			'posts_per_page' => -1
		));

		if ($terms) {
			foreach($terms as $term ) {
				//create category 
				$category = new \stdClass();
				$category->slug = $term->slug;
				$category->name = $term->name;
				$category->description = $term->description;
				$category->subhead = get_field('subhead', $term->taxonomy.'_'.$term->term_id);
				$category->icon = get_field('icon', $term->taxonomy.'_'.$term->term_id);
				$category->chat_url = get_field('chat_url', $term->taxonomy.'_'.$term->term_id);

				$subcatargs = array(
					'parent' => $term->term_id,
					'orderby' => 'slug',
					'hide_empty' => true,
					'meta_key' => 'order',
					'orderby' => 'meta_value',
					'order' => 'ASC',
					'posts_per_page' => -1
				);
				$category->parent = get_terms( $taxonomy, $subcatargs );

				
				if ( $category->parent ) {
					array_push($parent_categories, $category);
				} else {
					array_push($subcategories, $category);
				}
			}
		}

		wp_reset_query();

		$customer_service_subhead = get_field('customer_service_subhead');
		$customer_service_icon = get_field('customer_service_icon');
		$customer_service_cta = get_field('customer_service_cta');
		$customer_service_chat_url = get_field('customer_service_chat_url');
		$title = get_the_title();


		$output .= <<<CPT
			<h1>{$title}</h1>
			<div class="expandable-categories">
				<article class="faq-categories expandable-categories__categories mobile-hide">
					<ul>
CPT;
			foreach($parent_categories as $category ) {
				$output .= 	'<li>';
				ob_start(); 
				include(locate_template('snippet-button-expand-category.php'));
				$output .= 	ob_get_contents();
				ob_end_clean();	
				$output .=  '</li>';
			}
				
				$output .= <<<CPT
				 	</ul>

					<div class="faq-customer-service">
						<p class="faq-customer-service__subhead"><img src="{$customer_service_icon}" />{$customer_service_subhead}</p>

						<a href="{$customer_service_chat_url}" class="faq-customer-service__button button">{$customer_service_cta}</a>
					</div>
				</article>

				<!-- FAQ Questions container -->
				<article class="faq-questions expandable-categories__content container-shadow">
CPT;
					foreach($parent_categories as $category ) {
						ob_start(); 
						include(locate_template('snippet-button-expand-category.php'));
						$output .= 	ob_get_contents();
						ob_end_clean();

						$output .= <<<CPT

						<div id="{$category->slug}" class="faq-questions__group expandable-categories__group">
							<p class="faq-questions__category">{$category->name}</p>
CPT;
							if($category->subhead) { 
								$output .= '<p class="faq-questions__subhead">' . $category->subhead . '</p>';
							}

							if($category->description) { 
								$output .= '<p class="faq-questions__description">' . $category->description . '</p>';
							}
							
							if($category->chat_url) {
								$output .= '<div class="faq-questions__chat-icons">';
								$output .= '	<img class="faq-questions__chat-icon" src="' . get_template_directory_uri() . '/dist/images/icon-cs-chat.svg" />';
								$output .= '	<a href="' . $category->chat_url . '" class="faq-questions__chat-button button">Chat now</a>';
								$output .= '</div>';
							}

							foreach($category->parent as $subcategory) {
								$output .= '<p class="faq-questions__subcategory">' . $subcategory->name . '</p><ul>';

								$question_args = array(
									'post_type' => $post_type,
									'orderby' => 'menu_order',
									'order' => 'ASC',
									'posts_per_page' => -1,
									'tax_query' => array(
										array(
											'taxonomy' => $taxonomy,
											'field' => 'term_id',
											'terms' => $subcategory->term_id
										)
									)
								);

								//add array of questions for each category
								$subcategory_questions = array();
								$questions_query = new WP_Query($question_args);

								while ($questions_query->have_posts() ) : $questions_query->the_post();
								$questionObj = new \stdClass();
								$questionObj->question = get_the_title();
								$questionObj->answer = get_the_content();

								array_push($subcategory_questions, $questionObj);
								endwhile;

								$subcategory->questions = $subcategory_questions;

								foreach($subcategory->questions as $question ) {
									$output .= '<li class="faq-questions__item">';
									$output .= '	<p class="faq-questions__question">Q: ' . $question->question . '</p>';
									$output .= '	<p class="faq-questions__answer"><strong>A:</strong> ' . $question->answer . '</p>';
									$output .= '</li>';
								}

								$output .= '</ul>';

							}

						$output .= '</div>'; // close questions list and category div
					}
				$output .= '</article></div>'; // close faq-questions and expandable-categories

			$output .= '<div class="faq-customer-service mobile-only">';
			$output .= '	<img src="' . $customer_service_icon . '" />';
			$output .= '	<p class="faq-customer-service__subhead">' . $customer_service_subhead . '</p>';
			$output .= '		<a href="' . $customer_service_chat_url . '" class="faq-customer-service__button button">' . $customer_service_cta . '</a>';
			$output .= '</div>'; // close mobile-only customer service
	
	$output .= '</div>'; // close .main and section

	return $output;
}

} // End Element Class

// Element Class Init
new madwell_faqSection();
