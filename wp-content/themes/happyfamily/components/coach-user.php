<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_coachUser extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_coachuser_mapping' ) );
		add_shortcode( 'madwell_coachuser', array( $this, 'madwell_coachuser_html' ) );
	}

	// Element Mapping
	public function madwell_coachuser_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('Coach Users', 'madwell-vc-plugin'),
				'base' => 'madwell_coachuser',
				'description' => __('Grid of coach users', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => false,
						'weight' => 0,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_coachuser_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'custom_class' => ''
		) );

		$custom_class = esc_html( $data['custom_class'] );

		$query_args = array(
			'role' => 'coach',
			'orderby' => 'display_name',
    		'order' => 'ASC'
		);
		$loop = new WP_User_Query($query_args);
		if ( ! empty( $loop->get_results() ) ) :
			$output .= '<div class="users-coach-container">';

			foreach ( $loop->get_results() as $user ) :
				$avatar_attributes = wp_get_attachment_image_src( $user->coach_photo, 'full' );
				$coach_bio = wpautop($user->coach_bio); // This ensures paragraph tags are included
				$avatar = '';
				if ( $avatar_attributes ) {
					$avatar = $avatar_attributes[0];
				}

				if ( $user->coach_certifications != '' ) {
					$output .= '<div class="user-coach ' . $custom_class . '">';

					$output .= <<<CPT
						<div class="user-coach__modal-left">
							<div class="user-coach__base-info">
								<div class="user-coach__avatar">
									<span class="user-coach__expand" data-toggle="modal" data-target="#coach-modal" data-keyboard="true"></span>
									<img src="{$avatar}" alt="{$user->display_name}">
								</div>
								<hgroup>
									<h3>{$user->display_name}</h3>
									<h4>{$user->coach_certifications}</h4>
								</hgroup>
							</div>
							<div class="user-coach__callouts user-coach__modal-data">{$user->coach_callouts}</div>
						</div>

						<blockquote class="user-coach__quote user-coach__modal-data">{$user->coach_quote}</blockquote>
						<div class="user-coach__bio user-coach__modal-data">{$coach_bio}</div>
CPT;

					$output .= '</div>'; // close user-coach
				}

		endforeach;

		$output .= '</div>'; // close users-coach-container
		$output .= '<div id="coach-modal" class="coach-modal modal"><div class="modal-content coach-modal__content"><div class="modal-close coach-modal__close"><button></button></div><div class="coach-modal__content-container"></div></div></div>';

	endif;

	wp_reset_query();

	return $output;
}

} // End Element Class

// Element Class Init
new madwell_coachUser();
