<?php

/*
Element Description: Allergy Filter
Renders the allergy filter component
*/
 
// Element Class 
class allergyFilter extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'allergy_filter_mapping' ) );
        add_shortcode( 'allergy_filter', array( $this, 'allergy_filter_html' ) );
    }
     
    // Element Mapping
    public function allergy_filter_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }                       

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Allergy Filter', 'madwell-vc-plugin'),
                'base' => 'allergy_filter',
                'description' => __('Allergy Filter component', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type'        => 'textfield',
                        'holder' => 'h1',
                        'heading'     => __( 'Headline', 'madwell-elements' ),
                        'param_name'  => 'headline',
                        'description' => 'Allergy Filter headline',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textarea_html',
                        'holder' => 'div',
                        'heading'     => __( 'Content', 'madwell-elements' ),
                        'param_name'  => 'content',
                        'description' => 'Allergy Filter copy',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Category Filter',  "madwell-elements" ),
                        'param_name' => 'category',
                        'value' => array(
                            __( 'All',  "js_composer"  ) => '',
                            __( 'Baby',  "js_composer"  ) => 'Baby',
                            __( 'Tot',  "js_composer"  ) => 'Tot',
                            __( 'Kid',  "js_composer"  ) => 'Kid',
                            __( 'Mama',  "js_composer"  ) => 'Mama',
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Filters Checkbox', 'js_composer' ),
                        'param_name' => 'filters_checkbox',
                        'value' => array(
                            __( 'soy', 'js_composer' ) => 'soy',
                            __( 'dairy', 'js_composer' ) => 'dairy',
                            __( 'wheat', 'js_composer' ) => 'wheat',
                            __( 'egg', 'js_composer' ) => 'egg',
                            __( 'fish', 'js_composer' ) => 'fish & shellfish',
                            __( 'gluten', 'js_composer' ) => 'gluten',
                            __( 'treenut', 'js_composer' ) => 'tree nut',
                            __( 'peanut', 'js_composer' ) => 'peanut',
                            __( 'corn', 'js_composer' ) => 'corn',
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function allergy_filter_html( $atts, $content = null ) {
         
        $data = wp_parse_args( $atts, array(
            'headline'         => '',
            'custom_class'  => '',
            'category' => '',
            'filters_checkbox' => '',
        ) );

        // Start output
        $output = '';

        // Start section
        $output .= '<section class="allergy-filter clearfix ' . esc_html( $data['custom_class'] ) . '">';

        // Start content container
        $output .= '<div class="allergy-filter__container ' . esc_html( $data['custom_class'] ) . '">';

        // Output the title if one exists
        $output .= $data['headline'] ? '<h1 class="allergy-filter__headline ' . esc_html( $data['custom_class'] ) . '">' . esc_html( $data['headline'] ) . '</h1>' : '';

        // Output the content if it exists
        $output .= $content ? apply_filters( 'the_content', $content ) : '';

        // Output filters list
        if( ! empty( $data['filters_checkbox'] ) ) { 

            // Start filters list
            $output .= '<ul class="allergy-filter__list default ' . esc_html( $data['custom_class'] ) . '">';

            $allergens = explode(',', $data['filters_checkbox'] );

            foreach ($allergens as $allergy) {
                $output .= '<li data-allergy="' . strtolower( explode(' ', trim( $allergy ) )[0] ) . '" class="allergy-filter__item"><img class="color" src="' . get_template_directory_uri().'/components/assets/img/allergy-filters/' . strtolower( explode(' ', trim( $allergy ) )[0] ) . '.svg" /><img src="' . get_template_directory_uri().'/components/assets/img/allergy-filters/' . strtolower( explode(' ', trim( $allergy ) )[0] ) . '-grey.svg" /><span>' . strtolower( htmlspecialchars_decode ($allergy) ) . ' free</span></li>';
            }

            // Close filters list
            $output .= '</ul>';
        }

        // Output kosher filter
        $output .= <<<CPT

            <div class="kosher-toggle">
                <div data-allergy="kosher" class="kosher-toggle__button">
                    <div class="kosher-toggle__slider"></div>
                </div>
                <span class="kosher-toggle__label">kosher</span>
            </div>

CPT;

        // Close content container
        $output .= '</div>';

        // Close section
        $output .= '</section>';

        // Render product carousel section
        $output .= '<section data-category="' . esc_html( $data['category'] ) . '" class="stages-allergy-carousel-container desktop ' . esc_html ( $data['custom_class'] ) . ' carousel-products-small">';

        // Close product carousel section
        $output .= '</section>';

        return $output;
    }
     
} // End Element Class

// Element Class Init
new allergyFilter();