<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_pressArticle extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_pressarticle_mapping' ) );
		add_shortcode( 'madwell_pressarticle', array( $this, 'madwell_pressarticle_html' ) );
	}

	// Element Mapping
	public function madwell_pressarticle_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('Press Articles', 'madwell-vc-plugin'),
				'base' => 'madwell_pressarticle',
				'description' => __('Grid of press articles', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
					    'type'        => 'textfield',
					    'heading'     => __( 'Articles per page', 'madwell-elements' ),
					    'param_name'  => 'per_page',
					    'admin_label' => true,
					    'holder' => 'div',
					    'group' => 'Madwell',
					    'description' => 'Default is 12.'
					),
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => false,
						'weight' => 0,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_pressarticle_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'custom_class' => '',
			'per_page' => '12'
		) );
			
		$custom_class = esc_html( $data['custom_class'] );
		$posts_per_page = esc_html( $data['per_page']);

		echo '<div class="press-archive-container">';
		echo '<ul class="press-archive">';
		echo '<li class="press-archive__title">Sort by year</li>';
		echo '<li class="press-archive__all">All</li>';
		wp_get_archives(array(
			'type'=>'yearly', 
			'show_post_count'=>false,
			'post_type'=>'press-article', 
			'format'=>'custom',
			'before' => '<li class="press-archive__year">',
			'after' => '</li>'
		)); 

		echo '</ul></div>'; // close press-archive

		// Pagination
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$query_args = array(
			'post_type' => 'press-article',
			'post_status' => 'publish',
			'posts_per_page' => $posts_per_page,
			'paged' => $paged
		);
		$loop = new WP_Query($query_args);
		
		$total_post = $loop->found_posts;
		$pages = $total_post / $posts_per_page;
		if ($total_post % $posts_per_page) $pages=$pages+1; 
		$args = array(
			'base'               => str_replace( 999999, '%#%', esc_url( get_pagenum_link( 999999 ) ) ),
			'format'             => '?paged=%#%',
			'total'              => $pages,
			'show_all'           => true,
			'prev_next'          => true,
			'prev_text'          => __('Previous'),
			'next_text'          => __('Next'),
			'type'               => 'plain'
		);

		if ( $loop->have_posts() ) : 

			$output .= '<div class="press-container all-posts">';

			while ( $loop->have_posts() ) :

			$loop->the_post();
			$id = get_the_id();
			$title = esc_html(get_the_title());

			$output .= '<div class="press-article ' . $custom_class . '">';

			if ( has_post_thumbnail() ) {
				$featured_image = esc_url(get_the_post_thumbnail_url());
			} else {
				$featured_image = '';
			}
			$link = esc_url(get_field('press_link'));
			$source = esc_html(get_field('press_source'));
			$date = esc_html(get_the_date('m.d.y'));

			$output .= <<<CPT

				<a class="press-article__logo-container {$custom_class}__logo-container" target="_blank" href="{$link}">
					<img class="press-article__logo {$custom_class}__logo" src="{$featured_image}" alt="{$source}" />
				</a>
				<div class="press-article__details-container {$custom_class}__details-container">
					<p class="press-article__date {$custom_class}__date">{$date}</p>
					<h3 class="press-article__title {$custom_class}__title"><a target="_blank" href="{$link}">{$title}</a></h3>
				</div>
CPT;
		
		$output .= '</div>'; // close press-article

		endwhile;

		$output .= '</div><div class="posts-by-year press-container"></div>'; // close press-container

		if ( $posts_per_page >= 12 ) {
			$output .= '<div class="press-pagination">' . paginate_links($args) . '</div';
		}

	endif;

	wp_reset_query();

	return $output;
}

} // End Element Class

// Element Class Init
new madwell_pressArticle();