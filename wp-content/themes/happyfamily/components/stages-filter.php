<?php

/*
Element Description: Mini Hero With Background Image
Displays hero background with content box
*/
 
// Element Class 
class stagesFilter extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'stages_filter_mapping' ) );
        add_shortcode( 'stages_filter', array( $this, 'stages_filter_html' ) );
    }
     
    // Element Mapping
    public function stages_filter_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }                       

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Stages Filter', 'madwell-vc-plugin'),
                'base' => 'stages_filter',
                'description' => __('Stages Filter', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function stages_filter_html( $atts, $content = null ) {
         
        $data = wp_parse_args( $atts, array(
            'custom_class'  => '',
        ) );

        // Start output
        $output = '';

        // Start filter item container
        $output .= '<section class="lifestage-filter stages-filter__container ' . esc_html( $data['custom_class'] ) . '">';

        // Close filter item container
        $output .= '</section>';

        return $output;
    }
     
} // End Element Class
 
// Element Class Init
new stagesFilter();