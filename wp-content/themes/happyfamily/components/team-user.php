<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_teamUser extends WPBakeryShortCode {

  // Element Init
  function __construct() {
    add_action( 'init', array( $this, 'madwell_teamuser_mapping' ) );
    add_shortcode( 'madwell_teamuser', array( $this, 'madwell_teamuser_html' ) );
  }

  // Element Mapping
  public function madwell_teamuser_mapping() {

    // Stop all if VC is not enabled
    if ( !defined( 'WPB_VC_VERSION' ) ) {
      return;
    }

    // Map the block with vc_map()
    vc_map(

      array(
        'name' => __('Team Users', 'madwell-vc-plugin'),
        'base' => 'madwell_teamuser',
        'description' => __('Grid of team users', 'madwell-vc-plugin'),
        'category' => __('Madwell Elements', 'madwell-vc-plugin'),
        'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
        'params' => array(
          array(
            'type'        => 'textfield',
            'holder' => 'div',
            'heading'     => __( 'Custom Class', 'madwell-elements' ),
            'param_name'  => 'custom_class',
            'admin_label' => false,
            'weight' => 0,
            'group' => 'Madwell'
          )
        )
      )
    );
  }


  // Element HTML
  public function madwell_teamuser_html( $atts, $content = null ) {
    $output = '';

    $data = wp_parse_args( $atts, array(
      'custom_class' => ''
    ) );

    $custom_class = esc_html( $data['custom_class'] );

    $query_args = array(
      'role' => 'team_member',
      'meta_key' => 'last_name',
      'orderby' => 'meta_value',
      'order' => 'ASC'
    );
    $loop = new WP_User_Query($query_args);
    if ( ! empty( $loop->get_results() ) ) :
      $result = array_merge( $loop->get_results() );

      $featuredOutput .= '<div class="team-users-featured-container clearfix">';
      $output .= '<div class="team-users-container">';

      foreach ( $result as $user ) :
        $avatar_attributes = wp_get_attachment_image_src( $user->team_photo, 'full' );
        $avatar = '';
        if ( $avatar_attributes ) {
          $avatar = $avatar_attributes[0];
        }

        $userMarkup = <<<CPT
          <div class="team-user">
            <div class="team-user__avatar">
              <img src="{$avatar}" alt="{$user->display_name}">
            </div>
            <h4 class="team-user__name">{$user->display_name}</h4>
            <h4 class="team-user__job-title">{$user->job_title}</h4>
          </div>
CPT;
        if ( $user->team_show == true ) {
          if ( $user->last_name == 'Visram' ) {
            $firstMember .= $userMarkup;
          } elseif ( $user->last_name == 'Laraway' ) {
            $secondMember .= $userMarkup;
          } else {
            $output .= $userMarkup;
          }
        }

      endforeach;

      $featuredOutput .= $firstMember . $secondMember . '</div>'; // close team-users-featured-container
      $output .= '</div>'; // close team-users-container
      $output .= '<div class="team-users__see-all"><a href="#">See all</a></div>';

    endif;

  wp_reset_query();

  return $featuredOutput . $output;
}

} // End Element Class

// Element Class Init
new madwell_teamUser();
