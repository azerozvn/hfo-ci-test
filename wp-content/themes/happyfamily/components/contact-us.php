<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_contact extends WPBakeryShortCode {

	// Element Init
	function __construct() {
		add_action( 'init', array( $this, 'madwell_contact_mapping' ) );
		add_shortcode( 'madwell_contact', array( $this, 'madwell_contact_html' ) );
	}

	// Element Mapping
	public function madwell_contact_mapping() {

		// Stop all if VC is not enabled
		if ( !defined( 'WPB_VC_VERSION' ) ) {
			return;
		}

		// Map the block with vc_map()
		vc_map(

			array(
				'name' => __('Contact Us', 'madwell-vc-plugin'),
				'base' => 'madwell_contact',
				'description' => __('Contact information', 'madwell-vc-plugin'),
				'category' => __('Madwell Elements', 'madwell-vc-plugin'),
				'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
				'params' => array(
					array(
						'type'        => 'textfield',
						'holder' => 'div',
						'heading'     => __( 'Custom Class', 'madwell-elements' ),
						'param_name'  => 'custom_class',
						'admin_label' => false,
						'weight' => 0,
						'group' => 'Madwell'
					)
				)
			)
		);
	}


	// Element HTML
	public function madwell_contact_html( $atts, $content = null ) {
		$output = '';

		$data = wp_parse_args( $atts, array(
			'custom_class' => ''
		) );

		$custom_class = esc_html( $data['custom_class'] );

		$contact_form = get_field('contact_form');
		$location_new_york = get_field('location_new_york');
		$location_boise = get_field('location_boise');
		$departments = get_field('contact_departments', false);


		$output .= <<<CPT
			<div class="contact-container">

				<section class="contact-form col-5">
				  	{$contact_form}
				</section>

				<section class="contact-details push-1 col-6">
					<article class="contact-details__locations">
						<!-- new york location -->
						<div class="contact-details__group">
							<p class="subhead">{$location_new_york['headline']}</p>
							<p>{$location_new_york['street_address']}</p>
							<p>{$location_new_york['city_state_zip']}</p>
							<p>{$location_new_york['phone_number']}</p>
						</div>

						<!-- boise location -->

						<div class="contact-details__group">
							<p class="subhead">{$location_boise['headline']}</p>
							<p>{$location_boise['street_address']}</p>
							<p>{$location_boise['city_state_zip']}</p>
							<p>{$location_boise['phone_number']}</p>
						</div>
					</article>

					<article class="contact-details__departments">
						<ul>
CPT;

						foreach ( $departments as $department ) {  
					        if($department['show_on_contact_page']) {
					        	
					        	$output .= '<li class="contact-details__group">';
					        	$output .= '	<p class="subhead">';
					        	$output .= 			$department['department_headline'];
					        	$output .= '	</p><p>';
					        	$output .= 			$department['department_description'];
					        	$output .= '	<a href="mailto:';
					        	$output .= 			$department['department_email'];
					        	$output .= '">';
					        	$output .= 		$department['department_email'];
					        	$output .= '</a>.</p>';
					        	$output .= '</li>';
					           	
					    	} 
						       
						} 
		$output .= <<<CPT
						</ul>
					</article>
				</section>
			</div>
CPT;


	wp_reset_query();

	return $output;


}

} // End Element Class

// Element Class Init
new madwell_contact();
