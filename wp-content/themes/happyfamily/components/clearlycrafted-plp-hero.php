<?php

/*
Element Description: Madwell VC Partner Grid
*/

// Element Class
class madwell_ccPlpHero extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'madwell_cchero_mapping' ) );
        add_shortcode( 'madwell_cchero', array( $this, 'madwell_cchero_html' ) );
    }

    // Element Mapping
    public function madwell_cchero_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(

            array(
                'name' => __('Clearly Crafted PLP Hero Product', 'madwell-vc-plugin'),
                'base' => 'madwell_cchero',
                'description' => __('Clearly Crafted product that appears in hero and its various states', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => get_template_directory_uri().'/components/assets/img/mad_fullhero.png',
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "heading" => __( "Clearly Crafted Product Image", "madwell-elements" ),
                        "param_name" => "cc_image",
                        'group' => 'Madwell'
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Include clear packaging indicator?', 'js_composer' ),
                        'param_name' => 'cc_clearpackage',
                        'value' => array(
                            __( 'enabled', 'js_composer' ) => 'Enabled'
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Recipe Information', 'js_composer' ),
                        'param_name' => 'cc_recipetoggle',
                        'value' => array(
                            __( 'enabled', 'js_composer' ) => 'Enabled'
                        ),
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell'
                    )
                )
            )
        );
    }


    // Element HTML
    public function madwell_cchero_html( $atts, $content = null ) {
        $output = '';

        $data = wp_parse_args( $atts, array(
            'custom_class' => '',
            'cc_image' => '',
            'cc_clearpackage' => '',
            'cc_recipetoggle' => ''
        ) );

        $custom_class = esc_html( $data['custom_class'] );
        $ccImage = wp_get_attachment_url( esc_html( $data['cc_image']) );
        $ccClearPackage = esc_html( $data['cc_clearpackage']);
        $ccRecipeToggle = esc_html( $data['cc_recipetoggle']);
        $title = esc_html( get_the_title() );

        if ( $ccClearPackage == 'Enabled' ) {
            $output .= '<p class="clearly-crafted-hero__clear-package">I\'m clear!</p>';
        }

        if ( $ccRecipeToggle == 'Enabled' ) {
            $output .= '<p class="clearly-crafted-hero__recipe-toggle">Tap<br> to see recipe</p>';
        }

        $output .= '<img class="clearly-crafted-hero__product-image" src="' . $ccImage . '" alt="' . $title . '" />';

    return $output;
}

} // End Element Class

// Element Class Init
new madwell_ccPlpHero();
