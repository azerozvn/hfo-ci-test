<?php

// Our custom post type function
/*
* Creating a function to create our CPT
*/

function add_products_post_type() {

// Register Product Posts
    $products_args = array(
        'label'               => __( 'products', 'twentythirteen' ),
        'description'         => __( 'Products', 'twentythirteen' ),
        'labels'              => array(
            'name'          => __( 'Products' ),
            'singular_name' => __( 'Product' ),
        ),
        'menu_position' => 5,
        'menu_icon' => 'dashicons-store',
        // Features this CPT supports in Post Editor
        // 'supports'            => array( 'title', 'editor', 'revisions', 'custom-fields', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        // 'show_ui'             => true,
        // 'show_in_menu'        => true,
        // 'show_in_nav_menus'   => true,
        // 'show_in_admin_bar'   => true,
        'menu_position' => 5,
        // 'can_export'          => true,
        'has_archive'         => 'shop',
        // 'exclude_from_search' => false,
        // 'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array(
            'slug' =>'shop/%product_category%',
            "with_front" => false
        ),
    );

    register_post_type( 'products', $products_args );

// Register Taxonomy: "Product Categories"
    $labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Categories', 'twentythirteen' ),
		'all_items'         => __( 'All Categories', 'twentythirteen' ),
		'parent_item'       => __( 'Parent Category', 'twentythirteen' ),
		'parent_item_colon' => __( 'Parent Category:', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Category', 'twentythirteen' ),
		'update_item'       => __( 'Update Category', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Category', 'twentythirteen' ),
		'new_item_name'     => __( 'New Category Name', 'twentythirteen' ),
		'menu_name'         => __( 'Category', 'twentythirteen' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Product category', 'twentythirteen' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'shop', 'with_front' => false )
	);
	register_taxonomy( 'product_category', array(), $args );
    register_taxonomy_for_object_type( 'product_category', 'products' );
    
// Add a filter to post_type_link to substitute the product category in individual product permalinks
    function product_permalinks( $post_link, $post ){
        if ( is_object( $post ) && $post->post_type == 'products' ){
            $terms = wp_get_object_terms( $post->ID, 'product_category' );
            if( $terms ){
                return str_replace( '%product_category%' , $terms[0]->slug , $post_link );
            }
        }
        return $post_link;
    }
    add_filter( 'post_type_link', 'product_permalinks', 1, 2 );
    
// Register Taxonomy: "Age Range"
    $labels = array(
        'name'              => _x( 'Age Ranges', 'taxonomy general name', 'twentythirteen' ),
        'singular_name'     => _x( 'Age Range', 'taxonomy singular name', 'twentythirteen' ),
        'search_items'      => __( 'Search Age Ranges', 'twentythirteen' ),
        'all_items'         => __( 'All Age Ranges', 'twentythirteen' ),
        'parent_item'       => __( 'Parent Age Range', 'twentythirteen' ),
        'parent_item_colon' => __( 'Parent Age Range:', 'twentythirteen' ),
        'edit_item'         => __( 'Edit Age Range', 'twentythirteen' ),
        'update_item'       => __( 'Update Age Range', 'twentythirteen' ),
        'add_new_item'      => __( 'Add New Age Range', 'twentythirteen' ),
        'new_item_name'     => __( 'New Age Range Name', 'twentythirteen' ),
        'menu_name'         => __( 'Age Range', 'twentythirteen' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Product Age Range', 'twentythirteen' ),
        'hierarchical' => false,
        'meta_box_cb' => 'post_categories_meta_box',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => false,
        'show_in_rest' => true,
        'rewrite' => false,
    );
    register_taxonomy( 'age_range', array(), $args );
    register_taxonomy_for_object_type( 'age_range', 'products' );
    
// Register Taxonomy: "Development Stage"
	$labels = array(
        'name'              => _x( 'Development Stages', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Development Stage', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Development Stages', 'twentythirteen' ),
		'all_items'         => __( 'All Development Stages', 'twentythirteen' ),
		'parent_item'       => __( 'Parent Development Stage', 'twentythirteen' ),
		'parent_item_colon' => __( 'Parent Development Stage:', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Development Stage', 'twentythirteen' ),
		'update_item'       => __( 'Update Development Stage', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Development Stage', 'twentythirteen' ),
		'new_item_name'     => __( 'New Development Stage Name', 'twentythirteen' ),
		'menu_name'         => __( 'Development Stage', 'twentythirteen' ),
	);
	$args = array(
        'labels' => $labels,
		'description' => __( '', 'twentythirteen' ),
		'hierarchical' => false,
        'meta_box_cb' => 'post_categories_meta_box',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
		'rewrite' => false,
	);
    register_taxonomy( 'lifestage', array(), $args );
    register_taxonomy_for_object_type( 'lifestage', 'products' );
    
// Register Taxonomy: "Product Category"
	$labels = array(
        'name'              => _x( 'Product Categories', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Product Category', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Product Categories', 'twentythirteen' ),
		'all_items'         => __( 'All Product Categories', 'twentythirteen' ),
		'parent_item'       => __( 'Parent Product Category', 'twentythirteen' ),
		'parent_item_colon' => __( 'Parent Product Category:', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Product Category', 'twentythirteen' ),
		'update_item'       => __( 'Update Product Category', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Product Category', 'twentythirteen' ),
		'new_item_name'     => __( 'New Product Category Name', 'twentythirteen' ),
		'menu_name'         => __( 'Product Category', 'twentythirteen' ),
	);
	$args = array(
        'labels' => $labels,
		'description' => __( '', 'twentythirteen' ),
		'hierarchical' => false,
        'meta_box_cb' => 'post_categories_meta_box',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
		'rewrite' => false,
    );
    register_taxonomy( 'product_group', array(), $args );
    register_taxonomy_for_object_type( 'product_group', 'products' );
    
// Register Taxonomy: "Allergies & Dietary Needs"
	$labels = array(
        'name'              => _x( 'Allergies & Dietary Needs', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Allergies & Dietary Need', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Allergies & Dietary Needs', 'twentythirteen' ),
		'all_items'         => __( 'All Allergies & Dietary Needs', 'twentythirteen' ),
		'parent_item'       => __( 'Parent Allergies & Dietary Need', 'twentythirteen' ),
		'parent_item_colon' => __( 'Parent Allergies & Dietary Need:', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Allergies & Dietary Need', 'twentythirteen' ),
		'update_item'       => __( 'Update Allergies & Dietary Need', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Allergies & Dietary Need', 'twentythirteen' ),
		'new_item_name'     => __( 'New Allergies & Dietary Need Name', 'twentythirteen' ),
		'menu_name'         => __( 'Allergies & Dietary Need', 'twentythirteen' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'twentythirteen' ),
		'hierarchical' => false,
        'meta_box_cb' => 'post_categories_meta_box',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
		'rewrite' => false,
    );
    register_taxonomy( 'allergies', array(), $args );
    register_taxonomy_for_object_type( 'allergies', 'products' );
    
// Register Taxonomy: "Product Line"
	$labels = array(
        'name'              => _x( 'Product Lines', 'taxonomy general name', 'twentythirteen' ),
		'singular_name'     => _x( 'Product Line', 'taxonomy singular name', 'twentythirteen' ),
		'search_items'      => __( 'Search Product Lines', 'twentythirteen' ),
		'all_items'         => __( 'All Product Lines', 'twentythirteen' ),
		'parent_item'       => __( 'Parent Product Line', 'twentythirteen' ),
		'parent_item_colon' => __( 'Parent Product Line:', 'twentythirteen' ),
		'edit_item'         => __( 'Edit Product Line', 'twentythirteen' ),
		'update_item'       => __( 'Update Product Line', 'twentythirteen' ),
		'add_new_item'      => __( 'Add New Product Line', 'twentythirteen' ),
		'new_item_name'     => __( 'New Product Line Name', 'twentythirteen' ),
		'menu_name'         => __( 'Product Line', 'twentythirteen' ),
	);
	$args = array(
        'labels' => $labels,
		'description' => __( '', 'twentythirteen' ),
		'hierarchical' => false,
        'meta_box_cb' => 'post_categories_meta_box',
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
		'rewrite' => false,
	);
	register_taxonomy( 'product_line', array(), $args );
    register_taxonomy_for_object_type( 'product_line', 'products' );



// Prevent WordPress from re-ordering categories/taxonomies in the CMS UI when they are selected
    function stop_reordering_my_categories($args) {
        $args['checked_ontop'] = false;
        return $args;
    }
    add_filter('wp_terms_checklist_args','stop_reordering_my_categories');

    
// Show the shop page instead of a 404 if you go to a link that filters by a taxonomy term that does not exist.
// Example: "/shop?product_group=974"
    function hfo_replace_404_template( $template ){
        if( is_404() ){
            global $wp_query;
            if( $wp_query->query['post_type'] == 'products' ) {
                // print_r($wp_query); die();
                $new_template = locate_template( array( 'taxonomy-product_category.php' ) );
                if ( $new_template != '' ) {
                    $wp_query->was_404 = true;
                    $wp_query->is_404 = false;
                    return $new_template;
                }
            }
        }
        return $template;
    }
    add_action( 'template_include', 'hfo_replace_404_template' );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'add_products_post_type', 0 );



