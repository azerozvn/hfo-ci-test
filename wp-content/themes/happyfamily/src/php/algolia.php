<?php

////////////////////////////////////////////////////////////////////////////
// Algolia Search Stuff

// Helper functions copied from taxonomy-product_category.php
function algolia_get_price_per_quantity($price, $qty) {
    if (!$price || !$qty) { return ''; }
    return '$' . number_format( ( floatval($price) / floatVal($qty) ), 2 );
}

function algolia_echo_price_per_quantity($packaging_qty, $price, $packaging_type) {
    $packaging_type = $packaging_type ?: 'Item';

    if( !$packaging_qty || !$price ) {
      return;
    }
    return algolia_get_price_per_quantity($price, $packaging_qty) . ' / ' . $packaging_type;
}

/**
 * Cut string to n symbols and add delim but do not break words.
 *
 * Example:
 * <code>
 *  $string = 'this sentence is way too long';
 *  echo neat_trim($string, 16);
 * </code>
 *
 * Output: 'this sentence is...'
 *
 * @access public
 * @param string string we are operating with
 * @param integer character count to cut to
 * @param string|NULL delimiter. Default: '...'
 * @return string processed string
 **/
function neat_trim($str, $n, $delim='...') {
    $len = strlen($str);
    if ($len > $n) {
        preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
        return rtrim($matches[1]) . $delim;
    }
    else {
        return $str;
    }
 }

// Customize how we convert a Wordpress post into an Algolia record.
function algolia_post_to_record(WP_Post $post) {
    
    $post_type = $post->post_type;
    $id = $post->ID;
    $category = get_the_terms($id, 'product_category')[0]->name;
    $product_group = get_the_terms($id, 'product_group')[0]->name;
    $packaging_qty = get_field('packaging_qty', $id);
    $packaging_type = get_field('packaging_type', $id);
    $price = get_field('price', $id);

    // Product Line
    $product_line = get_the_terms($id, 'product_line');
    if($product_line && $product_line[0]) {
      $product_line = $product_line[0] -> name;
    } else {
      $product_line = '';
    }

    // Category Color
    $category_color = 'green';
    if($category == 'Baby') { $category_color = 'green'; }
    if($category == 'Tot') { $category_color = 'purple'; }
    if($category == 'Kid') { $category_color = 'blue'; }
    if($category == 'Mama') { $category_color = 'red'; }


    // error_log('algolia_post_to_record: post_type: ' . $post_type);
    
    if($post_type == 'products') {
        $result = [
            'objectID' => $id,
            'image' => get_field('base_image', $id)['url'],
            'category' => $category,
            'product_group' => $product_group,
            'category_color' => $category_color,
            'title' => $post->post_title,
            'packaging_size' => get_field('packaging_size', $id),
            'product_line' => $product_line,
            'url' => get_permalink($id),
            'price_per_quantity' => algolia_echo_price_per_quantity( $packaging_qty, $price, $packaging_type ),
            'type' => 'product',
        ];
        return $result;
    }
    
    if($post_type == 'post') {

        $categories = array_map(function (WP_Term $term) {
            return $term->name;
        }, wp_get_post_terms($id, 'category'));
        
        // Is it a recipe?
        if (in_array('Recipes', $categories)) { $type = 'recipe'; }
        else if (in_array('Recipes &amp Meal Plans', $categories)) { $type = 'recipe'; }
        else if (in_array('Meal Plan', $categories)) { $type = 'recipe'; }
        else if (in_array('Recipe', $categories)) { $type = 'recipe'; }
        else { $type = 'article'; }

        // Excerpt
        // We have to shorten with neat_trim, because doing a naive substr to 35 chars caused PHP error "Malformed UTF-8 characters"
        $excerpt = get_the_excerpt($id);
        $excerpt_short = neat_trim($excerpt, 35, '...');
        // error_log('algolia_post_to_record: shortVersion: ' . $excerpt_short);

        // Title
        $title = $post->post_title;
        $title_short = neat_trim($title, 45, '...');

        return [
            'objectID' => $id,
            'image' => wp_get_attachment_url( get_post_thumbnail_id($id) ),
            'category' => $categories[0],
            'title' => $title,
            'title_short' => $title_short,
            'url' => get_permalink($id),
            'excerpt' => $excerpt,
            'excerpt_short' => $excerpt_short,
            'type' => $type,
        ];
    }
    
    return [
        'objectID' => implode('#', [$post->post_type, $post->ID]),
        'title' => $post->post_title,
        'url' => get_permalink($id),
        'type' => $post_type,
    ];
}
add_filter('algolia_post_to_record', 'algolia_post_to_record');

