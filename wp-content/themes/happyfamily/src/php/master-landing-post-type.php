<?php

function add_master_landing_post_type() {
		// Set UI labels for Custom Post Type
    $labels = array(
			'name'                => _x( 'Mater Landing Posts', 'Post Type General Name', 'twentythirteen' ),
			'singular_name'       => _x( 'Mater Landing Post', 'Post Type Singular Name', 'twentythirteen' ),
			'menu_name'           => __( 'Master Landing Posts', 'twentythirteen' ),
	);

	// Set other options for Custom Post Type

	$args = array(
			'label'               => __( 'mater landing posts', 'twentythirteen' ),
			'description'         => __( 'Master Landing Posts', 'twentythirteen' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
			// You can associate this CPT with a taxonomy or custom taxonomy.
			'taxonomies'          => array( 'genres' ),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position' => 5,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
			'rewrite' => array(
					'slug' =>'our-mission',
					"with_front" => false
			),

	);

	// Registering your Custom Post Type
	register_post_type( 'mater landing posts', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'add_master_landing_post_type', 0 );

add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
    if ( is_home() && $query->is_main_query() )
        $query->set( 'post_type', array( 'post', 'mater landing posts' ) );
    return $query;
}
