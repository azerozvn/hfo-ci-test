<?php

function get_asset_from_manifest($asset_name) {
  $manifest = file_get_contents(get_stylesheet_directory() . '/dist-webpack/manifest.json');
  $manifest = json_decode($manifest, true); //decode json string to php associative array
  if (!isset($manifest[$asset_name])) return $asset_name; //if manifest.json doesn't contain $asset_name then return $asset_name itself
  return $manifest[$asset_name];
}

// Stylesheets and Javascript
function theme_scripts_styles() {
  // Generate random hash at build time, for cache-busting non-webpack JS and CSS
  $webpack_version_number = file_get_contents(get_stylesheet_directory() . '/dist-webpack/build-version.txt');
  
  // Syntax: 
  // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer )
  // wp_enqueue_style( $handle, $src, $deps, $ver, $media )

  // Theme registration stylesheet
  wp_enqueue_style( 'registration-stylesheet', get_stylesheet_directory_uri() . '/style.css', $webpack_version_number );

  // Slick Slider
  // hfo-marketing-script (slick carousel library)
  wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/dist/slick.min.js', array('jquery'), $webpack_version_number, true );

  // Madwell JS and CSS
  wp_enqueue_script( 'madwell-js', get_asset_from_manifest('madwell-js.js'), array( 'jquery' ), $webpack_version_number, true );
  wp_enqueue_style( 'madwell-css', get_asset_from_manifest('madwell-css.css'), $webpack_version_number);
  
  // Bukwild JS and CSS
  wp_enqueue_style( 'shopstyles', get_asset_from_manifest('shop-css.css'), array(), $webpack_version_number);
  wp_enqueue_script( 'shopjs', get_asset_from_manifest('shop-js.js'), array(), $webpack_version_number, true );

  // Search page JS and CSS (add to search results page only)
  if ( basename(get_page_template()) == 'page-search.php' ) {
      wp_enqueue_script( 'searchjs', get_asset_from_manifest('search-js.js'), array(), $webpack_version_number, true );
      wp_enqueue_style( 'searchstyles', get_asset_from_manifest('search-css.css'), array(), $webpack_version_number);
  }

  wp_localize_script( 'js', 'ajax_params', [
      'url'   => admin_url( 'admin-ajax.php' )
  ] );

  // Add store URL as JS variable
  wp_localize_script('js', 'shopDomain', get_theme_mod('shop_url'));
}

add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );


// Enqueue custom vc scripts if shortcode is used
// hfo-marketing-script (UI/component library scripts)
function custom_vc_component_scripts() {
  $webpack_version_number = file_get_contents(get_stylesheet_directory() . '/dist-webpack/build-version.txt');
  global $post;
  if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'allergy_filter') ) {
      wp_enqueue_script( 'allergy-filter', get_stylesheet_directory_uri() . '/components/assets/js/allergy-filter.js', array('js'), $webpack_version_number, true );
  }
  if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'madwell_productcarousel') ) {
      wp_enqueue_script( 'products-carousel', get_stylesheet_directory_uri() . '/components/assets/js/product-carousel.js', array('js'), $webpack_version_number, true );
  }
}
add_action( 'wp_enqueue_scripts', 'custom_vc_component_scripts');

// Remove Contact Form 7 scripts/styles, only enqueue in page-contact.php
add_filter( 'wpcf7_load_css', '__return_false' );
add_filter( 'wpcf7_load_js', '__return_false' );
