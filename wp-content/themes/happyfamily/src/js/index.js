import 'focus-visible'
import './lib/polyfills.js'
import './lib/modernizr.js'
import './single-products.js'
import './taxonomy-product_category.js'
import './destini.js'
import './search/mount.js'

// console.log('index.js')