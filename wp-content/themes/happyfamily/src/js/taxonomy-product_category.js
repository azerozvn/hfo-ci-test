// Scripts for taxonomy-product_category.php (PLP page)

function plp_init() {
    /* 
    Quick summary of how the PLP filters work:
    - As you can tell, it's vanilla javascript.  There's no global store.
    - What is the source of truth?  
        - "window.location" is the source of truth 1) immediately after pageload, and 2) immediately after a window location change event.
        - At all other times, the checkboxes are the source of truth.
    - On page load:
        1) PHP fetches and renders the full unfiltered list of products.
        2) JS grabs the list of active filters from window.location (if any), and updates the checkboxes and the product list accordingly.
        3) When the user clicks a checkbox, JS updates the window.location and the product list.
    */


    ///////////////////////////////////////////////////////////////////////////
    // MOUNTED

    onMount();

    ///////////////////////////////////////////////////////////////////////////
    // METHODS

    function onMount() {
        // If this page has no filter, bail.
        if( !document.querySelector('#plp-filter') ) { return };
        
        // Bind checkbox listeners
        const checkboxes = document.querySelectorAll("#plp-filter .term input[type=checkbox]");
        checkboxes.forEach(function(checkbox) {
            checkbox.addEventListener('change', onCheckboxChange)
        })

        // Bind "Clear All" button
        document.querySelector('#plp-filter-clear-all').addEventListener('click', clearAllFilters);
        document.querySelector('#plp-filter-clear-all-2').addEventListener('click', clearAllFilters);

        // Update checkboxes to match the window URL
        getFiltersFromWindow();

        // Listen for location change
        window.addEventListener('popstate', function(){
            getFiltersFromWindow();
        })

        mobileFilter_init();
    }

    function onCheckboxChange() {
        // console.log('onCheckboxChange');
        const activeFilters = getFilters();
        setCheckboxes(activeFilters);
        setWindowLocation(activeFilters);
        // console.log({activeFilters});
        // window.activeFilters = activeFilters;
    }
    
    function clearAllFilters() {
        // console.log('clearAllFilters');
        setCheckboxes({});
        setWindowLocation({});
    }

    // Get "activeFilters" object from checkbox checked states
    function getFilters() {
        const groups = document.querySelectorAll('#plp-filter .filter-group');
        let activeFilters = {}
        groups.forEach(function(group) {
            const groupCheckboxes = group.querySelectorAll('.term input');
            checked = Array.from(groupCheckboxes) // Convert checkboxes to an array to use filter and map.
                .filter(i => i.checked) // Use Array.filter to remove unchecked checkboxes.
                .map(i => i.value) // Use Array.map to extract only the checkbox values from the array of objects.
            if (!checked.length) { return }
            activeFilters[ group.getAttribute('data-slug') ] = checked;
        })
        return activeFilters;
    }
    
    // Get "activeFilters" object from the window location.
    function getFiltersFromWindow() {
        // console.log('getFiltersFromWindow');
        if(!window.location.search) {
            setCheckboxes({});
            return;
        }
        const search = window.location.search.split('?').join('');
        let activeFilters = {};
        search.split('&').forEach(function(item) { 
            item = item.split("=");
            item[1] = item[1].split("%2C");
            activeFilters[ item[0] ] = item[1];
        })
        
        // Set checkboxes based on activeFilters
        setCheckboxes(activeFilters);
    }

    // Set checkbox checked states to match "activeFilters" object
    function setCheckboxes(activeFilters) {
        // console.log('setCheckboxes', activeFilters);
        const products = document.querySelectorAll('#plp-list .product-wrap');
        const allCheckboxes = document.querySelectorAll('#plp-filter .term input');
        const filtersSelectedOuter = document.querySelector('#plp-filter .filters-selected-wrap');
        const filtersSelectedInner = document.querySelector('#plp-filter .filters-selected');
        const noMatchMessage = document.querySelector('#plp-list .filter-no-match');

        // Set all checkboxes unchecked
        allCheckboxes.forEach(function(checkbox) {
            if(!checkbox) {return}
            checkbox.checked = false;
        })

        let filtersSelected = [];

        // Set active checkboxes checked
        Object.keys(activeFilters).forEach(function(group) {
            activeFilters[group].forEach(function(slug) {
                checkbox = document.querySelector(`#plp-filter .filter-group[data-slug="${group}"] input[value="${slug}"]`);
                if(!checkbox) {return}
                checkbox.checked = true;
                filtersSelected.push( {slug, label: checkbox.getAttribute('aria-label') } )
            })
        });
        
        // Render filter count on the mobile title.
        document.querySelector('#plp-filter .title-mobile .count').innerHTML = filtersSelected.length;
        
        // Render filter count on the mobile Apply/Close button.
        document.querySelector('#plp-filter .plp-filter-close').innerHTML = filtersSelected.length ? `Apply (${filtersSelected.length})` : `Close`;

        
        // Special case: All taxonomy terms in the URL are invalid.  
        // Example: "/shop?product_group=974" (a discontinued product group)
        // If this happens, the code above will remove them all, which will show all products.  
        // Instead, let's keep the invalid URL and show "No matches".
        if(Object.keys(activeFilters).length && !filtersSelected.length) {
            // Hide all products
            products.forEach(function(product) {
                product.classList.add('hide');
            })
            // Show "No matches".
            noMatchMessage.classList.remove('hide');
            // Hide "Filters Selected"
            filtersSelectedOuter.classList.add('hide');
            return;
        }

        // If no filters selected, bail early.
        if(!filtersSelected.length) {
            // Show all products
            products.forEach(function(product) {
                product.classList.remove('hide');
            })
            // Hide "Filters Selected"
            filtersSelectedOuter.classList.add('hide');
            noMatchMessage.classList.add('hide');
            return;
        }

        // Filter the product list.
        products.forEach(function(product) {
            const taxonomies = ['age_range', 'lifestage', 'product_group', 'allergies', 'product_line'];
            
            // Version 1: This logic requires that only one taxonomy in activeFilters contain at least one match in a product.
            // const match = taxonomies.some(function (taxonomy) {
            //     const productTerms = product.getAttribute(`data-${taxonomy}`)?.split(',');
            //     return productTerms.some(slug => activeFilters[taxonomy]?.indexOf(slug) >= 0)
            // });
            
            // Version 2: Stricter, but this is how the old HFO website filters worked. This logic requires that all taxonomies in activeFilters contain at least one match in a product.
            const taxonomyMatches = taxonomies.map(function (taxonomy) {
                // Return true if activeFilters doesn't contain this taxonomy.
                if( !activeFilters[taxonomy] ) { return true } 
                const productTerms = product.getAttribute(`data-${taxonomy}`)?.split(',');
                // Return true if this product doesn't contain this taxonomy.
                // if( productTerms.length == 1 && productTerms[0] == '' ) { return true };
                // If activeFilters and this product both contain this taxonomy, then return true if there's at least one match.
                return productTerms.some(slug => activeFilters[taxonomy]?.indexOf(slug) >= 0)
            });
            // Return true only if every element is true
            const match = taxonomyMatches.every(function (taxonomy) { return taxonomy })
            // console.log('filter products', {taxonomyMatches, match});
            match ? product.classList.remove('hide') : product.classList.add('hide');
        })
        // If no products match filter, show message.
        if( document.querySelectorAll('#plp-list .product-wrap:not(.hide)').length == 0 ) {
            noMatchMessage.classList.remove('hide');
        } else {
            noMatchMessage.classList.add('hide');
        }

        // Render "Filters Selected" above checkboxes.
        // console.log('setCheckboxes', {activeFilters});
        filtersSelectedInner.innerHTML = 
            filtersSelected.map(function(filter) {
                return `<div class="filter-selected">${filter.label} <button class="text-btn" data-slug="${filter.slug}" aria-label="Remove filter: ${filter.label}">✖️</button></div>`;
            })
            .join('');
        
        // Bind click events
        const filtersSelectedItems = document.querySelectorAll('#plp-filter .filter-selected button');
        // console.log('filterSelectedItem binding events...', filtersSelectedItems);
        filtersSelectedItems.forEach(function(item) {
            item.addEventListener('click', function(event) {
                // console.log('filterSelectedItem click', event);
                const slug = event.target.getAttribute('data-slug');
                const checkbox = document.querySelector(`#plp-filter input[value="${slug}"]`);
                if(!checkbox) {return}
                checkbox.checked = false;
                onCheckboxChange();
            })
        })
        filtersSelectedOuter.classList.remove('hide');
    }

    // Set the window location to match "activeFilters" object
    function setWindowLocation(activeFilters) {
        // Convert "activeFilters" into a URL param format.  We do this to match the old site's Magento shop URLs.
        const search = Object.keys(activeFilters)
            .map(function(slug) {
                return `${slug}=${activeFilters[slug].join('%2C')}`;
            })
            .join('&');
        const questionMark = !!search ? '?' : ''; // Don't add questionMark if search is empty.
        const newLocation = window.location.origin + window.location.pathname + questionMark + search;
        history.pushState({id:'store'}, 'Store', newLocation);
    }

    function mobileFilter_init() {
        // console.log('mobileFilter_init');
        const groups = document.querySelectorAll('#plp-filter .filter-group');
        const buttons = document.querySelectorAll('#plp-filter .group-title.mobile');
        // Open the first group
        firstSlug = groups[0].getAttribute('data-slug');
        mobileFilter_update(firstSlug);
        // Bind group click events
        buttons.forEach(function(button) {
            button.addEventListener('click', function(event) {
                // console.log('mobileFilter click:', event.target);
                newSlug = event.target.getAttribute('data-slug');
                mobileFilter_update(newSlug);
            })
        })
        // Bind button events
        document.querySelector('#plp-filter .title-mobile').addEventListener('click', mobileFilter_toggle);
        document.querySelector('#plp-filter .buttons-mobile .plp-filter-clear').addEventListener('click', clearAllFilters);
        document.querySelector('#plp-filter .buttons-mobile .plp-filter-close').addEventListener('click', mobileFilter_toggle);
    }

    function mobileFilter_update(newSlug) {
        const groups = document.querySelectorAll('#plp-filter .filter-group');
        const oldSlugEl = document.querySelector('#plp-filter .filter-group.open');
        const oldSlug = oldSlugEl && oldSlugEl.getAttribute('data-slug');
        
        // Close all groups
        groups.forEach(function(group) {
            group.classList.remove('open');
            group.classList.add('closed');
        })
        
        // Return if we clicked on the active group (close all groups)
        if (newSlug == oldSlug) { return }
        
        // Open new group
        newGroup = document.querySelector( `#plp-filter .filter-group[data-slug="${newSlug}"]` );
        if(!newGroup) { return }
        newGroup.classList.remove('closed');
        newGroup.classList.add('open');
    }

    function mobileFilter_toggle() {
        const filter = document.querySelector('#plp-filter');
        if( filter.classList.contains('open') ) {
            filter.classList.remove('open');
            filter.classList.add('closed');
        } else {
            filter.classList.remove('closed');
            filter.classList.add('open');
        }
    }
}

document.addEventListener("DOMContentLoaded", plp_init);