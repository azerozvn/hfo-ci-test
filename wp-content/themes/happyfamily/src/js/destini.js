// Destini Popup

// Docs: http://destinidemo.com/pdpwidgetdemo/happyfamily/1957301175

const bodyScrollLock = require('body-scroll-lock');
const disableBodyScroll = bodyScrollLock.disableBodyScroll;
const enableBodyScroll = bodyScrollLock.enableBodyScroll;

function destini_init() {

    ////////////////////////////////////////////////////////////////////
    // METHODS
    function buyNowClickHandler(event) {
        const apo = event.target.getAttribute('data-destini-apo')
        showWidget(apo)
    }
    
    function showWidget(apo) {
        var containerId = 'destini-div'
        var associationId = 'HappyFamilyOrganicsStoreLocator'
        destini.init(containerId)
        destini.loadWidget(associationId, apo)
    }

    function hideModal() {
        $('#widget-modal').modal('hide');
    }

    ////////////////////////////////////////////////////////////////////
    // MOUNTED
    const modal = document.querySelector('#widget-modal');
    const closeBtn = document.querySelector('#widget-modal-close');

    // If no elements, bail
    if(!modal || !closeBtn) { return }

    // "Buy Now" button click
    const buttons = document.querySelectorAll('.buynow')
    buttons.forEach(function(button) {
        button.addEventListener('click', buyNowClickHandler);
    })

    // Modal show/hide (enable body scroll lock)
    $(window).on('load',function(){

        $('#widget-modal').on('show.bs.modal', function (e) {
            disableBodyScroll(modal);
            closeBtn.classList.remove('hide');
        })
        $('#widget-modal').on('hidden.bs.modal', function (e) {
            enableBodyScroll(modal);
            closeBtn.classList.add('hide');
        })
    });

    // "Close" click events
    // Must add this way because modal-backdrop does not exist until after bootstrap modal is shown.
    // "shown.bs.modal" event would be nice, but is never fired.  This is a workaround.
    document.addEventListener('click', function(event) {
        if(
            event.target.getAttribute('id') == 'widget-modal-close' || // Close "X"
            event.target.classList.contains('modal-body') || // Modal bkg
            event.target.classList.contains('modal-backdrop') // Other modal bkg
        ) {
            hideModal();
        }
    })


    // If url is "#buy-now", then open widget immediately.
    if( window.location.hash == "#buy-now" ) {
        if( !document.body.classList.contains('single-products') ) { return }
        const apo = document.querySelector('.buynow').getAttribute('data-destini-apo')
        $(window).on('load',function(){
            showWidget(apo);
            // Show bootstrap modal manually (safer inside window.on(load))
            $('#widget-modal').modal('show');
        });
    }
}

document.addEventListener("DOMContentLoaded", destini_init)