
function productSliderInit() {
	const slideWrap = document.querySelector('#product-image .slide-wrap');
	const slides = document.querySelectorAll('#product-image .slide');
	
	if(!slideWrap || !slides.length) {
		// console.warn('Flickity: slideWrap or slides were null.  Exiting.', {slideWrap, slides});
		return;
	}
	
	var Flickity = require('flickity');
	
	var slider = new Flickity( slideWrap, {
		wrapAround: true,
	});

	// Thumbs: Bind click events
	const thumbs = document.querySelectorAll('#product-image .thumb');
	thumbs.forEach(function(thumb) {
		thumb.addEventListener('click', function(event) {
			// In IE11, event.target is button.  In all others, event.target is img.  That's why I put data-key on both.
			slider.select( event.target.getAttribute('data-key'), false );
		})
	})

	// Manually resize flickity when image loads, because Flickity's imagesLoaded setting doesn't work.
	// This fixes bug where slider image doesn't appear for slow connections (Fast 3G)
	const slideImages = document.querySelectorAll('#product-image .slide img');
	slideImages.forEach(function(slideImage) {
		slideImage.addEventListener('load', (event) => {
			slider.resize();
		});
	});


	// Add "active" to first thumb.
	thumbs[0].classList.add('active');

	// On change, update active thumbnail.
	slider.on('change', function (index) {
		thumbs.forEach(function(thumb) {
			thumb.classList.remove('active');
		})
		thumbs[index].classList.add('active');
	})
}

productSliderInit();
