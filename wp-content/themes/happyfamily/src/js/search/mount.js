// console.log('mount.js')

function siteSearchHeader() {
    const mobileNavButton = document.querySelector('.nav-toggle')
    const mobileSearchButton = document.querySelector('.mobile-search-icon')
    const mobileSearchWrap = document.querySelector('.mobile-search .search__form, .mobile-search-space')
    const mobileSearchInput = document.querySelector('.mobile-search form')
    const desktopSearchInput = document.querySelector('.desktop-search form')
    const isSearchPage = window.location.pathname=='/search/'
    let isMobileSearchOpen = false

	///////////////////////////////////////////////////////////////////////////
    // MOUNTED

    onMount()
	function onMount() {
        // console.log('mount.js onMount', {isSearchPage})
        // Mobile: Show/hide global navigation.
        mobileNavButton.addEventListener('click', function(){
            toggleMobileNav('toggle')
        })
        
        desktopSearchInput.addEventListener('submit', handleSubmit)
        mobileSearchInput.addEventListener('submit', handleSubmit)
        
        desktopSearchInput.addEventListener('input', handleChange)
        mobileSearchInput.addEventListener('input', handleChange)
        
        if(isSearchPage) {
            // Search results page
            toggleMobileSearch('open')
            // Disable the button
            mobileSearchButton.setAttribute('disabled', '')
        } else {
            // Not the search results page
            // Clicking outside closes search field
            document.body.addEventListener('click', handleClickOutside)
            // Mobile: On icon click, show/hide the search input.
            mobileSearchButton.addEventListener('click', toggleMobileSearch)
        }
        

        // Run once on mount just in case input already has text.
        handleChange()
    }

	///////////////////////////////////////////////////////////////////////////
    // METHODS

    // Open and close the mobile search input
    function toggleMobileSearch(newState) {
        // console.log('toggleMobileSearch', {newState, isSearchPage})
        if(isMobileSearchOpen || newState=='closed') {
            // Close mobile search
            mobileSearchWrap.classList.remove('expanded')
            mobileSearchButton.classList.remove('expanded')
            mobileSearchButton.setAttribute('aria-expanded', 'true')
            isMobileSearchOpen = false
        } else {
            // Open mobile search
            mobileSearchWrap.classList.add('expanded')
            mobileSearchButton.classList.add('expanded')
            mobileSearchButton.setAttribute('aria-expanded', 'false')
            isMobileSearchOpen = true
            toggleMobileNav('closed')
        }
    }
    
    // Open and close the mobile hamburger menu
    function toggleMobileNav(newState) {
        // console.log('toggleMobileNav', {newState, isSearchPage})
        const icon = $('.nav-toggle')
		const container = $('.menus-container')
		const bodyEl = $('body')
		const oldState = container.hasClass('expanded') ? 'open' : 'closed'

        if(newState=='open' || (newState=='toggle' && oldState=='closed') ) {
			// Open menu
			bodyEl.addClass('pushNotificationFixed')
			icon.addClass('expanded')
			container.addClass('expanded')
            if(!isSearchPage) {
                toggleMobileSearch('closed')
            }
		} else {
            // Close menu
			bodyEl.removeClass('pushNotificationFixed')
			icon.removeClass('expanded')
			container.removeClass('expanded')
            if(isSearchPage && !isMobileSearchOpen) {
                toggleMobileSearch('open')
            }
		}
	}

    function handleClickOutside(event) {
        // console.log('handleClickOutside', event.target)
        // Close the mobile search if we click outside the header.
        if (isMobileSearchOpen && !document.querySelector('#header').contains(event.target)) {
            toggleMobileSearch('closed')
        }
    }

    function handleSubmit(event) {
        event.preventDefault()
        // Add submit handlers everywhere except the search page.  The search page has its own handlers.
        const pathname = window.location.pathname.split('/').join('')
        if(pathname!='search') {
            const query = event.target.querySelector('input').value
            // console.log('handleSubmit', window.location.pathname, query)
            window.location.href = '/search?q=' + encodeURIComponent(query)
        }
    }

    function handleChange(event) {
        isMobile = mobileSearchInput.contains(event?.target)
        input = isMobile ? mobileSearchInput : desktopSearchInput
        oppositeInput = isMobile ? desktopSearchInput : mobileSearchInput
        // console.log('handleChange', {isMobile, input})
        
        if(input.querySelector('input').value.length) {
            desktopSearchInput.classList.add('has-text')
            mobileSearchInput.classList.add('has-text')
        } else {
            desktopSearchInput.classList.remove('has-text')
            mobileSearchInput.classList.remove('has-text')
        }
        // Set value of the opposite input, so both inputs always have same value.
        oppositeInput.querySelector('input').value = input.querySelector('input').value
    }
}

document.addEventListener("DOMContentLoaded", siteSearchHeader)