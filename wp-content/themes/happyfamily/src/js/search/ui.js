export const uiMarkup = `
	<div id="site-search">
		<div class="container">

			<header class="site-search_header row">

				<div class="site-search_heder-left col">
					<div class="site-search_categories-widget">
						<img src="/wp-content/themes/amplitude-blog/images/icons/arrow-down-select.svg" class="site-search_arrow-down">
					</div>
				</div>

				<div class="site-search_header-right col-sm-6">
					<div class="site-search_stats-widget"></div>
					<div class="site-search_search-widget"></div>
				</div>

			</header>

			<div class="site-search_results-widget"></div>
		</div>
	</div>
`

export const resultCard = `
	{{#isProduct}}
		<div class="item type-product">
			<a class="item-image" href="{{ url }}" style="background-image:url('{{ image }}')" aria-label="{{ title }}"></a>
			
			<div class="item-terms">
				{{#category}}<span class="term term-category color-{{ category_color }}">{{ category }}</span>{{/category}}
				{{#product_line}}<span class="term term-product-line">{{ product_line }}</span>{{/product_line}}
			</div>
			
			<a class="item-title" href="{{ url }}" tabindex="-1">
				{{ title }}
			</a>

			<div class="item-packaging">
				{{#packaging_size}}<span class="packaging">{{ packaging_size }}</span>{{/packaging_size}}
				{{#price_per_quantity}}<span class="price-per-quantity">{{ price_per_quantity }}</span>{{/price_per_quantity}}
			</div>

			<div class="price-and-buy">
				<a href="{{ url }}#buy-now" class="btn small outline arrow product-buy">Buy Now</a>
			</div>

		</div>
	{{/isProduct}}

	{{#isArticle}}
		<div class="item type-article learning-center-card learning-center-carousel blue">
			<a class="learning-center-card__link" href="{{ url }}" alt="{{ title }}">
				<img class="learning-center-card__image" src="{{ image }}" alt="{{ title }}" />
			</a>
			<div class="learning-center-card__details">
				<p class="learning-center-card__breadcrumb"><strong>{{ category }}</strong></p>
				<h4 class="learning-center-card__title">
					<a href="{{ url }}" alt="{{ title }}" tabindex="0">{{ title_short }}</a>
				</h4>
				<p class="learning-center-card__excerpt">{{ excerpt_short }}</p>
			</div>
		</div>
	{{/isArticle}}

	{{#isRecipe}}
		<div class="item type-recipe learning-center-card learning-center-carousel purple">
			<a class="learning-center-card__link" href="{{ url }}" alt="{{ title }}">
				<img class="learning-center-card__image" src="{{ image }}" alt="{{ title }}" />
			</a>
			<div class="learning-center-card__details">
				<p class="learning-center-card__breadcrumb"><strong>{{ category }}</strong></p>
				<h4 class="learning-center-card__title">
					<a href="{{ url }}" alt="{{ title }}" tabindex="0">{{ title_short }}</a>
				</h4>
				<p class="learning-center-card__excerpt">{{ excerpt_short }}</p>
			</div>
		</div>
	{{/isRecipe}}
`

