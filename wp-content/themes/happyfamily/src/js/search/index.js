// This bundle is loaded only on "/search" page.

// Deps
import algoliasearch from "algoliasearch/lite"
import instantsearch from "instantsearch.js"
import {
	configure,
	searchBox,
	stats,
	infiniteHits
} from "instantsearch.js/es/widgets"
import {connectRefinementList} from "instantsearch.js/es/connectors"
import { resultCard } from './ui.js'

function siteSearchMain() {
	const searchInputDesktop = document.querySelector('.desktop-search .ais-SearchBox input')
	const searchInputMobile = document.querySelector('.mobile-search .ais-SearchBox input')
	const hideProductsStr = '&noproducts' // If added to end of URL, hide products.
	const tabs = ['products', 'articles', 'recipes']
	
	// Map each URL param to the corresponding Algoila refinement.
	const tabRefinements = {
		products: 'product',
		articles: 'article',
		recipes: 'recipe'
	}

	let hasInitialized = false
	let algoliaSearch = null
	const indexName = getIndexName()	
	let lastQuery = null
	let hideProducts = false
	let preferredTab = 'products' // If URL contains a preferred tab, store it in this variable
	let tabFallbackEnabled = true // If true, automatically switch to another tab if active tab has no results.

	///////////////////////////////////////////////////////////////////////////
	// MOUNT

	onMount()	
	function onMount(desktopOrMobile) {
		initializeSiteSearch();
		// Add UI events
		window.addEventListener('popstate', handlePopState)	
	}
	
	///////////////////////////////////////////////////////////////////////////
	// METHODS
	
	// One time initializer.  Fired only once, when the search results are shown.
	function initializeSiteSearch() {
		if (hasInitialized) return
		initAlgolia()
		hasInitialized = true
	}
	
	// Boot up the Algolia config
	function initAlgolia() {
		// For the initial query, use 1) any text the user already typed into the input, 2) fallback to query from the URL, if any
		const userQuery = $('.desktop-search .ais-SearchBox-input').val() || getQueryFromUrl()
		
		// Search Box Widget
		const searchBoxWidgetDesktop = searchBox({
			container: ".desktop-search #algolia-searchbox",
			placeholder: $('.desktop-search .ais-SearchBox-input').attr("placeholder") || "Search",
			searchAsYouType: false,
			showSubmit: true,
			showReset: false,
			showLoadingIndicator: false,
		})
		
		const searchBoxWidgetMobile = searchBox({
			container: ".mobile-search #algolia-searchbox",
			placeholder: $('.mobile-search .ais-SearchBox-input').attr("placeholder"),
			searchAsYouType: false,
			showSubmit: true,
			showReset: false,
			showLoadingIndicator: false,
		})
	
	
		// Refinement List Widget (Products, Articles, Recipes & Meal Plans)
		const renderRefinementList = (renderOptions, isFirstRender) => {
			const { items } = renderOptions
			const tabs = [...document.querySelectorAll(`#search-results .menu-select .tab`)]
			const query = algoliaSearch?.helper?.state?.query
			const hasResults = !!algoliaSearch?.helper?.lastResults?.hits?.length
			// console.log('renderRefinementList', {query, algoliaSearch, isFirstRender})

			// Update the header ("Search Results for...")
			// Would love to use the Stats Widget for this, but can't because 
			// when query was "supplementing", it rendered "formula breastfeeding" because of one of our rules,
			// whereas getting the query straight from the algoliaSearch object returned "supplementing", which is what we want.
			let str = ''
			if(!algoliaSearch?.helper?.state) {
				// Before Algolia inits, keep this empty to prevent flash. 
				str = ''
			} else if(!query) {
				str = 'Enter your search terms.'
			} else if(!hasResults) {
				str = `No results for “${query}”`
			} else {
				str = `Search results for “${query}”`
			}
			document.querySelector('#search-results .stats-widget').innerHTML = `<h1>${str}</h1>`
			
			if(isFirstRender) {
				tabs.forEach(tab => {
					
					const tabClickHandler = event => {
						tabFallbackEnabled = false
						// Our event.target could be ".tab", the span inside it.
						const refinement = $(event.target).closest('.tab').attr('data-refinement') // Use jQuery because it has IE11 shim for closest() 
						const label = $(event.target).closest('.tab').attr('data-label') // Use jQuery because it has IE11 shim for closest() 
						const newQuery = algoliaSearch.helper.state.query
						// console.log('tab click', {label, newQuery})
						algoliaSearch.setUiState({
							[indexName]: {
								query: newQuery,
								refinementList: {
									type: [refinement],
								},
							}
						})
						preferredTab = label
						setUrlFromState(true)
					}

					// Bind tab click event
					tab.addEventListener('click', tabClickHandler) // useCapture: true
				})			
				return
			}
					
			// Update counts and selected
			tabs.forEach(tab => {
				// Get matching Algolia attribute
				const tabLabel = tab.getAttribute('data-refinement')
				let item = items.filter(item => {return item.label == tabLabel})[0]
	
				if(!item) {
					// If a refinement has no hits, Algolia removes it.  Manually return a refinement with count=0.
					item = {count: 0}
				}
	
				// Update tab count and selected state
				if(item.isRefined) {
					tab.classList.add('selected')
				} else {
					tab.classList.remove('selected')
				}
				tab.querySelector('.count').innerText = item.count
				
				// Update "no results" element
				const noResultsEl = document.querySelector(`#search-results .no-results.${tabLabel}`)
				if(item.isRefined && !item.count) {
					noResultsEl.classList.add('show')
				} else {
					noResultsEl.classList.remove('show')
				}
				// console.log('render tabs', {tabLabel, tab, item})
			})

			// If no results, show "no-results all" element
			const noResultsAll = document.querySelector(`#search-results .no-results.all`)
			if(items.length == 0) {
				noResultsAll.classList.add('show')
			} else {
				noResultsAll.classList.remove('show')
			}
			
			// Get the active tab from the current Algolia state
			const activeTab = items.find(item => { return item.isRefined })
			
			// If we have results, but no results in the current tab
			if( tabFallbackEnabled && items.length != 0 && activeTab.count == 0 ) {
				// Specify what tabs to fallback on.
				let fallbackOrder
				if(activeTab.label == 'articles') {
					fallbackOrder = ['recipes', 'products']
				} else if(activeTab.label == 'recipes') {
					fallbackOrder = ['articles', 'products']
				} else {
					fallbackOrder = ['articles', 'recipes']
				}
				
				// Find the first tab that has results
				const firstTabWithResults = fallbackOrder.find(label => {
					const refinement = tabRefinements[label] 
					// If hideProducts==true, then never choose the product tab
					const item = items.find(item=>{return item.label==refinement})
					return !!item?.count
				})
				
				const firstItemWithResults = tabRefinements[firstTabWithResults]

				if(firstItemWithResults) {
					// Change the active tab by setting Algolia state
					algoliaSearch.setUiState({
						[indexName]: {
							query: query,
							refinementList: {
								type: [firstItemWithResults],
							},
						}
					})				
				}
				// console.log('no results in the active tab', {firstTabWithResults, label})
			}

			// Change the current tab by setting the Algolia state with a new refinement.
			// const setTab = (newLabel) => {
			// }

			// console.log('render tabs done. ', {algoliaSearch, renderOptions, activeTab, firstTabWithResults})	
			// console.log('render tabs done. ', {query, items, activeTab})		
			lastQuery = query
		}
		
		const customRefinementList = connectRefinementList(renderRefinementList)
	
		// Infinite Hits Widget (Search Results)
		const infiniteHitsWidget = infiniteHits({
			container: "#search-results .infinite-hits-widget",
			transformItems: items => items.map(item => {
				const result = {
					...item,
					isProduct: item.type=="product",
					isArticle: item.type=="article",
					isRecipe: item.type=="recipe",
					// "item.category" might be a string, or an array of multiple categories.  Handle both scenarios.
					category: Array.isArray(item.category) ? item.category[0] : item.category,
					product_line: htmlDecode(item.product_line)
				}
				// console.log('transformItems', result)
				return result
			}),
			cssClasses: {
				loadMore: ['btn', 'small'],
			},
			templates: {
				item: resultCard,
				showMoreText: 'Load More',
			}
		})
	
		const searchClient = algoliasearch(
			"LRSU9YV1PY",
			"f8a6e5a97f2292094c89031817990488"
		)

		algoliaSearch = instantsearch({
			indexName: indexName,

			searchClient,
			initialUiState: {
				[indexName]: {
					query: userQuery,
					refinementList: {
						// For initial state, use the preferredTab variable.
						type: [ tabRefinements[preferredTab] ],
					},
				}
			},
			onStateChange({ uiState, setUiState }) {				
				hideProducts = false // Remove hideProducts restriction on the first new query
				setUrlFromState()
				setUiState(uiState)
				setSearchFocus()
			},
		})
	
		algoliaSearch.addWidgets([
			configure({ 
				hitsPerPage: 24,
				// On page load, if URL contains '&noproducts', filter out all products.
				// filters: hideProducts ? 'type:article OR type:recipe' : undefined
			}),
			searchBoxWidgetDesktop,
			searchBoxWidgetMobile,
			customRefinementList({
				attribute: 'type'
			}),
			infiniteHitsWidget
		])
		
		algoliaSearch.start()
		// window.algoliaSearch = algoliaSearch  // DEBUG
		
		// Manually dispatch an "input" event on the header search input.
		// This is because if the page loads and a search query exists in the URL, when Algolia inits it adds text into
		// the header search input.  If `mount.js` runs before Algolia, it won't see the text, and it won't set 
		// the correct icon (caret icon instead of magnifying glass).
		document.querySelector('.desktop-search form input').dispatchEvent(new Event('input', {bubbles: true, cancelable: true}))
	}
	
	function handlePopState(event) {
		const query = getQueryFromUrl()
		// console.log('handlePopState', query, algoliaSearch)
		algoliaSearch.setUiState({ [indexName]: { query: query, refinementList: {type: [algoliaSearch.helper.state.disjunctiveFacetsRefinements.type]} }})
	}

	function getBreakpoint() {
		return window.innerWidth < 1024 ? 'mobile' : 'desktop'
	}

	function setSearchFocus() {
		getBreakpoint() == 'mobile' ? searchInputMobile.focus() : searchInputDesktop.focus()
	}
	
	function getQueryFromUrl() {
		// "Tab" URL param
		preferredTab = window.location.search.split('&tab=')[1]
		// If not a valid tab, fallback to first tab ('products')
		if( tabs.indexOf(preferredTab) == -1 ) {
			preferredTab = tabs[0]
		}

		// Remove '&noproducts' from the end, if present.  Return the string after '?q='
		let str = window.location.search.split('&tab=')[0]?.split('?q=')[1]
		// Convert "%20" to spaces
		str = decodeURI(str)
		return str
	}

	function setUrlFromState(shouldReplace) {
		const query = algoliaSearch.helper.state.query
		const newUrl = `/search/${query || preferredTab ? '?q=' + query : ''}${preferredTab ? `&tab=${preferredTab}` : ''}`
		if(shouldReplace) {
			history.replaceState({}, null, newUrl)
			return
		}
		history.pushState({}, null, newUrl)
	}

	// Convert HTML characters ("&amp;") into pretty characters ("&")  https://stackoverflow.com/a/1912522
	function htmlDecode(input){
		var e = document.createElement('textarea');
		e.innerHTML = input;
		// handle case of empty input
		return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
	  }
	
	// Reset Algolia state
	// function resetSearch() {
	// 	algoliaSearch.setUiState({ [indexName]: { query: '', refinementList: {type: ['product']} }})
	// }	
	
	// Get the index to search
	function getIndexName() {
		// return 'searchable_posts_prod' // DEBUG: Hardcode index

		switch (window.location.host) {
			case 'hfo02.localdev':
				// console.log('using index: searchable_posts_local')
				return 'searchable_posts_local'
			case 'hfodev2.wpengine.com':
			case 'staging.happyfamilyorganics.com':
				// console.log('using index: searchable_posts_staging')
				return 'searchable_posts_staging'
			default:
				// console.log('using index: searchable_posts_prod')
				return 'searchable_posts_prod'
		}
	}
	
}


document.addEventListener("DOMContentLoaded", siteSearchMain)