<!DOCTYPE html>

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<html <?php language_attributes(); ?>>

<head>
	
<title>
	<?php wp_title( '-', true, 'right' ); ?>
</title>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php
$prodUrl = 'https://www.happyfamilyorganics.com';
$stageUrl = 'https://staging.happyfamilyorganics.com';
$currentUrl = get_site_url();
if($currentUrl === $stageUrl) {
?>
<meta name="robots" content="NOINDEX,NOFOLLOW"/>
<?php
}
if ( is_front_page() ) : ?>
<meta name="google-site-verification" content="SrCJ-s1TdUW-qcSizzXdaEAxgYeSlsjxqAHKy0bKUVs" />
<?php endif; ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/favicon.ico" />

<?php wp_head(); ?>

<?php // hfo-marketing-script-start ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P6KPH82"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6KPH82');</script>

<?php
if ($currentUrl === $prodUrl) {
    ?>
    <script type="text/javascript" src="//assets.adobedtm.com/launch-ENa4d367204fd24e7c94a1f581118680b5.min.js" async></script>
<?php
} else {
?>
    <script type="text/javascript" src="//assets.adobedtm.com/launch-EN5e5aa9b52d6e413aaa9fe8a0749951b4-staging.min.js" async></script>
    <?php
}
?>

<?php // hfo-marketing-script-end ?>

</head>

<body <?php body_class(); ?>>

	<?php get_template_part('dist/snippets/header'); ?>
	 

	<div id="content" class="site-content clearfix">
