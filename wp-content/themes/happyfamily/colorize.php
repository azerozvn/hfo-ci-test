<?php
$im = imagecreatefrompng( dirname(__FILE__) . "/" . $_GET["path"] );
$color = hex2rgb( isset( $_GET['color'] ) ? $_GET['color'] : "ee8b2d" );

if($im){

	imageAlphaBlending($im, false);
	imageSaveAlpha($im, true);
	imagefilter($im, IMG_FILTER_COLORIZE, $color[0], $color[1], $color[2]);
	header("Content-type: image/png");
    imagepng($im);
    imagedestroy($im);

}


function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}


?>