<?php
	
	wp_enqueue_script( 'destini_widget_js', 'https://destinilocators.com/happyfamily/pdpwidget/install/', array(), $version_number, false );
	wp_enqueue_script( 'destini_bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), $version_number, true );
	add_action('wp_head', 'echo_meta_tags');

  ////////////////////////////////////////////////////////////////////////////////////
  // PROPS

	$ingredients_title = get_field('ingredients_title') ?: 'Key Ingredients & Nutrients';
	$nutrition_title = get_field('nutrition_title') ?: 'Nutrition Facts';
	$destini_upc = substr(get_field('upc_code'), 1, -1); // UPC with first and last character removed.

	// Images: remove empty images, then reindex array.
	$images = array_values( array_filter( get_field('images'), function($image) {
		return $image['image'];
	} ) );

	$category = get_the_terms(get_the_ID(), 'product_category');
	// print_r($category); die();
	if($category && $category[0] && $category[0] -> name != 'undefined') {
		$category_slug = $category[0] -> slug;
		$category_display = $category[0] -> name;
		$category_display_long = 'Happy ' . $category_display . ' Organics';
		$category_link = '/shop/' . $category_slug . '/';
	} else {
		$category = false;
	}
	
	$product_line = get_the_terms(get_the_ID(), 'product_line');
	// print_r($category); die();
	if($product_line && $product_line[0]) {
		$product_line_slug = $product_line[0] -> slug;
		$product_line_display = $product_line[0] -> name;
		$product_line_link = '/shop/?product_line=' . $product_line_slug;
	} else {
		$product_line = false;
	}

?>

<?php
  ////////////////////////////////////////////////////////////////////////////////////
  // METHODS

	function get_marquee_icon($label) {
		$label = strtolower( str_replace(' ', '_', $label) );
		return '/wp-content/themes/happyfamily/dist/images/marquee-icons/' . $label . '.svg';
	}
	
	function get_price_per_quantity($price, $qty) {
		if (!$price || !$qty) { return ''; }
    return '$' . number_format( ( floatval($price) / floatVal($qty) ), 2 );
  }
	
  function echo_price_per_quantity() {
		$packaging_qty = get_field('packaging_qty');
    $price = get_field('price');
    $packaging_type = get_field('packaging_type') ?: 'Item';
		
    if( !$packaging_qty || !$price ) {
			return;
    }
    echo get_price_per_quantity($price, $packaging_qty) . ' / ' . $packaging_type;
  }
	
	function get_ingredient_image($image) {
		if(!is_string($image) || $image == '') { return false; }
		$image = strtolower( str_replace(' ', '_', $image) );
		return '/wp-content/themes/happyfamily/dist/images/ingredients/' . $image . '.svg';
	}

	// The site uses Yoast, and it would have been smarter to import the meta tags into the Yoast fields, but this way was easier and good enough for now.
	function echo_meta_tags() {
		echo '
		<meta name="title" content="' . get_field("meta_title") . '" />
		<meta name="description" content="' . get_field("meta_description") . '" />
		<meta name="keywords" content="' . get_field("meta_keywords") . '" />		
		';
	}

?>

<?php
  ////////////////////////////////////////////////////////////////////////////////////
  // TEMPLATE
?>

<?php get_header(); ?>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<a href="/shop/" class="crumb">Shop</a> 
	<?php if($category_slug) : ?>
		<span class="crumb-div">|</span><a href="<?php echo $category_link ?>" class="crumb"><?php echo $category_display ?></a>
	<?php endif ?>
	<?php if($product_line_slug) : ?>
		<span class="crumb-div">|</span><a href="<?php echo $product_line_link ?>" class="crumb"><?php echo $product_line_display ?></a>
	<?php endif ?>
	<span class="crumb-div">|</span><span class="crumb crumb-title"><?php echo get_the_title() ?></span>
</div>

<!-- Marquee -->
<div class="marquee">
	
	<div class="mobile-title-wrap">
		<?php if($category_display_long || $product_line_display) : ?>
			<div class="eyebrow"><?php echo $category_display_long . ' ' . $product_line_display ?> </div>
		<?php endif ?>
		<h1 class="marquee-title"><?php echo get_the_title() ?></h1>
		<div class="price"><?php echo_price_per_quantity() ?></div>
		<div class="packaging"><?php echo get_field('packaging_size') . ' ' . get_field('packaging_type') ?></div>
	</div>

	<!-- Images -->
	<?php if ($images) : ?>
		<div id="product-image">
			
			<!-- Thumbs -->
			<div class="thumbs">
				<?php foreach ( $images as $key => $image ) : ?> 
					<button class="thumb" data-key="<?php echo $key ?>"> <img src="<?php echo $image['image']['url'] ?>" alt="<?php echo $image['alt'] ?>" data-key="<?php echo $key ?>" /></button> 
				<?php endforeach; ?>
			</div>

			<!-- Flickity slider -->
			<div class="slide-wrap">
				<?php foreach ( $images as $key => $image ) : ?> 
					<div class="slide" data-key="<?php echo $key ?>"> <img src="<?php echo $image['image']['url'] ?>" alt="<?php echo $image['alt'] ?>" /></div> 
				<?php endforeach; ?>
			</div>
		</div>
		<?php endif ?>
		
	<!-- Description -->
	<div class="description-wrap">
		<div class="desktop-title-wrap">
			<?php if($category_display_long || $product_line_display) : ?>
				<div class="eyebrow"><?php echo $category_display_long . ' ' . $product_line_display ?> </div>
			<?php endif ?>
			<h1 class="marquee-title"><?php echo get_the_title() ?></h1>
			<div class="price"><?php echo_price_per_quantity() ?></div>
			<div class="packaging"><?php echo get_field('packaging_size') . ' ' . get_field('packaging_type') ?></div>
		</div>
		<div class="description"><?php echo get_field('short_description') ?></div>
		
		<!-- Icons -->
		<?php if (get_field('icons')) : ?>
			<div class="icons">
				<?php foreach (get_field('icons') as $key => $icon) : ?>
					<div class="icon">
						<img class="icon-image" src="<?php echo get_marquee_icon($icon['icon']) ?>" alt="<?php echo $icon['label'] ?>">
						<div class="icon-label"><?php echo $icon['label'] ?></div>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif ?>

		<button 
			class="btn shop-now buynow"
			data-toggle="modal"
			data-target="#widget-modal"
			data-destini-apo = "<?php echo $destini_upc; ?>"
		>Buy Now</button>
	</div>
</div>

<!-- Key Ingredients -->
<?php if (get_field('ingredients') || get_field('ingredients_description')) : ?>
	<div class="card">
		<h2 class="ingredients-title"><?php echo $ingredients_title ?></h2>
		<div class="ingredients">
			<?php foreach ( get_field('ingredients') as $key => $ingredient ) : ?>
				<div class="ingredient"> 
					<?php $image = get_ingredient_image($ingredient['image']) ?>
					<?php if ($image) : ?><img class="ingredient-image" src="<?php echo $image ?>" alt="<?php echo $ingredient['name'] ?>" /> <?php endif ?>
					<div class="ingredient-label"><?php echo $ingredient['quantity'] . ' ' . $ingredient['name']?></div>
				</div> 
			<?php endforeach; ?>
		</div>
		<div class="ingredients-description wysiwyg"><?php echo get_field('ingredients_description') ?></div>
	</div>
<?php endif ?>

<!-- Nutrition Facts -->
<?php if (get_field('nutrition_facts') || get_field('nutrition_subhead')) : ?>
	<div class="card orange-card nutrition-card">
		<h2 class="nutrition-title"><?php echo $nutrition_title ?></h2>
		<?php if (get_field('nutrition_subhead')) : ?>
			<div class="nutrition-subhead"><?php echo get_field('nutrition_subhead') ?></div> 
		<?php endif ?>
		<div class="nutrition-facts">
			<?php foreach ( get_field('nutrition_facts') as $key => $fact ) : ?>
				<div class="fact"> 
					<div class="fact-title"><?php echo $fact['title'] ?></div>
					<div class="fact-subhead"><?php echo $fact['subhead'] ?></div>
				</div> 
			<?php endforeach; ?>
		</div>
	</div>
<?php endif ?>

<!-- Side by Side Block-->
<?php if (get_field('side_by_side_image')) : ?>
	<div class="side-by-side">
		<div class="side-by-side-text wysiwyg"><?php echo get_field('side_by_side_text') ?></div>
		<div class="side-by-side-image desktop" style='background-image:url("<?php echo get_field('side_by_side_image')['url'] ?>")'></div>
		<div class="side-by-side-image mobile"><img src="<?php echo get_field('side_by_side_image')['url'] ?>" alt="<?php echo get_field('side_by_side_image')['alt'] ?>" /></div>
	</div>
<?php endif ?>

<!-- Destini Widget -->
<button id="widget-modal-close" class="close hide" type="button" data-dismiss="modal" aria-label="Close">&times;</button>
<div id="widget-modal" class="modal fade" role="dialog">
	<div class="modal-body" >
		<div id="destini-div" src="">
		</div>
	</div>
</div>

<?php get_footer(); ?>