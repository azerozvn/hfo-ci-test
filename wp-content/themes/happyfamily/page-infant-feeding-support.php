<?php get_header(); ?>

	<!-- Hero -->

	<div class="hero formula">
		<div class="shell">
			<div class="feeding-hero" style="background-image: url('<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/InfantFeeding-HERO-min.png');">

				<video class="homepage-hero__video" autoplay muted loop playsinline>
					<source type="video/mp4" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/infant-feeding-hero.mp4">
				</video>

				<div class="feeding-base__introcopy white hero">
					<h2>This Is Feeding Your Baby With Confidence.</h2>
					<p>Every feeding journey is different. We support them all because we've been there.</p>
					<a id="feeding-video" class="feeding-base__ctalink white hero" data-fancybox-type="iframe" href="https://www.youtube.com/embed/8wvcBcHYeb8?rel=0&#038;amp;showinfo=0"><span class="triangle">Watch our video</span></a>
				</div>
			</div>
		</div>
	</div>

	<!-- End Hero -->

	<div class="shell formula">

		<div class="feeding-intro">
			<h2>Support For Every Family.</h2>
			<p>Breast milk is the best thing you can feed your infant and we want to help make breastfeeding a reality for you. But we also know that it isn't always possible or easy.</p>
		</div>

		<div class="feeding-supportnav">
			<h3>Get Support For:</h3>
			<ul class="feeding-supportnav__links">

				<a class="minimal-scroll" href="#breastfeeding">
					<li class="feeding-supportnav__link">
						<img class="breastfeeding" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/chatIcons-03.svg" />

						<p class="breastfeeding">Breastfeeding</p>

					</li>
				</a>

				<a class="minimal-scroll" href="#supplementing">
					<li class="feeding-supportnav__link">
						<img class="supplementing" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/chatIcons-04.svg" />

						<p class="supplementing">Supplementing
							<br/><span>Breast Milk with Formula</p>

								</li>
							</a>

							<a class="minimal-scroll" href="#formulafeeding">
								<li class="feeding-supportnav__link">
																			<img class="formula-link" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/chatIcons-05.svg" />

																		<p class="formula-link">Formula Feeding</p>

								</li>
							</a>

						</ul>

			</div>

		<div id="chat-support" class="feeding-chat clearfix">

			<div class="feeding-chat__image">
				<img class="feeding-chat__image--left" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/chatIcons-01-1.svg">
				<img class="feeding-chat__image--right" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/chatIcons-02-1.svg">
			</div>

			<div class="feeding-base__introcopy white chat">
				<h2>NEED HELP? CHAT WITH AN EXPERT.</h2>
				<p>Chat with our Happy Mama Mentors, a team of lactation specialists and dietitians, certified in maternal and infant nutrition from Cornell (and all moms!). Available 8am-8pm (EST), Mon-Fri and 8am-4pm (EST), Sat-Sun.</p>
				<a id="main-cta-launcher" class="feeding-base__ctalink white chat" href="#chat-now"><span class="triangle">Start chatting</span></a>
			</div>

		</div>

		<section id="testimonials" class="formula-testimonials">

			<div class="formula-base__introcopy testimonials">
				<h2>HAPPY TESTIMONIALS</h2>
			</div>

			<div class="formula-testimonials__container">

				<div class="formula-testimonials__slick-controls prev"></div>

				<ul class="formula-testimonials__slider">

					<li class="formula-testimonials__testimonial">

						<p>&quot;Our breastfeeding journey had a little bit of a rocky start in the beginning and with the help of the Happy Family LC chatline we were able to get back on track.&quot;</p>

						<p class="testimonial-creds">- Emily, Mom of 2 month old</p>

					</li>

					<li class="formula-testimonials__testimonial">

						<p>&quot;I can't believe how fast you answered, I was desperate, I didn't know what to do, I was two seconds away from tears.&quot;</p>

						<p class="testimonial-creds">- Melissa, Mom of 4 month old</p>

					</li>

					<li class="formula-testimonials__testimonial">

						<p>&quot;My baby is 4 weeks old and breastfeeding was a nightmare, but with the help of your team it has been going wonderfully.&quot;</p>

						<p class="testimonial-creds">- Rupal, Mom of 4 week old baby</p>

					</li>

					<li class="formula-testimonials__testimonial">

						<p>I just wanted to let you know that your tips worked. Over the last few days my granddaughter has tried (and liked!) more foods than I would have thought possible. What a lifesaver this site is!&quot;</p>

						<p class="testimonial-creds">- Joan, Grandmother of 25 month old</p>

					</li>

					<li class="formula-testimonials__testimonial">

						<p>&quot;This is the first time I have felt confident with information I have been given. I am going to start on your suggestions right now and come back to let you know how they work.&quot;</p>

						<p class="testimonial-creds">- Julie, Mom to 6 week old</p>

					</li>

					<li class="formula-testimonials__testimonial">

						<p>&quot;My baby latched for the first time ever!&quot;</p>

						<p class="testimonial-creds">- Alyssa, Mom to 10 day old baby</p>

					</li>

				</ul>

				<div class="formula-testimonials__slick-controls next"></div>

			</div>

		</section>

		<!-- <div class="feeding-testimonial">

			<img src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/shazi-image.png">

			<div class="feeding-testimonial__copy">
				<h3>It's our mission to change the trajectory of children's health. That means providing support for all parents from day one&mdash;from prenatal to breastfeeding and beyond.</h3>
				<p>- Shazi Visram, Happy Family Founder &amp; CEO</p>
			</div>

		</div> -->

		<!-- <div class="feeding-supportnav">

			<h3>Get Support For:</h3>

			<ul class="feeding-supportnav__links">

				<a class="minimal-scroll" href="#breastfeeding">
					<li class="feeding-supportnav__link">
						<img class="breastfeeding" src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/chatIcons-03.svg" />

						<p class="breastfeeding">Breastfeeding</p>

					</li>
				</a>

				<a class="minimal-scroll" href="#supplementing">
					<li class="feeding-supportnav__link">
						<img class="supplementing" src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/chatIcons-04.svg" />

						<p class="supplementing">Supplementing
							<br/><span>Breast Milk with Formula</p>

								</li>
							</a>

							<a class="minimal-scroll" href="#formula">
								<li class="feeding-supportnav__link">
																			<img class="formula-link" src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/chatIcons-05.svg" />

																		<p class="formula-link">Formula Feeding</p>

								</li>
							</a>

						</ul>

			</div> -->

			<!-- delete <div class="feeding-product">
				<h3>EVERYTHING YOU NEED FOR A NUTRITIOUS BEGINNING.</h3>
				<img src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/family-shot2.png">
			</div> -->

			<!-- Video Carousel -->

			<!-- <div id="videos" class="feeding-videos">

				<div class="feeding-videos__intro">
					<h2>WE'VE BEEN THERE.</h2>
		   <p>Hear from other parents about their experiences feeding their babies.</p>
				</div>

				<div class="feeding-videos__videos">

					<div class="feeding-videos__slick-controls prev"></div>

				  <ul class="feeding-videos__slider">

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/rat0wWwjS9g?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="/wp-content/uploads/2018/05/HF_IF_VignetteStill_Cotrell-1.jpg">
					 </div>

					 <p>Monica &amp; Michael</p>
					 <p class="video-attr">Parents of Ara, 6 months</p>
					 <p class="video-cred"></p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/r0sEnYEFllw?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="/wp-content/uploads/2018/05/HF_IF_VignetteStill_Anna-1.jpg">
					 </div>

					 <p>Anna</p>
					 <p class="video-attr">Mom of Sullivan, 9 months</p>
					 <p class="video-cred">Happy Family Packaging Graphics Manager</p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/PB4WfZNybyw?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="/wp-content/uploads/2018/05/HF_IF_VignetteStill_Barbagiovanni-1.jpg">
					 </div>

					 <p>Mark &amp; Santo</p>
					 <p class="video-attr">Parents of Declan, 5 months</p>
					 <p class="video-cred"></p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/C32GtQ8-SMg?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="/wp-content/uploads/2018/05/HF_IF_VignetteStill_Anne-1.jpg">
					 </div>

					 <p>Anne</p>
					 <p class="video-attr">Mom of Jack, 2 years</p>
					 <p class="video-cred">Happy Family SVP of Innovation &amp; Business Development</p>

					</li>

				</a>

						</ul>

			  <div class="feeding-videos__slick-controls next"></div>
				</div>

			</div> -->

			<div id="breastfeeding-block" class="feeding-breast">
				<div id="breastfeeding"></div>

				<!-- <div class="feeding-breast__image">
					<img src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/formula-support-bg.png">
				</div>

				<div class="feeding-intro">
					<h2>Breastfeeding</h2>
		   <p>Breast milk is amazing because it naturally provides all the nutrients your baby needs, but it isn't always easy. Get support and learn about the benefits of breast milk, latching tips, and more.</p>
		   <a class="feeding-base__ctalink green" href="/breastfeeding-support">Explore our lactation line<span class="triangle"></span></a>
		</div> -->

		<div class="feeding-testimonial first-testmnl-margin">

			<!--img src="file:///Users/lauraconde/OneDrive%20-%20Danone/Web/IF%20LP/Infant-Feeding-breastfeeding.jpg"-->
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Infant-Feeding-breastfeeding-1.jpg">

			<div class="feeding-testimonial__copy feeding-intro">
				<h2 style="color: #70a401">Breastfeeding</h2>
		   <p>Breast milk is amazing because it naturally provides all the nutrients your baby needs, but it isn't always easy. Get support and learn about the benefits of breast milk, latching tips, and more.</p><br>
		   <center><a class="feeding-base__ctalink green" href="/breastfeeding-support"><span class="triangle">Explore our lactation line</span></a></center>
			</div>

		</div> 

		<hr class="feeding white" />

		<div class="feeding-breast__intro">
			<h3>Breastfeeding Articles</h3>
			<p>Click for expert breastfeeding advice and information.</p>
		</div>

		<div class="feeding-breast__articles">

			<div class="feeding-breast__slick-controls prev breast"></div>
			<ul class="feeding-breast__slider breast">

				<a href="/learning-center/mama/breast-milk-benefits/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Breast-Milk-Benefits-1-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Breast Milk Benefits</p>
					</li>
				</a>

				<a href="/learning-center/mama/top-latching-tips/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Perfecting-the-Basic-Step-of-the-Breastfeeding-_Dance__-The-Latch-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Top Latching Tips</p>
					</li>
				</a>

				<a href="/learning-center/mama/what-to-eat-while-breastfeeding/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/08/26-What-to-eat-while-breastfeeding_shutterstock_286477241.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>What to Eat While Breastfeeding</p>
					</li>
				</a>

				<a href="/learning-center/mama/how-to-nurse-while-you-work/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/How-to-Nurse-While-You-Work-1-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>How to Nurse While You Work</p>
					</li>
				</a>

				<a href="/learning-center/mama/dealing-with-a-low-breastmilk-supply/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Low-Milk-Supply-1-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Low Milk Supply?</p>
					</li>
				</a>

			</ul>
			<div class="feeding-breast__slick-controls next breast"></div>
		</div>
		<hr class="feeding" />
	</div>


	<div id="supplementing-block" class="feeding-testimonial supplement-margin">
		    <div id="supplementing"></div>
			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Infant-Feeding-supplementing-1.jpg">

			<div class="feeding-testimonial__copy feeding-intro">
				<h2 style="color: #70a401">Supplementing with Formula</h2>
			<p>Exclusive breastfeeding isn't always possible. Having a nutritious, organic formula option on hand can help if you're struggling with low supply or if your baby just won't latch (it happens).</p> <br>
			<center><a class="feeding-base__ctalink green" href="<?php echo get_home_url(); ?>/infant-formula/"><span class="triangle">See our Stage 2 Formula</span></a></center>
			</div>

		</div> 

		<!--<div id="supplementing" class="feeding-supplementing">

		<div class="feeding-supplementing__image">
			<img src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/supplementing-image.png">
		</div>

		<div class="feeding-intro">
			<h2>Supplementing</h2>
			<p>Exclusive breastfeeding isn't always possible. Having a nutritious, organic formula option on hand can help.</p>
			<a class="feeding-base__ctalink green" href="/infant-formula#stage-2">See our Stage 2 Formula<span class="triangle"></span></a>
		</div> -->

	<!-- New article module -->

	<hr class="feeding white" />

		<div class="feeding-breast__intro">
			<h3>Supplementing Articles</h3>
			<p>Click for expert supplementing advice and information.</p>
		</div>

		<div class="feeding-breast__articles">

			<div class="feeding-breast__slick-controls prev"></div>
			<ul class="feeding-breast__slider">

				<a href="/learning-center/mama/preparing-to-wean-your-child/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/preparing-to-wean-your-child_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Preparing to Wean Your Child</p>
					</li>
				</a>

				<a href="/learning-center/mama/how-much-formula-is-enough/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/how-much-formula-is-enough_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>How Much Formula is Enough</p>
					</li>
				</a>

				<a href="/learning-center/little-one/choosing-the-right-bottles-and-nipples/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Choosing-the-right-bottles-and-nipples_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Choosing the Right Bottles and Nipples</p>
					</li>
				</a>

				<a href="/learning-center/little-one/formula-feeding-on-demand-vs-on-a-schedule/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Formula-feeding-on-demand-vs.-on-a-schedule_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Formula Feeding on Demand Vs. on a Schedule</p>
					</li>
				</a>

				<a href="/learning-center/little-one/breast-bottle-transition/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/breast-to-bottle-how-to-transition_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Breast Bottle Transition</p>
					</li>
				</a>

			</ul>
			<div class="feeding-breast__slick-controls next"></div>
		</div>

	<hr class="feeding feeding-formula-line" />

	<!-- End new article module -->

	<div id="formulafeeding-block" class="feeding-formula">
		<div id="formulafeeding"></div>
		<div class="feeding-testimonial formula-margin">

			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Infant-Feeding-formula-1.jpg">

			<div class="feeding-testimonial__copy feeding-intro">
				<h2 style="color: #70a401">Formula Feeding</h2>
			<p>If your family relies on formula, we know that every ingredient matters. That's why we chose to include key vitamins and minerals naturally found in breast milk-like DHA for brain development and prebiotics to support a healthy digestive system.</p><br>
			<center> <a class="feeding-base__ctalink green" href="/infant-formula"><span class="triangle">Learn more about formula</span></a></center>
			</div>

		</div> 


		<!-- <div class="feeding-formula__image">
			<img src="http://www.happyfamilyorganics.com/wp-content/uploads/2018/05/formula-image-min.jpg">
		</div>

		<div class="feeding-intro">
			<h2>Formula Feeding</h2>
			<p>If your family relies on formula, we know that every ingredient matters. We chose ingredients naturally found in breast milk.</p>
			<a class="feeding-base__ctalink green" href="/infant-formula">Learn more about formula<span class="triangle"></span></a>
		</div> -->

		<hr class="feeding white formula-article-line" />
		<div class="feeding-formula__intro">
			<h3>Formula Articles</h3>
			<p>Click for expert feeding advice and information.</p>
		</div>

		<div class="feeding-formula__articles">

			<div class="feeding-formula__slick-controls prev"></div>
			<ul class="feeding-formula__slider">

				<a href="/learning-center/little-one/does-your-baby-need-a-sensitive-formula/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/sensitive_formula_hero-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Does Your Formula-Fed Baby Need a Sensitive Formula?</p>
					</li>
				</a>

				<a href="/learning-center/mama/prebiotics-101/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Prebiotics-erica-thinking-about-this-one.-laying-down-baby.-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Prebiotics 101</p>
					</li>
				</a>

				<a href="/learning-center/little-one/nutrients-to-look-for-at-6-12-months/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Nutrition-Needs-6-12-Months-1-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Nutrition Needs 6-12 Months</p>
					</li>
				</a>

				<a href="/learning-center/little-one/tips-for-bottle-fed-baby-gas/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Managing-gas-in-a-formula-or-bottle-fed-baby-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Tips for Bottle Fed Baby Gas</p>
					</li>
				</a>

				<a href="/learning-center/mama/how-to-prep-formula/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Formula.-Preparing-Infant-formula-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>How to Prep Formula</p>
					</li>
				</a>

				<a href="/learning-center/little-one/breast-bottle-transition/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Formula.-Introducing-formula-to-breastfed-babies-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>Breast to Bottle: How to Transition</p>
					</li>
				</a>

				<a href="/learning-center/mama/how-much-formula-is-enough/">
					<li class="feeding-breast__article">
						<div class="feeding-breast__article--image">
							<img width="360" height="240" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/Feeding-for-healthy-weight-gain-in-babies-and-toddlers-360x240.jpg" class="attachment-360x240 size-360x240" alt="" /> </div>
						<p>How Much Formula is Enough?</p>
					</li>
				</a>

			</ul>
			<div class="feeding-formula__slick-controls next"></div>
		</div>
		
	</div>

	<!-- shazi and video module moved here --> 

	<div class="feeding-testimonial testimonial-last">

			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/shazi-image.png">

			<div class="feeding-testimonial__copy right-text">
				<h3>It's our mission to change the trajectory of children's health. That means providing support for all parents from day one&mdash;from prenatal to breastfeeding and beyond.</h3>
				<p>- Shazi Visram, Happy Family Founder &amp; CEO</p>
			</div>

		</div>

		<hr class="feeding white-last" />

		<div id="videos" class="feeding-videos">

				<div class="feeding-videos__intro">
					<h2>WE'VE BEEN THERE.</h2>
		   <p>Hear from other parents about their experiences feeding their babies.</p>
				</div>

				<div class="feeding-videos__videos">

					<div class="feeding-videos__slick-controls prev"></div>

				  <ul class="feeding-videos__slider">

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/rat0wWwjS9g?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/HF_IF_VignetteStill_Cotrell-1.jpg">
					 </div>

					 <p>Monica &amp; Michael</p>
					 <p class="video-attr">Parents of Ara, 6 months</p>
					 <p class="video-cred"></p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/r0sEnYEFllw?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/HF_IF_VignetteStill_Anna-1.jpg">
					 </div>

					 <p>Anna</p>
					 <p class="video-attr">Mom of Sullivan, 9 months</p>
					 <p class="video-cred">Happy Family Packaging Graphics Manager</p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/PB4WfZNybyw?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/HF_IF_VignetteStill_Barbagiovanni-1.jpg">
					 </div>

					 <p>Mark &amp; Santo</p>
					 <p class="video-attr">Parents of Declan, 5 months</p>
					 <p class="video-cred"></p>

					</li>

				</a>

							<a class="feeding-videos__link fancybox.iframe" href="https://www.youtube.com/embed/C32GtQ8-SMg?rel=0&#038;amp;showinfo=0">

					<li class="feeding-videos__video">

						<div class="feeding-videos__video--image">
					  <img class="video-image" src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/HF_IF_VignetteStill_Anne-1.jpg">
					 </div>

					 <p>Anne</p>
					 <p class="video-attr">Mom of Jack, 2 years</p>
					 <p class="video-cred">Happy Family SVP of Innovation &amp; Business Development</p>

					</li>

				</a>

						</ul>

			  <div class="feeding-videos__slick-controls next"></div>
				</div>

			</div></span></p></li></a></ul></div>
		</div>
	
	<!-- end shazi and video module --> 
<div class="shell formula social-footer">
		<div class="feeding-social"> 

		<h3>Share Your Story With Us.</h3>

		<h2>#THISISHAPPY</h2>

		<div class="feeding-social__icons">
			<a href="https://www.facebook.com/HappyFamilyOrganics/" target="_blank"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/facebook-white.png"></a>
			<a href="https://www.instagram.com/happyfamilyorganics/" target="_blank"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/instagram-white.png"></a>
			<a href="https://twitter.com/HappyFamily" target="_blank"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/twitter-white.png"></a>
			<a href="https://www.pinterest.com/happyfamilyorganics/" target="_blank"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/pinterest-white.png"></a>
		</div>

	</div>

	<div class="feeding-socialgrid">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-1.jpg">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-2.jpg">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-3.jpg">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-4.jpg">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-5.jpg">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2018/05/social-6.jpg">
	</div>
</div><!-- end shazi and video module --> 

	</div>

	</div>
	<!-- End Main -->

	<section id="formula-introduction" class="formula-modal vc_section modal viewed">
		<div class="main formula-modal__content modal-content">
			<div class="col-12">
				<div class="">
					<p style="text-align: center;"><strong>Our Promise to Parents</strong></p>
					<p style="text-align: center;">We know breast milk offers optimal nutrition (we’re sure you do, too!)
						and we support breastfeeding. But we’re parents too, so we know it isn’t always an option.</p>
					<p style="text-align: center;">Your baby deserves the best possible nutrition and you deserve to feel
						confident in how you nourish your family. That’s why we will never try to sell you on any product or
						method.</p>
					<p style="text-align: center;">Learn more about how we support every parent’s feeding journey.</p>

				</div>
				<div class="button-container  modal-close formula-modal__close button-inline">
					<button class="button button button button-style-outline button-o-empty button"><span
							class="vc_btn3-placeholder">&nbsp;</span></button>
				</div>
			</div>
		</div>
	</section>

	<!--div style="display:none">
		<a class="disclaimer-launch" href="#modal-disclaimer" data-fancybox-type="inline">launch</a>
		<div id="modal-disclaimer">
			<p>
				Our Promise to Parents
			</p>
			<p>
				We know that breast milk offers optimal nutrition (we're sure you do, too!) and we support breastfeeding. But we're parents too, so we know it isn't always an option.
			</p>
			<p>
				Your baby deserves the best possible nutrition and you deserve to feel confident in how you nourish your family. That's why we will never try to sell you on any product or method.
			</p>
			<p>
				Learn more about how we support every parent's feeding journey.
			</p>
		</div>
	</div-->


<?php get_footer(); ?>
<!-- Fancybox -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.css" type="text/css" media="screen" />
<script type="text/javascript" >
	function isTouchDevice(){
	var isTouchDevice = 'ontouchstart' in document.documentElement;
	return isTouchDevice;
}

function getIEversion(){
		var sAgent = window.navigator.userAgent;
		var Idx = sAgent.indexOf("MSIE");
		var version = 0;
    	// If IE, return version number.
        if (Idx > 0){
        		version = parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
        }  else if (!!navigator.userAgent.match(/Trident\/7\./)){
            		// If IE 11 then look for Updated user agent string.
            		version= 11;
        }

        return version
}

if(isTouchDevice()){
	jQuery('body').addClass('touch');
}

var ieversion = getIEversion();
if(ieversion > 0){
	jQuery('body').addClass('ie-' + ieversion);
}

</script>