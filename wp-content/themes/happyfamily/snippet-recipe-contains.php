<?php
	$contains = $contains_object['value'];
	if ( $contains ): ?>
		<?php foreach( $contains as $ingredient ): ?>
			<li class="learning-center-article__content-recipe-detail-contain">
				<div class="learning-center-article__content-recipe-image">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/recipe-icon-<?php echo esc_attr($ingredient); ?>.svg" alt="<?php echo esc_attr($ingredient); ?>">
				</div>
				<span class="learning-center-article__content-recipe-text"><?php echo esc_html($contains_object['choices'][ $ingredient ]); ?></span>
			</li>
		<?php endforeach; ?>
	<?php endif;
?>
