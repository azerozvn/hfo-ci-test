<?php if ( get_field('rich_snippet_title') ) { ?>

	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "ItemList",
		"name": "<?php echo esc_html(get_field('rich_snippet_title')); ?>",
		"itemListOrder": "http://schema.org/ItemListOrderDescending",
		"itemListElement": [

			<?php 
			$i = 1;
			if( have_rows('rich_snippet_list') ): 
				$item_count = count(get_field('rich_snippet_list'));
			while ( have_rows('rich_snippet_list') ) : the_row();
			?>
			
				{
					"@type": "ListItem",
					"position": <?php echo $i ?>,
					"item": {
						"name": "<?php echo esc_html(get_sub_field('rich_snippet_listitem')); ?>",
						"url": "<?php echo esc_url(get_permalink()); ?>#<?php echo $i ?>"
					}
				}<?php if( $i != $item_count ) { echo ','; } ?>
			
			<?php $i++;
			endwhile; endif; ?>
		]
	}
	</script>

<?php } elseif ( in_category( array( 'mama', 'little-one' ) ) ) { ?>

	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Article",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "<?php echo esc_url(get_permalink()); ?>"
		},
		"headline": "<?php the_title(); ?>",
		"image": "<?php the_post_thumbnail_url(); ?>",
		"datePublished": "<?php the_date(); ?>",
		"author": {
			"@type": "Person",
			"name": "<?php the_author(); ?>"
		},
		"publisher": {
			"@type": "Organization",
			"name": "Happy Family Organics",
			"logo": {
				"@type": "ImageObject",
				"url": "<?php echo get_stylesheet_directory_uri() . '/dist/images/favicon.ico'; ?>"
			}
		},
		"description": "<?php echo esc_html( get_field('rich_snippet_description') ); ?>"
	}
	</script>

<?php } elseif ( in_category( 'meal-plan' ) ) { ?>

	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Article",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "<?php echo esc_url(get_permalink()); ?>"
		},
		"headline": "<?php the_title(); ?>",
		"image": "<?php the_post_thumbnail_url(); ?>",
		"datePublished": "<?php the_date(); ?>",
		"author": {
			"@type": "Person",
			"name": "<?php the_author(); ?>"
		},
		"publisher": {
			"@type": "Organization",
			"name": "Happy Family Organics",
			"logo": {
				"@type": "ImageObject",
				"url": "<?php echo get_stylesheet_directory_uri() . '/dist/images/favicon.ico'; ?>"
			}
		},
		"description": "<?php echo esc_html( get_field('_yoast_wpseo_metadesc') ); ?>"
	}
	</script>

<?php } ?>