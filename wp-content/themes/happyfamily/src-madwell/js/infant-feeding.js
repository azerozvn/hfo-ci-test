if ( $('.page-infant-feeding-support').length ) {

	$('.formula-testimonials__slider').slick({
		dots: false,
		infinite: true,
		slidesToShow: 1,
		rows: 0,
		slidesToScroll: 1,
		prevArrow: $('.formula-testimonials__slick-controls.prev'),
		nextArrow: $('.formula-testimonials__slick-controls.next'),
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});


	$('.feeding-videos__slider').slick({
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow: $('.feeding-videos__slick-controls.prev'),
		nextArrow: $('.feeding-videos__slick-controls.next'),
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.feeding-breast__slider.breast').slick({
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: $('.feeding-breast__slick-controls.breast.prev'),
        nextArrow: $('.feeding-breast__slick-controls.breast.next'),
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

	$('.feeding-breast__slider:not(.breast)').slick({
		dots: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: $('.feeding-breast__slick-controls.prev:not(.breast)'),
		nextArrow: $('.feeding-breast__slick-controls.next:not(.breast)'),
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.feeding-formula__slider').slick({
		dots: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		prevArrow: $('.feeding-formula__slick-controls.prev'),
		nextArrow: $('.feeding-formula__slick-controls.next'),
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$("#feeding-video").fancybox({
		maxWidth    : 800,
		maxHeight   : 600,
		fitToView   : false,
		width       : '70%',
		height      : '70%',
		autoSize    : false,
		closeClick  : false,
		openEffect  : 'none',
		closeEffect : 'none'
	});

	$("#modal-disclaimer").fancybox({
		fitToView : false,
		wrapCSS : 'modal-disclaimer',
		width : '100%',
		maxWidth : 400,
		height : 'auto',
		autoSize : false,
		closeClick : false,
		openEffect : 'none',
		closeEffect : 'none',
	});

	$(".feeding-videos__link").fancybox({
		width       : 640,
		height      : 360,
		autoWidth   : true,
		autoHeight  : true,
		autoResize  : true,
		aspectRadio : true,
		type        : 'iframe',
		closeClick  : false,
		openEffect  : 'none',
		closeEffect : 'none'
	});

}

function setAnchor(anchor) {
	if (window.location.href.indexOf('#' + anchor) > -1) {
		var helphash = location.hash.split('#')[1];
		if (checkMediaQuery(767)) {
			var topPadding = -40;
		}else{
			var topPadding = -90;
		}

		if (helphash === anchor) {
			if(helphash === 'formulafeeding'){
				if (checkMediaQuery(767)) {
					topPadding = -55;
				}else{
					topPadding = -105;
				}
			}
			jQuery('html, body').animate({
				scrollTop: jQuery('#' + helphash).offset().top - jQuery('.site-header').height() - jQuery('.site-sub-header').height() - topPadding
			}, 600);
		}
	}
}

$(window).load(function () {
	setAnchor('breastfeeding');
	setAnchor('supplementing');
	setAnchor('formulafeeding');
});

checkMediaQuery = function (width) {
	if (navigator.appName != 'Microsoft Internet Explorer') {
		mql = window.matchMedia("screen and (max-width: " + width + "px)");
		return mql.matches;
	} else {
		return $(window).width() <= width;
	}
};