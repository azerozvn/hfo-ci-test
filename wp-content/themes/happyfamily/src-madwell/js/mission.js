$(window).load(function () {
    if (window.location.href.indexOf("#happytohelp") > -1) {
        var helphash = location.hash.split('#')[1];

        var anchorid;
        if (checkMediaQuery(767)) {
            anchorid = "happytohelpmob";
        }
        else if (checkMediaQuery(1023)) {
            anchorid = "happytohelpipad";
        }
        else {
            anchorid = "happytohelp";
        }

        if (helphash === 'happytohelp') {
            jQuery('html, body').animate({
                scrollTop: jQuery('#' + anchorid).offset().top - jQuery('.site-header').height() - jQuery('.site-sub-header').height() - 40
            }, 600);
        }
    }
});

checkMediaQuery = function (width) {
    if (navigator.appName != 'Microsoft Internet Explorer') {
        mql = window.matchMedia("screen and (max-width: " + width + "px)");
        return mql.matches;
    } else {
        return $(window).width() <= width;
    }
};

 $ = jQuery;
    // Hover to change post titles
    var cssStyles = "",
        addClsName = "",
        postTitleDesktopHover = "span.post_title_desktop",
        postTitleMobileHover = "span.post_title_mobile";

    function addDynamicClass(title, mode, clsElement){
        $(title).each(function(){
            var color = $(this).parent().data(mode),
                clsName = $(this).parent().data(clsElement);

            cssStyles += "."+clsName+"{ color:"+color+" !important;}";
        });
        $("<style type='text/css'>"+cssStyles+"</style>").appendTo("head");
        cssStyles="";
    }

    addDynamicClass('.post_title_desktop','color-desktop', 'class-desktop');
    addDynamicClass('.post_title_mobile','color-mobile', 'class-mobile');

    $(postTitleDesktopHover).hover(function(){
        var self = this;
        addClsName = $(this).parent().data('class-desktop');
        $(this).find('span').addClass(addClsName);
    },
    function () {
        var self = this;
        $(self).find('span').removeClass(addClsName);
    });

    $(postTitleMobileHover).hover(function(){
        var self = this;
        addClsName = $(this).parent().data('class-mobile');
        $(this).find('span').addClass(addClsName);
    },
    function () {
        var self = this;
        $(self).find('span').removeClass(addClsName);
    });

    $(".cross").on("click",function(){
        $("body").removeClass("sticky-header darkHeader");
        $(".overlay_1").slideUp();
        $(window).scrollTop(0);
    });

    $(".tog-ham").on("click",function(){
        var topbarSectionElem = $('.top-banner-block').innerHeight();
        $("body").addClass("sticky-header darkHeader");
        $(".overlay_1").slideDown();
        $(window).scrollTop(0);
    });