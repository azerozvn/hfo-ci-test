
if ( $('.modal').find('.modal-overlay').length < 1 ) {
	$('.modal').append('<div class="modal-overlay"></div>');
}

function triggerModal(modal) {
	modal.show();
	setTimeout(function() { 
		modal.find('.modal-content, .modal-overlay').css('opacity', '1');
	}, 100);
};

function closeModal(x) {
	var modal = x.closest('.modal');

	modal.find('.modal-content, .modal-overlay').css('opacity', '');
	setTimeout(function() { 
		modal.hide();
		unlockBody();

		if ( $('#coach-modal').length ) {
			$('.coach-modal__content-container').empty();
		}
	}, 600);
}

function lockBody() {
	$('body').addClass('locked');
}

function unlockBody() {
	$('body').removeClass('locked');
}

$('.modal-close, .modal-overlay').click(function(){
	closeModal( $(this) );
});

// =================================
// Page specific modal functionality
// =================================

var coachModal = $('#coach-modal');

$('.user-coach__avatar').click(function(e){
	triggerModal(coachModal);
	lockBody();

	// fill in coach content
	var modalContent = $(this).closest('.user-coach').html();
	coachModal.find('.coach-modal__content-container').append(modalContent);

});

if ( $('.page-infant-formula').length ) {
	if (readCookie('formulaModal') != 1) {
		setTimeout(function() { 
			triggerModal( $('.formula-modal') );
		}, 1000);
		createCookie('formulaModal', 1, 30);
	}
}

if ($('.page-infant-feeding-support').length) {
        if (readCookie('feedingModal') != 1) {
            setTimeout(function () {
                triggerModal($('.formula-modal'));
            }, 1000);
            createCookie('feedingModal', 1, 30);
        }
}
// ==============
// Youtube modals
// ==============

// https://gist.github.com/takien/4077195
function YouTubeGetID(url){
	var ID = '';
	url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
	if(url[2] !== undefined) {
		ID = url[2].split(/[^0-9a-z_\-]/i);
		ID = ID[0];
	}
	else {
		ID = url;
	}
		return ID;
}

$('.carousel-video, .modal-video .modal-overlay').on( 'click', function(){
	$('.modal-video .modal-video__content-container').html('');
	closeModal( $('.modal-video') );
});

var findOrCreateVideoModal = function(parent) {
	var modal = parent.find('.modal-video');
	if ( modal.length > 0 ) {
		return modal;
	}

	parent.append('<div class="modal modal-video"><div class="modal-overlay"></div><div class="modal-video__content modal-content"><div class="modal-close"><button></button></div><div class="modal-video__content-container"></div></div></div>');
	modal = parent.find('.modal-video');

	return modal;
}

$('.carousel-video').on('click', 'a', function() {
	if ( $(this).is('[href*="youtube"]') ) {
		var modal, youtubeId, parent;

		parent = $(this).closest('.carousel-video');
		youtubeId = YouTubeGetID($(this).attr('href'));
		modal = findOrCreateVideoModal(parent);
		triggerModal(modal);
		modal.find('.modal-video__content-container').html("<iframe src='https://www.youtube.com/embed/" + youtubeId + "?rel=0&showinfo=0&autoplay=1' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>");

		return false;
	}
});

// ============================
// Learning Center author modal
// ============================

$('.learning-center-article__author-info-trigger').on('click', function() {
	triggerModal($('#learning-center-article__author-info-modal'));

	return false;
});

$('.single-post').click(function(){
	if ( $('#learning-center-article__author-info-modal').css('opacity') == 1 ) {
		closeModal($('#learning-center-article__author-info-modal'));
	}
});


// =========================
// Our Mission certification
// =========================
$('.mission-certification__modal-link').on('click', function() {
	triggerModal( $('.mission-certification-modal') );

	return false;
});

if ( $('.page-our-mission').length && document.location.href.indexOf('#bcorp') > -1 ) {
	$('html, body').animate({
		scrollTop: $('#mission-certification').offset().top - $('.site-header').height() - $('.site-sub-header').height() - 20
	}, 600);

	triggerModal( $('.mission-certification-modal') );
}

// ===================
// Our Mission pop ups
// ===================
$('.mission-design').on('click', function(e) {
	$(this).find('strong.active').removeClass('active');
});

$('.mission-design__image-description li').on('click', function(e) {
	e.stopPropagation();

	if ( $(e.target).hasClass('close') ) {
		$('.mission-design strong.active').removeClass('active');
		return;
	}

	$(this).siblings().find('strong').removeClass('active');
	$(this).find('strong').addClass('active');
	if ( $(this).find('.close').length === 0 ) {
		$(this).find('strong').append('<span class="close"></span>')
	}
});


// ===================
// Clearly Crafted PLP modals
// ===================
$('.clearly-crafted-hero__product').click(function(){
	$('.clearly-crafted-hero__modal').addClass('visible');
});

$('.clearly-crafted-hero__modal .col-12').click(function(){
	$('.clearly-crafted-hero__modal').removeClass('visible');
});

$('.clearly-crafted-videos a[href*="instagram"]').click(function(e){
	e.preventDefault();

	var instagramModal = $(this).attr('href');

	triggerModal( $(instagramModal) );

	//$(instagramModal).find('.modal-content').prepend('<div class="modal-close"><button></button></div>');

	return false;
});
