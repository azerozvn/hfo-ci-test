// Functionality for header sticky, and header show/hide

// Looking for mobile hamburger nav, mobile search?  👉 mount.js

var headerHeight = $('.site-header').height();
var subHeaderHeight = $('.site-sub-header').height();

$(window).on('DOMMouseScroll mousewheel scroll keydown', function ( event ) {
    if (event['type'] === 'keydown' && [33, 34, 38, 40].indexOf(event['keyCode']) === -1) {
        return;
    }

    if( window.pageYOffset > headerHeight ) {
        if($('body').hasClass('page-template-page-masterbrand')){
            $('body').addClass('pageStickyHeader');
        }
        $('.site-header, .dropdown-menu').addClass('sticky');
    } else {
        if($('body').hasClass('page-template-page-masterbrand')){
            $('body').removeClass('pageStickyHeader');
        }
        $('.site-header, .dropdown-menu').removeClass('sticky');
    }

    if ( !!subHeaderHeight ) {
        if( window.pageYOffset > headerHeight + subHeaderHeight ) {
            $('.site-header, .dropdown-menu').addClass('hidden-sub-header');
        } else {
            $('.site-header, .dropdown-menu').removeClass('hidden-sub-header');
        }
    }
});

$(".main-menu__dropdown,.dropdown-menu").on({
    "mouseenter" : function(e){
        _delay = ($(this).hasClass("main-menu__dropdown")) ? 50 : 0 ;
        $(".dropdown-menu").stop(true);
        $(".dropdown-menu").delay(_delay).fadeIn(function(){
            $(this).addClass('visible');
        });
    },
    "mouseleave" : function(e){
        $(".dropdown-menu").stop(true);
        $(".dropdown-menu").delay(50).fadeOut(function(){
            $(this).removeClass('visible');
        });
    },
})

// Update minicart item count
// $(window).load(function () {
//     $('.cart').addClass('loading');
//     $.ajax({
//         url:'/customer/section/load/?sections=cart%2Ccustomer',
//         success: function (data) {
//             var count = data.cart.summary_count;
//             if (count > 0) {
//                 $('.cart').removeClass('cart-empty');
//                 $('.cart .counter-number').text(data.cart.summary_count);
//             } else {
//                 $('.cart').addClass('cart-empty');
//             }
//             $('.cart').removeClass('loading');

//             if (typeof data.customer['firstname'] !== 'undefined') {
//                 // Update login links
//                 var signInLinks = $('a[href*="/customer/account/login"]');
//                 signInLinks.attr('href', '/customer/account/logout').text('Sign Out');
//                 // Add account link and group with logout link
//                 var signInDesktop = $('.desktop-only a[href="/customer/account/logout"]');
//                 var accountDashboard = $('<a>', {href: '/customer/account'}).text('My Account')
//                 var parent = signInDesktop.parent();
//                 var wrapper = $('<div>', {class: 'account-links'});
//                 wrapper.append(accountDashboard);
//                 wrapper.append(signInDesktop);
//                 parent.append(wrapper);
//             }
//         }
//         ,
//         error:  function(response) {
//             console.log(response);
//             $('.cart').removeClass('loading');
//         }
//     });
// });