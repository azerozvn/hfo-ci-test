// Modify order of review on landing pages (bundles)
// interval detects when reviews is loaded and applies script

var reviewInterval;

function modifyReview() {

	if( $('.pr-review').length ) {

		clearInterval(reviewInterval);

		var review = $('.pr-review');

		review.each(function(){

			var reviewDate = $(this).find('time').html(),
				reviewerName = $(this).find('.pr-rd-author-nickname span span:not(.pr-rd-bold)').html(),
				reviewerCity = $(this).find('.pr-rd-author-location span span:not(.pr-rd-bold)').html(),
				reviewerDetails = '<div class="reviewer-details">';

			reviewerDetails = '<h2 class="reviewer-details__name">' + reviewerName + '</h2>'
							+ '<div class="reviewer-details__attributes">'
							+ '		<p class="reviewer-details__city">' + reviewerCity + '</p>'
							+ '		<p class="reviewer-details__date">' + reviewDate + '</p>';
							+ '</div></div>';

			$(this).prepend(reviewerDetails);

		});

		$('.pr-rd-main-footer, .pr-rd-main-header, .pr-accessible-focus-element, .pr-review-display script').remove();

		$('.pr-review-display').slick({
			slidesToShow: 3,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2
					},
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1
					},
				}
			]
		});

	}

}

if ( $('.review-cards').length ) {
	reviewInterval = setInterval(modifyReview, 300);
}