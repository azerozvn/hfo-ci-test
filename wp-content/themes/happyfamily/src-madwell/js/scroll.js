// Add class to elements that enter viewport and trigger animation
var $animatedElements = $('section, .animated');

function inView() {
	var windowHalfHeight = $(window).height() * 0.5;
	var windowTopPosition = $(window).scrollTop();
	var windowMiddlePosition = (windowTopPosition + windowHalfHeight);

	$.each($animatedElements, function() {
		var $element = $(this);
		var elementHalfHeight = $element.outerHeight() * 0.5;
		var elementTopPosition = $element.offset().top;
		var elementMiddlePosition = (elementTopPosition + elementHalfHeight);

		//check to see if this current container is within viewport
		if (elementMiddlePosition <= windowMiddlePosition) {
			$element.addClass('viewed');
		}
	});
}

$(window).on('scroll resize', inView);
$(window).trigger('scroll');

// Smooth scroll to links with hashes
$('a[href*="#"]').not('[href="#"]')
	.not('.mission-what-we-do > a[href*="#"]')
	.not('.tab-section a[href*="#"]')
	.not('.tab-header a[href*="#"]')
	.not('a[href*="instagram"]')
	.on('click touchend', function(event) {
		// On-page links
		if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top - $('.site-header').height() - $('.site-sub-header').height() - 20
				}, 600);
			}
		}
});

$(window).load(function () {
	if (window.location.hash) {
		var hash = window.location.hash;

		if (hash === '#chat-now') {
			$('html, body').animate({
				scrollTop: $('#chat-support').offset().top - $('.site-header').height() - $('.site-sub-header').height() - 60
			}, 600);
			setTimeout(function () {
				$('#open_fc_widget').trigger('click');
			}, 1000);
		} else if (hash === '#chat-support') {
			$('html, body').animate({
				scrollTop: $(hash).offset().top - $('.site-header').height() - $('.site-sub-header').height() - 60
			}, 600);
		}
	}
});
