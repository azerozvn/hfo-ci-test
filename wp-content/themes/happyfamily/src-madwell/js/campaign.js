$(document).ready(function(){
// Gallery Slick initialize
$('.slick-video-module-carousal .slides').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows:false,
    dots:true,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                adaptiveHeight: true,
            }
        },
    ]
});
// End here

//Team carousal
var $slider = $('.team-slides'),
    $progressBar = $('.progress'),
    $testimonialLeft = $('.testimonial-left'),
    $testimonialRight = $('.testimonial-right'),
    $sliderController = $('.slider-controller a');

$slider.on('init', function(event, slick) {
    var countSlide = slick.slideCount;
    initialProgress = (100 / countSlide);
    $progressBar
        .css('background-size', initialProgress + '% 100%')
        .attr('aria-valuenow', initialProgress );
    $testimonialLeft.addClass('slick-disabled');
    if(countSlide === 1){
        $sliderController.hide();
    }
});

$slider.slick({
    infinite: false,
    slidesToShow: 1.1,
    autoplay: false,
    arrows:false,
    adaptiveHeight: false,
    dots: false,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                arrows: true,
                adaptiveHeight: false,
            }
        },
    ]
});

$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    var calc = ((nextSlide + 1) / (slick.slideCount)) * 100,
        calcSlideCount = slick.slideCount - 1,
        actualSlideCount = slick.slideCount;

    $progressBar
        .css('background-size', calc + '% 100%')
        .attr('aria-valuenow', calc );
    if(actualSlideCount > 2) {
        if (nextSlide === 0) {
            $testimonialLeft.addClass('slick-disabled');
        }
        else if (nextSlide === calcSlideCount) {
            $testimonialRight.addClass('slick-disabled');
        } else {
            $sliderController.removeClass('slick-disabled');
        }
    }else{
        $sliderController.toggleClass('slick-disabled');
    }
});

$testimonialLeft.click(function(){
    $slider.slick('slickPrev');
});

$testimonialRight.click(function(){
    $slider.slick('slickNext');
});
// End here

//Svg color dynamic
$("img.svg").each(function () {
    // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
    var $img = jQuery(this);
    // Get all the attributes.
    var attributes = $img.prop("attributes");
    // Dynamic Color
    var dynamicColor = $img.parent('div.play-icon').data('color');
    // Get the image's URL.
    var imgURL = $img.attr("src");
    // Fire an AJAX GET request to the URL.
    $.get(imgURL, function (data) {
        // The data you get includes the document type definition, which we don't need.
        // We are only interested in the <svg> tag inside that.
        var $svg = $(data).find('svg');
        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');
        // Loop through original image's attributes and apply on SVG
        $.each(attributes, function() {
            $svg.attr(this.name, this.value);
        });
        $svg.find('circle').css('fill', dynamicColor);
        // Replace image with new SVG
        $img.replaceWith($svg);
    });
});
// End here


// Dynamic font size implementation
var cssStyles = "";
function addDynamicClassCampaign(title, mode, lHeight, clsElement) {
    $(title).each(function() {
        var size = $(this).parent().data(mode),
            lineHeight = $(this).parent().data(lHeight),
            clsName = $(this).parent().data(clsElement);
        $(this).addClass(clsName);
        cssStyles += "." + clsName + "{ font-size:" + size + "; line-height:" +lineHeight+";}";
    });
    $("<style type='text/css'>" + cssStyles + "</style>").appendTo("head");
    cssStyles = "";
}
addDynamicClassCampaign('.dynamic-font', 'size', 'line-height', 'class');
addDynamicClassCampaign('.dynamic-font-mob', 'mob-size', 'mob-line-height', 'mob-class');
// End here

// Scroll till that section
    $(window).on('load', function(){
        var currentUrl = window.location.href;
        if(currentUrl.indexOf('#our-story') != -1) {
            var target = $('.video-module'),
                targetHeight = $('.video-module').offset().top,
                headerHeight = $('.header-container').innerHeight(),
                scrollHeight = Number(targetHeight - headerHeight);
            if (target.length) {
                $('html,body').animate({
                    scrollTop: scrollHeight
                }, 1000);
                return false;
            }
        }
    });
// End here
// AdaptiveHeight solution for team slides
    $('.team-slides img.lazyload').on('lazyloaded', function(event, slick, image, imageSource) {
        $('.team-slides .slick-slide').each(function(){
            var heightSlide = $(this).innerHeight();
            $(this).css('height', heightSlide+'px');
        });
        var controlsHeight = $('.team-slides .slick-active .carousel-img').height();
        $('.campaign-wrapper .team-slides button.slick-next, .campaign-wrapper .team-slides button.slick-prev').css('top', ((controlsHeight/2) - 14)+'px');
    });
// End here
});