$('[data-expand]').on('click', function() {
	var target, expandBreakpoint, button, expand;

	button = $(this);
	expand = function() {
		button.toggleClass('active');
		target = $(button.data('expand'));
		target.slideToggle();
	}

	expandBreakpoint = button.data('expand-breakpoint');
	if ( expandBreakpoint > 0) {
		if ( $(window).width() <= expandBreakpoint) {
			expand();
		}
	} else {
		expand();
	}
});
