$('.hover-tab').click(function(e){
	e.preventDefault();
});

if ( $('.hover-tabs').hasClass('tablet-drawers') ) {
	var drawerBreakpoint = 1023;
} else {
	var drawerBreakpoint = 600;
}

function revealOnHover(){

	var tabIds = $(this).attr('data-content').split('&'),
		tabContainer = $(this).closest('.hover-tabs');

	for (var i = 0; i < tabIds.length; i++) {

		var tabContent = $('#' + tabIds[i]);

		tabContainer.find('.hover-tab').removeClass('active');
		tabContainer.find('.tab-content:not(#' + tabIds + ')').removeClass('active');
		$(this).addClass('active');
		tabContent.addClass('active');

		// Toggle bullet point on formula page
		if ( tabIds[i] == 'sensitive' || tabIds[i] == 'sensitive-image' ) {
			$('.made-not-with li:first-child').hide();
			$('.made-not-with li.hide-sensitive').hide();
		} else {
			$('.made-not-with li:first-child').show();
			$('.made-not-with li.hide-sensitive').show();
		}

		if ( tabContent.find('.slick-slider').length ) {
			tabContent.find('.slick-slider').slick('setPosition');
		}
	}

}

function positionHoverTabs() {

	if ( $(window).width() > drawerBreakpoint ) {
		var maxHeight = -1;

		// Set section height on page load
		$('.hover-tabs').each(function(){

			$(this).find('.tab-content').each(function() {
				if ( maxHeight > $(this).height() ) {
					maxHeight = maxHeight;
				} else {
					maxHeight = $(this).height();
				}
			});

			$(this).find('.tab-content').each(function() {
				$(this).height(maxHeight);
			});

			$(this).find('.hover-tabs__container').closest('.main').css('margin-bottom', maxHeight);

			if ( $('.hover-tab.active').length == 0 ) {
				$('.hover-tab').first().addClass('active');
				$('#' + $('.hover-tab.active').attr('data-content') ).addClass('active');
				$(this).find('.hover-tab').first().addClass('active');
			}
		});

		// Hovering interaction
		$('.hover-tab').mouseenter(revealOnHover);

	} else {

		// Reset desktop styles if resizing browser
		$('.tab-content').each(function() {
			$(this).css('height', '');
		});
		$('.hover-tabs .main').css('margin-bottom', '');
		$('.hover-tabs .active').removeClass('active');

		$('.hover-tab').unbind('mouseenter');

		// Toggle drawers
		$('.tab-drawer-header').unbind().click(function(){

			if ( $(this).hasClass('expanded') ) {
				$(this).removeClass('expanded');
				$(this).nextAll('div').slideUp();
			} else {
				$('.tab-drawer-header').removeClass('expanded');
				$('.tab-drawer-header ~ div').slideUp();
				$(this).addClass('expanded');
				$(this).nextAll('div').slideDown();
				
				// reset slick slider
				$(this).parent().find('.slick-slider').slick('setPosition');
			}

		});
	}

}

positionHoverTabs();
$(window).resize(positionHoverTabs);


function positionFormulaTabs() {
	$('.formula-stages .tab-content').each(function() {
		$(this).css('height', '');

		if ( $(this).hasClass('main') && $(window).width() < 1024 && $(window).width() > 600 ) {
			$(this).css('top', $('.formula-tabs-container').height() + 60);
		}

		if ( $(window).width() <= 600 ) {
			$('.hover-tab').mouseenter(revealOnHover);
		}
	});

	$('.formula-stages .hover-tabs__container').closest('.main').css('margin-bottom', '');

	$('#stage-2, [data-content="stage-2&stage-2-image"], #stage-2-image').addClass('active');
}

if ( $('.page-infant-formula').length ) {

	$('.formula-tabs-container .hover-tab').removeClass('active');
	$('[data-content="stage-2&stage-2-image"]').addClass('active');

	positionFormulaTabs();
	$(window).resize(positionFormulaTabs);

	// Formula nav below the hero
	$('.formula-products-carousel .slick-slide').click(function(){
		
		var formulaName = $(this).attr('class')
							.replace('slick-slide', '')
							.replace('slick-current', '')
							.replace('slick-active', '')
							.replace('toggle-', '')
							.replace(' ', '').replace(' ', '').replace(' ', ''),
			formulaTitle = '[data-content="' + formulaName + '&' + formulaName + '-image"]';


		$('.formula-stages').find('.tab-content, .hover-tab').removeClass('active');
		$('#' + formulaName + ', #' + formulaName + '-image, ' + formulaTitle).addClass('active');

		if ( formulaName.indexOf('sensitive') > -1 ) {
			$('.made-not-with li:first-child').hide();
		} else {
			$('.made-not-with li:first-child').show();
		}
	});

}



$('.made-with, .made-not-with').click(function(){

	if ( $(this).hasClass('active') ) {
		return;
	} else {
		$('.made-with, .made-not-with').toggleClass('active');
	}
});