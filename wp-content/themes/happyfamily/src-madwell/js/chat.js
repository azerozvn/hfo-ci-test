if ( $('#chat').length ) {

	var phoneImage = $('.chat__phone img'),
		phoneSrc = phoneImage.attr('src');

	phoneImage.wrap('<div class="chat__phone--masked"></div>')

	$('.chat__phone--masked').after(phoneImage.clone().addClass('animated'));

	var animatedPhone = $('.chat__phone img.animated');

	$(window).scroll(function(){

		if ( $('#chat').hasClass('viewed') && !animatedPhone.hasClass('loaded') ) {
			$('.chat__phone--masked').hide();
			animatedPhone.attr('src', phoneSrc + '?loaded').addClass('loaded');
		}

	});

}