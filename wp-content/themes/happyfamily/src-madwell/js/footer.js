$('.footer-newsletter__add-birthday').click(function (e) {
    $('.footer-newsletter__birthday-fields.visible').last().next().fadeIn('fast', function () {
        $(this).addClass('visible');

        if ($('.footer-newsletter__birthday-fields.visible').length === $('.footer-newsletter__birthday-fields').length) {
            $('.footer-newsletter__add-birthday').fadeOut('fast');
        }
    });
});

$(document).ready(function () {
    var $form = $('#mc-embedded-subscribe-form-footer');
    if ($form.length > 0) {
        $('#mc-embedded-subscribe-form-footer input[type="submit"]').bind('click', function (event) {
            if (event) event.preventDefault();
            if (validateEmail($form)) {
                register($form);
            }
        })
    }

    var $form2 = $('#mc-embedded-subscribe-form2');
    if ($form2.length > 0) {
        $('#mc-embedded-subscribe-form2 input[type="submit"]').bind('click', function (event) {
            if (event) event.preventDefault();
            if (validateEmail($form2)) {
                register2($form2);
            }
        })
    }
});

function validateEmail($form) {
    var emailFields = $($form).find('input.email');
    var pattern = /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i;
    for (var i = 0; i < emailFields.length; i++) {
        var value = $(emailFields[i]).val();
        if (!value || !pattern.test(value)) {
            $('#s-error-response, #error-response').html(
                '<span style="color:#ee7040">Please enter a valid email address.</span>'
            ).show();
            return false;
        }
    }

    $('#s-error-response, #error-response').hide();

    return true;
}

$(document).ready(function () {
    $('.datepart').on('change input', function (e) {
        var parent = $(e.target).parents('.footer-newsletter__birthday-fields');
        var month = $(parent).find('.monthfield input').val();
        var day = $(parent).find('.dayfield input').val();
        var year = $(parent).find('.yearfield input').val();
        /* Fill general input */
        $(parent).find('.birthday-input').attr('value', month + '-' + day + '-' + year);
    })
});

function register($form) {
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        cache: false,
        error: function (err) {
            alert('Could not connect to the registration server. Please try again later.');
        },
        success: function (data) {
            $('#responses .response').hide();

            if (data.msg.indexOf('is already subscribed') != -1) {
                data.msg = 'You are already subscribed to our Happy Family!';
                data.result = '';
            }

            if (data.result === 'success') {
                $('#mc_embed_signup_scroll').fadeOut(function () {
                    $('#success-response').html('<span style="color:#716F6C">' + data.msg + '</span>').fadeIn();
                });
            } else {
                $('#error-response').html('<span style="color:#ee7040">' + data.msg + '</span>').fadeIn();
            }
        }
    });
};

function register2($form2) {
    $.ajax({
        type: $form2.attr('method'),
        url: $form2.attr('action'),
        data: $form2.serialize(),
        cache: false,
        error: function (err) {
            alert('Could not connect to the registration server. Please try again later.');
        },
        success: function (data) {
            $('#responses .response-sidebar').hide();

            if (data.msg.indexOf('is already subscribed') != -1) {
                data.msg = 'You are already subscribed to our Happy Family!';
                data.result = '';
            }

            if (data.result === 'success') {
                $('#mc-embedded-subscribe-form2 #mc_embed_signup_scroll').fadeOut(function () {
                    $('#s-success-response').html('<span style="color:#716F6C">' + data.msg + '</span>').fadeIn();
                });
            } else {
                $('#s-error-response').html('<span style="color:#ee7040">' + data.msg + '</span>').fadeIn();
            }
        }
    });
};

$('[href*="#chat-now"]').click(function () {
    $("#open_fc_widget").trigger("click");
});

