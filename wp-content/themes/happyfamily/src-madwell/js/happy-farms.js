if ( $('.page-happy-farms').length ) {

	$("#hf-ingredients").find('.hf-ing-hero').each(function(idx, el) {
		var maxSize = 0;
		$(el).find(".hf-ing-hero-content ul li").each(function(iidx, lel) {
			maxSize = Math.max($(lel).text().length, maxSize);
		});
		$(el).toggleClass('two-line', maxSize > 22);
	});
	var urlHash = window.location.hash.replace("#", "").trim();
	if (urlHash != "") {
		$('header#main-header').removeClass('headroom--pinned').addClass('headroom--unpinned');
		var el = $('#' + urlHash).next('.hf-ing-dropdown');
		el.animate({
			height: el.find(">*").outerHeight(true, true)
		}, function() {
			el.height('auto');
		});
		el.data("open", true);
	}
	$(".hf-ing-learn-more").on('click', function(e) {
		e.preventDefault();
		var hero = $(this).parents('.hf-ing-hero');
		var el = hero.next('.hf-ing-dropdown');
		if (!el.data("open")) {
			el.animate({
				height: el.find(">*").outerHeight(true, true)
			}, function() {
				el.height('auto');
			});
		}
		el.data("open", true);
		if ($(window).width() > 640) {
			if (hero.offset().top > $('body').scrollTop() - 40) {
				$("body").animate({
					scrollTop: hero.offset().top
				});
			}
		} else {
			$("body").animate({
				scrollTop: el.offset().top
			});
		}
	});
	$(".hf-close").on('click', function(e) {
		e.preventDefault();
		var el = $(this).parents('.hf-ing-dropdown');
		if (el.data("open")) {
			el.animate({
				height: 0
			});
		}
		el.data("open", false);
	});

	$('a.fancybox-media, a.fancybox').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'minWidth': 0,
		helpers: {
			media: {}
		}
	});

	function checkHappyFarmsHomeHeroHeight() {
		if ($(window).width() <= 640) {
			$(".hf_home_hero .hero-image-mobile").outerHeight($(".hf_home_hero_content").height() + $(".hf-hero-image-right").height() + 40);
		} else {
			$(".hf_home_hero .hero-image-mobile").outerHeight('auto');
		}
	}
	$(window).on('resize', function() {
		var h1 = $(window).height() - $('#main-header').outerHeight();
		var h2 = $("#cc_hero").find("#cc_hero_content").outerHeight() + 170;
		$("#cc_hero").height(h2);
		var els = $("body.pl-layout-clearly_crafted").find(".product-body, .hero.product-line");
		var ww = $(window).width();
		if (ww < 350) {
			var pe = Math.max(0, Math.min(1, 1 - ((ww - 200) / 150)));
			var p = 470 + (pe * 200);
			els.css("background-size", p + "%");
		} else if (ww < 450) {
			var pe = Math.max(0, Math.min(1, 1 - ((ww - 300) / 150)));
			var p = 400 + (pe * 100);
			els.css("background-size", p + "%");
		} else {
			els.css("background-size", "initial");
		}
		$("body.pl-layout-clearly_crafted .product-body").css({
			backgroundPositionY: -$("body.pl-layout-clearly_crafted .hero.product-line").height() - 90
		})
		checkHappyFarmsHomeHeroHeight();
	}).resize();

}