// Homepage hero carousel

$('.home-hero-carousel > .col-12').slick({
	dots: true, 
	arrows: false,
	fade: true,
	autoplay: true,
	autoplaySpeed: 6000,
	rows: 0,
	customPaging: function(slider, i) {
		var thumb = $(slider.$slides[i]).find('.hero-content__fullwidth.wrapper img').attr('src');
		return '<img class="home-hero-carousel__thumb" src="' + thumb + '">';
	},
	responsive: [{
		breakpoint: 600,
		settings: {
			customPaging: function(slider, i) {
				return '<button type="button">1</button>';
			},
		}
	}]
});

// Homepage lifestage carousel

var lifestageCarousel = $('.home-lifestage-carousel  > .col-12');

lifestageCarousel.on('init', function(event, slick){ setCarouselColor( lifestageCarousel.find('.slick-current') ) });

lifestageCarousel.slick({
	dots: true,
	centerMode: true,
	centerPadding: '20%',
	responsive: [{
		breakpoint: 600,
		settings: {
			centerMode: false,
			centerPadding: 0
		}
	}]
});

lifestageCarousel.on('afterChange', function(event, slick, currentSlide, nextSlide){ 
	if ((currentSlide > nextSlide && (nextSlide !== 0 || currentSlide === 1)) || (currentSlide === 0 && nextSlide === slick.slideCount - 1)) {
		setCarouselColor( lifestageCarousel.find('.slick-current').prev() ) 
	}
	else {
		setCarouselColor( lifestageCarousel.find('.slick-current') ) 
	}
});

function setCarouselColor(slide) {
	var colorClass = slide.find('.hero-container__fullwidth').attr('class').replace('hero-container__fullwidth ', '');
	lifestageCarousel.parent().removeClass('blue green yellow purple').addClass(colorClass);
	$('.home-hero-intro').removeClass('section-blue section-green section-yellow section-purple').addClass('section-' + colorClass);
}

// Hompage Ingredient Carousel (mobile only)

$('.home-ingredient-carousel').slick({ dots: true, fade: true });

// Our Impact projects carousel (mobile only)

function impactCarousel(){
	if ( $(window).width() < 601 && !$('.home-our-impact__carousel').hasClass('slick-initialized') ) {
		$('.home-our-impact__carousel').slick();
	}
}
impactCarousel();
$(window).resize(impactCarousel());


// Fiber and protein products carousel
$('.fiber-protein-products-carousel  > .col-12').slick({
	slidesToShow: 4,
	slidesToScroll: 4,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		}
	}]
});

// Fiber and protein products carousel
$('.fiber-protein-family-carousel > .col-12').slick({
	slidesToShow: 3,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
		}
	}, {
		breakpoint: 601,
		settings: {
			slidesToShow: 1,
		}
	}]
});

// Formula products carousel
$('.formula-products-carousel  > .col-12').slick({
	slidesToShow: 3,
	rows: 0,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
		}
	}]
});

// Yogurt products carousel
$('.yogurt-products-carousel > .main > .col-12').slick({
	slidesToShow: 10,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3,
		}
	}, {
		breakpoint: 601,
		settings: {
			slidesToShow: 2,
		}
	}]
});

// Video carousel
$('.carousel-video-wrapper > .col-12').slick({
	slidesToShow: 10,
	rows: 0,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 1,
		}
	}]
});

if ( $('.home-learning-center').length && $(window).width() > 1023 ) {
	var carouselRows = 2;
} else {
	var carouselRows = 0;
}

// All the Learning Center carousels
$('.learning-center-carousel').each(function(){
	var slideCountDesktop = parseInt( $(this).attr('data-slides-desktop') ),
		slideCountTablet = parseInt( $(this).attr('data-slides-tablet') ),
		slideCountMobile = parseInt( $(this).attr('data-slides-mobile') );

	$(this).find('.learning-center-carousel__container').slick({
		slidesToShow: slideCountDesktop,
		slidesToScroll: slideCountDesktop,
		rows: carouselRows,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: slideCountTablet,
				slidesToScroll: slideCountTablet
			}
		}, {
			breakpoint: 601,
			settings: {
				slidesToShow: slideCountMobile,
				slidesToScroll: slideCountMobile
			}
		}]
	});
});

// Add dots to the Mission Learning Center carousel.
$('.mission-go-green-content .learning-center-carousel__container').slick('setOption', 'responsive',
	[{
		breakpoint: 601,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true
		}
	}],
	true
);

// Our Story timeline carousel
$('.story-carousel > .col-12').slick({
	dots: true,
	centerMode: true,
	centerPadding: '20%',
	appendDots: '.slick-dots-wrapper',
	customPaging: function(slider, i) {
		var year, color, slide;
		slide = $(slider.$slides[i]);
		year = slide.find('.hero-content__fullwidth.wrapper .hero').text();
		color = slide.find('.hero-content__fullwidth.wrapper')
			.attr('class')
			.replace(/hero-content__fullwidth\s|\swrapper/g, '');

		return '<span class="' + color + '">' + year + '</span>';
	},
	responsive: [{
		breakpoint: 1024,
		settings: {
			centerPadding: '12%'
		}
	}, {
		breakpoint: 601,
		settings: {
			centerMode: false,
			centerPadding: 0,
			arrows: false
		}
	}]
});

// Our Story timeline carousel dots when don't fit in the window
$('.slick-dots-slide').on('click', function() {
	if ( $(this).hasClass('disabled') ) {
		return false;
	}
	
	var currentLeft, dots, newLeft, prevButton, nextButton, maxRight, dotsWrapperWidth;
	var PIXELS_PER_SCROLL = 300;

	dots = $(this).closest('.slick-dots-wrapper').find('.slick-dots li');
	dotsWrapperWidth = $(this).closest('.slick-dots-wrapper').find('.slick-dots').width();
	prevButton = $(this).closest('.slick-dots-wrapper').find('.slick-dots-slide--prev');
	nextButton = $(this).closest('.slick-dots-wrapper').find('.slick-dots-slide--next');
	maxRight = 0;
	dots.each(function() {
		maxRight += $(this).width() + parseInt($(this).css('margin-left'));
	});
	currentLeft = parseInt(dots.css('left'));

	if ( $(this).hasClass('slick-dots-slide--next') ) {
		newLeft = currentLeft - PIXELS_PER_SCROLL;
	} else {
		newLeft = currentLeft + PIXELS_PER_SCROLL;
	}

	newLeftAndWrapperWidth = dotsWrapperWidth + Math.abs(newLeft);
	if ( newLeftAndWrapperWidth >= maxRight ) {
		nextButton.addClass('disabled');
	} else if ( newLeftAndWrapperWidth <= maxRight ) {
		nextButton.removeClass('disabled');
	}

	if ( newLeft < 0 ) {
		prevButton.removeClass('disabled');
	} else if ( newLeft === 0 ) {
		prevButton.addClass('disabled');
	}

	dots.css('left', newLeft + 'px');

	return false;
});

// Story press articles carousel
$('.story-press .press-container').slick({
	slidesToShow: 3,
	rows: 0,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
		}
	}, {
		breakpoint: 601,
		settings: {
			slidesToShow: 1,
		}
	}]
});

// Mission points carousel
$('.mission-organic-points-carousel').slick({
	slidesToShow: 10,
	responsive: [{
		breakpoint: 601,
		settings: {
			slidesToShow: 1,
			arrows: false,
			dots: true
		}
	}]
});

// Mission companies carousel
$('.mission-carousel-inner--companies, .mission-carousel-inner--world').slick({
	slidesToShow: 3,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
		}
	}, {
		breakpoint: 601,
		settings: {
			slidesToShow: 1,
			dots: true
		}
	}]
});

// Promotions carousel on coupon pages
if ( $(window).width() < 1024 ) {
    var firstSlide = $('.left-banner').clone();
    $('.right-banners-container > .col-12').prepend(firstSlide).slick({ dots: true });
}

jQuery(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        jQuery(this).trigger('resizeEnd');
    }, 200);
});

jQuery(window).on('resizeEnd', function() {
    if ( jQuery(window).width() < 1024 ) {
    	if ( jQuery('.right-banners-container > .col-12 .left-banner').length == 0 ) {
	        var firstSlide = jQuery('.left-banner').clone();
	        jQuery('.right-banners-container > .col-12').prepend(firstSlide)
       	}
        jQuery('.right-banners-container > .col-12').slick({ dots: true });
    } else if ( jQuery(window).width() >= 1024 && jQuery('.right-banners-container > .col-12 .left-banner').length > 0 ) {
        jQuery('.right-banners-container > .col-12').slick('unslick').find('.left-banner').remove();
    }
});

// Lifestage PLP carousels

$('.lifestage-filter__description + .vc_column_container').each(function(){
	$(this).slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		rows: 0,
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			}
		}, {
			breakpoint: 601,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		}]
	});
});