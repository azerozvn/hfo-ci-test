// Press Archive dropdown

$('.press-archive__title').click(function(e){
	$('.press-archive').toggleClass('expanded');
});

// Filtering by year

$('.press-archive__year a').click(function(e){
	e.preventDefault();
	$('.press-archive').toggleClass('expanded');

	var yearInt = parseInt($(this).html());

	$('.press-archive__title').text(yearInt);

	$.ajax({
		type: 'POST',
		url: ajax_params.url,
		data: {
			action: 'load_year',
			year: yearInt
		},
		success: function(response) {
			$('.posts-by-year').html(response).css('display', 'flex');
			$('.all-posts, .press-pagination').hide();
			return false;
		},
		error:  function(response) {
			console.log(response);
		}
	});
});

$('.press-archive__all').click(function(e){
	e.preventDefault();
	$('.press-archive').toggleClass('expanded');
	$('.press-archive__title').text('Sort by year');

	$('.posts-by-year').html('').hide();
	$('.all-posts').css('display', 'flex');
	$('.press-pagination').show();
});


