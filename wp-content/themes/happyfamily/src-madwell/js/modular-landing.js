function  updateProgress(e, slick) {
    var $dots = slick.$dots;

    if(!$dots){
        return;
    }

    var $activeDot = $dots.find('.slick-active');
    var $allPrev = $activeDot.prevAll();

    $dots.find('li').removeClass('progress');
    $allPrev.addClass('progress');
    $activeDot.addClass('progress');
}

$.each($('.image-slider'), function () {
    var $this = $(this);
    var $items = $this.find('.items');
    var data = $this.data('slickOptions');

    if(!data){
        return;
    }
    data = JSON.parse(data);

    var dotsSelector = data['dotsSelector'];

    if(dotsSelector){
        data.appendDots = $this.find(dotsSelector);
    }

    $items.on('afterChange init', function(e, slick){
        updateProgress(e, slick)
    });

    $items.slick(data);
});


var mobileBreakpoint = 768;

function isMobile () {
    return $(window).width() <= mobileBreakpoint
}

function initializeFancybox ($element) {
    $.fancybox.open($element, {
        type: 'iframe'
    });
}

$('.video-icon').click(function (e) {
    e.stopPropagation();
    e.preventDefault();

    var $this = $(this);
    var data = $this.data('sources');

    if(!data){
        return;
    }

    data = JSON.parse(data);

    if(isMobile() && data.mobile){
        $this.attr('href', data.mobile);
        initializeFancybox($this)

    }

    if(!isMobile() && data.desktop){
        $this.attr('href', data.desktop);
        initializeFancybox($this);
    }
});


/**
 * module-two class added when modular-button-wrapper block is present
 */
var resetModularPadding = function(){
    $('.modular-wrapper .main_module_modular + .modular-button-wrapper.main_module_modular')
    .prev('.modular-wrapper').addClass('modult-two');
}
$(document).ready(function(){
    resetModularPadding();
});