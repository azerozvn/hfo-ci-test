// Mark submenu lvls
$('.wp-megamenu .wp-megamenu-sub-menu .wp-megamenu-sub-menu .wp-megamenu-sub-menu').addClass('level-3');
$('.wp-megamenu .wp-megamenu-sub-menu .wp-megamenu-sub-menu').not('.level-3').addClass('level-2');
$('.wp-megamenu .wp-megamenu-sub-menu').not('.level-2').not('.level-3').addClass('level-1');

// Handle sub-lvl menu clicks

// Mark sub-submenus
$('.level-2 :eq(0)').addClass('sub-submenu');
$('.level-3 > .menu-item > [class*=container]').each(function () {
    $(this).prev('.wpmm-item-title').addClass('sub-submenu');
});

// Search icon toggle class on mobile view functionality
$('.mobile-search-icon').on('click', function(){
    $('body').toggleClass('headerSearchActive');
});

$('body').click(function(event) {
    $target = $(event.target);
    if(!$target.is('.mobile-search-icon') && !$target.is('.algolia-search-input')) {
        $('body').removeClass('headerSearchActive');
        $('.serach_mini_form, .algolia-search-input').removeClass('expanded');
    }
});
// End here

// Added class to submenu images
$('.menu-item').each(function(){
    if($(this).find('.image').length > 0){
        $(this).parents('li.wpmm-col').addClass('menu-promo-image');
    }
    var currentLinkVal = $(this).find('a').text().trim();
    if(currentLinkVal === "SHOP ALL"){
        $(this).find('a').addClass('shop-icon');
    }
});
// End here