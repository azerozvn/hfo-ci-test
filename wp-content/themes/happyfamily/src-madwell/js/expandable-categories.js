var ExpandableCategory = function() {
	this.currentCategory = null;
	
	this.categoryGroups = $('.expandable-categories__group');

	this.activeBtnClass = 'button-expand-category--active';
	this.activeGroupClass = 'expandable-categories__group--active';

}


ExpandableCategory.prototype.init = function() {
	this.isMobile = checkMobile();

	this.getCategoryButtons();

	this.currentCategory = getName($(this.categoryButtons[0]));
	
	if(!this.isMobile) {
		this.showCategory();
	}

	this.bindEvents();
}

ExpandableCategory.prototype.getCategoryButtons = function() {
	this.categoryButtons = $('.expandable-categories__categories .button-expand-category');
	this.categoryButtonsMobile = $('.expandable-categories__content .button-expand-category');
}

ExpandableCategory.prototype.bindEvents = function() {
	this.categoryButtons.each($.proxy(function(index, button) {
		var $button = $(button);

		$button.on('click', ($.proxy(function(e) {
			e.preventDefault();

			this.currentCategory = getName($button);
			this.showCategory();
		
		}, this)));
	}, this));

	this.categoryButtonsMobile.each($.proxy(function(index, button) {
		var $button = $(button);

		$button.on('click', ($.proxy(function(e) {
			e.preventDefault();
			this.currentCategory = getName($button);
			$button.siblings('#' + this.currentCategory).toggleClass(this.activeGroupClass);
			$button.toggleClass(this.activeBtnClass);
		}, this)));
	}, this));

	$(window).on('resize', $.proxy(function() {
		this.onResize();
	}, this));
}


ExpandableCategory.prototype.showCategory = function() {
	this.categoryGroups.each($.proxy(function(index, category) {
		var $category = $(category);
		var id = $category.attr('id');

		if(id !== this.currentCategory) {
			if(!$category.hasClass(this.activeGroupClass)) return;
			$category.removeClass(this.activeGroupClass);
		} else {
			$category.addClass(this.activeGroupClass);
		}
	}, this));


	this.categoryButtons.each($.proxy(function(index, button) {
		var $button = $(button);
		var target = getName($button);

		if(target !== this.currentCategory) {
			if(!$button.hasClass(this.activeBtnClass)) return;
			$button.removeClass(this.activeBtnClass);
		} else {
			$button.addClass(this.activeBtnClass);
		}
	}, this));
}

ExpandableCategory.prototype.onResize = function() {
	var isMobile = checkMobile();

	if(isMobile !== this.isMobile && isMobile) { //going from desktop to mobile	
		$('.' + this.activeGroupClass).removeClass(this.activeGroupClass);
		$('.' + this.activeBtnClass).removeClass(this.activeBtnClass);
	}

	if(isMobile !== this.isMobile && !isMobile) { //going from mobile to desktop
		this.showCategory();
	}
	this.isMobile = isMobile;
	
}

function getName($button) {
	return $button.data('target');
}

function checkMobile() {
	return window.innerWidth <= 600;
}

if($('#faq').size() > 0 || $('#careers').size() > 0) {

	new ExpandableCategory().init();
}