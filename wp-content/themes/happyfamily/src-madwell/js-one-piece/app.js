////////////////////////////////////////////////////
// HappyFamily theme scripts, by Madwell Agency
// 
// Note: I disabled two "require" statements because they were throwing errors, and 
// the repo didn't seem to have the required files.  The disabled statements contained the following:
// - require('jquery') 
// - require("lazysizes")
// 

// Removed Slick.js, moved it to `dist/slick.min.js`, enqueued by Wordpress as its own JS file.
// TODO: Remove this file, and load `src-madwell/js/*` into Webpack in the correct order.

jQuery(document).ready(function($){

function createCookie(name, value, days) {
   var expires;

   if (days) {
       var date = new Date();
       date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
       expires = "; expires=" + date.toGMTString();
   } else {
       expires = "";
   }
   document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
   var nameEQ = encodeURIComponent(name) + "=";
   var ca = document.cookie.split(';');
   for (var i = 0; i < ca.length; i++) {
       var c = ca[i];
       while (c.charAt(0) === ' ')
           c = c.substring(1, c.length);
       if (c.indexOf(nameEQ) === 0)
           return decodeURIComponent(c.substring(nameEQ.length, c.length));
   }
   return null;
}

function eraseCookie(name) {
   createCookie(name, "", -1);
}

$(document).ready(function(){
// Gallery Slick initialize
$('.slick-video-module-carousal .slides').slick({
   infinite: false,
   slidesToShow: 1,
   slidesToScroll: 1,
   autoplay: false,
   arrows:false,
   dots:true,
   responsive: [
       {
           breakpoint: 768,
           settings: {
               arrows: false,
               adaptiveHeight: true,
           }
       },
   ]
});
// End here

//Team carousal
var $slider = $('.team-slides'),
   $progressBar = $('.progress'),
   $testimonialLeft = $('.testimonial-left'),
   $testimonialRight = $('.testimonial-right'),
   $sliderController = $('.slider-controller a');

$slider.on('init', function(event, slick) {
   var countSlide = slick.slideCount;
   initialProgress = (100 / countSlide);
   $progressBar
       .css('background-size', initialProgress + '% 100%')
       .attr('aria-valuenow', initialProgress );
   $testimonialLeft.addClass('slick-disabled');
   if(countSlide === 1){
       $sliderController.hide();
   }
});

$slider.slick({
   infinite: false,
   slidesToShow: 1.1,
   autoplay: false,
   arrows:false,
   adaptiveHeight: false,
   dots: false,
   responsive: [
       {
           breakpoint: 768,
           settings: {
               slidesToShow: 1,
               arrows: true,
               adaptiveHeight: false,
           }
       },
   ]
});

$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
   var calc = ((nextSlide + 1) / (slick.slideCount)) * 100,
       calcSlideCount = slick.slideCount - 1,
       actualSlideCount = slick.slideCount;

   $progressBar
       .css('background-size', calc + '% 100%')
       .attr('aria-valuenow', calc );
   if(actualSlideCount > 2) {
       if (nextSlide === 0) {
           $testimonialLeft.addClass('slick-disabled');
       }
       else if (nextSlide === calcSlideCount) {
           $testimonialRight.addClass('slick-disabled');
       } else {
           $sliderController.removeClass('slick-disabled');
       }
   }else{
       $sliderController.toggleClass('slick-disabled');
   }
});

$testimonialLeft.click(function(){
   $slider.slick('slickPrev');
});

$testimonialRight.click(function(){
   $slider.slick('slickNext');
});
// End here

//Svg color dynamic
$("img.svg").each(function () {
   // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
   var $img = jQuery(this);
   // Get all the attributes.
   var attributes = $img.prop("attributes");
   // Dynamic Color
   var dynamicColor = $img.parent('div.play-icon').data('color');
   // Get the image's URL.
   var imgURL = $img.attr("src");
   // Fire an AJAX GET request to the URL.
   $.get(imgURL, function (data) {
       // The data you get includes the document type definition, which we don't need.
       // We are only interested in the <svg> tag inside that.
       var $svg = $(data).find('svg');
       // Remove any invalid XML tags as per http://validator.w3.org
       $svg = $svg.removeAttr('xmlns:a');
       // Loop through original image's attributes and apply on SVG
       $.each(attributes, function() {
           $svg.attr(this.name, this.value);
       });
       $svg.find('circle').css('fill', dynamicColor);
       // Replace image with new SVG
       $img.replaceWith($svg);
   });
});
// End here


// Dynamic font size implementation
var cssStyles = "";
function addDynamicClassCampaign(title, mode, lHeight, clsElement) {
   $(title).each(function() {
       var size = $(this).parent().data(mode),
           lineHeight = $(this).parent().data(lHeight),
           clsName = $(this).parent().data(clsElement);
       $(this).addClass(clsName);
       cssStyles += "." + clsName + "{ font-size:" + size + "; line-height:" +lineHeight+";}";
   });
   $("<style type='text/css'>" + cssStyles + "</style>").appendTo("head");
   cssStyles = "";
}
addDynamicClassCampaign('.dynamic-font', 'size', 'line-height', 'class');
addDynamicClassCampaign('.dynamic-font-mob', 'mob-size', 'mob-line-height', 'mob-class');
// End here

// Scroll till that section
   $(window).on('load', function(){
       var currentUrl = window.location.href;
       if(currentUrl.indexOf('#our-story') != -1) {
           var target = $('.video-module'),
               targetHeight = $('.video-module').offset().top,
               headerHeight = $('.header-container').innerHeight(),
               scrollHeight = Number(targetHeight - headerHeight);
           if (target.length) {
               $('html,body').animate({
                   scrollTop: scrollHeight
               }, 1000);
               return false;
           }
       }
   });
// End here
// AdaptiveHeight solution for team slides
   $('.team-slides img.lazyload').on('lazyloaded', function(event, slick, image, imageSource) {
       $('.team-slides .slick-slide').each(function(){
           var heightSlide = $(this).innerHeight();
           $(this).css('height', heightSlide+'px');
       });
       var controlsHeight = $('.team-slides .slick-active .carousel-img').height();
       $('.campaign-wrapper .team-slides button.slick-next, .campaign-wrapper .team-slides button.slick-prev').css('top', ((controlsHeight/2) - 14)+'px');
   });
// End here
});
// Homepage hero carousel

$('.home-hero-carousel > .col-12').slick({
   dots: true, 
   arrows: false,
   fade: true,
   autoplay: false,
   autoplaySpeed: 6000,
   rows: 0,
   customPaging: function(slider, i) {
       var thumb = $(slider.$slides[i]).find('.hero-content__fullwidth.wrapper img').attr('src');
       return '<img class="home-hero-carousel__thumb" src="' + thumb + '">';
   },
   responsive: [{
       breakpoint: 600,
       settings: {
           customPaging: function(slider, i) {
               return '<button type="button">1</button>';
           },
       }
   }]
});

// Homepage lifestage carousel

var lifestageCarousel = $('.home-lifestage-carousel  > .col-12');

lifestageCarousel.on('init', function(event, slick){ setCarouselColor( lifestageCarousel.find('.slick-current') ) });

lifestageCarousel.slick({
   dots: true,
   centerMode: true,
   centerPadding: '20%',
   responsive: [{
       breakpoint: 600,
       settings: {
           centerMode: false,
           centerPadding: 0
       }
   }]
});

lifestageCarousel.on('afterChange', function(event, slick, currentSlide, nextSlide){ 
   if ((currentSlide > nextSlide && (nextSlide !== 0 || currentSlide === 1)) || (currentSlide === 0 && nextSlide === slick.slideCount - 1)) {
       setCarouselColor( lifestageCarousel.find('.slick-current').prev() ) 
   }
   else {
       setCarouselColor( lifestageCarousel.find('.slick-current') ) 
   }
});

function setCarouselColor(slide) {
   var colorClass = slide.find('.hero-container__fullwidth').attr('class').replace('hero-container__fullwidth ', '');
   lifestageCarousel.parent().removeClass('blue green yellow purple').addClass(colorClass);
   $('.home-hero-intro').removeClass('section-blue section-green section-yellow section-purple').addClass('section-' + colorClass);
}

// Hompage Ingredient Carousel (mobile only)

$('.home-ingredient-carousel').slick({ dots: true, fade: true });

// Our Impact projects carousel (mobile only)

function impactCarousel(){
   if ( $(window).width() < 601 && !$('.home-our-impact__carousel').hasClass('slick-initialized') ) {
       $('.home-our-impact__carousel').slick();
   }
}
impactCarousel();
$(window).resize(impactCarousel());


// Fiber and protein products carousel
$('.fiber-protein-products-carousel  > .col-12').slick({
   slidesToShow: 4,
   slidesToScroll: 4,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 2,
           slidesToScroll: 2
       }
   }]
});

// Fiber and protein products carousel
$('.fiber-protein-family-carousel > .col-12').slick({
   slidesToShow: 3,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 2,
       }
   }, {
       breakpoint: 601,
       settings: {
           slidesToShow: 1,
       }
   }]
});

// Formula products carousel
$('.formula-products-carousel  > .col-12').slick({
   slidesToShow: 3,
   rows: 0,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 2,
       }
   }]
});

// Yogurt products carousel
$('.yogurt-products-carousel > .main > .col-12').slick({
   slidesToShow: 10,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 3,
       }
   }, {
       breakpoint: 601,
       settings: {
           slidesToShow: 2,
       }
   }]
});

// Video carousel
$('.carousel-video-wrapper > .col-12').slick({
   slidesToShow: 10,
   rows: 0,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 1,
       }
   }]
});

if ( $('.home-learning-center').length && $(window).width() > 1023 ) {
   var carouselRows = 2;
} else {
   var carouselRows = 0;
}

// All the Learning Center carousels
$('.learning-center-carousel').each(function(){
   var slideCountDesktop = parseInt( $(this).attr('data-slides-desktop') ),
       slideCountTablet = parseInt( $(this).attr('data-slides-tablet') ),
       slideCountMobile = parseInt( $(this).attr('data-slides-mobile') );

   $(this).find('.learning-center-carousel__container').slick({
       slidesToShow: slideCountDesktop,
       slidesToScroll: slideCountDesktop,
       rows: carouselRows,
       responsive: [{
           breakpoint: 1024,
           settings: {
               slidesToShow: slideCountTablet,
               slidesToScroll: slideCountTablet
           }
       }, {
           breakpoint: 601,
           settings: {
               slidesToShow: slideCountMobile,
               slidesToScroll: slideCountMobile
           }
       }]
   });
});

// Add dots to the Mission Learning Center carousel.
$('.mission-go-green-content .learning-center-carousel__container').slick('setOption', 'responsive',
   [{
       breakpoint: 601,
       settings: {
           slidesToShow: 1,
           slidesToScroll: 1,
           dots: true
       }
   }],
   true
);

// Our Story timeline carousel
$('.story-carousel > .col-12').slick({
   dots: true,
   centerMode: true,
   centerPadding: '20%',
   appendDots: '.slick-dots-wrapper',
   customPaging: function(slider, i) {
       var year, color, slide;
       slide = $(slider.$slides[i]);
       year = slide.find('.hero-content__fullwidth.wrapper .hero').text();
       color = slide.find('.hero-content__fullwidth.wrapper')
           .attr('class')
           .replace(/hero-content__fullwidth\s|\swrapper/g, '');

       return '<span class="' + color + '">' + year + '</span>';
   },
   responsive: [{
       breakpoint: 1024,
       settings: {
           centerPadding: '12%'
       }
   }, {
       breakpoint: 601,
       settings: {
           centerMode: false,
           centerPadding: 0,
           arrows: false
       }
   }]
});

// Our Story timeline carousel dots when don't fit in the window
$('.slick-dots-slide').on('click', function() {
   if ( $(this).hasClass('disabled') ) {
       return false;
   }
   
   var currentLeft, dots, newLeft, prevButton, nextButton, maxRight, dotsWrapperWidth;
   var PIXELS_PER_SCROLL = 300;

   dots = $(this).closest('.slick-dots-wrapper').find('.slick-dots li');
   dotsWrapperWidth = $(this).closest('.slick-dots-wrapper').find('.slick-dots').width();
   prevButton = $(this).closest('.slick-dots-wrapper').find('.slick-dots-slide--prev');
   nextButton = $(this).closest('.slick-dots-wrapper').find('.slick-dots-slide--next');
   maxRight = 0;
   dots.each(function() {
       maxRight += $(this).width() + parseInt($(this).css('margin-left'));
   });
   currentLeft = parseInt(dots.css('left'));

   if ( $(this).hasClass('slick-dots-slide--next') ) {
       newLeft = currentLeft - PIXELS_PER_SCROLL;
   } else {
       newLeft = currentLeft + PIXELS_PER_SCROLL;
   }

   newLeftAndWrapperWidth = dotsWrapperWidth + Math.abs(newLeft);
   if ( newLeftAndWrapperWidth >= maxRight ) {
       nextButton.addClass('disabled');
   } else if ( newLeftAndWrapperWidth <= maxRight ) {
       nextButton.removeClass('disabled');
   }

   if ( newLeft < 0 ) {
       prevButton.removeClass('disabled');
   } else if ( newLeft === 0 ) {
       prevButton.addClass('disabled');
   }

   dots.css('left', newLeft + 'px');

   return false;
});

// Story press articles carousel
$('.story-press .press-container').slick({
   slidesToShow: 3,
   rows: 0,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 2,
       }
   }, {
       breakpoint: 601,
       settings: {
           slidesToShow: 1,
       }
   }]
});

// Mission points carousel
$('.mission-organic-points-carousel').slick({
   slidesToShow: 10,
   responsive: [{
       breakpoint: 601,
       settings: {
           slidesToShow: 1,
           arrows: false,
           dots: true
       }
   }]
});

// Mission companies carousel
$('.mission-carousel-inner--companies, .mission-carousel-inner--world').slick({
   slidesToShow: 3,
   responsive: [{
       breakpoint: 1024,
       settings: {
           slidesToShow: 2,
       }
   }, {
       breakpoint: 601,
       settings: {
           slidesToShow: 1,
           dots: true
       }
   }]
});

// Promotions carousel on coupon pages
if ( $(window).width() < 1024 ) {
   var firstSlide = $('.left-banner').clone();
   $('.right-banners-container > .col-12').prepend(firstSlide).slick({ dots: true });
}

jQuery(window).resize(function() {
   if(this.resizeTO) clearTimeout(this.resizeTO);
   this.resizeTO = setTimeout(function() {
       jQuery(this).trigger('resizeEnd');
   }, 200);
});

jQuery(window).on('resizeEnd', function() {
   if ( jQuery(window).width() < 1024 ) {
       if ( jQuery('.right-banners-container > .col-12 .left-banner').length == 0 ) {
           var firstSlide = jQuery('.left-banner').clone();
           jQuery('.right-banners-container > .col-12').prepend(firstSlide)
          }
       jQuery('.right-banners-container > .col-12').slick({ dots: true });
   } else if ( jQuery(window).width() >= 1024 && jQuery('.right-banners-container > .col-12 .left-banner').length > 0 ) {
       jQuery('.right-banners-container > .col-12').slick('unslick').find('.left-banner').remove();
   }
});

// Lifestage PLP carousels

$('.lifestage-filter__description + .vc_column_container').each(function(){
   $(this).slick({
       slidesToShow: 4,
       slidesToScroll: 4,
       rows: 0,
       responsive: [{
           breakpoint: 1024,
           settings: {
               slidesToShow: 3,
               slidesToScroll: 3,
           }
       }, {
           breakpoint: 601,
           settings: {
               slidesToShow: 2,
               slidesToScroll: 2,
           }
       }]
   });
});
if ( $('#chat').length ) {

   var phoneImage = $('.chat__phone img'),
       phoneSrc = phoneImage.attr('src');

   phoneImage.wrap('<div class="chat__phone--masked"></div>')

   $('.chat__phone--masked').after(phoneImage.clone().addClass('animated'));

   var animatedPhone = $('.chat__phone img.animated');

   $(window).scroll(function(){

       if ( $('#chat').hasClass('viewed') && !animatedPhone.hasClass('loaded') ) {
           $('.chat__phone--masked').hide();
           animatedPhone.attr('src', phoneSrc + '?loaded').addClass('loaded');
       }

   });

}
$('[data-expand]').on('click', function() {
   var target, expandBreakpoint, button, expand;

   button = $(this);
   expand = function() {
       button.toggleClass('active');
       target = $(button.data('expand'));
       target.slideToggle();
   }

   expandBreakpoint = button.data('expand-breakpoint');
   if ( expandBreakpoint > 0) {
       if ( $(window).width() <= expandBreakpoint) {
           expand();
       }
   } else {
       expand();
   }
});

var ExpandableCategory = function() {
   this.currentCategory = null;
   
   this.categoryGroups = $('.expandable-categories__group');

   this.activeBtnClass = 'button-expand-category--active';
   this.activeGroupClass = 'expandable-categories__group--active';

}


ExpandableCategory.prototype.init = function() {
   this.isMobile = checkMobile();

   this.getCategoryButtons();

   this.currentCategory = getName($(this.categoryButtons[0]));
   
   if(!this.isMobile) {
       this.showCategory();
   }

   this.bindEvents();
}

ExpandableCategory.prototype.getCategoryButtons = function() {
   this.categoryButtons = $('.expandable-categories__categories .button-expand-category');
   this.categoryButtonsMobile = $('.expandable-categories__content .button-expand-category');
}

ExpandableCategory.prototype.bindEvents = function() {
   this.categoryButtons.each($.proxy(function(index, button) {
       var $button = $(button);

       $button.on('click', ($.proxy(function(e) {
           e.preventDefault();

           this.currentCategory = getName($button);
           this.showCategory();
       
       }, this)));
   }, this));

   this.categoryButtonsMobile.each($.proxy(function(index, button) {
       var $button = $(button);

       $button.on('click', ($.proxy(function(e) {
           e.preventDefault();
           this.currentCategory = getName($button);
           $button.siblings('#' + this.currentCategory).toggleClass(this.activeGroupClass);
           $button.toggleClass(this.activeBtnClass);
       }, this)));
   }, this));

   $(window).on('resize', $.proxy(function() {
       this.onResize();
   }, this));
}


ExpandableCategory.prototype.showCategory = function() {
   this.categoryGroups.each($.proxy(function(index, category) {
       var $category = $(category);
       var id = $category.attr('id');

       if(id !== this.currentCategory) {
           if(!$category.hasClass(this.activeGroupClass)) return;
           $category.removeClass(this.activeGroupClass);
       } else {
           $category.addClass(this.activeGroupClass);
       }
   }, this));


   this.categoryButtons.each($.proxy(function(index, button) {
       var $button = $(button);
       var target = getName($button);

       if(target !== this.currentCategory) {
           if(!$button.hasClass(this.activeBtnClass)) return;
           $button.removeClass(this.activeBtnClass);
       } else {
           $button.addClass(this.activeBtnClass);
       }
   }, this));
}

ExpandableCategory.prototype.onResize = function() {
   var isMobile = checkMobile();

   if(isMobile !== this.isMobile && isMobile) { //going from desktop to mobile	
       $('.' + this.activeGroupClass).removeClass(this.activeGroupClass);
       $('.' + this.activeBtnClass).removeClass(this.activeBtnClass);
   }

   if(isMobile !== this.isMobile && !isMobile) { //going from mobile to desktop
       this.showCategory();
   }
   this.isMobile = isMobile;
   
}

function getName($button) {
   return $button.data('target');
}

function checkMobile() {
   return window.innerWidth <= 600;
}

if($('#faq').size() > 0 || $('#careers').size() > 0) {

   new ExpandableCategory().init();
}
$('.footer-newsletter__add-birthday').click(function (e) {
   $('.footer-newsletter__birthday-fields.visible').last().next().fadeIn('fast', function () {
       $(this).addClass('visible');

       if ($('.footer-newsletter__birthday-fields.visible').length === $('.footer-newsletter__birthday-fields').length) {
           $('.footer-newsletter__add-birthday').fadeOut('fast');
       }
   });
});

$(document).ready(function () {
   var $form = $('#mc-embedded-subscribe-form-footer');
   if ($form.length > 0) {
       $('#mc-embedded-subscribe-form-footer input[type="submit"]').bind('click', function (event) {
           if (event) event.preventDefault();
           if (validateEmail($form)) {
               register($form);
           }
       })
   }

   var $form2 = $('#mc-embedded-subscribe-form2');
   if ($form2.length > 0) {
       $('#mc-embedded-subscribe-form2 input[type="submit"]').bind('click', function (event) {
           if (event) event.preventDefault();
           if (validateEmail($form2)) {
               register2($form2);
           }
       })
   }
});

function validateEmail($form) {
   var emailFields = $($form).find('input.email');
   var pattern = /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i;
   for (var i = 0; i < emailFields.length; i++) {
       var value = $(emailFields[i]).val();
       if (!value || !pattern.test(value)) {
           $('#s-error-response, #error-response').html(
               '<span style="color:#ee7040">Please enter a valid email address.</span>'
           ).show();
           return false;
       }
   }

   $('#s-error-response, #error-response').hide();

   return true;
}

$(document).ready(function () {
   $('.datepart').on('change input', function (e) {
       var parent = $(e.target).parents('.footer-newsletter__birthday-fields');
       var month = $(parent).find('.monthfield input').val();
       var day = $(parent).find('.dayfield input').val();
       var year = $(parent).find('.yearfield input').val();
       /* Fill general input */
       $(parent).find('.birthday-input').attr('value', month + '-' + day + '-' + year);
   })
});

function register($form) {
   $.ajax({
       type: $form.attr('method'),
       url: $form.attr('action'),
       data: $form.serialize(),
       cache: false,
       error: function (err) {
           // Disable alert message.  Form is showing error message even though Listrak says everything works on their end.
           // alert('Could not connect to the registration server. Please try again later.');
           console.log('app.js register() error:', err);
           $('#mc_embed_signup_scroll').fadeOut(function () {
               $('#success-response').html('<span style="color:#716F6C">Success!  Thank you for signing up.</span>').fadeIn();
           });
       },
       success: function (data) {
           $('#responses .response').hide();

           if (data.msg.indexOf('is already subscribed') != -1) {
               data.msg = 'You are already subscribed to our Happy Family!';
               data.result = '';
           }

           if (data.result === 'success') {
               $('#mc_embed_signup_scroll').fadeOut(function () {
                   $('#success-response').html('<span style="color:#716F6C">' + data.msg + '</span>').fadeIn();
               });
           } else {
               $('#error-response').html('<span style="color:#ee7040">' + data.msg + '</span>').fadeIn();
           }
       }
   });
};

function register2($form2) {
   $.ajax({
       type: $form2.attr('method'),
       url: $form2.attr('action'),
       data: $form2.serialize(),
       cache: false,
       error: function (err) {
           // Disable alert message.  Form is showing error message even though Listrak says everything works on their end.
           // alert('Could not connect to the registration server. Please try again later.');
           console.log('app.js register2() error:', err);
           $('#mc_embed_signup_scroll').fadeOut(function () {
               $('#success-response').html('<span style="color:#716F6C">Success!  Thank you for signing up.</span>').fadeIn();
           });
       },
       success: function (data) {
           $('#responses .response-sidebar').hide();

           if (data.msg.indexOf('is already subscribed') != -1) {
               data.msg = 'You are already subscribed to our Happy Family!';
               data.result = '';
           }

           if (data.result === 'success') {
               $('#mc-embedded-subscribe-form2 #mc_embed_signup_scroll').fadeOut(function () {
                   $('#s-success-response').html('<span style="color:#716F6C">' + data.msg + '</span>').fadeIn();
               });
           } else {
               $('#s-error-response').html('<span style="color:#ee7040">' + data.msg + '</span>').fadeIn();
           }
       }
   });
};

$('[href*="#chat-now"]').click(function () {
   $("#open_fc_widget").trigger("click");
});


if ( $('.page-happy-farms').length ) {

   $("#hf-ingredients").find('.hf-ing-hero').each(function(idx, el) {
       var maxSize = 0;
       $(el).find(".hf-ing-hero-content ul li").each(function(iidx, lel) {
           maxSize = Math.max($(lel).text().length, maxSize);
       });
       $(el).toggleClass('two-line', maxSize > 22);
   });
   var urlHash = window.location.hash.replace("#", "").trim();
   if (urlHash != "") {
       $('header#main-header').removeClass('headroom--pinned').addClass('headroom--unpinned');
       var el = $('#' + urlHash).next('.hf-ing-dropdown');
       el.animate({
           height: el.find(">*").outerHeight(true, true)
       }, function() {
           el.height('auto');
       });
       el.data("open", true);
   }
   $(".hf-ing-learn-more").on('click', function(e) {
       e.preventDefault();
       var hero = $(this).parents('.hf-ing-hero');
       var el = hero.next('.hf-ing-dropdown');
       if (!el.data("open")) {
           el.animate({
               height: el.find(">*").outerHeight(true, true)
           }, function() {
               el.height('auto');
           });
       }
       el.data("open", true);
       if ($(window).width() > 640) {
           if (hero.offset().top > $('body').scrollTop() - 40) {
               $("body").animate({
                   scrollTop: hero.offset().top
               });
           }
       } else {
           $("body").animate({
               scrollTop: el.offset().top
           });
       }
   });
   $(".hf-close").on('click', function(e) {
       e.preventDefault();
       var el = $(this).parents('.hf-ing-dropdown');
       if (el.data("open")) {
           el.animate({
               height: 0
           });
       }
       el.data("open", false);
   });

   $('a.fancybox-media, a.fancybox').fancybox({
       'transitionIn': 'elastic',
       'transitionOut': 'elastic',
       'minWidth': 0,
       helpers: {
           media: {}
       }
   });

   function checkHappyFarmsHomeHeroHeight() {
       if ($(window).width() <= 640) {
           $(".hf_home_hero .hero-image-mobile").outerHeight($(".hf_home_hero_content").height() + $(".hf-hero-image-right").height() + 40);
       } else {
           $(".hf_home_hero .hero-image-mobile").outerHeight('auto');
       }
   }
   $(window).on('resize', function() {
       var h1 = $(window).height() - $('#main-header').outerHeight();
       var h2 = $("#cc_hero").find("#cc_hero_content").outerHeight() + 170;
       $("#cc_hero").height(h2);
       var els = $("body.pl-layout-clearly_crafted").find(".product-body, .hero.product-line");
       var ww = $(window).width();
       if (ww < 350) {
           var pe = Math.max(0, Math.min(1, 1 - ((ww - 200) / 150)));
           var p = 470 + (pe * 200);
           els.css("background-size", p + "%");
       } else if (ww < 450) {
           var pe = Math.max(0, Math.min(1, 1 - ((ww - 300) / 150)));
           var p = 400 + (pe * 100);
           els.css("background-size", p + "%");
       } else {
           els.css("background-size", "initial");
       }
       $("body.pl-layout-clearly_crafted .product-body").css({
           backgroundPositionY: -$("body.pl-layout-clearly_crafted .hero.product-line").height() - 90
       })
       checkHappyFarmsHomeHeroHeight();
   }).resize();

}

// Hide header on scroll

var headerHeight = $('.site-header').height();
var subHeaderHeight = $('.site-sub-header').height();

$(window).on('DOMMouseScroll mousewheel scroll keydown', function ( event ) {
   if (event['type'] === 'keydown' && [33, 34, 38, 40].indexOf(event['keyCode']) === -1) {
       return;
   }

   if( window.pageYOffset > headerHeight ) {
       $('body').addClass('pageStickyHeader');
       $('.site-header, .dropdown-menu').addClass('sticky');
   } else {
       $('body').removeClass('pageStickyHeader');
       $('.site-header, .dropdown-menu').removeClass('sticky');
   }

   if ( !!subHeaderHeight ) {
       if( window.pageYOffset > headerHeight + subHeaderHeight ) {
           $('.site-header, .dropdown-menu').addClass('hidden-sub-header');
       } else {
           $('.site-header, .dropdown-menu').removeClass('hidden-sub-header');
       }
   }
});

$(".main-menu__dropdown,.dropdown-menu").on({
   "mouseenter" : function(e){
       _delay = ($(this).hasClass("main-menu__dropdown")) ? 50 : 0 ;
       $(".dropdown-menu").stop(true);
       $(".dropdown-menu").delay(_delay).fadeIn(function(){
           $(this).addClass('visible');
       });
   },
   "mouseleave" : function(e){
       $(".dropdown-menu").stop(true);
       $(".dropdown-menu").delay(50).fadeOut(function(){
           $(this).removeClass('visible');
       });
   },
})

// Update minicart item count
// $(window).load(function () {
//    $('.cart').addClass('loading');
//    $.ajax({
//        url:'/customer/section/load/?sections=cart%2Ccustomer',
//        success: function (data) {
//            var count = data.cart.summary_count;
//            if (count > 0) {
//                $('.cart').removeClass('cart-empty');
//                $('.cart .counter-number').text(data.cart.summary_count);
//            } else {
//                $('.cart').addClass('cart-empty');
//            }
//            $('.cart').removeClass('loading');

//            if (typeof data.customer['firstname'] !== 'undefined') {
//                // Update login links
//                var signInLinks = $('a[href*="/customer/account/login"]');
//                signInLinks.attr('href', '/customer/account/logout').text('Sign Out');
//                // Add account link and group with logout link
//                var signInDesktop = $('.desktop-only a[href="/customer/account/logout"]');
//                var accountDashboard = $('<a>', {href: '/customer/account'}).text('My Account')
//                var parent = signInDesktop.parent();
//                var wrapper = $('<div>', {class: 'account-links'});
//                wrapper.append(accountDashboard);
//                wrapper.append(signInDesktop);
//                parent.append(wrapper);
//            }
//        }
//        ,
//        error:  function(response) {
//            console.log(response);
//            $('.cart').removeClass('loading');
//        }
//    });
// });
if ( $('.page-infant-feeding-support').length ) {

   $('.formula-testimonials__slider').slick({
       dots: false,
       infinite: true,
       slidesToShow: 1,
       rows: 0,
       slidesToScroll: 1,
       prevArrow: $('.formula-testimonials__slick-controls.prev'),
       nextArrow: $('.formula-testimonials__slick-controls.next'),
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1,
                   infinite: true,
                   dots: false
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]
   });


   $('.feeding-videos__slider').slick({
       dots: false,
       infinite: true,
       slidesToShow: 3,
       slidesToScroll: 3,
       prevArrow: $('.feeding-videos__slick-controls.prev'),
       nextArrow: $('.feeding-videos__slick-controls.next'),
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 3,
                   slidesToScroll: 3,
                   infinite: true,
                   dots: false
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]
   });

   $('.feeding-breast__slider.breast').slick({
       dots: false,
       infinite: true,
       slidesToShow: 4,
       slidesToScroll: 4,
       prevArrow: $('.feeding-breast__slick-controls.breast.prev'),
       nextArrow: $('.feeding-breast__slick-controls.breast.next'),
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 3,
                   slidesToScroll: 3,
                   infinite: true,
                   dots: false
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]
   });

   $('.feeding-breast__slider:not(.breast)').slick({
       dots: false,
       infinite: true,
       slidesToShow: 4,
       slidesToScroll: 4,
       prevArrow: $('.feeding-breast__slick-controls.prev:not(.breast)'),
       nextArrow: $('.feeding-breast__slick-controls.next:not(.breast)'),
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 3,
                   slidesToScroll: 3,
                   infinite: true,
                   dots: false
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]
   });

   $('.feeding-formula__slider').slick({
       dots: false,
       infinite: true,
       slidesToShow: 4,
       slidesToScroll: 4,
       prevArrow: $('.feeding-formula__slick-controls.prev'),
       nextArrow: $('.feeding-formula__slick-controls.next'),
       responsive: [
           {
               breakpoint: 1024,
               settings: {
                   slidesToShow: 3,
                   slidesToScroll: 3,
                   infinite: true,
                   dots: false
               }
           },
           {
               breakpoint: 600,
               settings: {
                   slidesToShow: 2,
                   slidesToScroll: 2
               }
           },
           {
               breakpoint: 480,
               settings: {
                   slidesToShow: 1,
                   slidesToScroll: 1
               }
           }
       ]
   });

   $("#feeding-video").fancybox({
       maxWidth    : 800,
       maxHeight   : 600,
       fitToView   : false,
       width       : '70%',
       height      : '70%',
       autoSize    : false,
       closeClick  : false,
       openEffect  : 'none',
       closeEffect : 'none'
   });

   $("#modal-disclaimer").fancybox({
       fitToView : false,
       wrapCSS : 'modal-disclaimer',
       width : '100%',
       maxWidth : 400,
       height : 'auto',
       autoSize : false,
       closeClick : false,
       openEffect : 'none',
       closeEffect : 'none',
   });

   $(".feeding-videos__link").fancybox({
       width       : 640,
       height      : 360,
       autoWidth   : true,
       autoHeight  : true,
       autoResize  : true,
       aspectRadio : true,
       type        : 'iframe',
       closeClick  : false,
       openEffect  : 'none',
       closeEffect : 'none'
   });

}

function setAnchor(anchor) {
   if (window.location.href.indexOf('#' + anchor) > -1) {
       var helphash = location.hash.split('#')[1];
       if (checkMediaQuery(767)) {
           var topPadding = -40;
       }else{
           var topPadding = -90;
       }

       if (helphash === anchor) {
           if(helphash === 'formulafeeding'){
               if (checkMediaQuery(767)) {
                   topPadding = -55;
               }else{
                   topPadding = -105;
               }
           }
           jQuery('html, body').animate({
               scrollTop: jQuery('#' + helphash).offset().top - jQuery('.site-header').height() - jQuery('.site-sub-header').height() - topPadding
           }, 600);
       }
   }
}

$(window).load(function () {
   setAnchor('breastfeeding');
   setAnchor('supplementing');
   setAnchor('formulafeeding');
});

checkMediaQuery = function (width) {
   if (navigator.appName != 'Microsoft Internet Explorer') {
       mql = window.matchMedia("screen and (max-width: " + width + "px)");
       return mql.matches;
   } else {
       return $(window).width() <= width;
   }
};
/*! lazysizes - v4.0.1 */
!function(a,b){var c=b(a,a.document);a.lazySizes=c,"object"==typeof module&&module.exports&&(module.exports=c)}(window,function(a,b){"use strict";if(b.getElementsByClassName){var c,d,e=b.documentElement,f=a.Date,g=a.HTMLPictureElement,h="addEventListener",i="getAttribute",j=a[h],k=a.setTimeout,l=a.requestAnimationFrame||k,m=a.requestIdleCallback,n=/^picture$/i,o=["load","error","lazyincluded","_lazyloaded"],p={},q=Array.prototype.forEach,r=function(a,b){return p[b]||(p[b]=new RegExp("(\\s|^)"+b+"(\\s|$)")),p[b].test(a[i]("class")||"")&&p[b]},s=function(a,b){r(a,b)||a.setAttribute("class",(a[i]("class")||"").trim()+" "+b)},t=function(a,b){var c;(c=r(a,b))&&a.setAttribute("class",(a[i]("class")||"").replace(c," "))},u=function(a,b,c){var d=c?h:"removeEventListener";c&&u(a,b),o.forEach(function(c){a[d](c,b)})},v=function(a,d,e,f,g){var h=b.createEvent("CustomEvent");return e||(e={}),e.instance=c,h.initCustomEvent(d,!f,!g,e),a.dispatchEvent(h),h},w=function(b,c){var e;!g&&(e=a.picturefill||d.pf)?e({reevaluate:!0,elements:[b]}):c&&c.src&&(b.src=c.src)},x=function(a,b){return(getComputedStyle(a,null)||{})[b]},y=function(a,b,c){for(c=c||a.offsetWidth;c<d.minSize&&b&&!a._lazysizesWidth;)c=b.offsetWidth,b=b.parentNode;return c},z=function(){var a,c,d=[],e=[],f=d,g=function(){var b=f;for(f=d.length?e:d,a=!0,c=!1;b.length;)b.shift()();a=!1},h=function(d,e){a&&!e?d.apply(this,arguments):(f.push(d),c||(c=!0,(b.hidden?k:l)(g)))};return h._lsFlush=g,h}(),A=function(a,b){return b?function(){z(a)}:function(){var b=this,c=arguments;z(function(){a.apply(b,c)})}},B=function(a){var b,c=0,e=125,g=d.ricTimeout,h=function(){b=!1,c=f.now(),a()},i=m&&d.ricTimeout?function(){m(h,{timeout:g}),g!==d.ricTimeout&&(g=d.ricTimeout)}:A(function(){k(h)},!0);return function(a){var d;(a=a===!0)&&(g=33),b||(b=!0,d=e-(f.now()-c),0>d&&(d=0),a||9>d&&m?i():k(i,d))}},C=function(a){var b,c,d=99,e=function(){b=null,a()},g=function(){var a=f.now()-c;d>a?k(g,d-a):(m||e)(e)};return function(){c=f.now(),b||(b=k(g,d))}};!function(){var b,c={lazyClass:"lazyload",loadedClass:"lazyloaded",loadingClass:"lazyloading",preloadClass:"lazypreload",errorClass:"lazyerror",autosizesClass:"lazyautosizes",srcAttr:"data-src",srcsetAttr:"data-srcset",sizesAttr:"data-sizes",minSize:40,customMedia:{},init:!0,expFactor:1.5,hFac:.8,loadMode:2,loadHidden:!0,ricTimeout:300};d=a.lazySizesConfig||a.lazysizesConfig||{};for(b in c)b in d||(d[b]=c[b]);a.lazySizesConfig=d,k(function(){d.init&&F()})}();var D=function(){var g,l,m,o,p,y,D,F,G,H,I,J,K,L,M=/^img$/i,N=/^iframe$/i,O="onscroll"in a&&!/glebot/.test(navigator.userAgent),P=0,Q=0,R=0,S=-1,T=function(a){R--,a&&a.target&&u(a.target,T),(!a||0>R||!a.target)&&(R=0)},U=function(a,c){var d,f=a,g="hidden"==x(b.body,"visibility")||"hidden"!=x(a,"visibility");for(F-=c,I+=c,G-=c,H+=c;g&&(f=f.offsetParent)&&f!=b.body&&f!=e;)g=(x(f,"opacity")||1)>0,g&&"visible"!=x(f,"overflow")&&(d=f.getBoundingClientRect(),g=H>d.left&&G<d.right&&I>d.top-1&&F<d.bottom+1);return g},V=function(){var a,f,h,j,k,m,n,p,q,r=c.elements;if((o=d.loadMode)&&8>R&&(a=r.length)){f=0,S++,null==K&&("expand"in d||(d.expand=e.clientHeight>500&&e.clientWidth>500?500:370),J=d.expand,K=J*d.expFactor),K>Q&&1>R&&S>2&&o>2&&!b.hidden?(Q=K,S=0):Q=o>1&&S>1&&6>R?J:P;for(;a>f;f++)if(r[f]&&!r[f]._lazyRace)if(O)if((p=r[f][i]("data-expand"))&&(m=1*p)||(m=Q),q!==m&&(y=innerWidth+m*L,D=innerHeight+m,n=-1*m,q=m),h=r[f].getBoundingClientRect(),(I=h.bottom)>=n&&(F=h.top)<=D&&(H=h.right)>=n*L&&(G=h.left)<=y&&(I||H||G||F)&&(d.loadHidden||"hidden"!=x(r[f],"visibility"))&&(l&&3>R&&!p&&(3>o||4>S)||U(r[f],m))){if(ba(r[f]),k=!0,R>9)break}else!k&&l&&!j&&4>R&&4>S&&o>2&&(g[0]||d.preloadAfterLoad)&&(g[0]||!p&&(I||H||G||F||"auto"!=r[f][i](d.sizesAttr)))&&(j=g[0]||r[f]);else ba(r[f]);j&&!k&&ba(j)}},W=B(V),X=function(a){s(a.target,d.loadedClass),t(a.target,d.loadingClass),u(a.target,Z),v(a.target,"lazyloaded")},Y=A(X),Z=function(a){Y({target:a.target})},$=function(a,b){try{a.contentWindow.location.replace(b)}catch(c){a.src=b}},_=function(a){var b,c=a[i](d.srcsetAttr);(b=d.customMedia[a[i]("data-media")||a[i]("media")])&&a.setAttribute("media",b),c&&a.setAttribute("srcset",c)},aa=A(function(a,b,c,e,f){var g,h,j,l,o,p;(o=v(a,"lazybeforeunveil",b)).defaultPrevented||(e&&(c?s(a,d.autosizesClass):a.setAttribute("sizes",e)),h=a[i](d.srcsetAttr),g=a[i](d.srcAttr),f&&(j=a.parentNode,l=j&&n.test(j.nodeName||"")),p=b.firesLoad||"src"in a&&(h||g||l),o={target:a},p&&(u(a,T,!0),clearTimeout(m),m=k(T,2500),s(a,d.loadingClass),u(a,Z,!0)),l&&q.call(j.getElementsByTagName("source"),_),h?a.setAttribute("srcset",h):g&&!l&&(N.test(a.nodeName)?$(a,g):a.src=g),f&&(h||l)&&w(a,{src:g})),a._lazyRace&&delete a._lazyRace,t(a,d.lazyClass),z(function(){(!p||a.complete&&a.naturalWidth>1)&&(p?T(o):R--,X(o))},!0)}),ba=function(a){var b,c=M.test(a.nodeName),e=c&&(a[i](d.sizesAttr)||a[i]("sizes")),f="auto"==e;(!f&&l||!c||!a[i]("src")&&!a.srcset||a.complete||r(a,d.errorClass)||!r(a,d.lazyClass))&&(b=v(a,"lazyunveilread").detail,f&&E.updateElem(a,!0,a.offsetWidth),a._lazyRace=!0,R++,aa(a,b,f,e,c))},ca=function(){if(!l){if(f.now()-p<999)return void k(ca,999);var a=C(function(){d.loadMode=3,W()});l=!0,d.loadMode=3,W(),j("scroll",function(){3==d.loadMode&&(d.loadMode=2),a()},!0)}};return{_:function(){p=f.now(),c.elements=b.getElementsByClassName(d.lazyClass),g=b.getElementsByClassName(d.lazyClass+" "+d.preloadClass),L=d.hFac,j("scroll",W,!0),j("resize",W,!0),a.MutationObserver?new MutationObserver(W).observe(e,{childList:!0,subtree:!0,attributes:!0}):(e[h]("DOMNodeInserted",W,!0),e[h]("DOMAttrModified",W,!0),setInterval(W,999)),j("hashchange",W,!0),["focus","mouseover","click","load","transitionend","animationend","webkitAnimationEnd"].forEach(function(a){b[h](a,W,!0)}),/d$|^c/.test(b.readyState)?ca():(j("load",ca),b[h]("DOMContentLoaded",W),k(ca,2e4)),c.elements.length?(V(),z._lsFlush()):W()},checkElems:W,unveil:ba}}(),E=function(){var a,c=A(function(a,b,c,d){var e,f,g;if(a._lazysizesWidth=d,d+="px",a.setAttribute("sizes",d),n.test(b.nodeName||""))for(e=b.getElementsByTagName("source"),f=0,g=e.length;g>f;f++)e[f].setAttribute("sizes",d);c.detail.dataAttr||w(a,c.detail)}),e=function(a,b,d){var e,f=a.parentNode;f&&(d=y(a,f,d),e=v(a,"lazybeforesizes",{width:d,dataAttr:!!b}),e.defaultPrevented||(d=e.detail.width,d&&d!==a._lazysizesWidth&&c(a,f,e,d)))},f=function(){var b,c=a.length;if(c)for(b=0;c>b;b++)e(a[b])},g=C(f);return{_:function(){a=b.getElementsByClassName(d.autosizesClass),j("resize",g)},checkElems:g,updateElem:e}}(),F=function(){F.i||(F.i=!0,E._(),D._())};return c={cfg:d,autoSizer:E,loader:D,init:F,uP:w,aC:s,rC:t,hC:r,fire:v,gW:y,rAF:z}}});
$('.learning-center-article__jump-meal-plan-mobile, .learning-center-article__jump-meal-plan').attr(
   'href', '#' + $('.meal-plan-stage-title').first().attr('id')
);
/*! lazysizes - v4.0.1 */
!function(a,b){var c=function(){b(a.lazySizes),a.removeEventListener("lazyunveilread",c,!0)};b=b.bind(null,a,a.document),false?b(require("lazysizes")):a.lazySizes?c():a.addEventListener("lazyunveilread",c,!0)}(window,function(a,b,c){"use strict";function d(){this.ratioElems=b.getElementsByClassName("lazyaspectratio"),this._setupEvents(),this.processImages()}if(a.addEventListener){var e,f,g,h=Array.prototype.forEach,i=/^picture$/i,j="data-aspectratio",k="img["+j+"]",l=function(b){return a.matchMedia?(l=function(a){return!a||(matchMedia(a)||{}).matches})(b):a.Modernizr&&Modernizr.mq?!b||Modernizr.mq(b):!b},m=c.aC,n=c.rC,o=c.cfg;d.prototype={_setupEvents:function(){var a=this,c=function(b){b.naturalWidth<36?a.addAspectRatio(b,!0):a.removeAspectRatio(b,!0)},d=function(){a.processImages()};b.addEventListener("load",function(a){a.target.getAttribute&&a.target.getAttribute(j)&&c(a.target)},!0),addEventListener("resize",function(){var b,d=function(){h.call(a.ratioElems,c)};return function(){clearTimeout(b),b=setTimeout(d,99)}}()),b.addEventListener("DOMContentLoaded",d),addEventListener("load",d)},processImages:function(a){var c,d;a||(a=b),c="length"in a&&!a.nodeName?a:a.querySelectorAll(k);for(d=0;d<c.length;d++)c[d].naturalWidth>36?this.removeAspectRatio(c[d]):this.addAspectRatio(c[d])},getSelectedRatio:function(a){var b,c,d,e,f,g=a.parentNode;if(g&&i.test(g.nodeName||""))for(d=g.getElementsByTagName("source"),b=0,c=d.length;c>b;b++)if(e=d[b].getAttribute("data-media")||d[b].getAttribute("media"),o.customMedia[e]&&(e=o.customMedia[e]),l(e)){f=d[b].getAttribute(j);break}return f||a.getAttribute(j)||""},parseRatio:function(){var a=/^\s*([+\d\.]+)(\s*[\/x]\s*([+\d\.]+))?\s*$/,b={};return function(c){return!b[c]&&c.match(a)&&(RegExp.$3?b[c]=RegExp.$1/RegExp.$3:b[c]=1*RegExp.$1),b[c]}}(),addAspectRatio:function(b,c){var d,e=b.offsetWidth,f=b.offsetHeight;return c||m(b,"lazyaspectratio"),36>e&&0>=f?void((e||f&&a.console)&&console.log("Define width or height of image, so we can calculate the other dimension")):(d=this.getSelectedRatio(b),d=this.parseRatio(d),void(d&&(e?b.style.height=e/d+"px":b.style.width=f*d+"px")))},removeAspectRatio:function(a){n(a,"lazyaspectratio"),a.style.height="",a.style.width="",a.removeAttribute(j)}},f=function(){g=a.jQuery||a.Zepto||a.shoestring||a.$,g&&g.fn&&!g.fn.imageRatio&&g.fn.filter&&g.fn.add&&g.fn.find?g.fn.imageRatio=function(){return e.processImages(this.find(k).add(this.filter(k))),this}:g=!1},f(),setTimeout(f),e=new d,a.imageRatio=e,"object"==typeof module&&module.exports?module.exports=e:"function"==typeof define&&define.amd&&define(e)}});
// Mark submenu lvls
$('.wp-megamenu .wp-megamenu-sub-menu .wp-megamenu-sub-menu .wp-megamenu-sub-menu').addClass('level-3');
$('.wp-megamenu .wp-megamenu-sub-menu .wp-megamenu-sub-menu').not('.level-3').addClass('level-2');
$('.wp-megamenu .wp-megamenu-sub-menu').not('.level-2').not('.level-3').addClass('level-1');

// Handle sub-lvl menu clicks

// Mark sub-submenus
$('.level-2 :eq(0)').addClass('sub-submenu');
$('.level-3 > .menu-item > [class*=container]').each(function () {
   $(this).prev('.wpmm-item-title').addClass('sub-submenu');
});

// End here

// Added class to submenu images
$('.menu-item').each(function(){
   if($(this).find('.image').length > 0){
       $(this).parents('li.wpmm-col').addClass('menu-promo-image');
   }
   var currentLinkVal = $(this).find('a').text().trim();
   if(currentLinkVal === "SHOP ALL"){
       $(this).find('a').addClass('shop-icon');
   }
});
// End here
$(window).load(function () {
   if (window.location.href.indexOf("#happytohelp") > -1) {
       var helphash = location.hash.split('#')[1];

       var anchorid;
       if (checkMediaQuery(767)) {
           anchorid = "happytohelpmob";
       }
       else if (checkMediaQuery(1023)) {
           anchorid = "happytohelpipad";
       }
       else {
           anchorid = "happytohelp";
       }

       if (helphash === 'happytohelp') {
           jQuery('html, body').animate({
               scrollTop: jQuery('#' + anchorid).offset().top - jQuery('.site-header').height() - jQuery('.site-sub-header').height() - 40
           }, 600);
       }
   }
});

checkMediaQuery = function (width) {
   if (navigator.appName != 'Microsoft Internet Explorer') {
       mql = window.matchMedia("screen and (max-width: " + width + "px)");
       return mql.matches;
   } else {
       return $(window).width() <= width;
   }
};

$ = jQuery;
   // Hover to change post titles
   var cssStyles = "",
       addClsName = "",
       postTitleDesktopHover = "span.post_title_desktop",
       postTitleMobileHover = "span.post_title_mobile";

   function addDynamicClass(title, mode, clsElement){
       $(title).each(function(){
           var color = $(this).parent().data(mode),
               clsName = $(this).parent().data(clsElement);

           cssStyles += "."+clsName+"{ color:"+color+" !important;}";
       });
       $("<style type='text/css'>"+cssStyles+"</style>").appendTo("head");
       cssStyles="";
   }

   addDynamicClass('.post_title_desktop','color-desktop', 'class-desktop');
   addDynamicClass('.post_title_mobile','color-mobile', 'class-mobile');

   $(postTitleDesktopHover).hover(function(){
       var self = this;
       addClsName = $(this).parent().data('class-desktop');
       $(this).find('span').addClass(addClsName);
   },
   function () {
       var self = this;
       $(self).find('span').removeClass(addClsName);
   });

   $(postTitleMobileHover).hover(function(){
       var self = this;
       addClsName = $(this).parent().data('class-mobile');
       $(this).find('span').addClass(addClsName);
   },
   function () {
       var self = this;
       $(self).find('span').removeClass(addClsName);
   });

   $(".cross").on("click",function(){
       $("body").removeClass("sticky-header darkHeader");
       $(".overlay_1").slideUp();
       $(window).scrollTop(0);
   });

   $(".tog-ham").on("click",function(){
       var topbarSectionElem = $('.top-banner-block').innerHeight();
       $("body").addClass("sticky-header darkHeader");
       $(".overlay_1").slideDown();
       $(window).scrollTop(0);
   });

if ( $('.modal').find('.modal-overlay').length < 1 ) {
   $('.modal').append('<div class="modal-overlay"></div>');
}

function triggerModal(modal) {
   modal.show();
   setTimeout(function() { 
       modal.find('.modal-content, .modal-overlay').css('opacity', '1');
   }, 100);
};

function closeModal(x) {
   var modal = x.closest('.modal');

   modal.find('.modal-content, .modal-overlay').css('opacity', '');
   setTimeout(function() { 
       modal.hide();
       unlockBody();

       if ( $('#coach-modal').length ) {
           $('.coach-modal__content-container').empty();
       }
   }, 600);
}

function lockBody() {
   $('body').addClass('locked');
}

function unlockBody() {
   $('body').removeClass('locked');
}

$('.modal-close, .modal-overlay').click(function(){
   closeModal( $(this) );
});

// =================================
// Page specific modal functionality
// =================================

var coachModal = $('#coach-modal');

$('.user-coach__avatar').click(function(e){
   triggerModal(coachModal);
   lockBody();

   // fill in coach content
   var modalContent = $(this).closest('.user-coach').html();
   coachModal.find('.coach-modal__content-container').append(modalContent);

});

if ( $('.page-infant-formula').length ) {
   if (readCookie('formulaModal') != 1) {
       setTimeout(function() { 
           triggerModal( $('.formula-modal') );
       }, 1000);
       createCookie('formulaModal', 1, 30);
   }
}

if ($('.page-infant-feeding-support').length) {
       if (readCookie('feedingModal') != 1) {
           setTimeout(function () {
               triggerModal($('.formula-modal'));
           }, 1000);
           createCookie('feedingModal', 1, 30);
       }
}
// ==============
// Youtube modals
// ==============

// https://gist.github.com/takien/4077195
function YouTubeGetID(url){
   var ID = '';
   url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
   if(url[2] !== undefined) {
       ID = url[2].split(/[^0-9a-z_\-]/i);
       ID = ID[0];
   }
   else {
       ID = url;
   }
       return ID;
}

$('.carousel-video, .modal-video .modal-overlay').on( 'click', function(){
   $('.modal-video .modal-video__content-container').html('');
   closeModal( $('.modal-video') );
});

var findOrCreateVideoModal = function(parent) {
   var modal = parent.find('.modal-video');
   if ( modal.length > 0 ) {
       return modal;
   }

   parent.append('<div class="modal modal-video"><div class="modal-overlay"></div><div class="modal-video__content modal-content"><div class="modal-close"><button></button></div><div class="modal-video__content-container"></div></div></div>');
   modal = parent.find('.modal-video');

   return modal;
}

$('.carousel-video').on('click', 'a', function() {
   if ( $(this).is('[href*="youtube"]') ) {
       var modal, youtubeId, parent;

       parent = $(this).closest('.carousel-video');
       youtubeId = YouTubeGetID($(this).attr('href'));
       modal = findOrCreateVideoModal(parent);
       triggerModal(modal);
       modal.find('.modal-video__content-container').html("<iframe src='https://www.youtube.com/embed/" + youtubeId + "?rel=0&showinfo=0&autoplay=1' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>");

       return false;
   }
});

// ============================
// Learning Center author modal
// ============================

$('.learning-center-article__author-info-trigger').on('click', function() {
   triggerModal($('#learning-center-article__author-info-modal'));

   return false;
});

$('.single-post').click(function(){
   if ( $('#learning-center-article__author-info-modal').css('opacity') == 1 ) {
       closeModal($('#learning-center-article__author-info-modal'));
   }
});


// =========================
// Our Mission certification
// =========================
$('.mission-certification__modal-link').on('click', function() {
   triggerModal( $('.mission-certification-modal') );

   return false;
});

if ( $('.page-our-mission').length && document.location.href.indexOf('#bcorp') > -1 ) {
   $('html, body').animate({
       scrollTop: $('#mission-certification').offset().top - $('.site-header').height() - $('.site-sub-header').height() - 20
   }, 600);

   triggerModal( $('.mission-certification-modal') );
}

// ===================
// Our Mission pop ups
// ===================
$('.mission-design').on('click', function(e) {
   $(this).find('strong.active').removeClass('active');
});

$('.mission-design__image-description li').on('click', function(e) {
   e.stopPropagation();

   if ( $(e.target).hasClass('close') ) {
       $('.mission-design strong.active').removeClass('active');
       return;
   }

   $(this).siblings().find('strong').removeClass('active');
   $(this).find('strong').addClass('active');
   if ( $(this).find('.close').length === 0 ) {
       $(this).find('strong').append('<span class="close"></span>')
   }
});


// ===================
// Clearly Crafted PLP modals
// ===================
$('.clearly-crafted-hero__product').click(function(){
   $('.clearly-crafted-hero__modal').addClass('visible');
});

$('.clearly-crafted-hero__modal .col-12').click(function(){
   $('.clearly-crafted-hero__modal').removeClass('visible');
});

$('.clearly-crafted-videos a[href*="instagram"]').click(function(e){
   e.preventDefault();

   var instagramModal = $(this).attr('href');

   triggerModal( $(instagramModal) );

   //$(instagramModal).find('.modal-content').prepend('<div class="modal-close"><button></button></div>');

   return false;
});

function  updateProgress(e, slick) {
   var $dots = slick.$dots;

   if(!$dots){
       return;
   }

   var $activeDot = $dots.find('.slick-active');
   var $allPrev = $activeDot.prevAll();

   $dots.find('li').removeClass('progress');
   $allPrev.addClass('progress');
   $activeDot.addClass('progress');
}

$.each($('.image-slider'), function () {
   var $this = $(this);
   var $items = $this.find('.items');
   var data = $this.data('slickOptions');

   if(!data){
       return;
   }
   data = JSON.parse(data);

   var dotsSelector = data['dotsSelector'];

   if(dotsSelector){
       data.appendDots = $this.find(dotsSelector);
   }

   $items.on('afterChange init', function(e, slick){
       updateProgress(e, slick)
   });

   $items.slick(data);
});


var mobileBreakpoint = 768;

function isMobile () {
   return $(window).width() <= mobileBreakpoint
}

function initializeFancybox ($element) {
   $.fancybox.open($element, {
       type: 'iframe'
   });
}

$('.video-icon').click(function (e) {
   e.stopPropagation();
   e.preventDefault();

   var $this = $(this);
   var data = $this.data('sources');

   if(!data){
       return;
   }

   data = JSON.parse(data);

   if(isMobile() && data.mobile){
       $this.attr('href', data.mobile);
       initializeFancybox($this)

   }

   if(!isMobile() && data.desktop){
       $this.attr('href', data.desktop);
       initializeFancybox($this);
   }
});


/**
* module-two class added when modular-button-wrapper block is present
*/
var resetModularPadding = function(){
   $('.modular-wrapper .main_module_modular + .modular-button-wrapper.main_module_modular')
   .prev('.modular-wrapper').addClass('modult-two');
}
$(document).ready(function(){
   resetModularPadding();
});
/*! picturefill - v3.0.2 - 2016-02-12
* https://scottjehl.github.io/picturefill/
* Copyright (c) 2016 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT
*/
!function(a){var b=navigator.userAgent;a.HTMLPictureElement&&/ecko/.test(b)&&b.match(/rv\:(\d+)/)&&RegExp.$1<45&&addEventListener("resize",function(){var b,c=document.createElement("source"),d=function(a){var b,d,e=a.parentNode;"PICTURE"===e.nodeName.toUpperCase()?(b=c.cloneNode(),e.insertBefore(b,e.firstElementChild),setTimeout(function(){e.removeChild(b)})):(!a._pfLastSize||a.offsetWidth>a._pfLastSize)&&(a._pfLastSize=a.offsetWidth,d=a.sizes,a.sizes+=",100vw",setTimeout(function(){a.sizes=d}))},e=function(){var a,b=document.querySelectorAll("picture > img, img[srcset][sizes]");for(a=0;a<b.length;a++)d(b[a])},f=function(){clearTimeout(b),b=setTimeout(e,99)},g=a.matchMedia&&matchMedia("(orientation: landscape)"),h=function(){f(),g&&g.addListener&&g.addListener(f)};return c.srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",/^[c|i]|d$/.test(document.readyState||"")?h():document.addEventListener("DOMContentLoaded",h),f}())}(window),function(a,b,c){"use strict";function d(a){return" "===a||"	"===a||"\n"===a||"\f"===a||"\r"===a}function e(b,c){var d=new a.Image;return d.onerror=function(){A[b]=!1,ba()},d.onload=function(){A[b]=1===d.width,ba()},d.src=c,"pending"}function f(){M=!1,P=a.devicePixelRatio,N={},O={},s.DPR=P||1,Q.width=Math.max(a.innerWidth||0,z.clientWidth),Q.height=Math.max(a.innerHeight||0,z.clientHeight),Q.vw=Q.width/100,Q.vh=Q.height/100,r=[Q.height,Q.width,P].join("-"),Q.em=s.getEmValue(),Q.rem=Q.em}function g(a,b,c,d){var e,f,g,h;return"saveData"===B.algorithm?a>2.7?h=c+1:(f=b-c,e=Math.pow(a-.6,1.5),g=f*e,d&&(g+=.1*e),h=a+g):h=c>1?Math.sqrt(a*b):a,h>c}function h(a){var b,c=s.getSet(a),d=!1;"pending"!==c&&(d=r,c&&(b=s.setRes(c),s.applySetCandidate(b,a))),a[s.ns].evaled=d}function i(a,b){return a.res-b.res}function j(a,b,c){var d;return!c&&b&&(c=a[s.ns].sets,c=c&&c[c.length-1]),d=k(b,c),d&&(b=s.makeUrl(b),a[s.ns].curSrc=b,a[s.ns].curCan=d,d.res||aa(d,d.set.sizes)),d}function k(a,b){var c,d,e;if(a&&b)for(e=s.parseSet(b),a=s.makeUrl(a),c=0;c<e.length;c++)if(a===s.makeUrl(e[c].url)){d=e[c];break}return d}function l(a,b){var c,d,e,f,g=a.getElementsByTagName("source");for(c=0,d=g.length;d>c;c++)e=g[c],e[s.ns]=!0,f=e.getAttribute("srcset"),f&&b.push({srcset:f,media:e.getAttribute("media"),type:e.getAttribute("type"),sizes:e.getAttribute("sizes")})}function m(a,b){function c(b){var c,d=b.exec(a.substring(m));return d?(c=d[0],m+=c.length,c):void 0}function e(){var a,c,d,e,f,i,j,k,l,m=!1,o={};for(e=0;e<h.length;e++)f=h[e],i=f[f.length-1],j=f.substring(0,f.length-1),k=parseInt(j,10),l=parseFloat(j),X.test(j)&&"w"===i?((a||c)&&(m=!0),0===k?m=!0:a=k):Y.test(j)&&"x"===i?((a||c||d)&&(m=!0),0>l?m=!0:c=l):X.test(j)&&"h"===i?((d||c)&&(m=!0),0===k?m=!0:d=k):m=!0;m||(o.url=g,a&&(o.w=a),c&&(o.d=c),d&&(o.h=d),d||c||a||(o.d=1),1===o.d&&(b.has1x=!0),o.set=b,n.push(o))}function f(){for(c(T),i="",j="in descriptor";;){if(k=a.charAt(m),"in descriptor"===j)if(d(k))i&&(h.push(i),i="",j="after descriptor");else{if(","===k)return m+=1,i&&h.push(i),void e();if("("===k)i+=k,j="in parens";else{if(""===k)return i&&h.push(i),void e();i+=k}}else if("in parens"===j)if(")"===k)i+=k,j="in descriptor";else{if(""===k)return h.push(i),void e();i+=k}else if("after descriptor"===j)if(d(k));else{if(""===k)return void e();j="in descriptor",m-=1}m+=1}}for(var g,h,i,j,k,l=a.length,m=0,n=[];;){if(c(U),m>=l)return n;g=c(V),h=[],","===g.slice(-1)?(g=g.replace(W,""),e()):f()}}function n(a){function b(a){function b(){f&&(g.push(f),f="")}function c(){g[0]&&(h.push(g),g=[])}for(var e,f="",g=[],h=[],i=0,j=0,k=!1;;){if(e=a.charAt(j),""===e)return b(),c(),h;if(k){if("*"===e&&"/"===a[j+1]){k=!1,j+=2,b();continue}j+=1}else{if(d(e)){if(a.charAt(j-1)&&d(a.charAt(j-1))||!f){j+=1;continue}if(0===i){b(),j+=1;continue}e=" "}else if("("===e)i+=1;else if(")"===e)i-=1;else{if(","===e){b(),c(),j+=1;continue}if("/"===e&&"*"===a.charAt(j+1)){k=!0,j+=2;continue}}f+=e,j+=1}}}function c(a){return k.test(a)&&parseFloat(a)>=0?!0:l.test(a)?!0:"0"===a||"-0"===a||"+0"===a?!0:!1}var e,f,g,h,i,j,k=/^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i,l=/^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;for(f=b(a),g=f.length,e=0;g>e;e++)if(h=f[e],i=h[h.length-1],c(i)){if(j=i,h.pop(),0===h.length)return j;if(h=h.join(" "),s.matchesMedia(h))return j}return"100vw"}b.createElement("picture");var o,p,q,r,s={},t=!1,u=function(){},v=b.createElement("img"),w=v.getAttribute,x=v.setAttribute,y=v.removeAttribute,z=b.documentElement,A={},B={algorithm:""},C="data-pfsrc",D=C+"set",E=navigator.userAgent,F=/rident/.test(E)||/ecko/.test(E)&&E.match(/rv\:(\d+)/)&&RegExp.$1>35,G="currentSrc",H=/\s+\+?\d+(e\d+)?w/,I=/(\([^)]+\))?\s*(.+)/,J=a.picturefillCFG,K="position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)",L="font-size:100%!important;",M=!0,N={},O={},P=a.devicePixelRatio,Q={px:1,"in":96},R=b.createElement("a"),S=!1,T=/^[ \t\n\r\u000c]+/,U=/^[, \t\n\r\u000c]+/,V=/^[^ \t\n\r\u000c]+/,W=/[,]+$/,X=/^\d+$/,Y=/^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/,Z=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,d||!1):a.attachEvent&&a.attachEvent("on"+b,c)},$=function(a){var b={};return function(c){return c in b||(b[c]=a(c)),b[c]}},_=function(){var a=/^([\d\.]+)(em|vw|px)$/,b=function(){for(var a=arguments,b=0,c=a[0];++b in a;)c=c.replace(a[b],a[++b]);return c},c=$(function(a){return"return "+b((a||"").toLowerCase(),/\band\b/g,"&&",/,/g,"||",/min-([a-z-\s]+):/g,"e.$1>=",/max-([a-z-\s]+):/g,"e.$1<=",/calc([^)]+)/g,"($1)",/(\d+[\.]*[\d]*)([a-z]+)/g,"($1 * e.$2)",/^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/gi,"")+";"});return function(b,d){var e;if(!(b in N))if(N[b]=!1,d&&(e=b.match(a)))N[b]=e[1]*Q[e[2]];else try{N[b]=new Function("e",c(b))(Q)}catch(f){}return N[b]}}(),aa=function(a,b){return a.w?(a.cWidth=s.calcListLength(b||"100vw"),a.res=a.w/a.cWidth):a.res=a.d,a},ba=function(a){if(t){var c,d,e,f=a||{};if(f.elements&&1===f.elements.nodeType&&("IMG"===f.elements.nodeName.toUpperCase()?f.elements=[f.elements]:(f.context=f.elements,f.elements=null)),c=f.elements||s.qsa(f.context||b,f.reevaluate||f.reselect?s.sel:s.selShort),e=c.length){for(s.setupRun(f),S=!0,d=0;e>d;d++)s.fillImg(c[d],f);s.teardownRun(f)}}};o=a.console&&console.warn?function(a){console.warn(a)}:u,G in v||(G="src"),A["image/jpeg"]=!0,A["image/gif"]=!0,A["image/png"]=!0,A["image/svg+xml"]=b.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1"),s.ns=("pf"+(new Date).getTime()).substr(0,9),s.supSrcset="srcset"in v,s.supSizes="sizes"in v,s.supPicture=!!a.HTMLPictureElement,s.supSrcset&&s.supPicture&&!s.supSizes&&!function(a){v.srcset="data:,a",a.src="data:,a",s.supSrcset=v.complete===a.complete,s.supPicture=s.supSrcset&&s.supPicture}(b.createElement("img")),s.supSrcset&&!s.supSizes?!function(){var a="data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw==",c="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==",d=b.createElement("img"),e=function(){var a=d.width;2===a&&(s.supSizes=!0),q=s.supSrcset&&!s.supSizes,t=!0,setTimeout(ba)};d.onload=e,d.onerror=e,d.setAttribute("sizes","9px"),d.srcset=c+" 1w,"+a+" 9w",d.src=c}():t=!0,s.selShort="picture>img,img[srcset]",s.sel=s.selShort,s.cfg=B,s.DPR=P||1,s.u=Q,s.types=A,s.setSize=u,s.makeUrl=$(function(a){return R.href=a,R.href}),s.qsa=function(a,b){return"querySelector"in a?a.querySelectorAll(b):[]},s.matchesMedia=function(){return a.matchMedia&&(matchMedia("(min-width: 0.1em)")||{}).matches?s.matchesMedia=function(a){return!a||matchMedia(a).matches}:s.matchesMedia=s.mMQ,s.matchesMedia.apply(this,arguments)},s.mMQ=function(a){return a?_(a):!0},s.calcLength=function(a){var b=_(a,!0)||!1;return 0>b&&(b=!1),b},s.supportsType=function(a){return a?A[a]:!0},s.parseSize=$(function(a){var b=(a||"").match(I);return{media:b&&b[1],length:b&&b[2]}}),s.parseSet=function(a){return a.cands||(a.cands=m(a.srcset,a)),a.cands},s.getEmValue=function(){var a;if(!p&&(a=b.body)){var c=b.createElement("div"),d=z.style.cssText,e=a.style.cssText;c.style.cssText=K,z.style.cssText=L,a.style.cssText=L,a.appendChild(c),p=c.offsetWidth,a.removeChild(c),p=parseFloat(p,10),z.style.cssText=d,a.style.cssText=e}return p||16},s.calcListLength=function(a){if(!(a in O)||B.uT){var b=s.calcLength(n(a));O[a]=b?b:Q.width}return O[a]},s.setRes=function(a){var b;if(a){b=s.parseSet(a);for(var c=0,d=b.length;d>c;c++)aa(b[c],a.sizes)}return b},s.setRes.res=aa,s.applySetCandidate=function(a,b){if(a.length){var c,d,e,f,h,k,l,m,n,o=b[s.ns],p=s.DPR;if(k=o.curSrc||b[G],l=o.curCan||j(b,k,a[0].set),l&&l.set===a[0].set&&(n=F&&!b.complete&&l.res-.1>p,n||(l.cached=!0,l.res>=p&&(h=l))),!h)for(a.sort(i),f=a.length,h=a[f-1],d=0;f>d;d++)if(c=a[d],c.res>=p){e=d-1,h=a[e]&&(n||k!==s.makeUrl(c.url))&&g(a[e].res,c.res,p,a[e].cached)?a[e]:c;break}h&&(m=s.makeUrl(h.url),o.curSrc=m,o.curCan=h,m!==k&&s.setSrc(b,h),s.setSize(b))}},s.setSrc=function(a,b){var c;a.src=b.url,"image/svg+xml"===b.set.type&&(c=a.style.width,a.style.width=a.offsetWidth+1+"px",a.offsetWidth+1&&(a.style.width=c))},s.getSet=function(a){var b,c,d,e=!1,f=a[s.ns].sets;for(b=0;b<f.length&&!e;b++)if(c=f[b],c.srcset&&s.matchesMedia(c.media)&&(d=s.supportsType(c.type))){"pending"===d&&(c=d),e=c;break}return e},s.parseSets=function(a,b,d){var e,f,g,h,i=b&&"PICTURE"===b.nodeName.toUpperCase(),j=a[s.ns];(j.src===c||d.src)&&(j.src=w.call(a,"src"),j.src?x.call(a,C,j.src):y.call(a,C)),(j.srcset===c||d.srcset||!s.supSrcset||a.srcset)&&(e=w.call(a,"srcset"),j.srcset=e,h=!0),j.sets=[],i&&(j.pic=!0,l(b,j.sets)),j.srcset?(f={srcset:j.srcset,sizes:w.call(a,"sizes")},j.sets.push(f),g=(q||j.src)&&H.test(j.srcset||""),g||!j.src||k(j.src,f)||f.has1x||(f.srcset+=", "+j.src,f.cands.push({url:j.src,d:1,set:f}))):j.src&&j.sets.push({srcset:j.src,sizes:null}),j.curCan=null,j.curSrc=c,j.supported=!(i||f&&!s.supSrcset||g&&!s.supSizes),h&&s.supSrcset&&!j.supported&&(e?(x.call(a,D,e),a.srcset=""):y.call(a,D)),j.supported&&!j.srcset&&(!j.src&&a.src||a.src!==s.makeUrl(j.src))&&(null===j.src?a.removeAttribute("src"):a.src=j.src),j.parsed=!0},s.fillImg=function(a,b){var c,d=b.reselect||b.reevaluate;a[s.ns]||(a[s.ns]={}),c=a[s.ns],(d||c.evaled!==r)&&((!c.parsed||b.reevaluate)&&s.parseSets(a,a.parentNode,b),c.supported?c.evaled=r:h(a))},s.setupRun=function(){(!S||M||P!==a.devicePixelRatio)&&f()},s.supPicture?(ba=u,s.fillImg=u):!function(){var c,d=a.attachEvent?/d$|^c/:/d$|^c|^i/,e=function(){var a=b.readyState||"";f=setTimeout(e,"loading"===a?200:999),b.body&&(s.fillImgs(),c=c||d.test(a),c&&clearTimeout(f))},f=setTimeout(e,b.body?9:99),g=function(a,b){var c,d,e=function(){var f=new Date-d;b>f?c=setTimeout(e,b-f):(c=null,a())};return function(){d=new Date,c||(c=setTimeout(e,b))}},h=z.clientHeight,i=function(){M=Math.max(a.innerWidth||0,z.clientWidth)!==Q.width||z.clientHeight!==h,h=z.clientHeight,M&&s.fillImgs()};Z(a,"resize",g(i,99)),Z(b,"readystatechange",e)}(),s.picturefill=ba,s.fillImgs=ba,s.teardownRun=u,ba._=s,a.picturefillCFG={pf:s,push:function(a){var b=a.shift();"function"==typeof s[b]?s[b].apply(s,a):(B[b]=a[0],S&&s.fillImgs({reselect:!0}))}};for(;J&&J.length;)a.picturefillCFG.push(J.shift());a.picturefill=ba,"object"==typeof module&&"object"==typeof module.exports?module.exports=ba:"function"==typeof define&&define.amd&&define("picturefill",function(){return ba}),s.supPicture||(A["image/webp"]=e("image/webp","data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA=="))}(window,document);
// Modify order of review on landing pages (bundles)
// interval detects when reviews is loaded and applies script

var reviewInterval;

function modifyReview() {

   if( $('.pr-review').length ) {

       clearInterval(reviewInterval);

       var review = $('.pr-review');

       review.each(function(){

           var reviewDate = $(this).find('time').html(),
               reviewerName = $(this).find('.pr-rd-author-nickname span span:not(.pr-rd-bold)').html(),
               reviewerCity = $(this).find('.pr-rd-author-location span span:not(.pr-rd-bold)').html(),
               reviewerDetails = '<div class="reviewer-details">';

           reviewerDetails = '<h2 class="reviewer-details__name">' + reviewerName + '</h2>'
                           + '<div class="reviewer-details__attributes">'
                           + '		<p class="reviewer-details__city">' + reviewerCity + '</p>'
                           + '		<p class="reviewer-details__date">' + reviewDate + '</p>';
                           + '</div></div>';

           $(this).prepend(reviewerDetails);

       });

       $('.pr-rd-main-footer, .pr-rd-main-header, .pr-accessible-focus-element, .pr-review-display script').remove();

       $('.pr-review-display').slick({
           slidesToShow: 3,
           responsive: [
               {
                   breakpoint: 1024,
                   settings: {
                       slidesToShow: 2
                   },
               },
               {
                   breakpoint: 600,
                   settings: {
                       slidesToShow: 1
                   },
               }
           ]
       });

   }

}

if ( $('.review-cards').length ) {
   reviewInterval = setInterval(modifyReview, 300);
}
// Press Archive dropdown

$('.press-archive__title').click(function(e){
   $('.press-archive').toggleClass('expanded');
});

// Filtering by year

$('.press-archive__year a').click(function(e){
   e.preventDefault();
   $('.press-archive').toggleClass('expanded');

   var yearInt = parseInt($(this).html());

   $('.press-archive__title').text(yearInt);

   $.ajax({
       type: 'POST',
       url: ajax_params.url,
       data: {
           action: 'load_year',
           year: yearInt
       },
       success: function(response) {
           $('.posts-by-year').html(response).css('display', 'flex');
           $('.all-posts, .press-pagination').hide();
           return false;
       },
       error:  function(response) {
           console.log(response);
       }
   });
});

$('.press-archive__all').click(function(e){
   e.preventDefault();
   $('.press-archive').toggleClass('expanded');
   $('.press-archive__title').text('Sort by year');

   $('.posts-by-year').html('').hide();
   $('.all-posts').css('display', 'flex');
   $('.press-pagination').show();
});



// Add class to elements that enter viewport and trigger animation
var $animatedElements = $('section, .animated');

function inView() {
   var windowHalfHeight = $(window).height() * 0.5;
   var windowTopPosition = $(window).scrollTop();
   var windowMiddlePosition = (windowTopPosition + windowHalfHeight);

   $.each($animatedElements, function() {
       var $element = $(this);
       var elementHalfHeight = $element.outerHeight() * 0.5;
       var elementTopPosition = $element.offset().top;
       var elementMiddlePosition = (elementTopPosition + elementHalfHeight);

       //check to see if this current container is within viewport
       if (elementMiddlePosition <= windowMiddlePosition) {
           $element.addClass('viewed');
       }
   });
}

$(window).on('scroll resize', inView);
$(window).trigger('scroll');

// Smooth scroll to links with hashes
$('a[href*="#"]').not('[href="#"]')
   .not('.mission-what-we-do > a[href*="#"]')
   .not('.tab-section a[href*="#"]')
   .not('.tab-header a[href*="#"]')
   .not('a[href*="instagram"]')
   .on('click touchend', function(event) {
       // On-page links
       if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
           var target = $(this.hash);
           target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
           if (target.length) {
               event.preventDefault();
               $('html, body').animate({
                   scrollTop: target.offset().top - $('.site-header').height() - $('.site-sub-header').height() - 20
               }, 600);
           }
       }
});

$(window).load(function () {
   if (window.location.hash) {
       var hash = window.location.hash;

       if (hash === '#chat-now') {
           $('html, body').animate({
               scrollTop: $('#chat-support').offset().top - $('.site-header').height() - $('.site-sub-header').height() - 60
           }, 600);
           setTimeout(function () {
               $('#open_fc_widget').trigger('click');
           }, 1000);
       } else if (hash === '#chat-support') {
           $('html, body').animate({
               scrollTop: $(hash).offset().top - $('.site-header').height() - $('.site-sub-header').height() - 60
           }, 600);
       }
   }
});

$('.hover-tab').click(function(e){
   e.preventDefault();
});

if ( $('.hover-tabs').hasClass('tablet-drawers') ) {
   var drawerBreakpoint = 1023;
} else {
   var drawerBreakpoint = 600;
}

function revealOnHover(){

   var tabIds = $(this).attr('data-content').split('&'),
       tabContainer = $(this).closest('.hover-tabs');

   for (var i = 0; i < tabIds.length; i++) {

       var tabContent = $('#' + tabIds[i]);

       tabContainer.find('.hover-tab').removeClass('active');
       tabContainer.find('.tab-content:not(#' + tabIds + ')').removeClass('active');
       $(this).addClass('active');
       tabContent.addClass('active');

       // Toggle bullet point on formula page
       if ( tabIds[i] == 'sensitive' || tabIds[i] == 'sensitive-image' ) {
           $('.made-not-with li:first-child').hide();
           $('.made-not-with li.hide-sensitive').hide();
       } else {
           $('.made-not-with li:first-child').show();
           $('.made-not-with li.hide-sensitive').show();
       }

       if ( tabContent.find('.slick-slider').length ) {
           tabContent.find('.slick-slider').slick('setPosition');
       }
   }

}

function positionHoverTabs() {

   if ( $(window).width() > drawerBreakpoint ) {
       var maxHeight = -1;

       // Set section height on page load
       $('.hover-tabs').each(function(){

           $(this).find('.tab-content').each(function() {
               if ( maxHeight > $(this).height() ) {
                   maxHeight = maxHeight;
               } else {
                   maxHeight = $(this).height();
               }
           });

           $(this).find('.tab-content').each(function() {
               $(this).height(maxHeight);
           });

           $(this).find('.hover-tabs__container').closest('.main').css('margin-bottom', maxHeight);

           if ( $('.hover-tab.active').length == 0 ) {
               $('.hover-tab').first().addClass('active');
               $('#' + $('.hover-tab.active').attr('data-content') ).addClass('active');
               $(this).find('.hover-tab').first().addClass('active');
           }
       });

       // Hovering interaction
       $('.hover-tab').mouseenter(revealOnHover);

   } else {

       // Reset desktop styles if resizing browser
       $('.tab-content').each(function() {
           $(this).css('height', '');
       });
       $('.hover-tabs .main').css('margin-bottom', '');
       $('.hover-tabs .active').removeClass('active');

       $('.hover-tab').unbind('mouseenter');

       // Toggle drawers
       $('.tab-drawer-header').unbind().click(function(){

           if ( $(this).hasClass('expanded') ) {
               $(this).removeClass('expanded');
               $(this).nextAll('div').slideUp();
           } else {
               $('.tab-drawer-header').removeClass('expanded');
               $('.tab-drawer-header ~ div').slideUp();
               $(this).addClass('expanded');
               $(this).nextAll('div').slideDown();
               
               // reset slick slider
               $(this).parent().find('.slick-slider').slick('setPosition');
           }

       });
   }

}

positionHoverTabs();
$(window).resize(positionHoverTabs);


function positionFormulaTabs() {
   $('.formula-stages .tab-content').each(function() {
       $(this).css('height', '');

       if ( $(this).hasClass('main') && $(window).width() < 1024 && $(window).width() > 600 ) {
           $(this).css('top', $('.formula-tabs-container').height() + 60);
       }

       if ( $(window).width() <= 600 ) {
           $('.hover-tab').mouseenter(revealOnHover);
       }
   });

   $('.formula-stages .hover-tabs__container').closest('.main').css('margin-bottom', '');

   $('#stage-2, [data-content="stage-2&stage-2-image"], #stage-2-image').addClass('active');
}

if ( $('.page-infant-formula').length ) {

   $('.formula-tabs-container .hover-tab').removeClass('active');
   $('[data-content="stage-2&stage-2-image"]').addClass('active');

   positionFormulaTabs();
   $(window).resize(positionFormulaTabs);

   // Formula nav below the hero
   $('.formula-products-carousel .slick-slide').click(function(){
       
       var formulaName = $(this).attr('class')
                           .replace('slick-slide', '')
                           .replace('slick-current', '')
                           .replace('slick-active', '')
                           .replace('toggle-', '')
                           .replace(' ', '').replace(' ', '').replace(' ', ''),
           formulaTitle = '[data-content="' + formulaName + '&' + formulaName + '-image"]';


       $('.formula-stages').find('.tab-content, .hover-tab').removeClass('active');
       $('#' + formulaName + ', #' + formulaName + '-image, ' + formulaTitle).addClass('active');

       if ( formulaName.indexOf('sensitive') > -1 ) {
           $('.made-not-with li:first-child').hide();
       } else {
           $('.made-not-with li:first-child').show();
       }
   });

}



$('.made-with, .made-not-with').click(function(){

   if ( $(this).hasClass('active') ) {
       return;
   } else {
       $('.made-with, .made-not-with').toggleClass('active');
   }
});
$('.team-users__see-all a').click(function(){
   $('.team-users').addClass('see-all');
   return false;
});
$('.page-tmall .international-hero a').attr('target', '_blank');});
