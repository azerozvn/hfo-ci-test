// These were two `script` tags in `header.php`.  I moved them here.

$ = jQuery;
$(document).ready(function(){
    if(window.location.href.indexOf("our-mission") > -1) {
        if (window.location.href.indexOf("2025-packaging-promise") > -1) {
        } else {
            if (!jQuery(".packaging-promise-anchor").hasClass('mission-page')) {
                jQuery(".packaging-promise-anchor").addClass('mission-page');
            }
        }
    }

    $(".top-button").on("click",function(){
        if(window.location.href.indexOf("our-mission") > -1) {
            if (!jQuery(".packaging-promise-anchor").hasClass('mission-page')) {
                jQuery(".packaging-promise-anchor").addClass('mission-page');
            }
        }
    });
});

$( "body:has(.top-banner-block)" ).addClass( "top-banner-active" );

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() > $(document).height() - 90) {
        $("body").addClass("near-bottom");
    } else {
        $("body").removeClass("near-bottom");
    }
});

$(document).ready(function(){
    if($('.without-top-spacing').length){
    $( "body" ).addClass( "remove-top-spacing" );
    }
});

