<?php
/*
Template Name: Campaign Design
*/
get_header();
global $post;
if ( ! post_password_required( $post ) ) {
    ?>

    <?php /*START LOOP */
    if (get_posts()) : while (have_posts()) : the_post(); ?>
        <!--   Html code here-->

        <div class="campaign-wrapper">
            <!----------------Top Content start ---------------->
            <?php
            if (have_rows('top_content_section')):
                while (have_rows('top_content_section')) : the_row();
                    $enabledTopContent = get_sub_field('enabled_section_top_content');
                    if ($enabledTopContent['value'] === 'yes'):
                        ?>
                        <div class="top-content main_module"
                             data-sequence="<?php the_sub_field('top_content_sequence'); ?>">
                            <div class="back">
                                <a href="<?php the_sub_field('top_back_link'); ?>">
                            <span class="icon">
                                <img class="alignnone size-medium wp-image-3570" role="img"
                                     src="/wp-content/uploads/2018/05/back.svg" alt=""/>
                            </span>
                                    <span class="back-text"><?php the_sub_field('back_link_text_content_module') ?></span>
                                </a>
                            </div>
                            <div class="h-one float-left" data-class="top-content-desc<?php echo get_row_index(); ?>"
                                 data-size="<?php the_sub_field('font_size_top_content'); ?>"
                                 data-line-height="<?php the_sub_field('line_height_top_content'); ?>">
                                <div class="left"><?php the_sub_field('top_content_title'); ?></div>
                                <div class="right dynamic-font"><?php the_sub_field('top_content_description'); ?></div>
                            </div>
                        </div>
                    <?php
                    endif;
                endwhile;
            endif;
            ?>
            <!---------------- Top Content end---------------->

            <!----------------Mosaic grid start ---------------->

            <?php
            $enabledMosaicContent = get_field('enabled_mosaic_grid_section');
            if ($enabledMosaicContent['value'] === 'yes'):
                ?>
                <div class="mosaic-grid main_module" data-sequence="<?php the_field('order_mosaic_grid'); ?>">
                    <div class="mosaic-grid-wrap float-left">
                        <!----------------this container will be make repeat-- start ---------------->
                        <div class="six-images mos-desktop-view">
                            <div class="column first">
                                <?php
                                $count1 = 1;
                                if (have_rows('mosaic_grid_section')):
                                    while (have_rows('mosaic_grid_section')) : the_row();
                                        $mosaicIndex1 = get_row_index();
                                        $enabledMosaicSubgridSection1 = get_sub_field('enabled_mosaic_subgrid');
                                        if ($mosaicIndex1 === $count1):
                                            if ($enabledMosaicSubgridSection1['value'] === "yes"):
                                                ?>
                                                <div class="grid-img">
                                                    <div class="grid-view">
                                                        <?php if (get_sub_field('video_urldesktop_mosaic_grid')): ?>
                                                            <a class="mosaic-grid-popup desktop-view" href="
                            <?php the_sub_field('video_urldesktop_mosaic_grid'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if (get_sub_field('video_urlmobile_mosaic_module')): ?>
                                                            <a class="mosaic-grid-popup mobile-view" href="
                            <?php the_sub_field('video_urlmobile_mosaic_module'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <span class="image-view">
                            <picture>
                                <source media="(max-width: 767px)" data-srcset="
                                    <?php the_sub_field('imagemobile_mosaic_grid'); ?>">
                                    <img class="alignnone size-medium wp-image-3570 lazyload" role="img" data-src="
                                        <?php the_sub_field('imagedesktop_mosaic_grid'); ?>"
                                         alt="mosaic grid"/>
                                    </picture>
                                </span>
                                                    </div>
                                                </div>
                                            <?php
                                            endif;
                                            $count1 = $count1 + 3;
                                        endif;

                                    endwhile;
                                endif;
                                ?>
                            </div>
                            <div class="column center">
                                <?php
                                $count2 = 2;
                                if (have_rows('mosaic_grid_section')):
                                    while (have_rows('mosaic_grid_section')) : the_row();
                                        $mosaicIndex2 = get_row_index();
                                        $enabledMosaicSubgridSection2 = get_sub_field('enabled_mosaic_subgrid');
                                        if ($mosaicIndex2 === $count2):
                                            if ($enabledMosaicSubgridSection2['value'] === 'yes'):
                                                ?>
                                                <div class="grid-img">
                                                    <div class="grid-view">
                                                        <?php if (get_sub_field('video_urldesktop_mosaic_grid')): ?>
                                                            <a class="mosaic-grid-popup desktop-view" href="
                            <?php the_sub_field('video_urldesktop_mosaic_grid'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if (get_sub_field('video_urlmobile_mosaic_module')): ?>
                                                            <a class="mosaic-grid-popup mobile-view" href="
                            <?php the_sub_field('video_urlmobile_mosaic_module'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <span class="image-view">
                                                        <picture>
                                                            <source media="(max-width: 767px)" data-srcset="
                                                                <?php the_sub_field('imagemobile_mosaic_grid'); ?>">
                                                                <img class="alignnone size-medium wp-image-3570 lazyload"
                                                                     role="img" data-src="
                                                                    <?php the_sub_field('imagedesktop_mosaic_grid'); ?>"
                                                                     alt="mosaic grid"/>
                                                        </picture>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php
                                            endif;
                                            $count2 = $count2 + 3;
                                        endif;
                                    endwhile;
                                endif;
                                ?>
                            </div>
                            <div class="column last">
                                <?php
                                $count3 = 3;
                                if (have_rows('mosaic_grid_section')):
                                    while (have_rows('mosaic_grid_section')) : the_row();
                                        $mosaicIndex3 = get_row_index();
                                        $enabledMosaicSubgridSection3 = get_sub_field('enabled_mosaic_subgrid');
                                        if ($mosaicIndex3 === $count3):
                                            if ($enabledMosaicSubgridSection3['value'] === 'yes'):
                                                ?>
                                                <div class="grid-img">
                                                    <div class="grid-view">
                                                        <?php if (get_sub_field('video_urldesktop_mosaic_grid')): ?>
                                                            <a class="mosaic-grid-popup desktop-view" href="
                            <?php the_sub_field('video_urldesktop_mosaic_grid'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if (get_sub_field('video_urlmobile_mosaic_module')): ?>
                                                            <a class="mosaic-grid-popup mobile-view" href="
                            <?php the_sub_field('video_urlmobile_mosaic_module'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <span class="image-view">
                                            <picture>
                                                <source media="(max-width: 767px)" data-srcset="
                                                    <?php the_sub_field('imagemobile_mosaic_grid'); ?>">
                                                    <img class="alignnone size-medium wp-image-3570 lazyload" role="img" data-src="
                                                        <?php the_sub_field('imagedesktop_mosaic_grid'); ?>"
                                                         alt="mosaic grid"/>
                                                    </picture>
                                                </span>
                                                    </div>
                                                </div>
                                            <?php
                                            endif;
                                            $count3 = $count3 + 3;
                                        endif;
                                    endwhile;
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="six-images mos-mobile-view">
                            <div class="column first">
                                <?php
                                $count4 = 1;
                                if (have_rows('mosaic_grid_section')):
                                    while (have_rows('mosaic_grid_section')) : the_row();
                                        $mosaicIndex4 = get_row_index();
                                        $enabledMosaicSubgridSection4 = get_sub_field('enabled_mosaic_subgrid');
                                        if ($mosaicIndex4 === $count4):
                                            if ($enabledMosaicSubgridSection1['value'] === "yes"):
                                                ?>
                                                <div class="grid-img">
                                                    <div class="grid-view">
                                                        <?php if (get_sub_field('video_urldesktop_mosaic_grid')): ?>
                                                            <a class="mosaic-grid-popup desktop-view" href="
                            <?php the_sub_field('video_urldesktop_mosaic_grid'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if (get_sub_field('video_urlmobile_mosaic_module')): ?>
                                                            <a class="mosaic-grid-popup mobile-view" href="
                            <?php the_sub_field('video_urlmobile_mosaic_module'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <span class="image-view">
                            <picture>
                                <source media="(max-width: 767px)" data-srcset="
                                    <?php the_sub_field('imagemobile_mosaic_grid'); ?>">
                                    <img class="alignnone size-medium wp-image-3570 lazyload" role="img" data-src="
                                        <?php the_sub_field('imagedesktop_mosaic_grid'); ?>"
                                         alt="mosaic grid"/>
                                    </picture>
                                </span>
                                                    </div>
                                                </div>
                                            <?php
                                            endif;
                                            $count4 = $count4 + 2;
                                        endif;

                                    endwhile;
                                endif;
                                ?>
                            </div>
                            <div class="column center">
                                <?php
                                $count5 = 2;
                                if (have_rows('mosaic_grid_section')):
                                    while (have_rows('mosaic_grid_section')) : the_row();
                                        $mosaicIndex5 = get_row_index();
                                        $enabledMosaicSubgridSection5 = get_sub_field('enabled_mosaic_subgrid');
                                        if ($mosaicIndex5 === $count5):
                                            if ($enabledMosaicSubgridSection5['value'] === 'yes'):
                                                ?>
                                                <div class="grid-img">
                                                    <div class="grid-view">
                                                        <?php if (get_sub_field('video_urldesktop_mosaic_grid')): ?>
                                                            <a class="mosaic-grid-popup desktop-view" href="
                            <?php the_sub_field('video_urldesktop_mosaic_grid'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <?php if (get_sub_field('video_urlmobile_mosaic_module')): ?>
                                                            <a class="mosaic-grid-popup mobile-view" href="
                            <?php the_sub_field('video_urlmobile_mosaic_module'); ?>" data-fancybox-type="iframe">
                                                                <div class="play-icon color-change-dynm" data-color="
                                <?php the_sub_field('play_icon_color_mosaic_grid'); ?>">
                                                                    <img class="alignnone size-medium wp-image-3570 svg icon"
                                                                         role="img"
                                                                         src="/wp-content/uploads/2018/05/six-icon.svg"
                                                                         alt="watch the video"/>
                                                                </div>
                                                            </a>
                                                        <?php endif; ?>
                                                        <span class="image-view">
                                                        <picture>
                                                            <source media="(max-width: 767px)" data-srcset="
                                                                <?php the_sub_field('imagemobile_mosaic_grid'); ?>">
                                                                <img class="alignnone size-medium wp-image-3570 lazyload"
                                                                     role="img" data-src="
                                                                    <?php the_sub_field('imagedesktop_mosaic_grid'); ?>"
                                                                     alt="mosaic grid"/>
                                                        </picture>
                                                    </span>
                                                    </div>
                                                </div>
                                            <?php
                                            endif;
                                            $count5 = $count5 + 2;
                                        endif;
                                    endwhile;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <!----------------this container will be make repeat -- end ---------------->
                    <div class="show-more">
                        <div class="show-cover">
                            <a href="javascript:void(0);">
                                <span class="show-text">SHOW MORE</span>
                                <span class="icon">
                                            <img class="alignnone size-medium wp-image-3570" role="img"
                                                 src="/wp-content/uploads/2018/05/cam-show-more.svg" alt=""/>
                                        </span>
                            </a>
                        </div>
                    </div>
                    <!----------------Dash hudson start---------------->
                    <?php
                    if (have_rows('dash_module_section')): while (have_rows('dash_module_section')) : the_row();
                        $enabledDashContent = get_sub_field('enabled_section_dash_module');
                        if($enabledDashContent['value'] === 'yes'):
                            ?>
                            <div class="dash-module center-block">
                                <div class="scene-dash">
                                    <div class="text-dash-content" data-class="dash-content-desc<?php echo get_row_index(); ?>"
                                         data-size="<?php the_sub_field('font_size_dash_content'); ?>" data-line-height="<?php the_sub_field('line_height_dash_content'); ?>">
                                        <span class="header dynamic-font"><?php the_sub_field('title_dash_module'); ?></span>
                                        <div class="hudson-grid-container">
                                            <?php // hfo-marketing-script-start ?>
                                            <script src="https://cdn.dashhudson.com/web/js/board-carousel-embed.js" type="text/javascript" data-name="dhboard-carousel" data-lazy="true" data-gallery-id="52594" data-row-size="4" data-gap-size="4" data-mobile-row-size="2"   data-mobile-gap-size="2"  ></script>
                                            <?php // hfo-marketing-script-end ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endif;
                    endwhile; endif;
                    ?>
                    <!----------------Dash hudson end---------------->
                </div>
            <?php
            endif;
            ?>

            <!---------------- Mosaic grid end---------------->

            <!----------------Video Module start ---------------->
            <?php
            if (have_rows('video_module_section')):
                while (have_rows('video_module_section')) : the_row();
                    $enabledVideoContent = get_sub_field('enabled_section_video_module');
                    if ($enabledVideoContent['value'] === 'yes'):
                        ?>
                        <div class="video-module main_module"
                             data-sequence="<?php the_sub_field('sequence_video_module'); ?>">
                            <div class="about-us float-left">
                                <div class="text">
                                    <div class="top-text"><?php the_sub_field('label_video_module'); ?></div>
                                    <span class="header"><?php the_sub_field('title_video_module'); ?></span>
                                    <span class="discrip"><?php the_sub_field('content_video_module'); ?></span>
                                </div>
                                <div class="pic">
                                    <div class="play-text">
                                        <span class="video-title"><?php the_sub_field('video_title_video_module'); ?></span>
                                        <span class="video-discrip"><?php the_sub_field('video_subtitle_video_module'); ?></span>
                                        <a class="banner-popup-video-module" data-fancybox-type="iframe"
                                           href="<?php the_sub_field('video_url_video_module'); ?>">                                           
                                                <img class="alignnone size-medium wp-image-3570" role="img"
                                                     src="/wp-content/uploads/2018/05/cam-video-play-module.svg"
                                                     alt="Watch the video"/>                                            
                                        </a>
                                    </div>
                                    <picture>
                                        <source media="(max-width: 767px)"
                                                data-srcset="<?php the_sub_field('video_background_image_mobile_video_module'); ?>">
                                        <img class="alignnone size-medium wp-image-3570 lazyload" role="img"
                                             data-src="<?php the_sub_field('video_background_imagedesktop_video_module'); ?>"
                                             alt="<?php the_sub_field('video_title_video_module'); ?>"/>
                                    </picture>
                                </div>
                            </div>
                        </div>
                    <?php
                    endif;
                endwhile;
            endif;
            ?>
            <!---------------- Video Module end---------------->


            <!----------------Gallery Module  start ---------------->
            <?php
            if (have_rows('gallery_module_section')):
                while (have_rows('gallery_module_section')) : the_row();
                    $enabledGalleryContent = get_sub_field('enabled_section_gallery_module');
                    if ($enabledGalleryContent['value'] === 'yes'):
                        ?>
                        <div class="gallery-module main_module"
                             data-sequence="<?php the_sub_field('sequence_gallery_module'); ?>">
                            <div class="scene float-left">
                                <div class="text">
                                    <div class="top-text"><?php the_sub_field('label_gallery_module'); ?></div>
                                    <span class="header"><?php the_sub_field('title_gallery_module'); ?></span>
                                </div>
                                <span class="discrip"><?php the_sub_field('content_gallery_module'); ?></span>
                                <div class="pic gallery-carousal">
                                    <div id="carousel" class="slick-video-module-carousal">
                                        <ul class="slides">
                                            <?php
                                            if (have_rows('gallery_carousal_mosaic')): while (have_rows('gallery_carousal_mosaic')) : the_row();
                                                if (have_rows('gallery_mosaic')): while (have_rows('gallery_mosaic')) : the_row();
                                                    ?>
                                                    <li>
                                                        <picture>
                                                            <source media="(max-width: 767px)"
                                                                    data-srcset="<?php the_sub_field('imagemobile_mosaic'); ?>">
                                                            <img class="alignnone size-medium wp-image-3570 lazyload" role="img"
                                                                 data-src="<?php the_sub_field('imagedesktop_mosaic') ?>"
                                                                 alt="<?php echo $image['alt']; ?>"/>
                                                        </picture>
                                                    </li>
                                                <?php
                                                endwhile; endif;
                                            endwhile; endif;
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endif;
                endwhile;
            endif;
            ?>
            <!---------------- Gallery Module end---------------->


            <!----------------Team carousel start ---------------->
            <?php
            $enabledTestimonialContent = get_field('enabled_testimonial_module');
            if ($enabledTestimonialContent['value'] === 'yes'):
                ?>
                <div class="team-carousel main_module" data-sequence="<?php the_field('order_testimonial_module'); ?>">
                    <div class="team float-left">
                        <div class="text">
                            <div class="top-text"><?php the_field('label_testimonial'); ?></div>
                            <span class="header"><?php the_field('text_testimonial'); ?></span>
                        </div>
                        <div class="slider-controller">
                            <a href="javascript:void(0);" class="testimonial-left">
                            <span class="prev">
                                Prev
                            </span>
                            </a>
                            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                <span class="slider__label sr-only"></span>
                            </div>
                            <a href="javascript:void(0);" class="testimonial-right">
                            <span class="nex">
                                Next
                            </span>
                            </a>
                        </div>
                        <div class="team-slides">
                            <?php
                            if (have_rows('testimonial_slides_section')): while (have_rows('testimonial_slides_section')) : the_row();
                                if (have_rows('testimonial_slides')): while (have_rows('testimonial_slides')) : the_row();
                                    $enabledTestimonialSelectedSlide = get_sub_field('enabled_selected_slide');
                                    if ($enabledTestimonialSelectedSlide['value'] === 'yes'):
                                        ?>
                                        <div class="pic-container">
                                            <div class="pic-parents">
                                                <div class="carousel-img">
                                                    <picture>
                                                        <source media="(max-width: 767px)"
                                                                data-srcset="<?php the_sub_field('imagemobile_testimonial_slides'); ?>">
                                                        <img class="alignnone size-medium wp-image-3570 lazyload" role="img"
                                                             data-src="<?php the_sub_field('imagedesktop_testimonial_slides'); ?>"
                                                             alt=""/>
                                                    </picture>
                                                </div>
                                                <div class="carousel-text">
                                                    <h2><?php the_sub_field('name_testimonial_slides'); ?></h2>
                                                    <p><?php the_sub_field('designation_testimonial_slides'); ?></p>
                                                </div>
                                            </div>
                                            <div class="pic-text">
                                                <div class="up-text">
                                                    <h2 class="header-right"><?php the_sub_field('heading_testimonial_slides'); ?></h2>
                                                    <p class="short"><?php the_sub_field('short_description_testimonial_slides'); ?></p>
                                                </div>
                                                <div class="down-text desktop-view"><?php the_sub_field('brief_description_testimonial_slides'); ?></div>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                                endwhile; endif;
                            endwhile; endif;
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            endif;
            ?>
            <!---------------- Team carousel end---------------->


        </div>

    <?php /*END LOOP */
    endwhile; endif; ?>

    <?php // hfo-marketing-script-start ?>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.3/jquery.fancybox.min.css"
          type="text/css" media="screen"/>
    <script src="https://cdn.jsdelivr.net/npm/mediaCheck@0.4.6/js/mediaCheck.js"></script>
    <?php // hfo-marketing-script-end ?>

    <?php $orderType = get_field('order_bymodules_campaign');
    $mosaicGridCountDesktop = get_field('number_of_grid_mosaic');
    $mosaicGridCountMobile = get_field('number_of_grid_mosaic_mobile');
    ?>
    <script>
        $ = jQuery;
        // Sort by functionality for all modules
        var order_type = '<?php echo $orderType; ?>';

        function sortModule(element, wrapper, orderType, dataAttr) {
            $(element).sort(sort_li).appendTo(wrapper);

            function sort_li(a, b) {
                if (orderType === 'asc') {
                    return ($(b).data(dataAttr)) < ($(a).data(dataAttr)) ? 1 : -1;
                } else {
                    return ($(b).data(dataAttr)) > ($(a).data(dataAttr)) ? 1 : -1;
                }
            }
        }

        sortModule(".main_module", ".campaign-wrapper", order_type, "sequence");
        // End here

        // Video popup iframe
        $(".banner-popup-video-module, .mosaic-grid-popup").fancybox();
        // End here

        // Show More Functionality
        var gridImage = ".grid-img";


        function showMosaicDesktopElement(initialCount, lastCount) {
            $(gridImage, ".mos-desktop-view .column.first").slice(initialCount, lastCount).fadeIn(2000);
            $(gridImage, ".mos-desktop-view .column.last").slice(initialCount, lastCount).fadeIn(2000);
            $(gridImage, ".mos-desktop-view .column.center").slice(initialCount, lastCount).fadeIn(2000);
        }

        function showMosaicMobileElement(initialCount, lastCount) {
            $(gridImage, ".mos-mobile-view .column.first").slice(initialCount, lastCount).fadeIn(2000);
            $(gridImage, ".mos-mobile-view .column.center").slice(initialCount, lastCount).fadeIn(2000);
        }

        function hideShowMore() {
            $('.show-more').hide();
        }

        function viewShowMore() {
            $('.show-more').show();
        }


        function hideGridImage() {
            $(gridImage, '.mosaic-grid').hide();
        }

        function showMoreDesktopMob(showFrontCount, rowCount, elem, viewType) {
            var mosaicGridLength = Number($(gridImage, elem).length),
                defaultCount = Number(showFrontCount),
                showMoreCount = Number(mosaicGridLength / rowCount),
                hideAfter = 0;
            if (mosaicGridLength >= defaultCount) {
                hideAfter = Math.ceil(defaultCount / rowCount);
                if (viewType === "mobile") {
                    showMosaicMobileElement(0, hideAfter);
                } else {
                    showMosaicDesktopElement(0, hideAfter);
                }
                if (mosaicGridLength === defaultCount) {
                    hideShowMore();
                } else {
                    viewShowMore();
                }
            } else {
                hideShowMore();
            }
            var initialMosaic = 0,
                afterMosaic = 0,
                loopCount = hideAfter;
            $('.show-more a').off('click').on('click.mosaic', function () {
                if (hideAfter !== 0) {
                    initialMosaic += hideAfter;
                    afterMosaic = initialMosaic + hideAfter;
                    if (loopCount <= showMoreCount) {
                        if (viewType === "mobile") {
                            showMosaicMobileElement(initialMosaic, afterMosaic);
                        } else {
                            showMosaicDesktopElement(initialMosaic, afterMosaic);
                        }
                        loopCount += hideAfter;
                    }
                    if (loopCount >= showMoreCount) {
                        hideShowMore();
                    }
                }
            })
        }

        var gridCountDek = '<?php echo $mosaicGridCountDesktop; ?>',
            gridCountMob = '<?php echo $mosaicGridCountMobile; ?>';
        mediaCheck({
            media: '(min-width: 768px)',
            entry: $.proxy(function () {
                hideGridImage();
                showMoreDesktopMob(gridCountDek, 3, ".mosaic-grid .mos-desktop-view", "desktop");
            }),
            exit: $.proxy(function () {
                hideGridImage();
                showMoreDesktopMob(gridCountMob, 2, ".mosaic-grid .mos-mobile-view", "mobile");
            })
        });

        //End here
    </script>

    <?php
}else{
    // we will show password form here
    echo get_the_password_form();
}
get_footer();
?>
