</div> <!-- /#content -->


<?php get_template_part('dist/snippets/footer'); ?>

<!-- reset query so that conditional tags work -->
<?php wp_reset_query(); ?>

<!-- Typekit for Omnes font -->
<link rel="stylesheet" href="https://use.typekit.net/yzv1pam.css">

<?php wp_footer(); ?>
<!-- PageType Event added -->
<?php if(is_front_page()){
    $pagetype = "Home";
}
if(is_category()){

}
if(is_page()){
    $cmp = array("learning-center","little-one","mama","recipes-meal-plans");
    $pageslug = $post->post_name;
    if(in_array($pageslug,$cmp))
    {
        $pagetype = "Blog - Learning Center";
    }
    if($pageslug == "bundles"){
        $pagetype = "Bundles";
    }
    if($pageslug == "careers")
    {
        $pagetype = "Careers";
    }
    if($pageslug == "clearly-crafted-cereal" || $pageslug == "clearly-crafted-purees" || $pageslug == "clearly-crafted-jars"){
        $pagetype = "Clearly Crafted PLP";
    }
    if($pageslug == "contact-us")
    {
        $pagetype = "Contact";
    }
    if(strpos($pageslug,"coupon") !== false){
        $pagetype = "Coupon";
    }
    if($pageslug == "faqs"){
        $pagetype = "FAQs";
    }
    if($pageslug == "happy-farms")
    {
        $pagetype = "Happy Farms";
    }
    if($pageslug == "infant-feeding-support"){
        $pagetype = "Infant Feeding Hub";

    }if( $pageslug == "infant-formula"){
        $pagetype = "Infant Formula PLP";
    }
    if($pageslug == "mission-report" || $pageslug == "our-mission"){
        $pagetype = "Mission";
    }if($pageslug == "our-experts"){
        $pagetype = "Our Experts";
    }if($pageslug == "our-story")
    {
        $pagetype = "Our Story";
    }if($pageslug == "our-team"){
        $pagetype = "Our Team";
    }if($pageslug == "picky-eating"){
        $pagetype = "Picky Eating Hub";
    }if($pageslug == "press"){
        $pagetype = "Press";
    }
    $pline= array("breastfeeding-support","fiber-protein","fruit-oat-bars","love-my-veggies","simple-combos-hearty-meals","superfoods","super-morning","super-smart","whole-milk-yogurt");
    if(in_array($pageslug,$pline))
    {
        $pagetype = "Product Line Page";
    }
    if($pageslug == "store-locator"){
        $pagetype = "Store Locator";
    }
    $pcmsurl= array("baby","kid","mama","tot");
    if(in_array($pageslug,$pcmsurl))
    {
        $pagetype = "Lifestage";
    }
}
if ( is_single() ) {

    $category = get_the_category();
    for($i=0;$i<count($category);$i++){
        $catarray[$i] = (array) $category[$i];
    }
    if (isset($catarray)) {
        usort($catarray,"cmp");
        for($i=0;$i<count($catarray);$i++){
            $category[$i] = (object) $catarray[$i];
        }
    }
    if( count($category) ) {
        $cat = $category[0];
        $slug = $cat->slug;
        if($slug == "recipes-meal-plans")
        {
            $pagetype = "Blog - Learning Center - Recipe - Meal Plan";
        }
        else
        {
            // Note: this will match all single product pages, but we'll overwrite below.
            $pagetype = "Blog - Learning Center - Article";
        }
    }
}
$category = get_queried_object();
if ($category) {    
    // Product details page
    if( is_single() && $category->post_type == 'products' ) {
        $pagetype = "Product Details Page";
    }
    
    // Product listing page (Shop main)
    if( $category->name == 'products' ) {
        $pagetype = "Product Listing Page";
    }
    
    // Product listing page (Category, ex: "baby", "tot")
    if( $category->taxonomy == 'product_category' ) {
        $pagetype = "Product Listing Page";
    }
}
?>
<?php // hfo-marketing-script-start ?>
<?php if (isset($pagetype)) : ?>
<script type="text/javascript">
    var pageType = '<?php echo $pagetype;?>';

    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        event: 'Custom Page View Event',
        'pageType': pageType
    });
</script>
<?php endif; ?>

<script type="text/javascript">
    (function(d) {
        if (document.addEventListener) {
            document.addEventListener('ltkAsyncListener', d);
        } else {
            e = document.documentElement;
            e.ltkAsyncProperty = 0;
            e.attachEvent('onpropertychange', function (e) {
                if (e.propertyName == 'ltkAsyncProperty'){
                    d();
                }
            });
        }
    })(function() {
        /********** Begin Custom Code **********/
        _ltk.Activity.AddPageBrowse();
        _ltk.Activity.Submit();
        _ltk.SCA.CaptureEmail('subscription_email');
        _ltk.SCA.CaptureEmail('email');
        /********** End Custom Code ************/
    });
</script>
<!-- Listrak Analytics – Javascript Framework -->
<script type="text/javascript">
    var biJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
    (function (d, s, id, tid, vid) {
        var js, ljs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
        js.src = biJsHost + "cdn.listrakbi.com/scripts/script.js?m=" + tid + "&v=" + vid;
        ljs.parentNode.insertBefore(js, ljs);
    })(document, 'script', 'ltkSDK', 'INPzm0LdrYzZ', '1');
</script>
<script>
    (function($) {
        $("document").ready(function(){
            $("li.mission_report_link a").attr("target","_blank");
        });
    })(jQuery)
</script>

<?php // hfo-marketing-script-end ?>

<!-- MyFonts licensing -->
<!--
/**
 * @license
 * MyFonts Webfont Build ID 3650797, 2018-10-04T14:35:07-0400
 * 
 * The fonts listed in this notice are subject to the End User License
 * Agreement(s) entered into by the website owner. All other parties are 
 * explicitly restricted from using the Licensed Webfonts(s).
 * 
 * You may obtain a valid license at the URLs below.
 * 
 * Webfont: BoosterNextFY-Thin by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/thin/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: BoosterNextFY-Regular by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/regular/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: BoosterNextFY-Medium by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/medium/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: BoosterNextFY-Black by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/black/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: BoosterNextFY-Light by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/light/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: BoosterNextFY-Bold by Black Foundry
 * URL: https://www.myfonts.com/fonts/black-foundry/booster-next-fy/bold/
 * Copyright: Copyright (c) 2013 by FONTYOU. All rights reserved. www.fontyou.com
 * 
 * Webfont: Organico-Regular by Meat Studio
 * URL: https://www.myfonts.com/fonts/meat-studio/organico/regular/
 * Copyright: Stew Deane
 * 
 * 
 * License: https://www.myfonts.com/viewlicense?type=web&buildid=3650797
 * Licensed pageviews: 600,000
 * 
 * © 2018 MyFonts Inc
*/

-->

</body>
</html>