<?php

// Enqueue JavaScript and CSS files
@include 'src/php/enqueue-scripts.php';

// Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Custom menus
function theme_setup() {

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() some locations (not header or footer)
    register_nav_menus( array(
        'menu-hf-top' => __( 'header-top-mission-report-label', 'happyfamily' ),
        'menu-hf-footer1' => __( 'customer-service', 'happyfamily' ),
        'menu-hf-footer2' => __( 'about-us', 'happyfamily' ),
        'menu-hf-footer3' => __( 'connect-with-us', 'happyfamily' ),
        'menu-hf-footer4' => __( 'footer-copyright', 'happyfamily' ),
        'menu-hf-left' => __( 'main-menu-left', 'happyfamily' ),
        'menu-hf-center' => __( 'logo-center', 'happyfamily' ),
        'menu-hf-right' => __( 'main-menu-right', 'happyfamily' )
    ) );
    //  Enable featured images for posts
    add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'theme_setup' );

// Allow to upload SVGs
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function vc_before_init_actions() {

    // Link your VC elements's folder
    if( function_exists('vc_set_shortcodes_templates_dir') ){
        vc_set_shortcodes_templates_dir( get_template_directory() . '/vc_templates' );
    }

}
add_action( 'vc_before_init', 'vc_before_init_actions' );

// Visual Composer default overrides
// @include 'utilities/vc-overrides.php';

// Custom User roles
@include 'utilities/custom-users.php';

// Tags to allow in 'escaped' fields
@include 'utilities/allowed-tags.php';

// Disable/hide unneccessary tools, organize CMS menu items
@include 'utilities/reset.php';

// Customize menus
@include 'utilities/walker.php';

// Post Types (Press)
@include 'utilities/post-types.php';

// Post Taxonomies (Topics)
@include 'utilities/post-taxonomies.php';

// Filtering
@include 'utilities/press-filters.php';

@include 'utilities/contact-filters.php';

// Shortcodes for use in templates and post WYSIWYGs
@include 'utilities/shortcodes.php';

// Add field to customizer for shop URL
@include 'utilities/shop-url.php';

// Create truncated descriptions for image carousels
@include 'utilities/create-excerpts.php';

// Push data to Algolia to use on search results page
@include 'utilities/algolia-data.php';

// include for checking on mobile
@include  'utilities/is-mobile.php';

// Image Srcset
@include 'utilities/image-srcset.php';

// Include custom API endpoints
@include 'rest-api/endpoints.php';

// Custom fields
@include 'fields/press.php';
@include 'fields/team-member.php';
@include 'fields/coach.php';
@include 'fields/learning-center.php';
@include 'fields/faq.php';
@include 'fields/careers.php';
@include 'fields/stages.php';
@include 'fields/contact.php';
function cmp($a, $b)
{
    if ($a["term_id"] == $b["term_id"]) {
        return 0;
    }
    return ($a["term_id"] < $b["term_id"]) ? -1 : 1;
}
/**
 * Gets the primary category set by Yoast SEO.
 *
 * @return array The category name, slug, and URL.
 */
function get_primary_category( $cat = 0 ) {

    $post = get_the_ID();

    if(!$cat){
        $cat = "category";
    }
    // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
    $category = get_the_category( $post );
    $primary_category = array();
    // If post has a category assigned.
    if ($category){
        $category_display = '';
        $category_slug = '';
        $category_link = '';
        if ( class_exists('WPSEO_Primary_Term') )
        {
            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( $cat, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );
            if (is_wp_error($term)) {
                // Default to first category (not Yoast) if an error is returned
                $category_display = $category[0]->name;
                $category_slug = $category[0]->slug;
                $category_link = get_category_link( $category[0]->term_id );
                $category_id = $category[0]->term_id;
            } else {
                // Yoast Primary category
                $category_display = $term->name;
                $category_slug = $term->slug;
                $category_link = get_category_link( $term->term_id );
                $category_id = $term->term_id;
            }
        }
        else {
            // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_slug = $category[0]->slug;
            $category_link = get_category_link( $category[0]->term_id );
            $category_id = $category[0]->term_id;
        }
        $primary_category['id'] = $category_id;
        $primary_category['url'] = $category_link;
        $primary_category['slug'] = $category_slug;
        $primary_category['title'] = $category_display;
    }
    return $primary_category;
}
// Breadcrumbs
function custom_breadcrumbs() {

    // Settings
    $separator          = '  |  ';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Homepage';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        //echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        // echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';

        if ( is_single() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a>';
                echo $separator ;

            }

            $test = get_primary_category("category");
            $primarycatid = $test['id'];
            // Get post category info
            $category = get_the_category();

            for($i=0;$i<count($category);$i++){
                $catarray[$i] = (array) $category[$i];
            }
            usort($catarray,"cmp");
            //print_r($catarray);
            for($i=0;$i<count($catarray);$i++){
                $category[$i] = (object) $catarray[$i];
            }

            $a = 0;
            for($i=0;$i<count($category);$i++){
                if($category[$i]->parent == $primarycatid)
                {
                    $catarr[1] = $category[$i];
                }
                if($category[$i]->term_id == $primarycatid){
                    $catarr[0] = $category[$i];
                }
            }
            $category = $catarr;


            if(!empty($category) && $category[0]->term_id != 1 ) {

                $is_recipe = false;
                // If first category name contains 'Recipe', then it's a recipe or meal plan.  Might be "Recipes", or "Recipes & Meal Plans"
                if ( strpos($category[0]->name, 'Recipe') !== false) {
                    $is_recipe = true;
                }
                $search_tab = $is_recipe ? 'recipes' : 'articles';

                // List all categories this post belongs to, wrapped in a search link. ("category" taxonomy)
                $category_names = array_map(function ($term) {
                    return $term->name;
                }, $category);
                $category_links = array_map(function ($term) use ($search_tab) {
                    return '<a href="/search?q=' . strtolower($term->name) . '&tab=' . $search_tab . '">' . $term->name . '</a>';
                }, $category);
                $cat_display = implode(' | ', $category_links);

                // Get last category post is in
                $categories = array_values($category);
                $last_category = end($categories);

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);


            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;

            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;

            } else if(!empty($cat_id)) {
                // Post is in a custom taxonomy

                echo '<a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                //echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            } else {

                //echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            }

        }

        // echo '</ul>';

    }

}
$blogusers = get_users( 'role=coach' );
foreach ( $blogusers as $user ) {
    $user->add_cap('level_1');
}
add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
add_filter('manage_pages_columns', 'posts_columns_id', 5);
add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);

function posts_columns_id($defaults){
    $defaults['wps_post_id'] = __('ID');
    return $defaults;
}
function posts_custom_id_columns($column_name, $id){
    if($column_name === 'wps_post_id'){
        echo $id;
    }
}

// Product Post Type And Taxonomy
@include 'src/php/product-post-type.php';

// Master Landing Page Post Type
@include 'src/php/master-landing-post-type.php';



// Custom logo setting
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

////////////////////////////////////////////////////////////////////////////
// Algolia Search Stuff
@include 'src/php/algolia.php';

// Change Yoast SEO Prev/Next URL on some pages
// Fixes bug where on some pages (eg "/shop/?age_range=1341") Yoast added a link tag pointing to a 404 (eg "<link rel='next' href=...") 
add_filter( 'wpseo_next_rel_link', 'custom_change_wpseo_next' );
// In case we need "prev" link, too 👇
// add_filter( 'wpseo_prev_rel_link', 'custom_change_wpseo_prev' );
function custom_change_wpseo_next( $link ) {
    // Remove rel=next on shop pages
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $is_shop = strpos( $url , '/shop' ) != false;
    if ( $is_shop ) {
        return false;
    }
    return $link;
}

// disable pingbacks
add_filter( 'xmlrpc_methods', function( $methods ) {
	// Recommended to disable, per HFO client IT team security scan.
    unset( $methods['pingback.ping'] );
    // Another two methods recommended to be disabled, per this blog post.  Not disabling them for now, just saving for future reference:
    // https://the-bilal-rizwan.medium.com/wordpress-xmlrpc-php-common-vulnerabilites-how-to-exploit-them-d8d3c8600b32
	// unset( $methods['wp.getCategories'] );
	// unset( $methods['metaWeblog.getUsersBlogs'] );
	return $methods;
} );

function remove_json_api () {

    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

    // Remove all embeds rewrite rules.
    // add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

    // Remove RSS feed links
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    remove_action( 'wp_head', 'feed_links', 2 );

}
add_action( 'after_setup_theme', 'remove_json_api' );


// End here
