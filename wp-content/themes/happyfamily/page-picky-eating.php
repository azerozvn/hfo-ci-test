<?php
/*
Template Name: Basic Page
*/

get_header();

?>

<?php /*START LOOP */
if (get_posts()) : while (have_posts()) : the_post(); ?>


  <section class="bclearfix backlearning-center-article viewed">
    <div class="learning-center-article__backcontent">
      <div class="learning-center-article__backcontainer bclearfix">

        <section id="picky-eating-hero" class="vc_section hero text-white viewed">
        <div class="fordesktop main no-padding vc_picky_eating main" style="background-image: url(<?php echo get_field('main_hero_image');?>) !important;">
                        <div class="col-12">
                            <div class=" hero__description mission-hero__description text-center">
                            	 <img class="curly-arrow" src="/wp-content/themes/happyfamily/dist/images/arrow-looped-svg.png"/>
                                <h1><?php the_field("hero_title");?></h1>
                                <a href="<?php the_field('cta_pdf_link');?>"><?php the_field("cta_title");?></a>

                            </div>
                        </div>
                    </div>
                    <div class="fortablet main no-padding vc_picky_eating main" style="background-image: url(<?php echo get_field('main_hero_image_tablet');?>) !important;">
                        <div class="col-12">
                            <div class=" hero__description mission-hero__description text-center">
                            	 <img class="curly-arrow" src="/wp-content/themes/happyfamily/dist/images/arrow-looped-svg.png"/>
                                <h1><?php the_field("hero_title");?></h1>
                                <a href="<?php the_field('cta_pdf_link');?>"><?php the_field("cta_title");?></a>

                            </div>
                        </div>
                    </div>
                    <div class="formobile main no-padding vc_picky_eating main" style="background-image: url(<?php echo get_field('main_hero_image_mobile');?>) !important;">
                        <div class="col-12">
                            <div class=" hero__description mission-hero__description text-center">
                            	 <img class="curly-arrow" src="/wp-content/themes/happyfamily/dist/images/arrow-looped-svg.png"/>
                                <h1><?php the_field("hero_title");?></h1>
                                <a href="<?php the_field('cta_pdf_link');?>"><?php the_field("cta_title");?></a>

                            </div>
                        </div>
                    </div>
        </section>

        <section id="picky-eating-second-section"
                 class="vc_section text-center border-beige viewed">
          <div class="main no-padding">
            <div class="col-6 pad48">
              <div class="">
                <h1 class="text-green"><?php the_field("second_section_title");?></h1>
                <div class="content"><?php the_field("second_section_text");?></div>

              </div>
            </div>
          </div>
        </section>

        <section id="picky-eating-dropdown" class="vc_section text-green viewed">
          <div class="main no-padding vc_picky_eating main" >
            <div class="col-12">
                <div class=" text-center">
                <?php $cnt = 1;
                    while (have_rows('menu_for_picky_eating')) : the_row(); ?>

                        <div class="allhide drop-background drop-common drop-val-<?php echo $cnt; ?>">
                            <img src="<?php the_sub_field('image_1'); ?>"
                             class="allhide drop-common desktop-view drop-val-<?php echo $cnt; ?>">
                              <img src="<?php the_sub_field('image_2'); ?>"
                             class="allhide mobile-view drop-common drop-val-<?php echo $cnt; ?>">
                        </div>

                        <?php $cnt++; endwhile; ?>

                    <div class="food-block border-beige">
                    <div class="foodOptions desktop">
                        <?php $count = 0; ?>
                        <ul name="drop" class="dropDownList" id="drop">
                            <?php while (have_rows('menu_for_picky_eating')) : the_row(); ?>
                                <?php $count++; ?>
                                <?php if ($count == 1): ?>
                                    <li class="dropDownSelect" href="javascript:void(0);"
                                        rowid="drop-val-<?php echo $count; ?>">
                                        <span><?php the_sub_field('dropdown_value'); ?></span>
                                    </li>
                                <?php else: ?>
                                    <li class="dropdownList dropdown-value"
                                        rowid="drop-val-<?php echo $count; ?>">
                                        <span class="li-text"><?php the_sub_field('dropdown_value'); ?></span><span class="arrow-r"></span>
                                    </li>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </ul>
                    </div>

                    <?php $cnt = 1;
                    while (have_rows('menu_for_picky_eating')) : the_row(); ?>
                        <div class="dropdown-section col-6 drop-common allhide drop-val-<?php echo $cnt; ?>">
                            <h1><?php the_sub_field('title'); ?></h1>
                            <?php the_sub_field('content'); ?>
                            <a class="l-more" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_titles'); ?><?php echo "Learn More"; ?></a>
                        </div>
                 <?php $cnt++; endwhile; ?>

                </div>

              </div>
              <div style="clear:both"></div>
            </div>
            <div style="clear:both"></div>
          </div>
          <div style="clear:both"></div>
        </section>

          <?php the_content();?>



      </div>
    </div>
  </section>


<?php /*END LOOP */
endwhile; endif; ?>


<?php get_footer(); ?>
<style>
  .hero__description a{background:#70A400 !important}
  #picky-eating-second-section .content p{color: #716F6C;font-size: 18px;
      font-weight: 500; line-height: 24px; text-align: center;font-style: normal;
  }
  .page-picky-eating .hero__description a{
      font-family: "Booster Next FY", sans-serif;
      font-size: 16px; font-weight: bold;
  }
  .pad48{padding:45px 0;}
  .page-picky-eating h1{font-size: 60px; font-weight: normal;}
  .page-picky-eating .site-footer h1{font-size: 30px;font-weight: 700; letter-spacing: 0;}
  .learning-center-card h4{ font-size: 24px; line-height: 28px;}
  .page-picky-eating .carousel-ideas.border-white:before { background-position: top center; top: -10px; }
  .learning-center.one-carousel .learning-center__content{padding:0 !important; width: 1320px;}
  .learning-center-carousel.blue .slick-arrow, .learning-center-grid.blue .slick-arrow {
    background-image: url(<?php echo esc_url(home_url()); ?>/wp-content/themes/happyfamily/dist/images/carousel-arrow-blue.svg) !important;
  }
   #picky-eating-dropdown .desktop-view {display:block;} 
   #picky-eating-dropdown .mobile-view {display:none;}
  .page-picky-eating .learning-center.one-carousel {padding-bottom: 0px; }
  .page-picky-eating .ideas-title{padding:75px 0 40px 0;}
  .page-picky-eating #content.site-content{overflow: hidden;}
  .carousel-ideas{background:#fff !important;}
  .dropdown-section a, .dropDownSelect {
    display: inline-block; font-family: BoosterNextFY,sans-serif;
    font-weight: bold; color: #fff !important;line-height: 50px;
    font-size: 20px; padding: 0px 65px 0 50px;
    border-radius: 25px; text-decoration: none; white-space: nowrap;
    position: relative; color: inherit; text-align: left;
    background-color: #70A400 !important; cursor: pointer;
  }
  .page-picky-eating .hero .main {background-position: center;background-repeat: no-repeat;}
  .dropdown-section a{min-width: 200px;}
  .dropDownSelect{ overflow: hidden;width: 100%; cursor:pointer;}
  .dropDownSelect span{ color: #fff; text-decoration: underline; }
  .dropDownSelect span:after
  {   content: ''; height: 11px; width: 11px; border-right: 4px solid #fff;
      border-bottom: 4px solid #fff; position: absolute;
      transform: rotate(45deg) translateY(-8px);
      right: 45px; margin-top: 23px;
  }
  .dropDownSelect.noBorders{
      border-bottom-right-radius: 0px;
      border-bottom-left-radius: 0px; margin-bottom: -5px;
  }
  .dropDownSelect.noBorders span:after{
      transform: rotate(225deg) translateY(-8px);
      right: 35px; margin-top: 18px;
  }
  .dropdown-section a:after {
    content: ''; width: 11px; height: 11px; position: absolute;
    top: 50%; transform: translateY(-50%);right: 44px;
    border-right: 4px solid #fff; border-bottom: 4px solid #fff;
    transform: translateY(-50%) rotate(-45deg);
    transition: .2s ease-in-out;
  }
  .allhide{display:none;}
  .activeshow{display:block;}
  #picky-eating-dropdown .col-6{
      overflow: hidden; margin: 0 auto;
      position: relative;float: none; width: 68%;
  }
  .page-picky-eating .stages-video{z-index: unset;}
  .page-picky-eating .modal-overlay, .page-picky-eating .modal-content{z-index: 4;}
  .page-picky-eating .site-footer__container {margin: 20px auto 50px;}
  #picky-eating-second-section {background: #fcf8f2;}
  #picky-eating-second-section .col-6{float:none; margin: 0 auto;}
  .dropdownList{display:none; border: 2px solid #70a400;
      border-bottom: none;background: #fff; padding: 8px 0px;
      font-size: 18px; padding-right: 15px; cursor:pointer;
      padding-left: 50px; text-align: left;}
  .arrow-r{display:block; float: right;}
  .dropdownList .arrow-r:after{
      content: ''; height: 10px; width: 10px; border-right: 3px solid #70a400;
      border-bottom: 3px solid #70a400; position: absolute;
      transform: rotate(310deg) translateY(-8px);
      margin-top: 13px; margin-left: -29px;
  }
  .dropDownList{ width: 470px; margin: 0 auto; z-index: 1; position: relative;}
  .dropDownList li:last-child{
      border-bottom: 2px solid #70a400;
      border-bottom-right-radius: 15px;
      border-bottom-left-radius: 15px;
  }
  .food-block{
      display: block; width: 70%; margin: 0 auto; text-align: center;
      margin-top: -104px; margin-bottom: 35px; padding-bottom: 40px;
      background-color: rgba(252, 248, 242, 0.8); position: relative; padding: 30px 0px;
  }
  .food-block p{color: #808080;font-size: 18px; font-weight: 500; line-height: 27px;}
  .food-block h1{ padding: 15px 0px; margin-top: 105px; font-weight: normal;font-size: 55px; }
  .l-more {margin-top: 25px;}
  .foodOptions.desktop{ position: absolute; overflow: hidden; width: 100%; }
  #picky-eating-hero .hero__description { margin-top: 30px; position: absolute;
      z-index: 1;right: 0; margin-right: 80px; max-width: 420px;
  }
  #picky-eating-hero .hero__description h1{width: 100%; margin-bottom: 30px;font-size: 60px;font-weight: 500;
      line-height: 70px; text-align: center; margin-right: 0px; float: right;padding: 0px;}
  .curly-arrow{position: absolute; max-width: 52px; margin-left: -27px; margin-top: 130px;left: 0px;
      float: left; -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); -o-transform: rotate(90deg);
      -ms-transform: rotate(90deg); transform: rotate(20deg);
  }
  .food-block.border-beige:before {background-color: transparent;background-image: url(<?php echo esc_url(home_url()); ?>/wp-content/themes/happyfamily/dist/images/border-beige-opacity.png?v=2);}
  .border-beige {position: relative;}
  .border-beige:after, .border-beige:before {
      content: ''; background-image: url(<?php echo esc_url(home_url()); ?>/wp-content/themes/happyfamily/dist/images/border-beige-top.svg?v=2);
      width: 100%; height: 10px; background-size: 890px auto;
      position: absolute; left: 0; transition: .3s all;
  }
  #picky-eating-second-section.border-beige:after{background-position: bottom left; bottom: 9px; margin-bottom: -15px; z-index: 2;
      background-image: url(<?php echo esc_url(home_url()); ?>/wp-content/themes/happyfamily/dist/images/border-beige-bottom.svg?v=2);
  }
  #picky-eating-second-section h1{font-size: 60px; padding-bottom: 15px; padding-left: 10%; padding-right: 10%;}
  #picky-eating-hero .hero__description a{max-width: 382px;
      text-decoration: none; white-space: nowrap; position: relative; color: inherit;
      margin: 0 auto; overflow: hidden;float: none;display: block; padding: 0px 70px 0 50px;}
  #picky-eating-hero .hero__description a:after{right: 34px; border-radius: 2px;width: 11px;
      height: 11px;border-right: 4px solid #fff; border-bottom: 4px solid #fff;}
  .page-picky-eating #chat{margin-top: 60px;}
  .page-picky-eating #chat .push-1{margin-left: 3%;}
  .page-picky-eating .chat__content h1 { margin-bottom: 5px; font-size: 60px; line-height: 70px;}
  .page-picky-eating #chat .chat__content{padding-top: 10px;}
  .page-picky-eating .chat__phone{overflow: inherit;background-size: auto 630px;}
  .page-picky-eating .chat .col-4>*{width: auto; max-width: 530px;}
  .page-picky-eating .chat__hours p { margin-bottom: 25px;font-size: 18px; font-weight: 500; line-height: 24px;}
  .page-picky-eating .chat__phone img{max-width: 320px; max-height: 630px;}
  .page-picky-eating .chat{height: 715px;}
  .page-picky-eating .carousel-products-small .slick-slider {max-width: 94%; width: 94%;}
  .page-picky-eating .hero .main {
      height: 100%; background-position: inherit; background-size: cover;
  }
  .drop-background{ height: auto; width: 100%; background-position: center; background-size: cover;}
  .page-picky-eating .chat__content img {width: 140px;margin-bottom: 25px;}
  .page-picky-eating .site-header, .page-picky-eating .top-banner-block{z-index: 2;}
  .page-picky-eating .chat__phone{ z-index: 1;}
  .chat-icon-oval{ margin-right: 10px;}
  .page-picky-eating div.site-footer__container{padding-bottom: 5px;}
  .page-picky-eating div section#stages-carousel-video{margin-bottom: 10px;}
  .page-picky-eating .learning-center.one-carousel { z-index: 1; padding-bottom: 0px;}
  .page-picky-eating .wpb_content_element { margin-bottom: 70px;}
  .formobile {display: none;}
  .fordekstop{display:block;}
   .fortablet{display:none;}
  @media only screen and (max-width: 320px){
      section.learning-center.one-carousel div.learning-center__content{max-width: 270px;}
      section#picky-eating-hero div div.hero__description a { padding: 0px 35px 0 20px; font-size: 14px;font-weight: bold;}
      #picky-eating-hero div div.hero__description a:after { right: 20px !important; }
      .foodOptions ul.dropDownList { width: 280px; }
      .foodOptions li.dropDownSelect { padding: 0px 10px 0 20px; }
      .foodOptions li.dropdownList {font-size: 14px; padding-left: 20px; }
      .page-picky-eating div section.hero div.main {background-position: center top; }
      #picky-eating-hero .main div div.hero__description h1 { font-size: 30px; line-height: 35px; letter-spacing: 1pt;}
      .main div div .curly-arrow {margin-right: 37px; margin-top: 44px;}
      .page-picky-eating div div.drop-background{ height: 320px;}
      div li.dropDownSelect{font-size: 14px;}
      section div div.learning-center-carousel__container{max-width: 246px;}
      #picky-eating-dropdown div div.col-6 { width: 86%; }
      div div.food-block h1 {font-size: 20px;}
  }
  @media only screen and (max-width: 375px){
      .main div div.chat__content h3{ width: 66%;}
      .page-picky-eating section#chat.chat { height: 1040px; }
      section#picky-eating-hero div.hero__description a { padding: 0px 50px 0 30px;}
      .learning-center.one-carousel div.learning-center__content{max-width: 320px;}
      div div.learning-center-carousel__container{max-width: 300px;}
      #picky-eating-hero div div.hero__description h1 { font-size: 30px;
          text-align: center; line-height: 35px; margin-top: -5px;
      }
      #picky-eating-dropdown div div.col-6 {width: 88%;}
      .page-picky-eating section.hero div.main {background-position: center top; padding-top:0px; }
      div div .curly-arrow {margin-right: 45px; margin-top: 45px;}
      .page-picky-eating div.drop-background{ height: 370px;}
      div div .food-block h1 {font-size: 19px;}
  }

  @media only screen and (max-width: 420px){
    .page-picky-eating .hero div.main { background-position: center top; }
  }

   @media only screen and (max-width:450px) {
    .page-picky-eating .hero {height:457px;}
    #picky-eating-dropdown .desktop-view {display:none;} 
    #picky-eating-dropdown .mobile-view {display:block;}
   }

   @media only screen and (min-width:451px) and (max-width:600px) {  
    .page-picky-eating .hero {height:600px;}
   }

  @media only screen and (max-width: 767px){
      .page-picky-eating .top-banner-block{padding: 0px 20px;}
      .formobile {display: block !important;}
	   .fordekstop{display:none !important;}
	   .fortablet{display:none !important;}
	   .page-picky-eating .hero .main {background-position:center;}
  }
 @media only screen and (min-width : 768px) and (max-width : 1024px) {
      .formobile {display: none !important;}
      .fordekstop{display:none !important;}
      .fortablet{display:block !important;}
      .page-picky-eating .hero .main {background-position:center;}
  }
  @media only screen and (min-width : 1024px) and (max-width : 1150px) {
      .page-picky-eating #picky-eating-second-section h1 {font-size: 45px;}
      .page-picky-eating .learning-center-card h4 { font-size: 21px; line-height: 23px; }
      .food-block h1{font-size: 42px;letter-spacing: 3.7px;}      
  }
  @media only screen and (min-width : 1151px) and (max-width : 1290px) {
      .page-picky-eating #picky-eating-second-section h1 {font-size: 52px;}
      .food-block h1{font-size: 48px;}
  }

  @media only screen and (min-width : 768px) and (max-width : 1024px) {
      .page-picky-eating .chat .main{width: 94%;}
      .page-picky-eating .chat__content.col-4 { width: 50%;}
      .page-picky-eating section.chat .col-4>* {  width: auto; }
      .page-picky-eating div.chat__content h1{font-size: 35px;line-height: 40px;}
      .page-picky-eating div.chat__phone { background-size: auto 520px; margin: 5px 0 0;}
      .page-picky-eating div.chat__phone img { max-width: 300px; max-height: 590px; width: 290px;}
      .page-picky-eating div.chat__content img{max-width: 120px;}
      #picky-eating-hero div.hero__description a { position:relative;left: 0;
          margin: 0 auto;overflow: hidden; transform: translateX(0%); bottom: 0px;}
      .page-picky-eating #picky-eating-second-section h1 {font-size: 33px;}
      div.food-block h1 {font-size: 35px; letter-spacing: 1.71pt;padding: 0 0px 15px;max-width: 412px;margin: 94px auto 10px auto;}
      #picky-eating-dropdown .col-6{width: 74%;}
      div div.border-beige:before { top: -10px;}
      .page-picky-eating section.chat { height: 690px; }
      .page-picky-eating .learning-center-card h4 { font-size: 21px; line-height: 23px; }
  }
  @media only screen and (max-width: 769px){
      .page-picky-eating .top-banner-block{padding: 0px 20px;}
      .page-picky-eating .chat .main { width: 96%;}
      .page-picky-eating .chat__content.col-4 { width: 50%;}
      .page-picky-eating .chat div .col-4>* {width: fit-content; text-align: center;}
      .page-picky-eating .chat__phone {background-size: auto 580px;  margin: 15px 0 0;}
      .page-picky-eating .chat__phone img { width: 280px;}
      .page-picky-eating .chat__phone--masked {height: 100%;}
      .page-picky-eating .chat__content h1 {margin-bottom: 5px;line-height: 39px; font-size: 30px; }
      .page-picky-eating div.chat__content img{max-width: 110px;}
      section.border-purple:after {bottom: -9px;}
      div div.border-beige:before { top: -10px;}
      section.border-green:before { top: -9px;}
      #picky-eating-second-section div h1 {font-size: 24px; line-height: 30px; font-weight: bold; letter-spacing: 1.71pt;max-width: 300px;
    margin: 0 auto;}
      #picky-eating-hero div.hero__description h1{font-size: 49px;line-height: 51px; display: block; margin-bottom: 55px;}
      #picky-eating-hero div.hero__description { text-align: right; max-width: 340px;margin-top: 30px}
      div .curly-arrow{max-width: 42px; margin-left: -27px; margin-top: 102px;}
      #picky-eating-hero div.hero__description a { padding: 0px 40px 0 30px;  margin-top: 15px;text-align: left; max-width: 360px;}
      div.food-block h1 { font-size: 40px; margin-top: 94px; letter-spacing: 1.6pt;}
      div.food-block { width: 86%;}
      #picky-eating-dropdown .col-6{width: 78%;}
  }
  @media only screen and (max-width: 650px){
      div.food-block{width: 92%;margin-top: -75px; padding: 20px 0px;}
      ul.dropDownList {width: 330px;}
      li.dropdownList{font-size: 15px;padding-left: 30px;}
      li.dropDownSelect{ font-size: 16px; padding: 0 22px 0 30px; line-height: 42px;}
      li.dropdownList .arrow-r:after{margin-top:13px;margin-left: -16px;}
      li.dropDownSelect span:after{margin-top: 18px;right: 35px;border-radius: 2px;}
      li.dropDownSelect.noBorders span:after {margin-top: 11px; right: 23px;}
      div.food-block h1{ font-size: 22px; margin-top: 75px;letter-spacing: 1.71pt; font-weight: bold;}
      #picky-eating-second-section div.col-6{width: 84%;}
      #picky-eating-second-section h1{ font-size: 28px; margin-bottom: 10px;padding-left: 6%; padding-right: 6%;}
      #picky-eating-second-section .content p{color: #808080; font-size: 12px; font-weight: 500; line-height: 18px;}
      div.border-white:after, div.border-white:before { background-position: top;}
      #picky-eating-hero div.hero__description {padding-top: 0px; width: 100%;padding: 0px 5px;position: relative; margin: 30px auto;}
      #picky-eating-hero div.hero__description h1{font-size: 30px; text-align: center;font-weight: normal; line-height: 35px;letter-spacing: 1.5pt;}
      div .curly-arrow { position: absolute; margin-right: 35px;margin-top: 48px; float: right;
          right: 0px; max-width: 35px; margin-top: 48px; left: unset;}
      #picky-eating-second-section div.col-6{ padding: 30px 0;}
      div.food-block.border-beige:before { top: -10px; }
      #picky-eating-dropdown div.col-6{width: 80%;}
      .page-picky-eating section#chat{margin-top: 10px;}
      .page-picky-eating section.ideas-title{padding:35px 0 0px 0;}
      section#stages-carousel-video{margin-top: 0px; }
      .learning-center.one-carousel div.learning-center__content{max-width: 340px;}
      div.learning-center-carousel__container{max-width: 340px;}
      .page-picky-eating div.site-footer__container {margin: 15px auto 30px;}
      .page-picky-eating .hero .main { background-position: center top; padding-top:0px;}
      .page-picky-eating .hero__description a{bottom: 60px;}
      .page-picky-eating .hero__description a:after{ width: 11px; height: 11px; right: 29px !important;
          border-right: 4px solid #fff; border-bottom: 4px solid #fff;border-radius: 3px;
      }
      /*.page-picky-eating .drop-background{ height: 414px;}
      .page-picky-eating .drop-background img{ display: none;}*/
      .chat-icon-q{margin-top: 45px;}
      .page-picky-eating .chat__content h3 img {display: block; float: left;width:100px;}
      .main div.chat__content h3{text-align: center; width: 60%; margin: 0 auto; padding-bottom: 5px; overflow: hidden;}
      .page-picky-eating #chat.chat { height: 1040px; }
      #learning-center-title .main .wpb_content_element{margin-bottom: 0px;}
      .page-picky-eating .carousel-products-small div.slick-slider{width: 88%;}
      .page-picky-eating .border-blue:before { top: -6px;}
      .page-picky-eating section.carousel-ideas.border-white:before{display:none;}
      .dropdown-section a.l-more{ min-width: 200px; width: 200px; line-height: 36px;
          font-size: 14px; font-weight: bold; padding: 0px 35px 0 50px;
      }
      .dropdown-section a.l-more:after{right: 45px; border-radius: 2px;}
      .page-picky-eating div.chat__phone{ margin-top: 25px;}
      .page-picky-eating .learning-center-card__details{ padding: 14px;}
      .page-picky-eating  div.chat__content h1 { font-size: 35px; letter-spacing: 1.75pt;
          line-height: 40px; font-weight: bold; }
      .page-picky-eating div.chat__hours p{margin-bottom: 15px;}
      .page-picky-eating h4.learning-center-card__title { font-size: 12px; line-height: 14px; }
      .page-picky-eating .learning-center-card__excerpt { font-size: 10px; line-height: 12px; }
      .page-picky-eating .carousel-video h1 { margin-top: 26px; margin-bottom: -9px;}
      .page-picky-eating .carousel-video .slick-slider { margin-bottom: 45px; }
      .page-picky-eating #chat div.push-1{ margin-left: auto; margin: 0 auto; display: block;
          position: relative; text-align: center; width: 100%;}
      .page-picky-eating .chat div .col-4>* {width: fit-content; text-align: center;}
      .page-picky-eating .learning-center.one-carousel { background: #2d9ed2!important;}
      #picky-eating-hero div.hero__description a {position: absolute; font-size: 14px;font-weight: bold;}
      .page-picky-eating div.chat__content.col-4 { width: 100%;}
      section#picky-eating-hero div.hero__description{margin-top: 30px;}
      .page-picky-eating .chat__hours p{ font-size: 13px; line-height: 18px;}
      .page-picky-eating .chat__hours a.chat-now-picky {width: 300px;}
      .page-picky-eating .chat__hours a.chat-now-picky:after {right: 100px;}
      .page-picky-eating h1{ font-size: 35px;}
      .page-picky-eating .learning-center__content h1{font-size: 35px; line-height: 40px;
      font-weight: bold; letter-spacing: 1.75pt; margin: 0px 15px;}
      .page-picky-eating .food-block p{ font-size: 12px; font-weight: 500; line-height: 18px;}
      .page-picky-eating .chat { padding-top: 15px;}
      .carousel-video div.slick-slide p:not(:first-child){padding-left: 12px; padding-right: 12px; font-size: 11px; line-height: 12px;}
      .carousel-video div.slick-slide p:not(:first-child) a{ font-size: 13px; line-height: 24px;}
      .page-picky-eating div div.chat__phone img{max-width: 300px; width: 300px; max-height: 630px;}
  }
</style>
<script>
    jQuery(document).ready(function(){
        jQuery(".drop-val-1").addClass("activeshow");
        jQuery(".drop-val-1").removeClass("allhide");

        jQuery('.foodOptions.desktop .dropDownSelect').click(function (event) {
            jQuery(this).parent().find(".dropdownList").slideToggle("slow");
            if (!jQuery(this).hasClass('noBorders')) {
                jQuery(this).addClass('noBorders');
            } else {
                jQuery(this).removeClass('noBorders');
            }
        });

        jQuery(".dropdown-value").on("click",function(){
            var dval = jQuery(this).attr("rowid");
            var dvalText = jQuery(this).find(".li-text").text();
            var selectedText = jQuery(".dropDownSelect").find("span").text();
            var selectedValue = jQuery(".dropDownSelect").attr("rowid");

            jQuery(".dropDownSelect").find("span").text(dvalText);
            jQuery(".dropDownSelect").attr("rowid", dval);

            jQuery(".drop-common").removeClass("activeshow");
            jQuery(".drop-common").addClass("allhide");

            jQuery("."+dval).addClass("activeshow");
            jQuery("."+dval).removeClass("allhide");

            jQuery(this).attr("rowid", selectedValue);
            jQuery(this).find(".li-text").text(selectedText);


            jQuery(".foodOptions.desktop .dropDownSelect").parent().find(".dropdownList").slideToggle(250);
            if (!jQuery(".foodOptions.desktop .dropDownSelect").hasClass('noBorders')) {
                jQuery(".foodOptions.desktop .dropDownSelect").addClass('noBorders');
            } else {
                jQuery(".foodOptions.desktop .dropDownSelect").removeClass('noBorders');
            }

            return true;
        });
    });

</script>
