
<button class="button-expand-category" data-target="<?php echo $category->slug ?>">
	<?php if(property_exists($category, 'icon')) { ?>
		<img class="button-expand-category__icon" src=<?php echo $category->icon ?> /> 
	<?php } ?>
	<span class="button-expand-category__label"><?php echo $category->name ?></span>

	<?php if(property_exists($category, 'locations')) { 

		foreach($category->locations as $location ) {
			echo '<span class="button-expand-category__subtext">' . $location . '</span>';
		}?>
	<?php } ?>
</button>
