<div class="learning-center-article__share">
	<h1><?php echo esc_html($share_heading); ?></h1>
	<a class="learning-center-article__share-link learning-center-article__share-link--twitter" target="_blank" href="https://twitter.com/home?status=<?php echo esc_url(the_title()); ?>%20<?php echo esc_url(get_permalink()); ?>"></a>
	<a class="learning-center-article__share-link learning-center-article__share-link--facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_permalink()); ?>"></a>
	<?php if ( has_post_thumbnail( $post->ID ) ): ?>
		<a class="learning-center-article__share-link learning-center-article__share-link--pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo esc_url(get_permalink()); ?>&media=<?php echo esc_url($image); ?>&description=<?php echo esc_url(the_title()); ?>"></a>
	<?php else: ?>
		<a class="learning-center-article__share-link learning-center-article__share-link--pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo esc_url(get_permalink()); ?>&description=<?php echo esc_url(the_title()); ?>"></a>
	<?php endif; ?>
	<a class="learning-center-article__share-link learning-center-article__share-link--email" target="_blank" href="mailto:?&subject=<?php echo esc_url(the_title()); ?>&body=<?php echo esc_url(get_permalink()); ?>"></a>
</div>
