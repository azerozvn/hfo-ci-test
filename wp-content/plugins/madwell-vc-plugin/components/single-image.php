<?php

/*
Element Description: Madwell VC Single Image/Video
*/
 
// Element Class
class madSingleImage extends WPBakeryShortCode
{
     
    // Element Init
    public function __construct()
    {
        add_action('init', array( $this, 'mad_singleimage_mapping' ));
        add_shortcode('mad_singleimage', array( $this, 'mad_singleimage_html' ));
    }
     
    // Element Mapping
    public function mad_singleimage_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Single Image/Video', 'madwell-vc-plugin'),
                'base' => 'mad_singleimage',
                'description' => __('Single Image/Video', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => plugins_url('/../assets/img/mad_fullhero.png', __FILE__),
                'params' => array(
                    array(
                        'heading'     => __('Image', 'madwell-elements'),
                        'param_name'  => 'image',
                        'type'        => 'attach_image',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                        'admin_label' => true,
                    ),
                    array(
                        'heading' => __('Video', 'madwell-elements'),
                        'param_name' => 'video',
                        'type' => 'file_picker',
                        'class' => '',
                        'description' => 'If provided, video is shown instead of the image.',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                        'value' => '',
                    ),
                    array(
                        'heading'     => __('Custom Class', 'madwell-elements'),
                        'param_name'  => 'custom_class',
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'heading' => __('Element ID', 'js_composer'),
                        'param_name' => 'el_id',
                        'type' => 'el_id',
                        'description' => sprintf(__('Enter element ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'js_composer'), 'http://www.w3schools.com/tags/att_global_id.asp'),
                        'group' => 'Madwell',
                    ),
                    array(
                        'heading' => __('CSS box', 'js_composer'),
                        'param_name' => 'css',
                        'type' => 'css_editor',
                        'group' => __('Design Options', 'js_composer'),
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function mad_singleimage_html($atts, $content = null)
    {
        $data = wp_parse_args($atts, array(
            'image'         => '',
            'video'         => '',
            'custom_class'    => '',
            'el_id'   => '',
            'el_class'  => '',
            'css'  => '',
        ));

        // Grab the image
        $image = wp_get_attachment_image_src($data['image'], 'full');
        $image_alt = get_post_meta($data['image'], '_wp_attachment_image_alt', true);

        // Grab the video
        $video = array_key_exists('video', $data) ? wp_get_attachment_url($data['video']) : false;

        // Start output
        $output = '';

        // Start section
        $output .= '<div id="' . esc_html($data['el_id']) . '" class="' . esc_html($data['custom_class']) . ' ' . vc_shortcode_custom_css_class($data['css'], ' ') . '">';

        // Render video or image
        if ($video) {
            $output .= '<video autoplay loop muted ><source src="' . $video . '"></video>';
        } else {
            $output .= '<img src="' . esc_url($image[0]) . '" sizes="100vw" alt="' . $image_alt . '" loading="lazy">';
        }

        // Close section
        $output .= '</div>';
        return $output;
    }
} // End Element Class
 
// Element Class Init
new madSingleImage();
