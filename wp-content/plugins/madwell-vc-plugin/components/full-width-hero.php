<?php

/*
Element Description: Madwell VC Full Width Hero
Displays hero background with h1 heading and p subheding
*/
 
// Element Class 
class madFullHero extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'mad_fullhero_mapping' ) );
        add_shortcode( 'mad_fullhero', array( $this, 'mad_fullhero_html' ) );
    }
     
    // Element Mapping
    public function mad_fullhero_mapping() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }                       

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Full Width Hero', 'madwell-vc-plugin'),
                'base' => 'mad_fullhero',
                'description' => __('Full Width Hero', 'madwell-vc-plugin'),
                'category' => __('Madwell Elements', 'madwell-vc-plugin'),
                'icon' => plugins_url('/../assets/img/mad_fullhero.png', __FILE__),            
                'params' => array(
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Header Image (Desktop)', 'madwell-elements' ),
                        'param_name'  => 'image',
                        'description' => '600px up',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __( 'Header Image (Mobile)', 'madwell-elements' ),
                        'param_name'  => 'mobile-image',
                        'description' => '600px down',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type' => 'file_picker',
                        'heading' => __( 'Header Video (Desktop)', 'madwell-elements' ),
                        'class' => '',
                        'param_name' => 'video',
                        'description' => 'If provided, replaces desktop image.  (600px up)',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                        'value' => '',
                    ),
                    array(
                        'type' => 'file_picker',
                        'heading' => __( 'Header Video (Mobile)', 'madwell-elements' ),
                        'class' => '',
                        'param_name' => 'mobile-video',
                        'description' => 'If provided, replaces mobile image.  (600px down)',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                        'value' => '',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'h1',
                        'heading'     => __( 'Title', 'madwell-elements' ),
                        'param_name'  => 'title',
                        'description' => 'The heading on the hero image',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textarea_html',
                        'holder' => 'div',
                        'heading'     => __( 'Content', 'madwell-elements' ),
                        'param_name'  => 'content',
                        'description' => 'The subheading of the hero image',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Button URL', 'madwell-elements' ),
                        'param_name'  => 'button_url',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Button Text', 'madwell-elements' ),
                        'param_name'  => 'button_text',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                    array(
                        'type'        => 'textfield',
                        'holder' => 'div',
                        'heading'     => __( 'Custom Class', 'madwell-elements' ),
                        'param_name'  => 'custom_class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Madwell',
                    ),
                )
            )
        );
    }

     
    // Element HTML
    public function mad_fullhero_html( $atts, $content = null ) {
         
        $data = wp_parse_args( $atts, array(
            'image'         => '',
            'mobile-image'  => '',
            'title'         => '',
            'button_url'    => '',
            'button_text'   => '',
            'custom_class'  => '',
        ) );

        // Grab the images
        $image = wp_get_attachment_image_src( $data['image'], 'full' );
        $mobileImage = wp_get_attachment_image_src( $data['mobile-image'], 'full' );

        // Grab the videos
        $video = array_key_exists('video', $data) ? wp_get_attachment_url($data['video']) : false;
        $mobileVideo = array_key_exists('mobile-video', $data) ? wp_get_attachment_url($data['mobile-video']) : false;

        // Build our button output
        $button_output = '';
        if ( $data['button_url'] && $data['button_text'] ) {
            $button_output = '<a href="' . esc_url( $data['button_url'] ) . '" class="hero-button">' . esc_html( $data['button_text'] ) . '</a>';
        }

        // Start output
        $output = '';

        // Start section
        $output .= '<section class="hero-container__fullwidth ' . esc_html( $data['custom_class'] ) . '">';

        // Start section with background image if exists
        // Desktop image
        if( $video ) {
            $output .= '<video class="home-hero__background desktop-only" autoplay loop muted ><source src="' . $video . '"></video>';
        } else {
            $output .= '<img class="home-hero__background desktop-only" src="' . esc_url( $image[0] ) . '" sizes="100vw">';
        }
        // Mobile image
        if( $mobileVideo ) {
            $output .= '<video class="home-hero__background desktop-hide" autoplay loop muted ><source src="' . $mobileVideo . '"></video>';
        } else {
            $output .= '<img class="home-hero__background desktop-hide" src="' . esc_url( $mobileImage[0] ) . '" sizes="100vw">';
        }

        // Start content div
        $output .= '<div class="hero-content__fullwidth ' . esc_html( $data['custom_class'] ) . ' wrapper">';

        // Output the title if one exists
        $output .= $data['title'] ? '<h1 class="hero">' . esc_html( $data['title'] ) . '</h1>' : '';

        // Output the content if it exists
        $output .= $content ? apply_filters( 'the_content', $content ) : '';

        // Output the button
        $output .= wp_kses_post( $button_output );

        // Close content div
        $output .= '</div>';

        // Close section
        $output .= '</section>';
        return $output;
    }
     
} // End Element Class
 
// Element Class Init
new madFullHero();