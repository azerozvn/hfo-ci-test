<?php
// Add a settings page to the WP Admin so we can manually re-index everything.

function hfoalgolia_register_settings() {
  add_option( 'hfoalgolia_option_name', 'This is my option value.');
  register_setting( 'hfoalgolia_options_group', 'hfoalgolia_option_name', 'hfoalgolia_callback' );
}
add_action( 'admin_init', 'hfoalgolia_register_settings' );


function hfoalgolia_register_options_page() {
  add_options_page('Algolia Search Index', 'Algolia Search Index', 'manage_options', 'hfoalgolia', 'hfoalgolia_options_page');
}
add_action('admin_menu', 'hfoalgolia_register_options_page');


function hfoalgolia_options_page() {
?>
  <div>
    <?php screen_icon(); ?>
    <h2>Manually Push to Algolia</h2>
    <p>This site uses Algolia index: <strong><?php echo hfoalgolia_get_index_name() ?></strong></p>
    <p>
      <a class="button btn" href="<?php echo HFOALGOLIA_PLUGIN_SETTINGS_PAGE . "&action=hfoalgolia_push_everything" ?>">Push Everything to Algolia</a>  (Click once. Be patient, this can take a while.)
      <?php 
        if ($_GET['action'] == 'hfoalgolia_push_everything') {
          $result = hfoalgolia_push_everything_to_algolia();
          echo '<p style="color: green;">' . $result . '</p>';
        }
      ?>
    </p>
    <p>
      If the Algolia index becomes out of sync with Wordpress for any reason, click the "Push Everything" button.
    </p>
    <p>
      The Algolia index contains this site's Products, Recipes (learning center), and Articles (learning center).  These entries are automatically pushed to the Algolia index whenever they are saved in the WP Admin.  The index is used to populate search results when using the site search on the front end.
      
    </p>
  </div>
<?php
}
