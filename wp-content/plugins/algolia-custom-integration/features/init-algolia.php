<?php

// require_once __DIR__ . '/api-client/autoload.php';
// If you're using Composer, require the Composer autoload
require_once __DIR__ . '/../vendor/autoload.php';

// Define Constants
define('HFOALGOLIA_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('HFOALGOLIA_PLUGIN_URL', plugin_dir_url(__FILE__));
define('HFOALGOLIA_PLUGIN_SETTINGS_PAGE', get_admin_url() . 'options-general.php?page=hfoalgolia');

global $algolia;

$algolia = \Algolia\AlgoliaSearch\SearchClient::create("LRSU9YV1PY", "5e0c73c5638ba7f1c9e02a08ddb122bc");

function hfoalgolia_get_index_name() {
	// Get index name
  $index_name = 'searchable_posts_local';
  $url = '';
	
  if( function_exists("home_url") && isset($wp)) {
    // On Wordpress request use this
    $url = home_url( $wp->request );
  } else if( isset($_SERVER) ) {
    // On AJAX request use this
    $url = $_SERVER['SERVER_NAME'];
  }
	
	if( strpos($url, 'happyfamilyorganics.com') !== false ) { $index_name = 'searchable_posts_prod'; }
	if( strpos($url, 'hfodev2.wpengine.com') !== false ) { $index_name = 'searchable_posts_staging'; }

	// error_log('algolia_get_index_name: url: ' . $url . ', index_name: ' . $index_name);
	return $index_name;
}
