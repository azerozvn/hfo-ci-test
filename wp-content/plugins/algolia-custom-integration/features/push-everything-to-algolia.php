<?php

function hfoalgolia_push_everything_to_algolia() {
  global $algolia;
  $result = '';
  $all_records = [];
  
  $DEBUG = false; // If true, don't push to Algolia

  // Print every Algolia request and response.
  // \Algolia\AlgoliaSearch\Log\DebugLogger::enable();
		
  // Getting error "undefined method Algolia_Plugin::initIndex()"?  Turn off "Search by Algolia" plugin.
  $index_name = hfoalgolia_get_index_name();
  $index = $algolia->initIndex($index_name);
  // $response = $index->clearObjects()->wait();
  
  // $result .= 'Clearing Algolia Index...<br/>';
  // $result .= $response['message'] . '<br/>';

  $paged = 1;
  $count = 0;

  if($DEBUG) {
    $result .= 'DID NOT PUSH TO ALGOLIA, BECAUSE $DEBUG is true.<br/>';
  }

  do {
    $posts = new WP_Query([
      'posts_per_page' => 100,
      'paged' => $paged,
      'post_type' => array('post', 'products'),
    ]);
    
    if (!$posts->have_posts()) {
      // Break us out of while(true)
      break;
    }
    
    $records = [];
    
    foreach ($posts->posts as $post) {
      // if ($assoc_args['verbose']) {
      //   WP_CLI::line('Serializing ['.$post->post_type.']');
      // }

      // print_r($post);
      
      // Don't push to Algolia if post_status is not "publish" (eg "draft")
      if($post->post_status != 'publish') {
        $result .= 'Didn\'t push this entry to Algolia, because it\'s not published: ' . $post->post_title . '<br/>';
        continue;
      }
      
      $record = (array) apply_filters('algolia_post_to_record', $post);
      
      if (!isset($record['objectID'])) {
        $record['objectID'] = implode('#', [$post->post_type, $post->ID]);
      }

      // if($record['type'] == 'article') {
      //   print_r($post);
      // }
      
      $records[] = $record;
      $count++;
    }
        
    $paged++;

    $all_records = array_merge($all_records, $records);

      
  } while (true);
  
  if(!$DEBUG) {
    $index->replaceAllObjects($all_records, [
      'safe' => true
    ]);
  }
    
  $total_products = array_filter($all_records, function($record) { return $record['type'] == "product"; });
  $total_articles = array_filter($all_records, function($record) { return $record['type'] == "article"; });
  $total_recipes = array_filter($all_records, function($record) { return $record['type'] == "recipe"; });

  $result .= '<br/>';
  $result .= 'Done.  Pushed ' . $count . ' entries (' . count($total_products) . ' products, ' . count($total_articles) . ' articles, ' . count($total_recipes) . ' recipes) to Algolia index "' . $index_name . '".<br/>';
  
  return $result;
}