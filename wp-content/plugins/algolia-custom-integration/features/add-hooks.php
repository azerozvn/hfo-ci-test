<?php
// These Wordpress hooks will fire each time we save a Wordpress post, and keep the Algolia index up-to-date.


// Copied from Algolia docs: https://www.algolia.com/doc/integration/wordpress/indexing/automatic-updates/?client=php

// "Save_post" hook gets fired every time you hit "Update", and every autosave. 
// Priority="100" is necessary so that this runs this after the postmeta is saved, which provides $post with the latest ACF field values.
add_action('save_post_products', 'hfo_algolia_save_post', 100, 3);
add_action('save_post_post', 'hfo_algolia_save_post', 100, 3);

function hfo_algolia_save_post($id, WP_Post $post, $update) {
    // error_log('hfo_algolia_save_post: ' . $id);
    
    // If autosave, don't send.
    if (wp_is_post_autosave($id)) {
        // error_log('hfo_algolia_save_post: is autosave.  return.');
        return $post;
    }
    
    // If not published, don't send.
    if ($post->post_status != 'publish') {
        // error_log('hfo_algolia_save_post: != publish.  return.');
        return $post;
    }
    
    global $algolia;
    
    $record = (array) apply_filters('algolia_post_to_record', $post);
    
    if (!isset($record['objectID'])) {
        $record['objectID'] = implode('#', [$post->post_type, $post->ID]);
    }
    
    $index = $algolia->initIndex( hfoalgolia_get_index_name() );
    
    if ($post->post_status == 'trash') {
        // If post was trashed, delete record.
        $index->deleteObject($record['objectID']);
    } else {
        $index->saveObject($record);
        // error_log('Post ID ' . $id . ' sent to Algolia.');
        // error_log(print_r($record, true));
    }
    return $post;
}

// Don't use the updated_postmeta hook.  It fires once for every ACF field that got changed.  We don't want to send multiple index updates.  It does not get fired if we update only the post title.
// add_action('updated_postmeta', 'hfo_updated_postmeta', 1, 4);
// function hfo_updated_postmeta($meta_id, $id, $meta_key, $meta_value) {
    
//     // Ignore "_edit_lock" key, it gets fired a lot.
//     if($meta_key === '_edit_lock') {
//         return;
//     }
//     error_log('hfo_updated_postmeta: ' . $meta_id . ', ' . $id . ', ' . $meta_key . ', ' . $meta_value);
// }

