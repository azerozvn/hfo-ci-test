<?php

/**
 * Plugin Name:     Algolia Custom Integration
 * Description:     Add Algolia Search feature
 * Text Domain:     algolia-custom-integration
 * Version:         1.0.0
 *
 * @package         Algolia_Custom_Integration
 */

// Your code starts here.

require_once __DIR__ . '/features/init-algolia.php';
require_once __DIR__ . '/features/push-everything-to-algolia.php';
require_once __DIR__ . '/wp-cli.php';
require_once __DIR__ . '/features/add-hooks.php';
require_once __DIR__ . '/features/add-settings-page.php';
