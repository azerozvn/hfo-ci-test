<?php
require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/../features/init-algolia.php';
require_once __DIR__ . '/../features/push-everything-to-algolia.php';

if (isset($_POST['action'])) {
  if ($_POST['action'] == 'hfoalgolia_push_everything' ) {
    // error_log('received post request with action: hfoalgolia_push_everything');
    hfoalgolia_push_everything_to_algolia();
    exit;
  }
}