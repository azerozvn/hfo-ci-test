<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (file_exists(dirname(__FILE__) . '/wp-config-local.php')) {
    /**
    * Local configuration information.
    *
    * If you are working in a local/desktop development environment and want to
    * keep your config separate, we recommend using a 'wp-config-local.php' file,
    * which you should also make sure you .gitignore.
    */
    require_once(dirname(__FILE__) . '/wp-config-local.php');
} else {
    /**
     * Fallback configuration.
     * This block is not used on WP Engine (wp-config.php cannot be pushed with Git)
     * https://wpengine.com/support/git/#Disallowed_Files_and_Types
     */
    // ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'local');

    /** MySQL database username */
    define('DB_USER', 'root');

    /** MySQL database password */
    define('DB_PASSWORD', 'root');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');

    /**
     * Authentication Unique Keys and Salts.
     *
     * Change these to different unique phrases!
     * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
     * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
     *
     * @since 2.6.0
     */
    define('AUTH_KEY', 'z1qRbVnFw8wgnYr/uj22pogcuN6pqavMSSiywNMqH6EouB8gOgYVgIVecFmR78MRJx4L7WNyU4JKIsOPaZ8tQg==');
    define('SECURE_AUTH_KEY', 'WyJwF5+QYR9AZwTP3KXF47TKqf882GN0079QkFzR7svLcyX1ia17xSzB+/cqOutJQ4jE9/oICcPufYUqFmkhag==');
    define('LOGGED_IN_KEY', '0hH7mSJi3PMuyC6bPMmpd1OhDqhwfp8EO4JerDNouvrQ8luBAXWSasbLH2pHyjfZUWguFKyX9iGbkI/oM4lJsw==');
    define('NONCE_KEY', 'gcb8NyACAKuYEBQF3f2kjU5Q/DFv0kfslkWNpYgpMVaRkQbSxDwsIEFGf72W9ShMH8LOy80FX3NHafRlxzxMBg==');
    define('AUTH_SALT', 'ak7eKdoy3tKF6Wm5niGk1kPvnYrL1KE3X0IKv1xsY0EVWRZ0JbdYu4Nor53A/r7FP73wf1zNIVbNX/YwSO9nqg==');
    define('SECURE_AUTH_SALT', 'f0/Yjmx4kC30JR7OBWBHX7ouIiO4y8ZdXVJCjzKdCSSsiPewYkYl5mhEjkWN7VFLo8JGLJyNur6xGEFOv5GR3Q==');
    define('LOGGED_IN_SALT', 'xgTxkQKYI2PLW1KyVoVm2K3ZlLJBfMGSb3IYoB2dfriCsH9Lecqe9YxPZAHXEB+pyqGWEA74XB1DJDv/ESF/Vg==');
    define('NONCE_SALT', 'nNhE5G0gM1yI4/OdHZqQh3hFYbX13L3A+ymJtEgL85bJMNzc9ssK4vc3vkrOLOBG/0iwfQfgQsaRYTN9rbNx1g==');
}

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
