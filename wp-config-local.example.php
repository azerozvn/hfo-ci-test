<?php
/**
 * wp-config.php will load this file if it exists.
 */

// Valet Site URL.  Example: "https://hfo.test".
// Hardcoding these values causes the two fields to become disabled in "Settings > General", but that's OK.
define('WP_HOME', 'https://hfo.test');
define('WP_SITEURL', 'https://hfo.test');

// Database Name
define('DB_NAME', 'happyfamily');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

// Debug Mode
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

define('AUTH_KEY', 'z1qRbVnFw8wgnYr/uj22pogcuN6pqavMSSiywNMqH6EouB8gOgYVgIVecFmR78MRJx4L7WNyU4JKIsOPaZ8tQg==');
define('SECURE_AUTH_KEY', 'WyJwF5+QYR9AZwTP3KXF47TKqf882GN0079QkFzR7svLcyX1ia17xSzB+/cqOutJQ4jE9/oICcPufYUqFmkhag==');
define('LOGGED_IN_KEY', '0hH7mSJi3PMuyC6bPMmpd1OhDqhwfp8EO4JerDNouvrQ8luBAXWSasbLH2pHyjfZUWguFKyX9iGbkI/oM4lJsw==');
define('NONCE_KEY', 'gcb8NyACAKuYEBQF3f2kjU5Q/DFv0kfslkWNpYgpMVaRkQbSxDwsIEFGf72W9ShMH8LOy80FX3NHafRlxzxMBg==');
define('AUTH_SALT', 'ak7eKdoy3tKF6Wm5niGk1kPvnYrL1KE3X0IKv1xsY0EVWRZ0JbdYu4Nor53A/r7FP73wf1zNIVbNX/YwSO9nqg==');
define('SECURE_AUTH_SALT', 'f0/Yjmx4kC30JR7OBWBHX7ouIiO4y8ZdXVJCjzKdCSSsiPewYkYl5mhEjkWN7VFLo8JGLJyNur6xGEFOv5GR3Q==');
define('LOGGED_IN_SALT', 'xgTxkQKYI2PLW1KyVoVm2K3ZlLJBfMGSb3IYoB2dfriCsH9Lecqe9YxPZAHXEB+pyqGWEA74XB1DJDv/ESF/Vg==');
define('NONCE_SALT', 'nNhE5G0gM1yI4/OdHZqQh3hFYbX13L3A+ymJtEgL85bJMNzc9ssK4vc3vkrOLOBG/0iwfQfgQsaRYTN9rbNx1g==');

define('WP_AUTO_UPDATE_CORE', false);
