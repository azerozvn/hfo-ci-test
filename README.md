# Happyfamilyorganics.com Wordpress Site

# Local Development Setup

## Clone the Git repo
- Clone it to your computer.
- Rename `wp-config.example.php` to `wp-config.php`
- Rename `wp-config-local.example.php` to `wp-config-local.php`
- Remember the DB name and user.  You'll use this when you create your local DB.

## Valet setup

- `cd path/to/git/root`
- `valet link`
- Make the Valet site URL "hfo.test"

## Security headers
- Per Danone IT request, the site has custom security headers.  
- If you're using Valet, your Nginx config is located in `~/.config/valet/Nginx/hfo.test`
- To make your local headers match the production headers, paste the lines below into your server block.
- To change the headers on the WP Engine servers (staging or prod), you can't do it, WP Engine support needs to do it for you.  Go to WP Engine support, start a live chat, and tell them what config lines to add.  https://my.wpengine.com/support

```
add_header Content-Security-Policy-Report-Only "object-src 'none'; font-src https: data:; default-src https: 'unsafe-eval' 'unsafe-inline'; report-uri /csp-violation-report-endpoint;" always;
add_header Feature-Policy "geolocation *; fullscreen *;" always;
add_header Referrer-Policy "origin" always;
add_header Strict-Transport-Security "max-age=31536000" always;
add_header X-Content-Type-Options "nosniff" always;
add_header X-Frame-Options "SAMEORIGIN" always;
add_header "X-XSS-Protection" "1; mode=block" always;
```

## Install WP CLI, WP Core.

- Install the Wordpress command line: https://wp-cli.org
- WP Core is not included in the git repository, because we want to upgrade WP Core via the WP admin.

```
cd path/to/git/root
# Download and extract Wordpress core files to the current directory.  Specifying version is important.  "skip-content" means don't download the default themes and plugins.
wp core download --version=5.5.3 --skip-content
```

## Create Local Database

- Export a .sql.gz DB dump from Wordpress.  To do this, go into (Wordpress CMS > Tools > Migrate DB)[https://www.happyfamilyorganics.com/wp-admin/tools.php?page=wp-sync-db].  
Choose Export File.  Under "find and replace", replace the following:
- "//www.happyfamilyorganics.com" >> "hfo.test"  (This will be your local Valet URL)
- "/nas/content/live/hfoprod" >> "/your/local/path/to/repository/root"
- Import the DB dump into Seqel Pro on your computer.

## Fix wp-megamenu plugin warning

If you see a header warning "Undefined property: stdClass::$container_aria_label...", change this in WP Core:

```
# wp-includes/nav-menu-template.php  
# Change line 190 to:
$aria_label = ( 'nav' === $args->container && !empty($args->container_aria_label) ) ? ' aria-label="' . esc_attr( $args->container_aria_label ) . '"' : '';
```

Not ideal to patch WP core, but it's wp-megamenu plugin's fault, and this is the easiest fix I found to make the warning go away.  It disappears and works fine if you turn off WP_DEBUG, so it's ok on production.  Upgrading the wp-megamenu would be a better solution, but it caused more problems, so I didn't do that.

## Enter your Public Key to WP Engine

Enter your public key into WP Engine in 2 different places:

- (User public key)[https://my.wpengine.com/ssh_keys].  This lets you SSH into servers (For rsync and other stuff.  Everything but Git.)
- Git public key: Go to the site environment > Git push > Add SSH public key.  This lets you `git push` to the WP Engine git remote for a particular environment, which is how we will trigger a deploy.

Test that you can SSH into the servers:
`ssh <hfoprod@hfoprod.ssh.wpengine.net`
`ssh <hfodev2@hfodev2.ssh.wpengine.net`

This will list the WP Engine repositories you can push to:
`ssh git@git.wpengine.com info`

## Pull Media Library to Local

Now that you have SSH access to prod, you can pull down the Media Library to local:
```
cd path/to/git/root
# Sync Prod to Local
rsync -av --progress -e ssh hfoprod@hfoprod.ssh.wpengine.net:/home/wpe-user/sites/hfoprod/wp-content/uploads/ wp-content/uploads/
```


# Theme Development

- Theme is in `wp-content/themes/happyfamily`
- Gitlab Wiki: https://gitlab.com/happy-family-organics/happyfamilyorganics.com-wordpress/-/wikis/home

## Where is stuff?

- `/src`: Main JS, CSS, and PHP source code.  Webpack compiles this.
- `/src-madwell`: JS and CSS written by previous dev agency (Madwell).  Webpack compiles this too.
- `/dist-webpack`: Webpack compiles to this folder.
- `/dist`: Pre-compiled things.  Used by the site.  Not compiled by webpack.
- PHP templates are located in the theme root, in `/components`, and elsewhere.

## Develop Locally

```
# Go to theme folder
cd wp-content/themes/happyfamily

# Install dependencies
nvm use
yarn

# Compile JS and CSS for development to `dist-webpack`, and watch for changes
yarn start

# Open https://hfo.test in your browser
# There is no hot reload, so you have to refresh your browser to see changes.
```

## Deploy to Staging

- `yarn build` will compile minified JS and CSS to `dist-webpack`.
- Commit work, including `/dist-webpack`, to `staging` branch.
- Add the WP Engine git remote: `git remote add wpengine-staging git@git.wpengine.com:production/hfodev2.git` (or similar)
- To deploy to WP Engine, push to the git remote.  Include the "-v" verbose flag, so you can see the deploy steps, or why it rejected your push.  In this example, we deploy our local "staging" branch to "wpengine-staging" remote: `git push -v wpengine-staging staging`
